﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	Объект.Период = Новый СтандартныйПериод(ВариантСтандартногоПериода.Вчера);
	ДатаПоследнегоДокументаПоВыработке = ПолучитьДатуПоследнегоДокументаПоВыработке();
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандЭлементовФормы

&НаКлиенте
Процедура СоздатьВыработку(Команда)
	СоздатьВыработкуСервер();
КонецПроцедуры

#КонецОбласти

#Область ПрограммныйИнтерфейс

&НаСервере
Процедура СоздатьВыработкуСервер() 
	
	ЗначениеОбъект = РеквизитФормыВЗначение("Объект"); 
	
	ТекДата = КонецДня(Объект.Период.ДатаНачала);
	
	Пока ТекДата <= КонецДня(Объект.Период.ДатаОкончания) Цикл
		
		ЗначениеОбъект.СоздатьВыработкуНаДату(ТекДата);
		ТекДата = ТекДата + 86400;
		
	КонецЦикла;
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Функция ПолучитьДатуПОследнегоДокументаПоВыработке()
	
	Запрос = Новый Запрос( 
	"ВЫБРАТЬ ПЕРВЫЕ 1
	|	ВыработкаСотрудников.Дата КАК Дата
	|ИЗ
	|	Документ.ВыработкаСотрудников КАК ВыработкаСотрудников
	|ГДЕ
	|	ВыработкаСотрудников.ПометкаУдаления = ЛОЖЬ
	|
	|УПОРЯДОЧИТЬ ПО
	|	Дата УБЫВ"
	);
	
	Выборка = Запрос.Выполнить().Выбрать();
	
	Если Выборка.Следующий() Тогда
		Возврат Выборка.Дата;
	Иначе
		Возврат Дата(1,1,1);
	КонецЕсли;
	
КонецФункции

#КонецОбласти


