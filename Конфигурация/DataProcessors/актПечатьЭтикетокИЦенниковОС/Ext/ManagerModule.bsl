﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ПрограммныйИнтерфейс

// Функция подготавливает структуру данных, необходимую для печати ценников и этикеток.
//
// Возвращаемое значение:
//  Стрруктура - данные, необходимые для печати этикеток и ценников.
//
Функция ПодготовитьСтруктуруДанных(СтруктураНастроек) Экспорт
	
	Если СтруктураНастроек.ИсходныеДанные <> Неопределено Тогда
		ИсходныеДанные = СтруктураНастроек.ИсходныеДанные.Скопировать();
	Иначе
		ИсходныеДанные = Неопределено;
	КонецЕсли;
	
	СтруктураРезультата = Новый Структура;
	СтруктураРезультата.Вставить("Таблица",                             Неопределено);
	СтруктураРезультата.Вставить("СоответствиеПолейСКДКолонкамТаблицы", Новый Соответствие);
	
	// Схема компоновки.
	СхемаКомпоновкиДанных = Обработки.актПечатьЭтикетокИЦенниковОС.ПолучитьМакет(СтруктураНастроек.ИмяМакетаСхемыКомпоновкиДанных);
	ТекстЗапроса = СхемаКомпоновкиДанных.НаборыДанных.НаборДанных.Запрос;
	
	СхемаКомпоновкиДанных.НаборыДанных.НаборДанных.Запрос = ТекстЗапроса;
	
	// Подготовка компоновщика макета компоновки данных.
	Компоновщик = Новый КомпоновщикНастроекКомпоновкиДанных;
	Компоновщик.Инициализировать(Новый ИсточникДоступныхНастроекКомпоновкиДанных(СхемаКомпоновкиДанных));
	Компоновщик.ЗагрузитьНастройки(СхемаКомпоновкиДанных.НастройкиПоУмолчанию);
	Компоновщик.Настройки.Отбор.Элементы.Очистить();
	
	// Отбор компоновщика настроек.
	Если СтруктураНастроек.КомпоновщикНастроек <> Неопределено Тогда
		КомпоновкаДанныхКлиентСервер.СкопироватьЭлементы(Компоновщик.Настройки.Отбор, СтруктураНастроек.КомпоновщикНастроек.Настройки.Отбор);
	КонецЕсли;
	
	// Выбранные поля компоновщика настроек.
	Для Каждого ОбязательноеПоле Из СтруктураНастроек.ОбязательныеПоля Цикл
		ПолеСКД = КомпоновкаДанныхСервер.НайтиПолеСКДПоПолномуИмени(Компоновщик.Настройки.Выбор.ДоступныеПоляВыбора.Элементы, ОбязательноеПоле);
		Если ПолеСКД <> Неопределено Тогда
			ВыбранноеПоле = Компоновщик.Настройки.Выбор.Элементы.Добавить(Тип("ВыбранноеПолеКомпоновкиДанных"));
			ВыбранноеПоле.Поле = ПолеСКД.Поле;
		КонецЕсли;
	КонецЦикла;
	
	// Заполнение параметров.
	Для Каждого ПараметрДанных Из СтруктураНастроек.ПараметрыДанных Цикл
		УстановитьЗначениеПараметраСКД(Компоновщик, ПараметрДанных.Ключ, ПараметрДанных.Значение);
	КонецЦикла;
	УстановитьЗначениеПараметраСКД(Компоновщик, "ТекущееВремя",        ТекущаяДатаСеанса());
	УстановитьЗначениеПараметраСКД(Компоновщик, "ТекущийПользователь", Пользователи.ТекущийПользователь());
	
	// Компоновка макета компоновки данных.
	КомпоновщикМакета = Новый КомпоновщикМакетаКомпоновкиДанных;
	МакетКомпоновкиДанных = КомпоновщикМакета.Выполнить(СхемаКомпоновкиДанных, Компоновщик.ПолучитьНастройки(),,,Тип("ГенераторМакетаКомпоновкиДанныхДляКоллекцииЗначений"));

	Для каждого Поле Из МакетКомпоновкиДанных.НаборыДанных.НаборДанных.Поля Цикл
		СтруктураРезультата.СоответствиеПолейСКДКолонкамТаблицы.Вставить(Справочники.актШаблоныЭтикетокИЦенниковОС.ИмяПоляВШаблоне(Поле.ПутьКДанным), Поле.Имя);
	КонецЦикла;
	
	Запрос = Новый Запрос(МакетКомпоновкиДанных.НаборыДанных.НаборДанных.Запрос);
	
	// Заполнение параметров с полей отбора компоновщика настроек формы обработки.
	Для каждого Параметр Из МакетКомпоновкиДанных.ЗначенияПараметров Цикл
		Запрос.Параметры.Вставить(Параметр.Имя, Параметр.Значение);
	КонецЦикла;
		
	// Подмена запроса при печати этикеток... // !!! Нужно разбираться и сравнивать с СКД
	Если ИсходныеДанные <> Неопределено Тогда
		
		ТекстВременнойТаблицы =
		"	(ВЫБРАТЬ
		|		ЗНАЧЕНИЕ(Справочник.ОбъектыЭксплуатации.ПустаяСсылка) КАК ОсновноеСредство,
		|		0 КАК Порядок,
		|		ЛОЖЬ КАК ЕстьВДокументе,
		|		""ШаблонЭтикетки"" КАК ШаблонЭтикетки)";
		
		Запрос.Текст = СтрЗаменить(Запрос.Текст, ТекстВременнойТаблицы, "&Таблица");
		
		ОбщегоНазначенияУТ.ПронумероватьТаблицуЗначений(ИсходныеДанные, "Порядок");
		Запрос.Параметры.Вставить("Таблица", ИсходныеДанные);

		ЗаменитьПолеВЗапросе(ИсходныеДанные, Запрос, "КоличествоЭтикеток");
		
		ЗаменитьПолеВЗапросе(ИсходныеДанные, Запрос, "ЕстьВДокументе");

		Если ИсходныеДанные.Колонки.Найти("КоличествоЭтикеток") <> Неопределено Тогда
			Запрос.Текст = СтрЗаменить(Запрос.Текст, "КОЛИЧЕСТВО(ИсходныеДанные.КоличествоЭтикеток)",
		                                         	"СУММА(ИсходныеДанные.КоличествоЭтикеток)");
		КонецЕсли;
		
		ЗаменитьПолеВЗапросе(ИсходныеДанные, Запрос, "ШаблонЭтикетки");
		
		ЗаменитьПолеВЗапросе(ИсходныеДанные, Запрос, "Штрихкод");
		
	КонецЕсли;
	
	СтруктураРезультата.Таблица = Запрос.Выполнить().Выгрузить();
	
	Возврат СтруктураРезультата;
	
КонецФункции

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область Печать

Процедура Печать(МассивОбъектов, ПараметрыПечати, КоллекцияПечатныхФорм, ОбъектыПечати, ПараметрыВывода) Экспорт
	
	УстановитьПривилегированныйРежим(Истина);
		
	СформироватьПечатныеФормыЭтикетокИЦенниковТоваров(ПараметрыПечати, ОбъектыПечати, КоллекцияПечатныхФорм);
	
	Если ПривилегированныйРежим() Тогда
		УстановитьПривилегированныйРежим(Ложь);
	КонецЕсли;
	
КонецПроцедуры

Процедура СформироватьПечатныеФормыЭтикетокИЦенниковТоваров(ДанныеПечати, ОбъектыПечати, КоллекцияПечатныхФормБСП)
	
	ИсходныеДанные = ПолучитьИзВременногоХранилища(ДанныеПечати.Товары);
	
	НужноПечататьЭтикетки = Ложь;
	Если УправлениеПечатью.НужноПечататьМакет(КоллекцияПечатныхФормБСП, "ЭтикеткаТовары") Тогда
		НужноПечататьЭтикетки = Истина;
		КоллекцияПечатныхФормБСП.Удалить(КоллекцияПечатныхФормБСП.Найти(ВРег("ЭтикеткаТовары"), "ИмяВРЕГ"));
	КонецЕсли;
	
	СтруктураНастроек = СтруктураНастроек();
	СтруктураНастроек.ОбязательныеПоля.Добавить("КоличествоЭтикетокДляПечати");
	СтруктураНастроек.ОбязательныеПоля.Добавить("ШаблонЭтикеткиДляПечати");
	СтруктураНастроек.ОбязательныеПоля.Добавить("ОсновноеСредство.Наименование");
	СтруктураНастроек.ОбязательныеПоля.Добавить("ОсновноеСредство.Изготовитель");
	СтруктураНастроек.ОбязательныеПоля.Добавить("ОсновноеСредство.ИнвентарныйНомер");
	
	СтруктураНастроек.ИмяМакетаСхемыКомпоновкиДанных = "ПоляШаблонаПечатьТовары";
	
	// Собираем используемые поля из шаблонов.
	СоответствиеШаблонов = Новый Соответствие;
	Для Каждого СтрокаТЧ Из ИсходныеДанные Цикл
		Если ЗначениеЗаполнено(СтрокаТЧ.ШаблонЭтикетки) И СтрокаТЧ.КоличествоЭтикеток > 0 И НужноПечататьЭтикетки Тогда
			СоответствиеШаблонов.Вставить(СтрокаТЧ.ШаблонЭтикетки);
		КонецЕсли;
	КонецЦикла;
	Если ДанныеПечати.Свойство("СтруктураМакетаШаблона") И ЗначениеЗаполнено(ДанныеПечати.СтруктураМакетаШаблона) Тогда
		СоответствиеШаблонов.Вставить(Справочники.актШаблоныЭтикетокИЦенниковОС.ПустаяСсылка());
	КонецЕсли;
	
	// Заполняем коллекцию обязательных полей и формируем соответствие шаблонов.
	Для Каждого КлючИЗначение ИЗ СоответствиеШаблонов Цикл
		
		ШаблонЭтикетокИЦенников = КлючИЗначение.Ключ;
		
		Если ЗначениеЗаполнено(ШаблонЭтикетокИЦенников) Тогда
			СтруктураШаблона = КлючИЗначение.Ключ.Шаблон.Получить();
		Иначе
			СтруктураШаблона = ДанныеПечати.СтруктураМакетаШаблона;
		КонецЕсли;
		
		// Структура шаблонов.
		СтруктураНастроек.СоответствиеШаблоновИСтруктурыШаблонов.Вставить(ШаблонЭтикетокИЦенников, СтруктураШаблона);
		
		// Добавляем в массив обязательных полей поля, присутствующие в печатной форме ценника.
		Для Каждого Элемент Из СтруктураШаблона.ПараметрыШаблона Цикл
			СтруктураНастроек.ОбязательныеПоля.Добавить(Элемент.Ключ);
		КонецЦикла;
		
	КонецЦикла;
	
	СтруктураНастроек.ПараметрыДанных.Вставить("Организация", ДанныеПечати.Организация);
	
	СтруктураНастроек.ПараметрыДанных.Вставить("Дата",          ДанныеПечати.Дата);
	
	СтруктураНастроек.ИсходныеДанные = ИсходныеДанные;

#Область ПодготовкаСтруктурыДанныхШаблона
	
	СтруктураРезультата = ПодготовитьСтруктуруДанных(СтруктураНастроек);

#КонецОбласти

#Область ФормированиеТабличногоДокумента
	
	Эталон = Обработки.актПечатьЭтикетокИЦенниковОС.ПолучитьМакет("Эталон");
	КоличествоМиллиметровВПикселе = Эталон.Рисунки.Квадрат100Пикселей.Высота / 100;
	
	// Подготовка коллекции печатных форм.
	КоллекцияПечатныхФорм = Новый ТаблицаЗначений;
	КоллекцияПечатныхФорм.Колонки.Добавить("ИмяМакета");
	КоллекцияПечатныхФорм.Колонки.Добавить("СинонимМакета");
	КоллекцияПечатныхФорм.Колонки.Добавить("ТабличныйДокумент");
	КоллекцияПечатныхФорм.Колонки.Добавить("ИмяКолонкиКоличество");
	КоллекцияПечатныхФорм.Колонки.Добавить("ИмяКолонкиШаблон");
	КоллекцияПечатныхФорм.Колонки.Добавить("Шаблон");
	
	Для Каждого КлючИЗначение Из СтруктураНастроек.СоответствиеШаблоновИСтруктурыШаблонов Цикл
		
		Если (ЗначениеЗаполнено(КлючИЗначение.Ключ) ИЛИ Не ЗначениеЗаполнено(КлючИЗначение.Ключ))
			И НужноПечататьЭтикетки Тогда
			
			ПечатнаяФорма = КоллекцияПечатныхФорм.Добавить();
			ПечатнаяФорма.ИмяМакета            = "Этикетка: "+КлючИЗначение.Ключ;
			ПечатнаяФорма.СинонимМакета        = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(НСтр("ru = 'Этикетка: %1'"), КлючИЗначение.Ключ);
			ПечатнаяФорма.ИмяКолонкиКоличество = "КоличествоЭтикетокДляПечати";
			ПечатнаяФорма.ИмяКолонкиШаблон     = "ШаблонЭтикеткиДляПечати";
			ПечатнаяФорма.Шаблон = КлючИЗначение.Ключ;
			
		КонецЕсли;
		
	КонецЦикла;
	
	Для Каждого ПечатнаяФорма Из КоллекцияПечатныхФорм Цикл
		
		НомерКолонки = 0;
		НомерРяда = 0;
		
		Для Каждого СтрокаТовары Из СтруктураРезультата.Таблица Цикл
			
			Если СтрокаТовары[ПечатнаяФорма.ИмяКолонкиКоличество] > 0 И СтрокаТовары[ПечатнаяФорма.ИмяКолонкиШаблон] = ПечатнаяФорма.Шаблон Тогда
				
				СтруктураШаблона = СтруктураНастроек.СоответствиеШаблоновИСтруктурыШаблонов.Получить(СтрокаТовары[ПечатнаяФорма.ИмяКолонкиШаблон]);
				
				Если ПечатнаяФорма.ТабличныйДокумент = Неопределено Тогда
					ПечатнаяФорма.ТабличныйДокумент = Новый ТабличныйДокумент;
				КонецЕсли;
				
				Область = СтруктураШаблона.МакетЭтикетки.ПолучитьОбласть(СтруктураШаблона.ИмяОбластиПечати);
				
				// Применение настроек табличного документа.
				ЗаполнитьЗначенияСвойств(ПечатнаяФорма.ТабличныйДокумент, СтруктураШаблона.МакетЭтикетки, , "ОбластьПечати");
				
				НомерСтрокиНачало = ПечатнаяФорма.ТабличныйДокумент.ВысотаТаблицы + 1;

				Для каждого ПараметрШаблона Из СтруктураШаблона.ПараметрыШаблона Цикл
					Если ОбщегоНазначенияУТКлиентСервер.ЕстьРеквизитОбъекта(Область.Параметры, ПараметрШаблона.Значение) Тогда
						НаименованиеКолонки = СтруктураРезультата.СоответствиеПолейСКДКолонкамТаблицы.Получить(Справочники.актШаблоныЭтикетокИЦенниковОС.ИмяПоляВШаблоне(ПараметрШаблона.Ключ));
						Если НаименованиеКолонки <> Неопределено Тогда
							Область.Параметры[ПараметрШаблона.Значение] = СтрокаТовары[НаименованиеКолонки];
						КонецЕсли;
					КонецЕсли;
				КонецЦикла;
				
				Для каждого Рисунок Из Область.Рисунки Цикл
					
					Если СтрНайти(Рисунок.Имя, Справочники.актШаблоныЭтикетокИЦенниковОС.ИмяПараметраШтрихкод()) = 1 Тогда
						
						ЗначениеШтрихкода = СтрокаТовары[СтруктураРезультата.СоответствиеПолейСКДКолонкамТаблицы.Получить(Справочники.актШаблоныЭтикетокИЦенниковОС.ИмяПараметраШтрихкод())];
						Если ЗначениеЗаполнено(ЗначениеШтрихкода) Тогда
							ЗначениеШтрихкодаДляКомпоненты = ЗначениеШтрихкода;
							Если СтруктураШаблона.ТипКода = 2 ИЛИ СтруктураШаблона.ТипКода = 17 Тогда
								ЧтениеШтрихкода = ШтрихкодыУпаковокКлиентСервер.ПараметрыШтрихкода(СокрЛП(ЗначениеШтрихкода));
								Если НЕ ЧтениеШтрихкода.Результат = Неопределено Тогда
									Если ЧтениеШтрихкода.ТипШтрихкода = Перечисления.ТипыШтрихкодов.GS1_128
										ИЛИ ЧтениеШтрихкода.ТипШтрихкода = Перечисления.ТипыШтрихкодов.GS1_DataBarExpandedStacked Тогда
										ЗначениеШтрихкодаДляКомпоненты = ШтрихкодыУпаковокКлиентСервер.ШтрихкодGS1(ЧтениеШтрихкода.Результат,
										Истина, ШтрихкодыУпаковокКлиентСервер.СимволОкончанияСтрокиПеременнойДлины());
									КонецЕсли;
								КонецЕсли;
							КонецЕсли;
							
							ПараметрыШтрихкода = Новый Структура;
							ПараметрыШтрихкода.Вставить("Ширина",          Окр(Рисунок.Ширина / КоличествоМиллиметровВПикселе));
							ПараметрыШтрихкода.Вставить("Высота",          Окр(Рисунок.Высота / КоличествоМиллиметровВПикселе));
							ПараметрыШтрихкода.Вставить("Штрихкод",        СокрЛП(ЗначениеШтрихкодаДляКомпоненты));
							ПараметрыШтрихкода.Вставить("ТипКода",         СтруктураШаблона.ТипКода);
							ПараметрыШтрихкода.Вставить("ОтображатьТекст", СтруктураШаблона.ОтображатьТекст);
							ПараметрыШтрихкода.Вставить("РазмерШрифта",    СтруктураШаблона.РазмерШрифта);
							
							Если СтруктураШаблона.Свойство("GS1DatabarКоличествоСтрок") Тогда
								ПараметрыШтрихкода.Вставить("GS1DatabarКоличествоСтрок", СтруктураШаблона.GS1DatabarКоличествоСтрок);
							КонецЕсли;
							Если СтруктураШаблона.Свойство("ТипШрифта") Тогда
								ПараметрыШтрихкода.Вставить("ТипШрифта", СтруктураШаблона.МонохромныйШрифт);
							КонецЕсли;
							Если СтруктураШаблона.Свойство("УголПоворота") Тогда
								ПараметрыШтрихкода.Вставить("УголПоворота", СтруктураШаблона.УголПоворота);
							КонецЕсли;
							
							Рисунок.Картинка = МенеджерОборудованияВызовСервера.ПолучитьКартинкуШтрихкода(ПараметрыШтрихкода);
							
						КонецЕсли;
						
					КонецЕсли;
					
					Если СтрНайти(Рисунок.Имя, "ЗнакВалюты") = 1 Тогда
						ЗначениеКодаВалюты = СтрокаТовары[СтруктураРезультата.СоответствиеПолейСКДКолонкамТаблицы.Получить(Справочники.актШаблоныЭтикетокИЦенниковОС.ИмяПараметраКодВалюты())];
						Попытка
							Рисунок.Картинка = Новый Картинка(Справочники.актШаблоныЭтикетокИЦенниковОС.ПолучитьМакет("ЗнакВалюты" + ЗначениеКодаВалюты), Истина);
						Исключение
							Рисунок.Картинка = Новый Картинка;
						КонецПопытки;
					КонецЕсли;
					
				КонецЦикла;
				
				Для Инд = 1 По СтрокаТовары[ПечатнаяФорма.ИмяКолонкиКоличество] Цикл // Цикл по количеству экземпляров
					
					НомерКолонки = НомерКолонки + 1;
					
					Если НомерКолонки = 1 Тогда
						
						НомерРяда = НомерРяда + 1;
						
						ПечатнаяФорма.ТабличныйДокумент.Вывести(Область);
						
					Иначе
						
						ПечатнаяФорма.ТабличныйДокумент.Присоединить(Область);
						
					КонецЕсли;
					
					Если НомерКолонки = СтруктураШаблона.КоличествоПоГоризонтали И НомерРяда = СтруктураШаблона.КоличествоПоВертикали Тогда
						
						НомерРяда    = 0;
						НомерКолонки = 0;
						
						ПечатнаяФорма.ТабличныйДокумент.ВывестиГоризонтальныйРазделительСтраниц();
						
					ИначеЕсли НомерКолонки = СтруктураШаблона.КоличествоПоГоризонтали Тогда
						
						НомерКолонки = 0;
						
					КонецЕсли;
					
				КонецЦикла; // Цикл по количеству экземпляров
				
			КонецЕсли;
			
		КонецЦикла; // Цикл по строкам таблицы товаров
		
	КонецЦикла;
	
#КонецОбласти
	
	МассивСтрокДляУдаления = Новый Массив;
	
	Для Каждого ПечатнаяФорма Из КоллекцияПечатныхФорм Цикл
		Если ПечатнаяФорма.ТабличныйДокумент <> Неопределено Тогда
			НоваяСтрока = КоллекцияПечатныхФормБСП.Добавить();
			
			НоваяСтрока.ИмяМакета = ПечатнаяФорма.ИмяМакета;
			НоваяСтрока.ИмяВРЕГ   = ВРег(ПечатнаяФорма.ИмяМакета);
			НоваяСтрока.Экземпляров = 1;
			
			НоваяСтрока.ТабличныйДокумент = ПечатнаяФорма.ТабличныйДокумент;
			НоваяСтрока.СинонимМакета = ПечатнаяФорма.СинонимМакета;
		КонецЕсли;
	КонецЦикла;
	
КонецПроцедуры

#КонецОбласти

#Область Прочее

Функция УстановитьЗначениеПараметраСКД(КомпоновщикНастроек, ИмяПараметра, ЗначениеПараметра, ИспользоватьНеЗаполненный = Истина)
	
	ПараметрУстановлен = Ложь;
	
	ПараметрКомпоновкиДанных = Новый ПараметрКомпоновкиДанных(ИмяПараметра);
	ЗначениеПараметраКомпоновкиДанных = КомпоновщикНастроек.Настройки.ПараметрыДанных.НайтиЗначениеПараметра(ПараметрКомпоновкиДанных);
	Если ЗначениеПараметраКомпоновкиДанных <> Неопределено Тогда
		
		ЗначениеПараметраКомпоновкиДанных.Значение = ЗначениеПараметра;
		ЗначениеПараметраКомпоновкиДанных.Использование = ?(ИспользоватьНеЗаполненный, Истина, ЗначениеЗаполнено(ЗначениеПараметраКомпоновкиДанных.Значение));
		
		ПараметрУстановлен = Истина;
		
	КонецЕсли;
	
	Возврат ПараметрУстановлен;
	
КонецФункции

// Функция возвращает пустую структуру настроек
// 
// Параметры: 
//  Нет
// 
// Возвращаемое значение: 
//  Структура - структура настроек.
Функция СтруктураНастроек() Экспорт
	
	СтруктураНастроек = Новый Структура;
	СтруктураНастроек.Вставить("ИсходныеДанные",                         Неопределено);
	СтруктураНастроек.Вставить("ОбязательныеПоля",                       Новый Массив);
	СтруктураНастроек.Вставить("СоответствиеШаблоновИСтруктурыШаблонов", Новый Соответствие);
	СтруктураНастроек.Вставить("ПараметрыДанных"    ,                    Новый Структура);
	СтруктураНастроек.Вставить("КомпоновщикНастроек",                    Неопределено);
	СтруктураНастроек.Вставить("ИмяМакетаСхемыКомпоновкиДанных",         Неопределено);
	
	Возврат СтруктураНастроек;
	
КонецФункции

Процедура ЗаменитьПолеВЗапросе(ИсходныеДанные, Запрос, НазваниеПоля)
	Если ИсходныеДанные.Колонки.Найти(НазваниеПоля) <> Неопределено Тогда
		Запрос.Текст = СтрЗаменить(Запрос.Текст, """" + НазваниеПоля + """", "ИсходныеДанные." + НазваниеПоля);
	КонецЕсли;	
КонецПроцедуры

#КонецОбласти

#КонецОбласти

#КонецЕсли
