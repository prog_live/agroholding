﻿
&НаКлиенте
Процедура ОбработкаКоманды(ПараметрКоманды, ПараметрыВыполненияКоманды)
	
	ОткрытьФорму(
		"Обработка.актПанельАдминистрированияАкита.Форма.СкладИДоставка",
		Новый Структура,
		ПараметрыВыполненияКоманды.Источник,
		"Обработка.актПанельАдминистрированияАкита.Форма.АктСкладИДоставка" + ?(ПараметрыВыполненияКоманды.Окно = Неопределено, ".ОтдельноеОкно", ""),
		ПараметрыВыполненияКоманды.Окно);

КонецПроцедуры
