﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Параметры.Свойство("Организация", Организация);
	Если НЕ ЗначениеЗаполнено(Организация) Тогда
		Организация = Справочники.Организации.ОрганизацияПоУмолчанию();
	КонецЕсли;
	
	// Выполним настройку свойств элементов формы.
	ЗаполнитьТаблицуРегламентныхЗаданий();

КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовФормы

&НаКлиенте
Процедура ТаблицаРегламентныхЗаданийПредставлениеРасписанияНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	
	Если Элементы.ТаблицаРегламентныхЗаданий.ТекущиеДанные.Расписание = Неопределено Тогда
		Элементы.ТаблицаРегламентныхЗаданий.ТекущиеДанные.Расписание = Новый РасписаниеРегламентногоЗадания;
	КонецЕсли;
	
	Диалог = Новый ДиалогРасписанияРегламентногоЗадания(Элементы.ТаблицаРегламентныхЗаданий.ТекущиеДанные.Расписание);
	
	ОписаниеОповещения = Новый ОписаниеОповещения(
		"НастроитьРасписаниеРегламентногоЗаданияЗавершение",
		ЭтотОбъект,
		Новый Структура("Диалог", Диалог));
	
	Диалог.Показать(ОписаниеОповещения);
	
КонецПроцедуры

&НаКлиенте
Процедура ТаблицаРегламентныхЗаданийПриОкончанииРедактирования(Элемент, НоваяСтрока, ОтменаРедактирования)
	Если Не ОтменаРедактирования Тогда
		ЗаписатьРегламентныеЗаданияНаСервере();	
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура ТаблицаРегламентныхЗаданийПослеУдаления(Элемент)
	ЗаписатьРегламентныеЗаданияНаСервере();
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура ЗаполнитьТаблицуРегламентныхЗаданий()
	
	ТаблицаРегламентныхЗаданий.Очистить();
	
	СтруктураОтбора = Новый Структура;
	СтруктураОтбора.Вставить("Метаданные", Метаданные.РегламентныеЗадания.актАвтоматическоеЗакрытиеПериода);
	
	НайденныеЗадания = РегламентныеЗадания.ПолучитьРегламентныеЗадания(СтруктураОтбора);

	Для Каждого НайденноеЗадание Из НайденныеЗадания Цикл
		
		НоваяСтрока = ТаблицаРегламентныхЗаданий.Добавить();
		
		НоваяСтрока.Использование					= НайденноеЗадание.Использование;
		НоваяСтрока.УникальныйИдентификатор			= НайденноеЗадание.УникальныйИдентификатор;
		НоваяСтрока.Расписание						= НайденноеЗадание.Расписание;
		НоваяСтрока.ПредставлениеРасписания			= НайденноеЗадание.Расписание;
		НоваяСтрока.КоличествоДнейОтсрочки			= НайденноеЗадание.Параметры[0].КоличествоДнейОтсрочки;
		
		Если НайденноеЗадание.ПоследнееЗадание <> Неопределено Тогда
			НоваяСтрока.Состояние =
				СокрЛП(НайденноеЗадание.ПоследнееЗадание.Состояние) + ": " + СокрЛП(НайденноеЗадание.ПоследнееЗадание.Конец);
		КонецЕсли;
		
	КонецЦикла;
	
КонецПроцедуры

&НаСервере
Процедура ЗаписатьРегламентныеЗаданияНаСервере()
	
	СтруктураОтбора = Новый Структура;
	СтруктураОтбора.Вставить("Метаданные", Метаданные.РегламентныеЗадания.актАвтоматическоеЗакрытиеПериода);
	
	НайденныеЗадания = РегламентныеЗадания.ПолучитьРегламентныеЗадания(СтруктураОтбора);
	
	Для Каждого НайденноеЗадание Из НайденныеЗадания Цикл
		
		СтруктураОтбора = Новый Структура("УникальныйИдентификатор", НайденноеЗадание.УникальныйИдентификатор);
		НайденныеСтроки = ТаблицаРегламентныхЗаданий.НайтиСтроки(СтруктураОтбора);
		
		Если НайденныеСтроки.Количество() = 0 Тогда
			НайденноеЗадание.Удалить();
		КонецЕсли;
		
	КонецЦикла;
	
	Для Каждого СтрокаЗадание Из ТаблицаРегламентныхЗаданий Цикл
		
		Если СтрокаЗадание.Расписание = Неопределено Тогда
			Продолжить;
		КонецЕсли;

		Если ЗначениеЗаполнено(СтрокаЗадание.УникальныйИдентификатор) Тогда
			РегламентноеЗадание = РегламентныеЗадания.НайтиПоУникальномуИдентификатору(СтрокаЗадание.УникальныйИдентификатор);
		Иначе
			РегламентноеЗадание = РегламентныеЗадания.СоздатьРегламентноеЗадание(Метаданные.РегламентныеЗадания.актАвтоматическоеЗакрытиеПериода);
		КонецЕсли;
		
		РегламентноеЗадание.Использование	= СтрокаЗадание.Использование;
		РегламентноеЗадание.Расписание 		= СтрокаЗадание.Расписание;
		РегламентноеЗадание.Наименование	= Строка(РегламентноеЗадание.Метаданные) + СтрШаблон("- Отсрочка: %1 дн.", СтрокаЗадание.КоличествоДнейОтсрочки);
		
		СтруктураПараметров = Новый Структура("Организация,КоличествоДнейОтсрочки", Организация, СтрокаЗадание.КоличествоДнейОтсрочки);
		
		РегламентноеЗадание.Параметры		= ОбщегоНазначенияКлиентСервер.ЗначениеВМассиве(СтруктураПараметров);
		
		РегламентноеЗадание.Записать();
			
	КонецЦикла;
	
КонецПроцедуры

&НаКлиенте
Процедура НастроитьРасписаниеРегламентногоЗаданияЗавершение(Расписание, ДополнительныеПараметры) Экспорт
	
	Диалог = ДополнительныеПараметры.Диалог;
	
	Если Расписание <> Неопределено Тогда
		
		Элементы.ТаблицаРегламентныхЗаданий.ТекущиеДанные.Расписание 				= Диалог.Расписание;
		Элементы.ТаблицаРегламентныхЗаданий.ТекущиеДанные.ПредставлениеРасписания 	= Диалог.Расписание;
		
	КонецЕсли;
	
КонецПроцедуры


#КонецОБласти
