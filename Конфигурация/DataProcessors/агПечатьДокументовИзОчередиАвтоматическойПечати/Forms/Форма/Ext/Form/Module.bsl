﻿#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	УстановитьОтборыДинамическогоСписка();
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура Распечатать(Команда)
	
	МассивСтруктурДляПечати = ПолучитьМассивСтруктурДляПечати();
	
	Для Каждого СтруктураДляПечати Из МассивСтруктурДляПечати Цикл
		
		ОписаниеОповещения = Новый ОписаниеОповещения("РаспечататьЗавершение", ЭтотОбъект, СтруктураДляПечати);
		ПоказатьВводЧисла(ОписаниеОповещения, СтруктураДляПечати.КоличествоЭлементов, "Введите количество этикеток",3,0);	
		
	КонецЦикла;
	
КонецПроцедуры

&НаСервере
Функция ПолучитьМассивСтруктурДляПечати()
	
	МассивСтруктур = Новый Массив;
	
	Для Каждого Строка Из Элементы.ОчередьАвтоматическойПечати.ВыделенныеСтроки Цикл
		
		СтруктураПечати = Новый Структура;
		СтруктураПечати.Вставить("Дата",				Строка.Дата);
		СтруктураПечати.Вставить("ОбъектПечати",		Строка.ОбъектПечати);
		
		ИнформацияОПаллете = агАвтоматизацияОтгрузокВызовСервера.ПолучитьИнформациюПоПаллете(Строка.ОбъектПечати);
		
		СтруктураПечати.Вставить("Пользователь",		Строка.Пользователь);
		СтруктураПечати.Вставить("ВидПечатнойФормы",	Строка.ВидПечатнойФормы);
		
		ЕстьПринтер = "";
		Если СтруктураПечати.ВидПечатнойФормы = Перечисления.актВидыПечатныхФормДляАвтоматическойПечаи.ГрупповаяЭтикетка Тогда
			СтруктураПечати.Вставить("КоличествоЭлементов", ИнформацияОПаллете.КоличествоКоробок);
			ЕстьПринтер = агАвтоматизацияОтгрузокВызовСервера.ПолучитьПринтерЭтикеток();
		ИначеЕсли СтруктураПечати.ВидПечатнойФормы = Перечисления.актВидыПечатныхФормДляАвтоматическойПечаи.ПаспортПаллеты Тогда
			СтруктураПечати.Вставить("КоличествоЭлементов", 1);
			ЕстьПринтер = агАвтоматизацияОтгрузокВызовСервера.ПолучитьПринтерПаллет();
		КонецеСли;
		СтруктураПечати.Вставить("ЕстьПринтер",	ЕстьПринтер);
		
		МассивСтруктур.Добавить(СтруктураПечати);
		
	КонецЦикла;
	
	Возврат МассивСтруктур;
	
КонецФункции

&НаКлиенте
Процедура РаспечататьЗавершение(КоличествоЭлементов, СтруктураПечати) Экспорт
	
	Если НЕ ЗначениеЗаполнено(КоличествоЭлементов) Тогда
		Возврат;
	КонецЕсли;
	
	СтруктураПечати.КоличествоЭлементов = КоличествоЭлементов;
	
	МассивТабличныхДокументов = агАвтоматизацияОтгрузокВызовСервера.СформироватьМассивТабличныхДокументовДляАвтоматическойПечати(СтруктураПечати);
	
	Для Каждого ТабличныйДокумент Из МассивТабличныхДокументов Цикл
		
		ЕстьПринтер = СтруктураПечати.Естьпринтер;
		
		Если ЕстьПринтер = "" Тогда
		Иначе
			ТабличныйДокумент.ИмяПринтера = ЕстьПринтер;
		КонецЕсли;
		ТабличныйДокумент.Показать();
		
	КонецЦикла;
	
	Если МассивТабличныхДокументов.Количество() <> 0 Тогда
		агАвтоматизацияОтгрузокВызовСервера.УстановитьОтметкуОтправленоНаПечать(СтруктураПечати.Дата,
			СтруктураПечати.ОбъектПечати,
			СтруктураПечати.Пользователь,
			СтруктураПечати.ВидПечатнойФормы);	
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

Процедура УстановитьОтборыДинамическогоСписка()
	
	Если РольДоступна("ПолныеПрава") Тогда
		Возврат;
	КонецЕсли;
	
	ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(ОчередьАвтоматическойПечати,
		"Пользователь",
		ПараметрыСеанса.ТекущийПользователь,
		ВидСравненияКомпоновкиДанных.Равно);
	
	
КонецПроцедуры	

#КонецОбласти
