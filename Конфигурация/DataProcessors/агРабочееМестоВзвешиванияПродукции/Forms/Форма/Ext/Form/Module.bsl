﻿
&НаКлиенте
Перем ЭлектронныеВесы;

#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Объект.ДатаРаботы = ТекущаяДата();
	Режим = 0;
	ПолучитьНастройкиВзвешиваний();	
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	МенеджерОборудованияКлиент.ОбновитьРабочееМестоКлиента();
	
	ПодключитьОбработчикОжидания("Подключаемый_НачатьПодключениеОборудования", 1, Истина);
	УправлениеФормой();
	
	// Подключаемое оборудование +
	ЭтаФорма.ИспользоватьПодключаемоеОборудование = Истина;
	МенеджерОборудованияКлиент.НачатьПодключениеОборудованиеПриОткрытииФормы(Неопределено,этаФорма, "СканерШтрихкода");
	// Подключаемое оборудование -
КонецПроцедуры

&НаКлиенте
Процедура ПередЗакрытием(Отказ, ЗавершениеРаботы, ТекстПредупреждения, СтандартнаяОбработка)
	
	Если ЗавершениеРаботы Тогда
		Возврат;
	КонецЕсли;
	
	Если Модифицированность Тогда
		Отказ = Истина;
		
		Оповещение = Новый ОписаниеОповещения("ЗавершитьЗакрытиеФормы", ЭтотОбъект);
		ПоказатьВопрос(Оповещение, НСтр("ru = 'Вы уверены, что хотите выйти из терминала'"), РежимДиалогаВопрос.ДаНетОтмена);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ЗавершитьЗакрытиеФормы(РезультатВопроса, ДополнительныеПараметры) Экспорт
	
	Если РезультатВопроса = КодВозвратаДиалога.Да Тогда
		Модифицированность = Ложь;
		ЗавершитьРаботуСТерминалом();
		Закрыть();
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	Если Источник = "ПодключаемоеОборудование"  Тогда
		Если ИмяСобытия = "ScanData" И МенеджерОборудованияУТКлиент.ЕстьНеобработанноеСобытие() Тогда
			Если ОбработатьШтрихкоды(МенеджерОборудованияУТКлиент.ПреобразоватьДанныеСоСканераВМассив(Параметр)) тогда
				Подключаемый_ПолучитьВес();
				ЗаписатьВзвешиваниеНаСервере();
			КонецеСли;
			ОчиститьФорму();
		КонецЕсли;
	КонецЕсли;
//	УправлениеФормой();
КонецПроцедуры

#КонецОбласти

#Область РаботаСканера

&НаСервере
Функция ОбработатьШтрихкоды(Данные)
	//сообщить(Данные[0].Штрихкод);
	ШтрихКод = СокрЛП(Данные[0].ШтрихКод);
	//Два варианта штрих кода
	
	Если СтрДлина(ШтрихКод) = 9 тогда
		
		//2 - это паллета
		Если ЗначениеЗаполнено(ТекущееВзвешивание) И ТекущееВзвешивание <> Документы.актВыработкаСборщиков.ПустаяСсылка() тогда
			СообщитьПредупреждение("Не завершено взвешивание текущей паллеты! Ввод новой камеры невозможен!");
			Возврат Ложь;
		КонецЕсли;
		
		ЕстьТеплица = Справочники.актСтруктураТеплиц.НайтиПоКоду(ШтрихКод);
		если ЕстьТеплица<> Неопределено и ЕстьТеплица <> Справочники.актСтруктураТеплиц.ПустаяСсылка() тогда
			Камера = ЕстьТеплица.Ссылка;
			ПолучитьНастройкиВзвешиваний();
			ПолучитьЗаказИТоварПоКамере();
		КонецЕСли;
		
		Возврат Ложь;
	иначе
		//Это код СУРВ
		Если НЕ ЗначениеЗаполнено(ТекущееВзвешивание) ИЛИ ТекущееВзвешивание = Неопределено Или ТекущееВзвешивание = Документы.актВыработкаСборщиков.ПустаяСсылка() тогда
			СообщитьПредупреждение("Не готов документ взвешивания! Ввод веса невозможен!");
			Возврат ЛОжь;
		КонецЕсли;
		Если Не ЗначениеЗаполнено(Ящик) тогда
			СообщитьПредупреждение("Не заполнена ЯЩИК! Ввод веса невозможен!");
			Возврат ЛОЖЬ;
		КонецЕСли;
		
		Сотрудник = Неопределено;
		Бригада = Неопределено;
		
		ШтрихКод = ЛЕВ(СокрЛП(СтрЗАменить(Данные[0].ШтрихКод,"""","")),4);
		КОДСУРВ = ШтрихКод;
		Запрос = Новый Запрос;
		Запрос.Текст = 
		"ВЫБРАТЬ
		|	АктКодыСУРВ.Сотрудник КАК Сотрудник,
		|	АктКодыСУРВ.КодСУРВ КАК КодСУРВ
		|ИЗ
		|	РегистрСведений.АктКодыСУРВ КАК АктКодыСУРВ
		|ГДЕ
		|	АктКодыСУРВ.КодСУРВ = &КодСУРВ";
		
		Запрос.УстановитьПараметр("КодСУРВ", ШтрихКод);
		
		РезультатЗапроса = Запрос.Выполнить();
		
		ВыборкаДетальныеЗаписи = РезультатЗапроса.Выбрать();
		
		Пока ВыборкаДетальныеЗаписи.Следующий() Цикл
			Сотрудник = ВыборкаДетальныеЗаписи.Сотрудник;
		КонецЦикла;
		
		Если ЗначениеЗаполнено(Сотрудник) тогда
			Запрос = Новый Запрос;
			Запрос.Текст = 
			"ВЫБРАТЬ
			|	актСоставБригадСрезПоследних.Период КАК Период,
			|	актСоставБригадСрезПоследних.Бригада КАК Бригада,
			|	актСоставБригадСрезПоследних.Сотрудник КАК Сотрудник,
			|	актСоставБригадСрезПоследних.Должность КАК Должность
			|ИЗ
			|	РегистрСведений.актСоставБригад.СрезПоследних(&Период, Сотрудник = &Сотрудник) КАК актСоставБригадСрезПоследних";
			
			Запрос.УстановитьПараметр("Период", Объект.ДатаРаботы);
			Запрос.УстановитьПараметр("Сотрудник", Сотрудник);
			РезультатЗапроса = Запрос.Выполнить();
			ВыборкаДетальныеЗаписи = РезультатЗапроса.Выбрать();
			
			Пока ВыборкаДетальныеЗаписи.Следующий() Цикл
				Бригада = ВыборкаДетальныеЗаписи.Бригада;
			КонецЦикла;
			
		КонецЕСли;
		Возврат Истина;
	КонецЕсли;	
	
	Возврат Ложь;
КонецФункции

#КонецОбласти


&НаСервере
Процедура ПолучитьЗаказИТоварПоКамере()
	
	ЗаказНаПроизводство = Неопределено;
	Товар = Неопределено;
	ЭтапВыпускаПродукцииПоЗаказу = Неопределено;
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
		"ВЫБРАТЬ
		|	акт_ГрафикВыпускаКамер.ЗаказНаПроизводство КАК ЗаказНаПроизводство
		|ИЗ
		|	РегистрСведений.акт_ГрафикВыпускаКамер КАК акт_ГрафикВыпускаКамер
		|ГДЕ
		|	акт_ГрафикВыпускаКамер.Период = &Период
		|	И акт_ГрафикВыпускаКамер.Камера = &Камера";
	
	Запрос.УстановитьПараметр("Камера", Камера);
	Запрос.УстановитьПараметр("Период", Объект.ДатаРаботы);
	
	РезультатЗапроса = Запрос.Выполнить();
	
	ВыборкаДетальныеЗаписи = РезультатЗапроса.Выбрать();
	
	Пока ВыборкаДетальныеЗаписи.Следующий() Цикл
		ЗаказНаПроизводство = ВыборкаДетальныеЗаписи.ЗаказНаПроизводство;
		Прервать;
	
	КонецЦикла; 
	
	Если ЗаказНаПроизводство = неопределено ИЛИ ЗаказНаПроизводство = Документы.ЗаказНаПроизводство2_2.ПустаяСсылка() тогда
		СообщитьПредупреждение("Отсутствует распределение для камеры " + Камера);
		Камера = Неопределено;
		Товар = Неопределено;
	Иначе
		Товар = ЗаказНаПроизводство.Продукция[0].Номенклатура;
		ЭтапВыпускаПродукцииПоЗаказу = ПолучитьЭтапВыпуска(Товар);
		если ЭтапВыпускаПродукцииПоЗаказу.Статус = Перечисления.СтатусыЭтаповПроизводства2_2.Начат тогда
			СообщитьУспех("Есть распределение для камеры " + Камера);
		Иначе
			СообщитьПредупреждение("НЕ ОТКРЫТ ЭТАП ПРОИЗВОДСТВА ГРИБА для камеры " + Камера);
			Камера = Неопределено;
			Товар = Неопределено;
		КонецЕсли;
	КонецЕсли
	
	
КонецПроцедуры

&НаСервере
Функция ПолучитьЭтапВыпуска(Продукция)
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
		"ВЫБРАТЬ
		|	ЭтапПроизводства2_2.Ссылка КАК Ссылка,
		|	ЭтапПроизводства2_2.Ссылка.НомерСледующегоЭтапа КАК НомерСледующегоЭтапа
		|ИЗ
		|	Документ.ЭтапПроизводства2_2 КАК ЭтапПроизводства2_2
		|ГДЕ
		|	ЭтапПроизводства2_2.Ссылка.Распоряжение = &Распоряжение
		|	И ЭтапПроизводства2_2.Ссылка.НомерСледующегоЭтапа = 0";
	
	Запрос.УстановитьПараметр("Распоряжение", ЗаказНаПроизводство);
	
	РезультатЗапроса = Запрос.Выполнить();
	
	ВыборкаДетальныеЗаписи = РезультатЗапроса.Выбрать();
	
	Пока ВыборкаДетальныеЗаписи.Следующий() Цикл
		Если ВыборкаДетальныеЗаписи.Ссылка.ВыходныеИзделия.Количество() > 0 тогда
			Если ВыборкаДетальныеЗаписи.Ссылка.ВыходныеИзделия[0].номенклатура = Продукция тогда
				Возврат ВыборкаДетальныеЗаписи.Ссылка;
			конецЕсли;
		КонецЕсли;
	КонецЦикла;
	
КонецФункции


#Область ЭлектронныеВесы

&НаКлиенте
Процедура НачатьПодключениеОборудованияРучнойРежим()
	
	ОписаниеОповещениеПриЗавершении = Новый ОписаниеОповещения("КоллбэкРучноеВзвешивание", ЭтотОбъект);
	НачатьПодключениеВесов(ОписаниеОповещениеПриЗавершении);
	
КонецПроцедуры

&НаКлиенте
Процедура НачатьПодключениеВесов(ОписаниеОповещениеПриЗавершении)
	
	РабочееМесто = МенеджерОборудованияКлиентПовтИсп.ПолучитьРабочееМестоКлиента();
	Объект.РабочееМесто = РабочееМесто;
	СписокОборудования = МенеджерОборудованияВызовСервера.ОборудованиеПоПараметрам(
		"ЭлектронныеВесы",
		Неопределено,
		РабочееМесто);
		
	Если СписокОборудования.Количество() = 0 Тогда
		СообщитьПредупреждение(НСтр("ru = 'Электроные весы не настроены'"));
		Возврат;
	КонецЕсли;
	
	ОборудованиеВесы = СписокОборудования[0].Ссылка;
	
	МенеджерОборудованияКлиент.НачатьПодключениеОборудованиеПоИдентификатору(ОписаниеОповещениеПриЗавершении, УникальныйИдентификатор, ОборудованиеВесы);
	
КонецПроцедуры

&НаКлиенте
Процедура КоллбэкРучноеВзвешивание(РезультатВыполнения, ДополнительныеПараметры) Экспорт
	
	Если Не РезультатВыполнения.Результат Тогда
		ТекстСообщения = НСтр("ru='Операция электронных весов завершилась с ошибкой:""%ОписаниеОшибки%""'");
		ТекстСообщения = СтрЗаменить(ТекстСообщения, "%ОписаниеОшибки%" , РезультатВыполнения.ОписаниеОшибки);
		СообщитьПредупреждение(ТекстСообщения);
		Инициализирован = Ложь;
	Иначе 
		Инициализирован = Истина;
		СообщитьУспех("Весы подключены");
	КонецЕсли;
	
	УправлениеФормой();
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ПолучитьВес()
	
	ДанныеВеса = Неопределено;
	
//	ВесСчитанУспешно = МенеджерОборудованияКлиент.ВыполнитьКоманду(ОборудованиеВесы, "ПолучитьВес",, ДанныеВеса, 5);
//	
//	ОбработатьПолучениеВеса(ВесСчитанУспешно, ДанныеВеса);
//	
//КонецПроцедуры

	МенеджерОборудованияКлиент.НачатьПолученияВесаСЭлектронныхВесов(
		Новый ОписаниеОповещения("ПолучитьВесЗавершение", ЭтотОбъект, ),
		УникальныйИдентификатор);
	
КонецПроцедуры

&НаКлиенте
Процедура ПолучитьВесЗавершение(РезультатВыполнения, ТекущаяСтрока) Экспорт
	
	Если РезультатВыполнения.Результат Тогда
		ОбработатьПолучениеВеса(Истина, РезультатВыполнения);
	Иначе
		МенеджерОборудованияУТКлиент.СообщитьОбОшибке(РезультатВыполнения);
		ОбработатьПолучениеВеса(ЛОЖЬ, РезультатВыполнения);
	КонецЕсли;
	
КонецПроцедуры


&НаКлиенте
Процедура ОбработатьПолучениеВеса(ВесСчитанУспешно, ДанныеВеса)
	
	Если ВесСчитанУспешно Тогда
		ВесБрутто = ДанныеВеса.Вес;
		ВремяВзвешивания = ТекущаяДата();
		РасчитатьВесНетто();
		УправлениеФормой();
	Иначе
		Элементы.ВвестиВесВручную.Видимость = Истина;
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ВвестиВесВручную(Команда)
	Перем Вес;
	подпись = "Введите вес холодильника вручную!";
	
	ПоказатьВводЧисла(Новый ОписаниеОповещения("ВвестиВесВручнуюЗавершение", ЭтаФорма, Новый Структура("вес", вес)), вес,Подпись,15,3);
КонецПроцедуры

&НаКлиенте
Процедура ВвестиВесВручнуюЗавершение(Число, ДополнительныеПараметры) Экспорт
	
	вес = ?(Число = Неопределено, ДополнительныеПараметры.вес, Число);
	
	
	Ура = (Число <> Неопределено);
	
	если Ура тогда 
		ВесБрутто = Вес;
		ВремяВзвешивания = ТекущаяДата();
	КонецЕСли;
	
	РасчитатьВесНетто();

КонецПроцедуры

&НаКлиенте
Процедура ОбнулитьТерминал(Команда)
	Стабильный = Ложь;
	ВесБрутто = 0;
	
КонецПроцедуры

&НаКлиенте
Процедура ОбновитьТерминал(Команда)
	Стабильный = Ложь;
	Подключаемый_ПолучитьВес();
КонецПроцедуры

#КонецОбласти

#Область ПодключаемоеОборудованиеОбщиеФункции

&НаКлиенте
Процедура Подключаемый_НачатьПодключениеОборудования() Экспорт
	

	НачатьПодключениеОборудованияРучнойРежим();
	
	УправлениеФормой();
	
КонецПроцедуры

&НаКлиенте
Процедура ЗавершитьОтключениеОборудования(РезультатВыполнения, ДополнительныеПараметры) Экспорт
	
	ОткрытаСессияСчитывателяШК = Ложь;
	Инициализирован = Ложь;
	
КонецПроцедуры

&НаКлиенте
Процедура ЗавершитьРаботуСТерминалом()
	
	ОтключитьОбработчикОжидания("Подключаемый_ПолучитьВес");
	Оповещение = Новый ОписаниеОповещения("ЗавершитьОтключениеОборудования", ЭтотОбъект);
	МенеджерОборудованияКлиент.НачатьОтключениеВсегоОборудования(Оповещение);
	
КонецПроцедуры

#КонецОбласти

#Область Взвешивание

&НаКлиенте
Процедура РасчитатьВесНетто()
	ВесНетто = ВесБрутто - ВесТары;
	УправлениеФормой();
КонецПРоцедуры

&НаСервере
Функция ПолучитьВесТары()
	
	Возврат ВесТары;

КонецФункции

&НаКлиенте
Процедура ЗавершитьРучнойВводВеса(Значение, ДополнительныеПараметры) Экспорт
	
	Если Значение = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	ВесБрутто = Значение;
	
КонецПроцедуры

&НаКлиенте
Процедура Всвесить(Команда)
	Подключаемый_ПолучитьВес();
КонецПроцедуры

#КонецОбласти

#Область УправленияФормой

&НаКлиенте
Процедура УправлениеФормой()
КонецПроцедуры

&НаСервере
Функция ПолучитьВесУпаковки()
	ВсегоВес = 0;
	//Считаем вес ящиков
	Если ЗначениеЗаполнено(Ящик) тогда
		Если Ящик.ВесЗнаменатель <> 0 тогда
			ВсегоВес = ВсегоВес + Ящик.ВесЧислитель/Ящик.ВесЗнаменатель;
		КонецЕсли
	Иначе
		КолЯщик = 0;
	КонецЕсли;
	//Считаем вес Упаковки 400 г.
	Если ЗначениеЗаполнено(Упаковка1) тогда
		Если Упаковка1.ВесЗнаменатель <> 0 тогда
			ВсегоВес = ВсегоВес + Упаковка1.ВесЧислитель/Упаковка1.ВесЗнаменатель * КолУпаковка1;
		КонецЕсли
	Иначе
		КолУпаковка1 = 0;
	КонецЕсли;	
	Возврат ВсегоВес;
КонецФункции

&НаКлиенте
Процедура ПоказателиУпаковкиПриИзменении(Элемент)
	ВесУпак = ПолучитьВесУпаковки();
	ВесТары = ВесУпак;
	РасчитатьВесНетто();
КонецПроцедуры

&НаСервере
Процедура ПолучитьНастройкиВзвешиваний()
		Запрос = Новый Запрос;
		Запрос.Текст = 
		"ВЫБРАТЬ ПЕРВЫЕ 1
		|	акт_НастройкаАРМВыпуска.Ферма КАК Ферма,
		|	акт_НастройкаАРМВыпуска.НоменклатураУпаковки КАК НоменклатураУпаковки,
		|	акт_НастройкаАРМВыпуска.Склад КАК Склад,
		|	акт_НастройкаАРМВыпуска.ВидыТары КАК ВидыТары,
		|	акт_НастройкаАРМВыпуска.ИспользоватьХолодильник КАК ИспользоватьХолодильник
		|ИЗ
		|	РегистрСведений.акт_НастройкаАРМВыпуска КАК акт_НастройкаАРМВыпуска
		|ГДЕ
		|	акт_НастройкаАРМВыпуска.Ферма = &Ферма";
		
		Запрос.УстановитьПараметр("Ферма", Камера.Родитель);
		
		РезультатЗапроса = Запрос.Выполнить();
		
		ВыборкаДетальныеЗаписи = РезультатЗапроса.Выбрать();
		
		Пока ВыборкаДетальныеЗаписи.Следующий() Цикл
			Склад = ВыборкаДетальныеЗаписи.Склад;
			ИспользоватьХолодильник = ВыборкаДетальныеЗаписи.ИспользоватьХолодильник;
			Поддон = ВыборкаДетальныеЗаписи.НоменклатураУпаковки;
			РаспакованнаяТара = ВыборкаДетальныеЗаписи.ВидыТары.Получить();
			Элементы.Упаковка1.РежимВыбораИзСписка = ЛОжь;;
			Если ТипЗнч(РаспакованнаяТара) = Тип("Массив") Тогда
				Элементы.Упаковка1.РежимВыбораИзСписка = Истина;
				Элементы.Упаковка1.СписокВыбора.Очистить();
				Элементы.Ящик.СписокВыбора.Очистить();
				Для каждого СтрокаТары Из РаспакованнаяТара Цикл
					Если СтрокаТары.ВидТары = "0" тогда
						Элементы.Упаковка1.СписокВыбора.Добавить(СтрокаТары.Тара);
					ИначеЕсли СтрокаТары.ВидТары = "1" тогда
						Элементы.Ящик.СписокВыбора.Добавить(СтрокаТары.Тара);
					КонецеСли;
				КонецЦикла;
			КонецЕсли;
		КонецЦикла;
		
КонецПроцедуры

#КонецОбласти

#Область ФормированиеДокумента

&НаСервере
Процедура ЗаписатьВзвешиваниеНаСервере()
	Если ВесНетто > 0 тогда
		
		//Запрос = Новый Запрос;
		//Запрос.Текст = 
		//"ВЫБРАТЬ
		//|	актВыработкаСборщиков.Ссылка КАК Ссылка
		//|ИЗ
		//|	Документ.актВыработкаСборщиков КАК актВыработкаСборщиков
		//|ГДЕ
		//|	актВыработкаСборщиков.Дата = &Дата
		//|	И актВыработкаСборщиков.РабочееМесто = &РабочееМесто";
		//
		//Запрос.УстановитьПараметр("Дата", Объект.ДатаРаботы+1);
		//Запрос.УстановитьПараметр("РабочееМесто", Объект.РабочееМесто);
		//
		//РезультатЗапроса = Запрос.Выполнить();
		//
		//ВыборкаДетальныеЗаписи = РезультатЗапроса.Выбрать();
		//
		//Пока ВыборкаДетальныеЗаписи.Следующий() Цикл
		//	ТекущееВзвешивание = ВыборкаДетальныеЗаписи.Ссылка;
		//КонецЦикла;
		//
		//Если  ЗначениеЗаполнено(ТекущееВзвешивание) и ТекущееВзвешивание <> Документы.актВыработкаСборщиков.ПустаяСсылка() тогда
		//Иначе
		//	НовоеВзвешивание = Документы.актВыработкаСборщиков.СоздатьДокумент();
		//	НовоеВзвешивание.Дата = Объект.ДатаРаботы+1;
		//	НовоеВзвешивание.Бригада = "Ручное";
		//	НовоеВзвешивание.РабочееМесто = Объект.РабочееМесто;
		//	НовоеВзвешивание.Записать();
		//	ТекущееВзвешивание = НовоеВзвешивание.Ссылка;
		//КонецЕсли;
		
		ТекДок = ТекущееВзвешивание.ПолучитьОбъект();
		СтрДок = ТекДок.Работа.Добавить();
		СтрДок.Сотрудник = Сотрудник;
		СтрДок.Количество = весНетто;
		СтрДок.упаковка = Упаковка1;
		СтрДок.КоличествоУпаковки = КолУпаковка1;
		СтрДок.КоличествоЯщиков = 1;
		СтрДок.ВидРабот = ПолучитьРаботуСотрудника();
		СтрДок.Паллета = ТекущаяПаллета;
		СтрДок.Бригада = Бригада;
		СтрДок.ВидРабот = ВидРабот;
		ТекДок.Записать();
		
		НовСтр = ТабДок.Добавить();
		новСтр.Сотрудник = СокрЛП(Сотрудник);
		НовСтр.весНетто = ВесНетто;
		СообщитьУспех("Зафиксирован вес:" + ВесНетто + " " + СокрЛП(Сотрудник));
	КонецЕсли;
	
	
КонецПроцедуры

&НаСервере
Функция ПолучитьРаботуСотрудника()
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
		"ВЫБРАТЬ
		|	актРаспределениеСборщиковПоКамерамСрезПоследних.Камера КАК Камера,
		|	актРаспределениеСборщиковПоКамерамСрезПоследних.ВидРабот КАК ВидРабот,
		|	актРаспределениеСборщиковПоКамерамСрезПоследних.Бригада КАК Бригада
		|ИЗ
		|	РегистрСведений.актРаспределениеСборщиковПоКамерам.СрезПоследних(&ДатаСбора, Сотрудник = &Сотрудник) КАК актРаспределениеСборщиковПоКамерамСрезПоследних";
	
	Запрос.УстановитьПараметр("ДатаСбора", Объект.ДатаРаботы);
	Запрос.УстановитьПараметр("Сотрудник", Сотрудник);
	
	РезультатЗапроса = Запрос.Выполнить();
	
	ВыборкаДетальныеЗаписи = РезультатЗапроса.Выбрать();
	
	Пока ВыборкаДетальныеЗаписи.Следующий() Цикл
		Возврат ВыборкаДетальныеЗаписи.ВидРабот;
	КонецЦикла;
	
	если ЗначениеЗаполнено(Упаковка1) тогда
		Возврат РегистрыСведений.актКонстанты.мПолучить("Справочники.ВидыРаботСотрудников.СборГриба");
	КонецЕСли;
	
КонецФункции

&НаКлиенте
Процедура ОчиститьФорму(); 
	ВесНетто = 0;
	весБрутто = 0;
КонецПроцедуры

&НаКлиенте
Процедура ЗаписатьВзвешивание(Команда)
	Если ЗначениеЗаполнено(ТекущееВзвешивание) тогда
		ЗаписатьВзвешиваниеПаллетыНаСервере();
		ОчиститьФорму();
		Камера = Неопределено;
		Категория = Неопределено;
		Элементы.ГрКамера.Доступность = Истина;
		Элементы.ФормаНачатьВзвешиваниеПаллеты.Доступность = Истина;
		ТабДок.Очистить();
		ТекущееВзвешивание = Неопределено;
	Иначе
		СообщитьПредупреждение("Не выбрана паллета! невозможно идентифицировать ЯЩИК!!!!");
	КонецЕсли;
КонецПроцедуры

&НАСервере
Процедура  ЗаписатьВзвешиваниеПаллетыНаСервере()
	
	ТекДок = ТекущееВзвешивание.ПолучитьОбъект();
	Попытка
		ТекДок.Записать(РежимЗаписиДокумента.Проведение);
		СообщитьУспех("Документ взвешивания паллеты: " + ПолучитьНомер() +" записан!!");
	Исключение
		СообщитьПредупреждение("Не удалось записать документ!!");
	КонецПопытки;
	
КонецПроцедуры


#КонецОбласти


#Область СтатусныеСообщения

&НаКлиенте
Процедура ПоследниеСообщения(Команда)
	
	ПоказатьЗначение(, СтекСообщений);
	
КонецПроцедуры

&НаСервере
Процедура СообщитьПрогресс(Текст = "")
	
	ПоказатьИнфо = ПустаяСтрока(Текст);
	
	Элементы.СтраницыСостояние.ТекущаяСтраница = ?(ПоказатьИнфо,
		Элементы.СостояниеПустаяСтраница, Элементы.СостояниеОжиданиеСтраница);
		
	ТекстСообщенияСтатуса = Текст;
	
	ЗаписатьСообщение(ЭтотОбъект, Текст, Элементы.ОжиданиеКартинка.Картинка);
	
КонецПроцедуры

&НаСервере
Процедура СообщитьУспех(Текст)
	
	Элементы.СтраницыСостояние.ТекущаяСтраница = Элементы.СостояниеУспехСтраница;
	
	ТекстСообщенияСтатуса = Текст;
	
	ЗаписатьСообщение(ЭтотОбъект, Текст, Элементы.УспехКартинка.Картинка);
	
КонецПроцедуры

&НаСервере
Процедура СообщитьПредупреждение(Текст)
	
	Элементы.СтраницыСостояние.ТекущаяСтраница = Элементы.СостояниеПредупреждениеСтраница;
	
	ТекстСообщенияСтатуса = Текст;
	
	ЗаписатьСообщение(ЭтотОбъект, Текст, Элементы.ОшибкаКартинка.Картинка);
	
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Процедура ЗаписатьСообщение(Форма, Текст, Картинка)
	
	Форма.СтекСообщений.Вставить(0, СтрШаблон("%1 %2", Формат(ТекущаяДата(), "ДЛФ=T"), Текст),,, Картинка);
	Для Сч = 10 По Форма.СтекСообщений.Количество() - 1 Цикл
		Форма.СтекСообщений.Удалить(Сч);
	КонецЦикла;
	
КонецПроцедуры

&НАСервере
Процедура  ЗаполнитьВзвешивание()
	
	НовоеВзвешивание = Документы.актВыработкаСборщиков.СоздатьДокумент();
	НовоеВзвешивание.Дата = Объект.ДатаРаботы+1;
	НовоеВзвешивание.Бригада = "Ручное";
	НовоеВзвешивание.РабочееМесто = Объект.РабочееМесто;
	НовоеВзвешивание.Камера = Камера;
	НовоеВзвешивание.Склад = Склад;
	НовоеВзвешивание.Номенклатура = Товар;
	НовоеВзвешивание.КатегорияНоменклатуры = Категория;
	НовоеВзвешивание.Заказ = ЗаказНаПроизводство;
	НовоеВзвешивание.ВыпускПродукции = Истина;
	НовоеВзвешивание.Записать();
	ТекущееВзвешивание = НовоеВзвешивание.Ссылка;
	
КонецПроцедуры

&НАСервере
Процедура  ЗаполнитьВидРабот()
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
		"ВЫБРАТЬ
		|	актВидыРаботНоменклатуры.ВидРабот КАК ВидРабот
		|ИЗ
		|	РегистрСведений.актВидыРаботНоменклатуры КАК актВидыРаботНоменклатуры
		|ГДЕ
		|	актВидыРаботНоменклатуры.Номенклатура = &Номенклатура
		|	И актВидыРаботНоменклатуры.Категория = &Категория
		|	И актВидыРаботНоменклатуры.Упаковка = ЗНАЧЕНИЕ(Справочник.Номенклатура.Пустаяссылка)";
	
	Запрос.УстановитьПараметр("Категория", Категория);
	Запрос.УстановитьПараметр("Номенклатура", Товар);
	
	РезультатЗапроса = Запрос.Выполнить();
	
	ВыборкаДетальныеЗаписи = РезультатЗапроса.Выбрать();
	
	Пока ВыборкаДетальныеЗаписи.Следующий() Цикл
		ВидРабот = ВыборкаДетальныеЗаписи.ВидРабот;
		Возврат;
	КонецЦикла;
	
	ВидРабот = РегистрыСведений.актКонстанты.мПолучить("Справочники.ВидыРаботСотрудников.СборГриба");
	
КонецПроцедуры



&НаКлиенте
Процедура НачатьВзвешиваниеПаллеты(Команда)
	Если Не ЗначениеЗаполнено(Камера) тогда
		СообщитьПредупреждение("Не заполнена КАМЕРА!!!");
		Возврат;
	КонецЕСли;
	Если Не ЗначениеЗаполнено(Категория) тогда
		СообщитьПредупреждение("Не заполнена КАТЕГОРИЯ!!!");
		Возврат;
	КонецЕСли;
	Если Не ЗначениеЗаполнено(ЗаказНаПроизводство) тогда
		СообщитьПредупреждение("Для данной камеры нет заказа на производство!!!");
		Возврат;
	КонецЕсли;
	// Формруем документ
	ЗаполнитьВидРабот();
	ЗаполнитьВзвешивание();
	Элементы.ГрКамера.Доступность = Ложь;
	Элементы.ФормаНачатьВзвешиваниеПаллеты.Доступность = ЛОжь;
	СообщитьУспех("Начато взвешивание паллеты: " + ПолучитьНомер());
КонецПроцедуры

&НаСервере
Функция ПолучитьНомер()
	Возврат ТекущееВзвешивание.Номер;
КонецФункции


&НаКлиенте
Процедура КамераПриИзменении(Элемент)
	ПолучитьНастройкиВзвешиваний();
	ПолучитьЗаказИТоварПоКамере();
КонецПроцедуры
#КонецОбласти
