﻿#Область ИнтерфейсБСП

Функция СведенияОВнешнейОбработке() Экспорт
	
	ПараметрыРегистрации = Новый Структура;
	МассивНазначений = Новый Массив;
	//МассивНазначений.Добавить("Документ.СчетНаОплатуКлиенту");
	//МассивНазначений.Добавить("Документ.ЗаказКлиента");
	
	ПараметрыРегистрации.Вставить("Вид", "ДополнительнаяОбработка");
	//ПараметрыРегистрации.Вставить("Назначение", МассивНазначений);
	ПараметрыРегистрации.Вставить("Наименование", "Рабочее место менеджера по продажам");
	ПараметрыРегистрации.Вставить("Версия", "1.0");
	ПараметрыРегистрации.Вставить("БезопасныйРежим", Ложь);
	ПараметрыРегистрации.Вставить("Информация", "Рабочее место менеджера по продажам");
	
	ТаблицаКоманд = ПолучитьТаблицуКоманд();
	
	ДобавитьКоманду(ТаблицаКоманд,
	"Рабочее место менеджера по продажам",
	"актРабочееМестоМенеджераПоПродажам",
	"ОткрытиеФормы",
	Истина,
	);
	
	ПараметрыРегистрации.Вставить("Команды", ТаблицаКоманд);
	
	Возврат ПараметрыРегистрации;

КонецФункции

Функция ПолучитьТаблицуКоманд()
  Команды = Новый ТаблицаЗначений;
  Команды.Колонки.Добавить("Представление", Новый ОписаниеТипов("Строка"));
  Команды.Колонки.Добавить("Идентификатор", Новый ОписаниеТипов("Строка"));
  Команды.Колонки.Добавить("Использование", Новый ОписаниеТипов("Строка"));
  Команды.Колонки.Добавить("ПоказыватьОповещение", Новый ОписаниеТипов("Булево"));
  Команды.Колонки.Добавить("Модификатор", Новый ОписаниеТипов("Строка"));
  Возврат Команды;
КонецФункции

Процедура ДобавитьКоманду(ТаблицаКоманд, Представление, Идентификатор, Использование, ПоказыватьОповещение = Ложь, Модификатор = "")

  НоваяКоманда = ТаблицаКоманд.Добавить();
  НоваяКоманда.Представление = Представление;
  НоваяКоманда.Идентификатор = Идентификатор;
  НоваяКоманда.Использование = Использование;
  НоваяКоманда.ПоказыватьОповещение = ПоказыватьОповещение;
  НоваяКоманда.Модификатор = Модификатор;

КонецПроцедуры

#КонецОбласти
