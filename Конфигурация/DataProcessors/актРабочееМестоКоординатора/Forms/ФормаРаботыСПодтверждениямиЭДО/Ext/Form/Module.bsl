﻿
#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура НадписьОткрытьСтаруюВерсиюРабочегоМестаНажатие(Элемент)
	
	ФормаСтарая = ПолучитьФорму("Обработка.актРабочееМестоКоординатора.Форма.Форма");
	ФормаСтарая.Открыть();
	ЭтаФорма.Закрыть();
	
КонецПроцедуры

&НаКлиенте
Процедура ПривязатьПодтверждениеКЗаказу(Команда)
	
	ОписаниеОповещения = Новый ОписаниеОповещения("ПривязатьПодтверждениеКЗаказуЗавершение", ЭтотОбъект);
	
	ПоказатьВопрос(ОписаниеОповещения, "Привязать подтверждение ЭДО к предварительному заказу клиента?", РежимДиалогаВопрос.ДаНет);	
	
КонецПроцедуры

&НаКлиенте
Процедура ОтменитьПодтверждениеИзЭДО(Команда)
	
	ОписаниеОповещения = Новый ОписаниеОповещения("ОтменитьПодтверждениеИзЭДОЗавершение", ЭтотОбъект);	
	
	ТекстВопроса = СтрШаблон("Отменить выбранное подтверждение №%1?",
		СокрЛП(ПолучитьЗначениеРеквизитаНаСервере(ПодтверждениеЗаказаКлиента,"Номер"))
		);
	
	ПоказатьВопрос(ОписаниеОповещения, ТекстВопроса, РежимДиалогаВопрос.ДаНет);	
	
КонецПроцедуры

&НаКлиенте
Процедура ПодтвердитьСопоставленноеПодтверждениеЗаказаКлиента(Команда)
	
	ОписаниеОповещения = Новый ОписаниеОповещения("ПодтвердитьСопоставленноеПодтверждениеЗаказаКлиентаЗавершение", ЭтотОбъект);	
	
	ТекстВопроса = СтрШаблон("Подтвердить выбранный контракт ЭДО №%1?",
		СокрЛП(ПолучитьЗначениеРеквизитаНаСервере(Элементы.СопоставленныеПодтвержденияЗаказовКлиента.ТекущаяСтрока,"Номер"))
		);
	
	ПоказатьВопрос(ОписаниеОповещения, ТекстВопроса, РежимДиалогаВопрос.ДаНет);	
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовФормы

&НаКлиенте
Процедура ЗаказыКлиентаПредварительныеПриАктивизацииСтроки(Элемент)
	
	Если ТипЗнч(Элемент.ТекущаяСтрока) = Тип("ДокументСсылка.ЗаказКлиента") Тогда
		
		ЗаполнитьДетализациюПоЗаказуНаСервере(Элемент.ТекущаяСтрока);
		ЭтаФорма.ЗаказКлиентаПредварительный = Элемент.ТекущаяСтрока;
		
	Иначе
		
		ЭтаФорма.ЗаказКлиентаПредварительный = Неопределено;
		ОчиститьДетализациюПоЗаказу();
		
	КонецЕсли;
	
	УстановитьВидимостьИДоступностьЭлементовФормы();	
	
КонецПроцедуры

&НаКлиенте
Процедура ПодтвержденияЗаказовКлиентаПриАктивизацииСтроки(Элемент)
	
	Если ТипЗнч(Элемент.ТекущаяСтрока) = Тип("ДокументСсылка.актПодтверждениеЗаказаКлиента") Тогда
		
		ЗаполнитьДетализациюПоПодтверждениюНаСервере(Элемент.ТекущаяСтрока);
		ЭтаФорма.ПодтверждениеЗаказаКлиента = Элемент.ТекущаяСтрока;
		
		Элементы.ПодтвержденияЗаказовКлиентаОтменитьПодтверждениеИзЭДО.Доступность = Истина;
		
	Иначе
		
		ЭтаФорма.ПодтверждениеЗаказаКлиента = Неопределено;
		ОчиститьДетализациюПоПодтверждению();
		
		Элементы.ПодтвержденияЗаказовКлиентаОтменитьПодтверждениеИзЭДО.Доступность = Ложь;
		
	КонецЕсли;
	
	УстановитьВидимостьИДоступностьЭлементовФормы();	

КонецПроцедуры


&НаКлиенте
Процедура СопоставленныеПодтвержденияЗаказовКлиентаПриАктивизацииСтроки(Элемент)
	
	Если ТипЗнч(Элемент.ТекущаяСтрока) = Тип("ДокументСсылка.актПодтверждениеЗаказаКлиента") Тогда
		
		ЗаполнитьДетализациюПоСопоставленномуПодтверждениюНаСервере(Элемент.ТекущаяСтрока);
		
		Элементы.ПодтвердитьСопоставленноеПодтверждениеЗаказаКлиента.Доступность = Истина;
		
	Иначе
		
		ОчиститьДетализациюПоСопоставленномуПодтверждению();
		
		Элементы.ПодтвердитьСопоставленноеПодтверждениеЗаказаКлиента.Доступность = Ложь;
		
	КонецЕсли;
	
	УстановитьВидимостьИДоступностьЭлементовФормы();
	
КонецПроцедуры


#КонецОбласти

#Область ВспомогательныеПроцедурыИФункции

&НаКлиенте
Процедура УстановитьВидимостьИДоступностьЭлементовФормы()
	
	Элементы.ПривязатьПодтверждениеКЗаказу.Доступность = ЗначениеЗаполнено(ПодтверждениеЗаказаКлиента) И ЗначениеЗаполнено(ЗаказКлиентаПредварительный);
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьДетализациюПоЗаказуНаСервере(ЗаказКлиента)
	
	ЭтаФорма.ТаблицаНоменклатурыЗаказыКлиентаПредварительные.Очистить();
	
	Для Каждого СтрокаТоварЗаказ ИЗ  ЗаказКлиента.Товары Цикл
		
		Если СтрокаТоварЗаказ.Отменено Тогда
			Продолжить;
		КонецЕсли;
		
		НоваяСтрока = ЭтаФорма.ТаблицаНоменклатурыЗаказыКлиентаПредварительные.Добавить();
		ЗаполнитьЗначенияСвойств(НоваяСтрока,СтрокаТоварЗаказ);
		
		Если ЗначениеЗаполнено(СтрокаТоварЗаказ.Упаковка) Тогда
			НоваяСтрока.ЕдиницаИзмерения = СтрокаТоварЗаказ.Упаковка;
		Иначе
			НоваяСтрока.ЕдиницаИзмерения = СтрокаТоварЗаказ.Номенклатура.ЕдиницаИзмерения;
		КонецЕсли;
		
	КонецЦикла;
	
	ЭтаФорма.ДетализацияПредварительныеАдресДоставки = ?(ЗначениеЗаполнено(ЗаказКлиента.АдресДоставки),ЗаказКлиента.АдресДоставки," <не указан> ");
	ЭтаФорма.ДетализацияПредварительныеДополнительнаяИнформация = ?(ЗначениеЗаполнено(ЗаказКлиента.Комментарий),ЗаказКлиента.Комментарий," <не заполнено> ");
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьДетализациюПоПодтверждениюНаСервере(актПодтверждениеЗаказаКлиента)
	
	ЭтаФорма.ТаблицаНоменклатурыПодтвержденияЗаказовКлиента.Очистить();
	
	Для Каждого СтрокаТоварПодтверждение ИЗ  актПодтверждениеЗаказаКлиента.Товары Цикл
		
		НоваяСтрока = ЭтаФорма.ТаблицаНоменклатурыПодтвержденияЗаказовКлиента.Добавить();
		ЗаполнитьЗначенияСвойств(НоваяСтрока,СтрокаТоварПодтверждение);
		
		Если ЗначениеЗаполнено(СтрокаТоварПодтверждение.Упаковка) Тогда
			НоваяСтрока.ЕдиницаИзмерения = СтрокаТоварПодтверждение.Упаковка;
		Иначе
			НоваяСтрока.ЕдиницаИзмерения = СтрокаТоварПодтверждение.Номенклатура.ЕдиницаИзмерения;
		КонецЕсли;
		
	КонецЦикла;
	
	ЭтаФорма.ДетализцаияПодтвержденияГрузополучатель = ?(ЗначениеЗаполнено(актПодтверждениеЗаказаКлиента.Грузополучатель),актПодтверждениеЗаказаКлиента.Грузополучатель," <не указан> ");
	ЭтаФорма.ДетализацияПодтвержденияАдресСтрокой = ?(ЗначениеЗаполнено(актПодтверждениеЗаказаКлиента.АдресСтрокой),актПодтверждениеЗаказаКлиента.АдресСтрокой," <не указан> ");
	
КонецПроцедуры

&НаКлиенте
Процедура ОчиститьДетализациюПоЗаказу()
	
	ТаблицаНоменклатурыЗаказыКлиентаПредварительные.Очистить();	
	ЭтаФорма.ДетализацияПредварительныеАдресДоставки = " <не указан> ";
	ЭтаФорма.ДетализацияПредварительныеДополнительнаяИнформация = " <не заполнено> ";
	
КонецПроцедуры

&НаКлиенте
Процедура ОчиститьДетализациюПоПодтверждению()
	
	ТаблицаНоменклатурыПодтвержденияЗаказовКлиента.Очистить();
	ЭтаФорма.ДетализцаияПодтвержденияГрузополучатель = " <не указан> ";
	ЭтаФорма.ДетализацияПодтвержденияАдресСтрокой = " <не указан> ";
	
КонецПроцедуры

&НаКлиенте
Процедура ПривязатьПодтверждениеКЗаказуЗавершение(РезультатВопроса, ДополнитвельныеПараметры) Экспорт
	
	Если РезультатВопроса <> КодВозвратаДиалога.Да Тогда
		Возврат;
	КонецЕсли;
	
	Отказ = Ложь;
	
	ПривязатьПодтверждениеКЗаказуЗавершениеНаСервере(Отказ);
	
	Если Отказ Тогда
		Возврат;
	КонецЕсли;
	
	ТекстОповещения = "Подтверждение ЭДО";
	ТекстПояснения	= "Выполнено успешно";
	КартинкаОповещения = БиблиотекаКартинок.ЗакрытыеЗаказыНаПроизводство;
	
	ПоказатьОповещениеПользователя(ТекстОповещения,, ТекстПояснения, КартинкаОповещения);
	
	Элементы.ЗаказыКлиентаПредварительные.Обновить();
	Элементы.ПодтвержденияЗаказовКлиента.Обновить();
	
КонецПроцедуры

&НаКлиенте
Процедура ОтменитьПодтверждениеИзЭДОЗавершение(РезультатВопроса, ДополнитвельныеПараметры) Экспорт
	
	Если РезультатВопроса <> КодВозвратаДиалога.Да Тогда
		Возврат;
	КонецЕсли;
	
	Отказ = Ложь;
	
	ОтменитьПодтверждениеИзЭДОЗавершениеНаСервере(Отказ);
	
	Если Отказ Тогда
		Возврат;
	КонецЕсли;
	
	ТекстОповещения = "Отмена подтверждения";
	ТекстПояснения	= "Выполнено успешно";
	КартинкаОповещения = БиблиотекаКартинок.ЗакрытыеЗаказыНаПроизводство;
	
	ПоказатьОповещениеПользователя(ТекстОповещения,, ТекстПояснения, КартинкаОповещения);
	
	Элементы.ЗаказыКлиентаПредварительные.Обновить();
	Элементы.ПодтвержденияЗаказовКлиента.Обновить();
	
КонецПроцедуры

&НаСервере
Процедура ПривязатьПодтверждениеКЗаказуЗавершениеНаСервере(Отказ) 
	
	ДокПодтверждение = ПодтверждениеЗаказаКлиента.ПолучитьОбъект();
	ДокПодтверждение.ДокументОснование = ЗаказКлиентаПредварительный;
	ДокПодтверждение.Статус	= Перечисления.СтатусыЗаказовКлиентов.КОтгрузке;
	ДокПодтверждение.ЗаполнитьРасхождения();
	
	Попытка
		ДокПодтверждение.Записать(РежимЗаписиДокумента.Проведение);
	Исключение
		Сообщить(ОписаниеОшибки());
		Отказ = Истина;
	КонецПопытки;
	
КонецПроцедуры

&НаСервере
Процедура ОтменитьПодтверждениеИзЭДОЗавершениеНаСервере(Отказ) 
	
	ДокПодтверждение = ПодтверждениеЗаказаКлиента.ПолучитьОбъект();
	ДокПодтверждение.Статус	= Перечисления.СтатусыЗаказовКлиентов.Закрыт;
	
	Попытка
		ДокПодтверждение.Записать();
	Исключение
		Сообщить(ОписаниеОшибки());
		Отказ = Истина;
	КонецПопытки;
	
КонецПроцедуры

&НаСервере
Функция ПолучитьЗначениеРеквизитаНаСервере(СсылкаНаОбъект, ИмяРеквизита)
	
	Возврат СсылкаНаОбъект[ИмяРеквизита];
	
КонецФункции

&НаКлиенте
Процедура ПодтвердитьСопоставленноеПодтверждениеЗаказаКлиентаЗавершение(РезультатВопроса, ДополнитвельныеПараметры) Экспорт
	
	Если РезультатВопроса <> КодВозвратаДиалога.Да Тогда
		Возврат;
	КонецЕсли;
	
	Отказ = Ложь;
	
	ПодтвердитьСопоставленноеПодтверждениеЗаказаКлиентаЗавершениеНаСервере(Отказ);
	
	Если Отказ Тогда
		Возврат;
	КонецЕсли;
	
	ТекстОповещения = "Подтверждение контракта";
	ТекстПояснения	= "Выполнено успешно";
	КартинкаОповещения = БиблиотекаКартинок.ПалецВверхЗеленый;
	
	ПоказатьОповещениеПользователя(ТекстОповещения,, ТекстПояснения, КартинкаОповещения);
	
	Элементы.СопоставленныеПодтвержденияЗаказовКлиента.Обновить();
	
КонецПроцедуры

&НаСервере
Процедура ПодтвердитьСопоставленноеПодтверждениеЗаказаКлиентаЗавершениеНаСервере(Отказ) 
	
	ДокПодтверждение = Элементы.СопоставленныеПодтвержденияЗаказовКлиента.ТекущаяСтрока.ПолучитьОбъект();
	ДокПодтверждение.Статус	= Перечисления.СтатусыЗаказовКлиентов.КОтгрузке;
	
	Попытка
		ДокПодтверждение.Записать(РежимЗаписиДокумента.Проведение);
	Исключение
		Сообщить(ОписаниеОшибки());
		Отказ = Истина;
	КонецПопытки;
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьДетализациюПоСопоставленномуПодтверждениюНаСервере(актПодтверждениеЗаказаКлиента)
	
	ТаблицаНоменклатурыПодтвержденияЗаказовКлиентаСопоставленные.Очистить();
	ТаблицаНоменклатурыЗаказыКлиентаПредварительныеСопоставленные.Очистить();
	
	Для Каждого СтрокаТоварПодтверждение ИЗ  актПодтверждениеЗаказаКлиента.Товары Цикл
		
		НоваяСтрока = ЭтаФорма.ТаблицаНоменклатурыПодтвержденияЗаказовКлиентаСопоставленные.Добавить();
		ЗаполнитьЗначенияСвойств(НоваяСтрока,СтрокаТоварПодтверждение);
		
		Если ЗначениеЗаполнено(СтрокаТоварПодтверждение.Упаковка) Тогда
			НоваяСтрока.ЕдиницаИзмерения = СтрокаТоварПодтверждение.Упаковка;
		Иначе
			НоваяСтрока.ЕдиницаИзмерения = СтрокаТоварПодтверждение.Номенклатура.ЕдиницаИзмерения;
		КонецЕсли;
		
	КонецЦикла;
	
	Для Каждого СтрокаТоварПодтверждение ИЗ  актПодтверждениеЗаказаКлиента.ДокументОснование.Товары Цикл
		
		Если СтрокаТоварПодтверждение.Отменено Тогда
			Продолжить;
		КонецЕсли;
		
		НоваяСтрока = ЭтаФорма.ТаблицаНоменклатурыЗаказыКлиентаПредварительныеСопоставленные.Добавить();
		ЗаполнитьЗначенияСвойств(НоваяСтрока,СтрокаТоварПодтверждение);
		
		Если ЗначениеЗаполнено(СтрокаТоварПодтверждение.Упаковка) Тогда
			НоваяСтрока.ЕдиницаИзмерения = СтрокаТоварПодтверждение.Упаковка;
		Иначе
			НоваяСтрока.ЕдиницаИзмерения = СтрокаТоварПодтверждение.Номенклатура.ЕдиницаИзмерения;
		КонецЕсли;
		
	КонецЦикла;
	
	//ЭтаФорма.ДетализцаияПодтвержденияГрузополучатель = ?(ЗначениеЗаполнено(актПодтверждениеЗаказаКлиента.Грузополучатель),актПодтверждениеЗаказаКлиента.Грузополучатель," <не указан> ");
	//ЭтаФорма.ДетализацияПодтвержденияАдресСтрокой = ?(ЗначениеЗаполнено(актПодтверждениеЗаказаКлиента.АдресСтрокой),актПодтверждениеЗаказаКлиента.АдресСтрокой," <не указан> ");
	
КонецПроцедуры

&НаКлиенте
Процедура ОчиститьДетализациюПоСопоставленномуПодтверждению()
	
	ТаблицаНоменклатурыПодтвержденияЗаказовКлиентаСопоставленные.Очистить();
	ТаблицаНоменклатурыЗаказыКлиентаПредварительныеСопоставленные.Очистить();
	//ЭтаФорма.ДетализцаияПодтвержденияГрузополучатель = " <не указан> ";
	//ЭтаФорма.ДетализацияПодтвержденияАдресСтрокой = " <не указан> ";
	
КонецПроцедуры

#КонецОбласти


