﻿
#Область ОбработчикиСобытийФормы

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	УстановитьВидимостьИДоступностьЭлементовФормы();
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаВыбора(ВыбранноеЗначение, ИсточникВыбора)
	
	Элементы.ЗаказыКлиентаПредварительные.Обновить();
	Элементы.ЗаказыКлиентаЭДО.Обновить();
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ОбъединитьЗаказы(Команда)
	
	ФормаОбъединения = ПолучитьФорму("Обработка.актРабочееМестоКоординатора.Форма.ФормаОбъединения",,ЭтаФорма); 
	ФормаОбъединения.ЗаказКлиентаПредварительный = ЭтаФорма.ЗаказКлиентаПредварительный;
	
	Для Каждого СтрокаЗаказКЛиента ИЗ ЭтаФорма.СписокЗаказовКлиентаЭДО Цикл
		НоваяСтрока = ФормаОбъединения.ЗаказыКлиентаЭДО.Добавить();
		НоваяСтрока.ЗаказКлиента = СтрокаЗаказКЛиента.Значение;
	КонецЦикла;
	
	ФормаОбъединения.Открыть();
	
КонецПроцедуры

&НаКлиенте
Процедура ПометитьЗаказЭДОКакОбработанный(Команда)
	
	Если Элементы.ЗаказыКлиентаЭДО.ТекущаяСтрока = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	НаименованиеЗаказа = СокрЛП(Элементы.ЗаказыКлиентаЭДО.ТекущаяСтрока);
	НаименованиеЗаказа = НРЕГ(Лев(НаименованиеЗаказа,1))+Сред(НаименованиеЗаказа,2);
	
	
	Оповещение = Новый ОписаниеОповещения("ПометитьЗаказЭДООповещение",ЭтотОбъект,Элементы.ЗаказыКлиентаЭДО.ТекущаяСтрока);
	ПоказатьВопрос(Оповещение, "Пометить " + НаименованиеЗаказа + " как обработанный ЭДО?", 
		РежимДиалогаВопрос.ДаНет,0,КодВозвратаДиалога.Нет);

КонецПроцедуры


#КонецОбласти

#Область ОбработчикиСобытийТаблицФормы

&НаКлиенте
Процедура ЗаказыКлиентаЭДОПриАктивизацииСтроки(Элемент)
	
	Если Элемент = Неопределено Тогда
		ЭтаФорма.СписокЗаказовКлиентаЭДО = Неопределено;
		ЭтаФорма.ЗаказКлиентаДляДетализацииПредварительный = Неопределено;
		ОчиститьДетализациюПоЗаказу("ЭДО");
		Возврат;
	КонецЕсли;
	
	Если Элемент.ТекущаяСтрока = Неопределено Тогда
		ЭтаФорма.СписокЗаказовКлиентаЭДО = Неопределено;
		ЭтаФорма.ЗаказКлиентаДляДетализацииПредварительный = Неопределено;
		ОчиститьДетализациюПоЗаказу("ЭДО");
		Возврат;
	КонецЕсли;
	
	ЭтаФорма.СписокЗаказовКлиентаЭДО.ЗагрузитьЗначения(Элемент.ВыделенныеСтроки);
	
	Если Элемент.ТекущаяСтрока = ЭтаФорма.ЗаказКлиентаДляДетализацииЭДО Тогда
		Возврат;
	Иначе
		ЭтаФорма.ЗаказКлиентаДляДетализацииЭДО = Элемент.ТекущаяСтрока;
	КонецЕсли;
	
	ЗаполнитьДетализациюПоЗаказуСервер(ЭтаФорма.ЗаказКлиентаДляДетализацииЭДО,"ЭДО");
	УстановитьВидимостьИДоступностьЭлементовФормы();
	
КонецПроцедуры

&НаКлиенте
Процедура ЗаказыКлиентаПредварительныеПриАктивизацииСтроки(Элемент)
	
	Если Элемент = Неопределено Тогда
		ЭтаФорма.ЗаказКлиентаПредварительный = Неопределено;
		ОчиститьДетализациюПоЗаказу("Предварительные");
		Возврат;
	КонецЕсли;
	
	Если Элемент.ТекущаяСтрока = Неопределено Тогда
		ЭтаФорма.ЗаказКлиентаПредварительный = Неопределено;
		ОчиститьДетализациюПоЗаказу("Предварительные");
		Возврат;
	КонецЕсли;
	
	ЭтаФорма.ЗаказКлиентаПредварительный = Элемент.ТекущаяСтрока;
	
	Если Элемент.ТекущаяСтрока = ЭтаФорма.ЗаказКлиентаДляДетализацииПредварительный Тогда
		Возврат;
	Иначе
		ЭтаФорма.ЗаказКлиентаДляДетализацииПредварительный = Элемент.ТекущаяСтрока;
	КонецЕсли;
	
	ЗаполнитьДетализациюПоЗаказуСервер(ЭтаФорма.ЗаказКлиентаДляДетализацииПредварительный,"Предварительные");
	УстановитьВидимостьИДоступностьЭлементовФормы();	
	
КонецПроцедуры

#КонецОбласти

#Область ВспомогательныеПроцедурыИФункции

&НаСервере
Процедура УстановитьУсловноеОформлениеСервер(ПравоеЗначение,ПолеКомпановки)
	
	Для Каждого ЭлементУсловногоОформления ИЗ ЭтаФорма.УсловноеОформление.Элементы Цикл
		Для Каждого ЭлементОтбора ИЗ ЭлементУсловногоОформления.Отбор.Элементы Цикл
			
			Если ЭлементОтбора.ЛевоеЗначение = Новый ПолеКомпоновкиДанных(ПолеКомпановки) Тогда
				ЭлементОтбора.ПравоеЗначение = ПравоеЗначение;
			КонецЕсли;
			
		КонецЦикла;
	КонецЦикла;
	
КонецПроцедуры

&НаКлиенте
Процедура УстановитьВидимостьИДоступностьЭлементовФормы()
	
	ЭтаФорма.Элементы.ОбъединитьЗаказы.Доступность = ЭтаФорма.СписокЗаказовКлиентаЭДО.Количество() <> 0
	И ЗначениеЗаполнено(ЭтаФорма.ЗаказКлиентаПредварительный);
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьДетализациюПоЗаказуСервер(СсылкаНаЗаказ,ИмяЭлемента)
	
	ЭтаФорма["ТаблицаНоменклатурыЗаказыКлиента"+ИмяЭлемента].Очистить();
	
	Для Каждого СтрокаТоварЗаказ ИЗ  СсылкаНаЗаказ.Товары Цикл
		
		НоваяСтрока = ЭтаФорма["ТаблицаНоменклатурыЗаказыКлиента"+ИмяЭлемента].Добавить();
		ЗаполнитьЗначенияСвойств(НоваяСтрока,СтрокаТоварЗаказ);
		Если ЗначениеЗаполнено(СтрокаТоварЗаказ.Упаковка) Тогда
			НоваяСтрока.ЕдиницаИзмерения = СтрокаТоварЗаказ.Упаковка;
		Иначе
			НоваяСтрока.ЕдиницаИзмерения = СтрокаТоварЗаказ.Номенклатура.ЕдиницаИзмерения;
		КонецЕсли;
		
	КонецЦикла;
	
	ЭтаФорма["Детализация"+ИмяЭлемента+"АдресДоставки"] = ?(ЗначениеЗаполнено(СсылкаНаЗаказ.АдресДоставки),СсылкаНаЗаказ.АдресДоставки," <не указан> ");
	ЭтаФорма["Детализация"+ИмяЭлемента+"ДополнительнаяИнформация"] = ?(ЗначениеЗаполнено(СсылкаНаЗаказ.ДополнительнаяИнформация),СсылкаНаЗаказ.ДополнительнаяИнформация," <не заполнено> ");
	ЭтаФорма["Детализация"+ИмяЭлемента+"НомерПоДаннымКлиента"] = ?(ЗначениеЗаполнено(СсылкаНаЗаказ.НомерПоДаннымКлиента),СсылкаНаЗаказ.НомерПоДаннымКлиента," <не указан> ");
	Если ЗначениеЗаполнено(СсылкаНаЗаказ.ДатаПоДаннымКлиента) Тогда
		ЭтаФорма["Детализация"+ИмяЭлемента+"НомерПоДаннымКлиента"] = ЭтаФорма["Детализация"+ИмяЭлемента+"НомерПоДаннымКлиента"] + " от "+Формат(СсылкаНаЗаказ.ДатаПоДаннымКлиента,"ДФ=dd.MM.yyyy");	
	КонецЕсли;	
	
	Если ИмяЭлемента = "Предварительные" Тогда
		ЭтаФорма.ДетализацияПредварительныеЗаданиеНаПеревозку = НайтиЗаданиеНаПеревозкуСервер(СсылкаНаЗаказ);
		ЭтаФорма.ДетализацияПредварительныеЗаданиеНаПеревозкуТекст = "<отсутствует> ";
		Если ЗначениеЗаполнено(ЭтаФорма.ДетализацияПредварительныеЗаданиеНаПеревозку) Тогда
			Элементы.ДетализацияПредварительныеЗаданиеНаПеревозку.Видимость = Истина;
			Элементы.ДетализацияПредварительныеЗаданиеНаПеревозкуТекст.Видимость = Ложь;
		Иначе
			Элементы.ДетализацияПредварительныеЗаданиеНаПеревозку.Видимость = Ложь;
			Элементы.ДетализацияПредварительныеЗаданиеНаПеревозкуТекст.Видимость = Истина;
		КонецЕсли;
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ОчиститьДетализациюПоЗаказу(ИмяЭлемента)
	
	ЭтаФорма["ТаблицаНоменклатурыЗаказыКлиента"+ИмяЭлемента].Очистить();
	
	ЭтаФорма["Детализация"+ИмяЭлемента+"АдресДоставки"] = " <не указан> ";
	ЭтаФорма["Детализация"+ИмяЭлемента+"ДополнительнаяИнформация"] = " <не заполнено> ";
	ЭтаФорма["Детализация"+ИмяЭлемента+"НомерПоДаннымКлиента"] = " <не указан> ";
	
	Если ИмяЭлемента = "Предварительные" Тогда
		ЭтаФорма.Элементы.ДетализацияПредварительныеЗаданиеНаПеревозку.Видимость = Ложь;
		ЭтаФорма.ДетализацияПредварительныеЗаданиеНаПеревозкуТекст = "<отсутствует> ";
		ЭтаФорма.Элементы.ДетализацияПредварительныеЗаданиеНаПеревозкуТекст.Видимость = Истина;
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Функция НайтиЗаданиеНаПеревозкуСервер(СсылкаНаЗаказ)
	
	Запрос = Новый Запрос(
	"ВЫБРАТЬ ПЕРВЫЕ 1
	|	ЗаданиеНаПеревозкуРаспоряжения.Ссылка КАК Ссылка
	|ИЗ
	|	Документ.ЗаданиеНаПеревозку.Распоряжения КАК ЗаданиеНаПеревозкуРаспоряжения
	|ГДЕ
	|	ЗаданиеНаПеревозкуРаспоряжения.Распоряжение = &СсылкаНаЗаказ
	|	И ЗаданиеНаПеревозкуРаспоряжения.Ссылка.ПометкаУдаления = ЛОЖЬ"
	);
	Запрос.УстановитьПараметр("СсылкаНаЗаказ",СсылкаНаЗаказ);
	Выборка = Запрос.Выполнить().Выбрать();
	
	Если Выборка.Следующий() Тогда
		Возврат Выборка.Ссылка;
	Иначе
		Документы.ЗаданиеНаПеревозку.ПустаяСсылка();
	КонецЕсли;	
	
КонецФункции

&НаКлиенте
Процедура ПометитьЗаказЭДООповещение(Результат, Параметры) Экспорт
	
	Если Результат = КодВозвратаДиалога.Да Тогда
		ПометитьЗаказЭДООповещениеСервер(Параметры);
		Элементы.ЗаказыКлиентаЭДО.Обновить();
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ПометитьЗаказЭДООповещениеСервер(СсылкаНаЗаказ)
	
	ДокОбъект = СсылкаНаЗаказ.ПолучитьОбъект();
	
	СтруктураОтбора = Новый Структура();
	СтруктураОтбора.Вставить("Свойство",ПланыВидовХарактеристик.ДополнительныеРеквизитыИСведения.актСтатусЗаказаЭДО);
	НайденныеСтроки = ДокОбъект.ДополнительныеРеквизиты.НайтиСтроки(СтруктураОтбора);
	Если НайденныеСтроки.Количество() > 0 Тогда
		НайденныеСтроки[0].Значение = Перечисления.актСтатусыЗаказаЭДО.ОбработанныйЭДО;
	Иначе
		НоваяСтрока = ДокОбъект.ДополнительныеРеквизиты.Добавить();
		НоваяСтрока.Свойство = ПланыВидовХарактеристик.ДополнительныеРеквизитыИСведения.актОбъединенСПредварительнымЗаказом;
		НоваяСтрока.Значение = Перечисления.актСтатусыЗаказаЭДО.ОбработанныйЭДО;
	КонецЕсли;
	
	Если ДокОбъект.Проведен Тогда
		ДокОбъект.Записать(РежимЗаписиДокумента.Проведение);
	Иначе
		ДокОбъект.Записать();
	КонецЕсли;
	//ДокОбъект.ОбменДанными.Загрузка = Истина;
	ДокОбъект.Записать();
	
КонецПроцедуры


#КонецОбласти


