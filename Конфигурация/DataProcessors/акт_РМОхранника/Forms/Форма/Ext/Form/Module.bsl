﻿
&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	Период.Вариант = ВариантСтандартногоПериода.Сегодня;
	ПериодПриИзмененииНаСервере();
КонецПроцедуры

&НаСервере
Процедура ПериодПриИзмененииНаСервере()
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
		"ВЫБРАТЬ
		|	ЗаданиеНаПеревозку.Ссылка КАК ЗаданиеНаПеревозку,
		|	ЗаданиеНаПеревозку.ТранспортноеСредство КАК ТранспортноеСредство,
		|	ЗаданиеНаПеревозку.Водитель КАК Водитель,
		|	ЗаданиеНаПеревозку.ДатаВремяРейсаПланС КАК ПланПрибытия,
		|	ЗаданиеНаПеревозку.ДатаВремяРейсаПланПо КАК ПланУбытия,
		|	ЗаданиеНаПеревозку.ДатаВремяРейсаФактС КАК ФактПрибытия,
		|	ЗаданиеНаПеревозку.ДатаВремяРейсаФактПо КАК ФактУбытия,
		|	ЗаданиеНаПеревозку.Номер КАК Номер,
		|	ЗаданиеНаПеревозку.Дата КАК Дата
		|ПОМЕСТИТЬ ТабЗадание
		|ИЗ
		|	Документ.ЗаданиеНаПеревозку КАК ЗаданиеНаПеревозку
		|ГДЕ
		|	ЗаданиеНаПеревозку.ДатаВремяРейсаПланС МЕЖДУ &ПериодНачала И &ПериодОкончания
		|	И ЗаданиеНаПеревозку.ПометкаУдаления = ЛОЖЬ
		|	И ЗаданиеНаПеревозку.Проведен = ИСТИНА
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ РАЗЛИЧНЫЕ
		|	ТабЗадание.ЗаданиеНаПеревозку КАК ЗаданиеНаПеревозку,
		|	ТабЗадание.ТранспортноеСредство КАК ТранспортноеСредство,
		|	ТабЗадание.Водитель КАК Водитель,
		|	ТабЗадание.ПланПрибытия КАК ПланПрибытия,
		|	ТабЗадание.ПланУбытия КАК ПланУбытия,
		|	ТабЗадание.ФактПрибытия КАК ФактПрибытия2,
		|	ТабЗадание.ФактУбытия КАК ФактУбытия2,
		|	ТабЗадание.Номер КАК Номер,
		|	ТабЗадание.Дата КАК Дата,
		|	актДвижениеТранспорта.Ссылка КАК ДвижениеАвто,
		|	актДвижениеТранспорта.ДатаВъезда КАК ФактПрибытия,
		|	актДвижениеТранспорта.ДатаВыезда КАК ФактУбытия
		|ИЗ
		|	ТабЗадание КАК ТабЗадание
		|		ЛЕВОЕ СОЕДИНЕНИЕ Документ.актДвижениеТранспорта КАК актДвижениеТранспорта
		|		ПО ТабЗадание.ЗаданиеНаПеревозку = актДвижениеТранспорта.ДокументОснование";
	
	Запрос.УстановитьПараметр("ПериодНачала", Период.ДатаНачала);
	Запрос.УстановитьПараметр("ПериодОкончания", Период.ДатаОкончания);
	
	РезультатЗапроса = Запрос.Выполнить();
	ТаблицаКонтроля.Очистить();
	
	
	ВыборкаДетальныеЗаписи = РезультатЗапроса.Выбрать();
	
	Пока ВыборкаДетальныеЗаписи.Следующий() Цикл
		// Вставить обработку выборки ВыборкаДетальныеЗаписи
		НовКонтроль = ТаблицаКонтроля.Добавить();
		ЗаполнитьЗначенияСвойств(НовКонтроль,ВыборкаДетальныеЗаписи);
		НовКонтроль.Номер = НовКонтроль.Номер + " от " + Формат(новКонтроль.Дата, "ДФ=dd.MM.yy"); 
		НовКонтроль.Автомобиль = СокрЛП(Новконтроль.ТранспортноеСредство.Марка) + "/ " + ФорматНомера(Новконтроль.ТранспортноеСредство.Код);
		НовКонтроль.Прицеп = СокрЛП(Новконтроль.ТранспортноеСредство.Прицеп) + " " + СокрЛП(Новконтроль.ТранспортноеСредство.ГосударственныйНомерПрицепа);
		НовКонтроль.статус = 1;
		
		Если ЗначениеЗаполнено(НовКонтроль.ФактПрибытия) тогда
			НовКонтроль.статус = 2;
		КонецЕсли;
		
		Если ЗначениеЗаполнено(НовКонтроль.ФактУбытия) тогда
			НовКонтроль.статус = 3;
		КонецЕсли;
		
		Если Не ЗначениеЗаполнено(ВыборкаДетальныеЗаписи.ДвижениеАвто) тогда
			НовКонтроль.ДвижениеАвто = СформироватьДвижение(ВыборкаДетальныеЗаписи.ЗаданиеНаПеревозку);
		КонецЕсли;
	КонецЦикла;
	
	
	ТаблицаКонтроля.Сортировать("Дата");
	КоличествоБортов = "Всего автотранспорта: " +  ТаблицаКонтроля.Количество() + " ед.";
КонецПроцедуры


&НаСервере
Функция ФорматНомера(Номер)
	НовНомер = СокрЛП(Номер);
	Если СтрДлина(НовНомер) > 7 тогда
		НовНомер = Лев(НовНомер,1) + " " + Сред(НовНомер,2,3) + " " + Сред(НовНомер,5,2) + " " + Сред(НовНомер,7);
	КонецЕсли;
	
	Возврат новНомер;
КонецФункции

&НаСервере
Функция ФорматНомераПрицепа(Номер)
	НовНомер = СокрЛП(Номер);
	Если СтрДлина(НовНомер) > 7 тогда
		НовНомер = Лев(НовНомер,1) + " " + Сред(НовНомер,2,3) + " " + Сред(НовНомер,5,2) + " " + Сред(НовНомер,7);
	КонецЕсли;
	
	Возврат новНомер;
КонецФункции

&НаКлиенте
Процедура ПериодПриИзменении(Элемент)
	ПериодПриИзмененииНаСервере();
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Функция ВремяПоУмолчанию(ИсхДата)
	
	Возврат НачалоДня(ИсхДата) + 32400 // 9 часов;
	
КонецФункции

&НаКлиентеНаСервереБезКонтекста
Функция ФорматДаты()
	
	Возврат "ДФ=dd.MM.yy";
	
КонецФункции

&наСервере
Процедура ЗафиксироватьВремяВъезда(НомерСтроки)
	СтрокаЗадания = ТаблицаКонтроля.Получить(НомерСтроки);
	ДокДвижение = СтрокаЗадания.ДвижениеАвто.Получитьобъект();
	ДокДвижение.ДатаВъезда = СтрокаЗадания.ФактПрибытия;
	ДокДвижение.записать();
КонецПроцедуры

&НаКлиенте
Процедура ОтметкаВъезда(Команда)
	
	 ТекСтрока = Элементы.ТаблицаКонтроля.ТекущиеДанные;
	 
	 Если ТекСтрока = Неопределено тогда
		 СообщитьПредупреждение("Не выбрана строка! Установить отметку въезда невозможно!");
		 Возврат;
	 КонецЕсли;
	 
	 Если ЗначениеЗаполнено(текСтрока.ФактПрибытия) тогда
		 СообщитьПредупреждение("По данному автомобилю уже установлена отметка въезда!");
		 Возврат;
	 КонецЕсли;
	 Если ЗначениеЗаполнено(текСтрока.ФактУбытия) тогда
		 СообщитьПредупреждение("По данному автомобилю уже установлена отметка выезда!");
		 Возврат;
	 КонецЕсли;
	 Попытка 
		 ТекСтрока.ФактПрибытия = ТекущаяДата();
		 ТекСтрока.Статус = 2;
		 ЗафиксироватьВремяВъезда(ТаблицаКонтроля.Индекс(Элементы.ТаблицаКонтроля.ТекущиеДанные));
		 СообщитьУспех("Успешно отмечен въезд автомобиля!");
	 Исключение
		 СообщитьПредупреждение("ОШИБКА! Установить отметку въезда невозможно!");
	 КонецПопытки;
	ЭтаФорма.ОбновитьОтображениеДанных();
КонецПроцедуры

&наСервере
Процедура ЗафиксироватьВремяУбытия(НомерСтроки)
	
	//фиксируем время выезда
	СтрокаЗадания = ТаблицаКонтроля.Получить(НомерСтроки);
	ДокДвижение = СтрокаЗадания.ДвижениеАвто.Получитьобъект();
	ДокДвижение.ДатаВыезда = СтрокаЗадания.ФактУбытия;
	ДокДвижение.записать();
	//Фиксируем начало рейса
	ДокЗадание = СтрокаЗадания.ЗаданиеНаПеревозку.Получитьобъект();
	ДокЗадание.Статус = Перечисления.СтатусыЗаданийНаПеревозку.Отправлено;
	ДокЗадание.ДатаВремяРейсаФактС = СтрокаЗадания.ФактУбытия;
	ДокЗадание.Записать();
	
КонецПроцедуры

&НаКлиенте
Процедура ОтметкаВыезда(Команда)
	 ТекСтрока = Элементы.ТаблицаКонтроля.ТекущиеДанные;
	 
	 Если ТекСтрока = Неопределено тогда
		 СообщитьПредупреждение("Не выбрана строка! Установить отметку въезда невозможно!");
		 Возврат;
	 КонецЕсли;
	 
	 Если НЕ ЗначениеЗаполнено(текСтрока.ФактПрибытия) тогда
		 СообщитьПредупреждение("Автомобиль еще не въехал! Установить отметку выезда невозможно!");
		 Возврат;
	 КонецЕсли;
	 Если ЗначениеЗаполнено(текСтрока.ФактУбытия) тогда
		 СообщитьПредупреждение("По данному автомобилю уже установлена отметка выезда!");
		 Возврат;
	 КонецЕсли;
	 Попытка 
		 ТекСтрока = Элементы.ТаблицаКонтроля.ТекущиеДанные;
		 ТекСтрока.ФактУбытия = ТекущаяДата();
		 ТекСтрока.Статус = 3;
		 ЗафиксироватьВремяУбытия(ТаблицаКонтроля.Индекс(Элементы.ТаблицаКонтроля.ТекущиеДанные));
		 СообщитьУспех("Успешно отмечен выезд автомобиля!");
	 Исключение
		 СообщитьПредупреждение("ОШИБКА! Установить отметку въезда невозможно!");
	 КонецПопытки;
	ЭтаФорма.ОбновитьОтображениеДанных();
 КонецПроцедуры
 

#Область СтатусныеСообщения

&НаКлиенте
Процедура ПоследниеСообщения(Команда)
	
	ПоказатьЗначение(, СтекСообщений);
	
КонецПроцедуры

&НаСервере
Процедура СообщитьПрогресс(Текст = "")
	
	ПоказатьИнфо = ПустаяСтрока(Текст);
	
	Элементы.СтраницыСостояние.ТекущаяСтраница = ?(ПоказатьИнфо,
		Элементы.СостояниеПустаяСтраница, Элементы.СостояниеОжиданиеСтраница);
		
	ТекстСообщенияСтатуса = Текст;
	
	ЗаписатьСообщение(ЭтотОбъект, Текст, Элементы.ОжиданиеКартинка.Картинка);
	
КонецПроцедуры

&НаСервере
Процедура СообщитьУспех(Текст)
	
	Элементы.СтраницыСостояние.ТекущаяСтраница = Элементы.СостояниеУспехСтраница;
	
	ТекстСообщенияСтатуса = Текст;
	
	ЗаписатьСообщение(ЭтотОбъект, Текст, Элементы.УспехКартинка.Картинка);
	
КонецПроцедуры

&НаСервере
Процедура СообщитьПредупреждение(Текст)
	
	Элементы.СтраницыСостояние.ТекущаяСтраница = Элементы.СостояниеПредупреждениеСтраница;
	
	ТекстСообщенияСтатуса = Текст;
	
	ЗаписатьСообщение(ЭтотОбъект, Текст, Элементы.ОшибкаКартинка.Картинка);
	
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Процедура ЗаписатьСообщение(Форма, Текст, Картинка)
	
	Форма.СтекСообщений.Вставить(0, СтрШаблон("%1 %2", Формат(ТекущаяДата(), "ДЛФ=T"), Текст),,, Картинка);
	Для Сч = 10 По Форма.СтекСообщений.Количество() - 1 Цикл
		Форма.СтекСообщений.Удалить(Сч);
	КонецЦикла;
	
КонецПроцедуры


#КонецОбласти

#область Сортировка

&НаСервере
Функция количествоПоФильтру(Номер)
	НовТаб = ТаблицаКонтроля.Выгрузить();
	Ном = 0;
	Для каждого Стр Из НовТаб цикл
		Если стр.Статус = Номер тогда
			Ном = Ном + 1;
		КонецЕсли;
	КонецЦикла;
	Возврат Ном;
КонецФункции

&НаКлиенте
Процедура СформироватьОтбор(Флаг = 10)
	
	Если Флаг < 10 тогда
		СтруктураОтбора = Новый ФиксированнаяСтруктура("Статус",Флаг);
		Элементы.ТаблицаКонтроля.ОтборСтрок = СтруктураОтбора;//Новый ФиксированнаяСтруктура("Бригада",Элементы.ТабБригады.ТекущиеДанные.Бригада);
		Если Флаг = 1 тогда
			КоличествоБортов = "Всего ожидается: "
		ИначеЕсли Флаг = 2 тогда
			КоличествоБортов = "Всего на территории: "
		Иначе
			КоличествоБортов = "Всего выехало: "
		КонецеСли;
		
		КоличествоБортов = КоличествоБортов +  КоличествопоФильтру(Флаг) + " ед.";
	Иначе
		Элементы.ТаблицаКонтроля.ОтборСтрок = Неопределено;
		КоличествоБортов = "Всего автотранспорта: " +  ТаблицаКонтроля.Количество() + " ед.";
	КонецЕсли;
	
	ЭтаФорма.ОбновитьОтображениеДанных();
	
КонецПроцедуры

&НаКлиенте
Процедура ВсеЗадания(Команда)
	СформироватьОтбор(10);
КонецПроцедуры

&НаКлиенте
Процедура ОжидаютПрибытия(Команда)
	СформироватьОтбор(1);
КонецПроцедуры

&НаКлиенте
Процедура НаПогрузке(Команда)
	СформироватьОтбор(2);
КонецПроцедуры

&НаКлиенте
Процедура Выехавшие(Команда)
	СформироватьОтбор(3);
КонецПроцедуры

#КонецОбласти

&НаСервере
Функция СформироватьДвижение(ДокЗадание)
	НовДок = документы.актДвижениеТранспорта.СоздатьДокумент();
	НовДок.ДокументОснование = ДокЗадание;
	НовДок.Дата = докЗадание.дата;
	НовДок.Записать();
	Возврат НовДок.Ссылка;
КонецФункции


&НаСервере
Функция ПечатьСервер()
	ТабДокумент = Новый ТабличныйДокумент;

	ПечатнаяФормаОбъект = РеквизитФормыВЗначение("Объект");
	Макет = ПечатнаяФормаОбъект.ПолучитьМакет("МакетОтчетОсновной");
	
	//Область Шапка
	ОбластьШапка = Макет.ПолучитьОбласть("Шапка");
	ТабДокумент.Вывести(ОбластьШапка);
	ОбластьМакета = Макет.ПолучитьОбласть("Строка");
	
	Для каждого Стр Из ТаблицаКонтроля цикл
		ОбластьМакета.Параметры.ЗаданиеДатаНомер = Стр.Номер;
		ОбластьМакета.Параметры.Перевозчик = Стр.КомпанияПеревозчик;
		ОбластьМакета.Параметры.МаркаТС = СокрЛП(Стр.ТранспортноеСредство.Марка);
		ОбластьМакета.Параметры.ГосномерТС = ФорматНомера(Стр.ТранспортноеСредство.Код);
		ОбластьМакета.Параметры.ГосНомерПрицепа = СокрЛП(Стр.ТранспортноеСредство.ГосударственныйНомерПрицепа);
		ОбластьМакета.Параметры.ФИО = СокрЛП(Стр.Водитель);
		ОбластьМакета.Параметры.ПланДата = Формат(Стр.ПланПрибытия, "ДФ=dd.MM.yy");
		ОбластьМакета.Параметры.ПланВремя = Формат(Стр.ПланПрибытия, "ДЛФ=T");
		ОбластьМакета.Параметры.ФактДата = Формат(Стр.ФактПрибытия, "ДФ=dd.MM.yy");
		ОбластьМакета.Параметры.ФактВремя = Формат(Стр.ФактПрибытия, "ДЛФ=T");
		ОбластьМакета.Параметры.ПланУДата = Формат(Стр.ПланУбытия, "ДФ=dd.MM.yy");
		ОбластьМакета.Параметры.ПланУВремя = Формат(Стр.ПланУбытия, "ДЛФ=T");
		ОбластьМакета.Параметры.ФактУДата = Формат(Стр.ФактУбытия, "ДФ=dd.MM.yy");
		ОбластьМакета.Параметры.ФактУВремя = Формат(Стр.ФактУбытия, "ДЛФ=T");
		ТабДокумент.Вывести(ОбластьМакета);
	КонецЦикла;
	
	ТабДокумент.ПолеСверху = 1;
	ТабДокумент.ПолеСнизу = 0;
	ТабДокумент.ПолеСлева = 2;
	ТабДокумент.ПолеСправа = 2;
	ТабДокумент.ОриентацияСтраницы = ОриентацияСтраницы.Ландшафт;
	
	Возврат ТабДокумент;
	
КонецФункции

&НаКлиенте
Процедура Печать(Команда)
	
	ТабДок = Новый ТабличныйДокумент;
	ТабДок = ПечатьСервер();
	
	//ТабДок = ПечатьСервер();
	//ТабДокумент.Показать();
	ТабДок.АвтоМасштаб = Истина;
	    
    //Типовая часть вывода в стандартное окно++
    ИдентификаторПечатнойФормы = "ПФ_MXL_ДвижениеАвтотранспорта";
    НазваниеПечатнойФормы = НСтр("ru = 'ДвижениеАвтотранспорта'");
    
    Если Не ОбщегоНазначенияКлиент.ПодсистемаСуществует("СтандартныеПодсистемы.Печать") Тогда
        ТабДок.Показать(НазваниеПечатнойФормы);
        ДокументыПечатались = Истина;
        Возврат;
    КонецЕсли;     
    МодульУправлениеПечатьюКлиент = ОбщегоНазначенияКлиент.ОбщийМодуль("УправлениеПечатьюКлиент");     
    КоллекцияПечатныхФорм = МодульУправлениеПечатьюКлиент.НоваяКоллекцияПечатныхФорм(ИдентификаторПечатнойФормы);
	
	ПечатнаяФорма = МодульУправлениеПечатьюКлиент.ОписаниеПечатнойФормы(КоллекцияПечатныхФорм, 
		ИдентификаторПечатнойФормы);
		
    ПечатнаяФорма.СинонимМакета = НазваниеПечатнойФормы;
    ПечатнаяФорма.ТабличныйДокумент = ТабДок;
    ПечатнаяФорма.ИмяФайлаПечатнойФормы = НазваниеПечатнойФормы;
    ПечатнаяФорма.Экземпляров = 1;
	
    ОбластиОбъектов = Новый СписокЗначений;
    МодульУправлениеПечатьюКлиент.ПечатьДокументов(КоллекцияПечатныхФорм, ОбластиОбъектов);
КонецПроцедуры


