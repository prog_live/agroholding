﻿
&НаСервере
Процедура ИнициализироватьПериод()
	Пользователь = ПараметрыСеанса.ТекущийПользователь;
	юзер = ПользователиИнформационнойБазы.НайтиПоУникальномуИдентификатору(Пользователь.ИдентификаторПользователяИБ);
	стр = ХранилищеОбщихНастроек.Загрузить("ЭкранЗаказов", "НастройкиОтчета5",, юзер.Имя);
	Если стр <> Неопределено Тогда
		стр.НачалоПериода = НачалоДня(ТекущаяДата());
		стр.КонецПериода = КонецДня(ТекущаяДата());
		ХранилищеОбщихНастроек.Сохранить("ЭкранЗаказов", "НастройкиОтчета5", стр, , юзер.Имя);
	КонецЕсли; 
	
	ДатаИзмененияПериода = ТекущаяДата();
КонецПроцедуры

&НаСервере
Процедура СформироватьНаСервере(ТД)
	ЗагрузитьНастройкиОтчета();
	
	Если ДатаИзмененияПериода < НачалоДня(ТекущаяДата()) и КонецПериода < НачалоДня(ТекущаяДата()) Тогда
		ИнициализироватьПериод();	
	КонецЕсли; 
	
	лог = "";  
	СекНачало = ТекущаяДата();
	СекНачало = СекНачало - НачалоДня(СекНачало);
	 
	ТД.Очистить();
	Обработка = РеквизитФормыВЗначение("Объект");
 	Макет = Обработка.ПолучитьМакет("ФиксированныйМакет2");
	Обл = Макет.ПолучитьОбласть("Область1");
	ДанныеПечати = Новый Структура;
	
	для нн = 1 по 4 Цикл
		Если Объект.ТЧ.Количество() = 0 Тогда
			рез = ПолучитьСписокЗаказов(); 
			Если нн>1 Тогда
				прервать;
			КонецЕсли; 
		КонецЕсли;
		Если Объект.ТЧ.Количество() = 0 Тогда
			лог = "@2заказы не найдены";
			прервать;	
		КонецЕсли;
		
		Заказ = Объект.ТЧ[0].Заказ;
		выб = Объект.ТЧ[0];
		
		ДанныеПечати.Вставить("Заказ" + нн, ?(выб.Подтвержден, "@1", "") + Строка(выб.РЦ) + " - " + Заказ.Номер + ?(выб.Подтвержден, " - [подтвержден]", ""));
		ДанныеПечати.Вставить("Дт" + нн, "[Дт"+нн+"]" + Формат(выб.ДатаОтгрузки,"ДФ=dd/MM") + ?(ЗначениеЗаполнено(выб.ВремяОтгрузки) и выб.ВремяОтгрузки<>дата(1,1,1), "  " + Формат(выб.ВремяОтгрузки,"ДФ=HH:mm"), " 20:00"));
		коммент = СтрЗаменить(Заказ.Комментарий, Символы.ПС, " ");
		коммент = СтрЗаменить(коммент, Символы.ВК, " ");
		ДанныеПечати.Вставить("комм" + нн, коммент); //"коммент "+строка(нн)
		
		товары = ПолучитьСписокТоваров(Заказ);
		для каждого стр из товары Цикл
			пп = Строка(стр.ПорядковыйНомер);
			ТекВремяОтгрузки = ?(ЗначениеЗаполнено(выб.ВремяОтгрузки) и выб.ВремяОтгрузки<>дата(1,1,1), выб.ВремяОтгрузки, дата(1,1,1,20,0,0));
			Если (СекНачало > (ТекВремяОтгрузки - НачалоДня(ТекВремяОтгрузки))) или НачалоДня(выб.ДатаОтгрузки) < НачалоДня(ТекущаяДата()) Тогда
				префикс = ?(стр.Статус=3, "@3", "@2");
			иначе	
				префикс = ?(стр.Статус=3, "@3", "");
			КонецЕсли; 
			
			Если префикс = "@2" и Лев(ДанныеПечати["Дт" + нн],1)<>"@" Тогда
				ДанныеПечати["Дт" + нн] = префикс + ДанныеПечати["Дт" + нн];	
			КонецЕсли;	
			//ДанныеПечати.Вставить("Ш" + нн + "_" + пп, стр.Заголовок);
			ДанныеПечати.Вставить("К" + нн + "_" + пп, префикс + Формат(?(флПересчитыватьВУпаковки,стр.Количество,стр.Количество/стр.Квант), "ЧГ=0"));
			ДанныеПечати.Вставить("кв" + нн + "_" + пп, префикс + стр.Квант);
		КонецЦикла;
		
		Объект.ТЧ.Удалить(0);
		
	КонецЦикла;	
	
	ДанныеПечати.Вставить("лог", лог);
	Обл.Параметры.Заполнить(ДанныеПечати);
	ТД.Вывести(Обл);
	Разукрасить(ТД);
	
КонецПроцедуры

&НаСервере
Процедура СтеретьГраницы(обл)
	обл.ГраницаСлева = Новый Линия(ТипЛинииЯчейкиТабличногоДокумента.НетЛинии);
	обл.ГраницаСправа = Новый Линия(ТипЛинииЯчейкиТабличногоДокумента.НетЛинии);
	обл.ГраницаСверху = Новый Линия(ТипЛинииЯчейкиТабличногоДокумента.НетЛинии);
	обл.ГраницаСнизу = Новый Линия(ТипЛинииЯчейкиТабличногоДокумента.НетЛинии);

КонецПроцедуры	

&НаСервере
Процедура Разукрасить(ТД)
	ШапкаТаблицыВыборка = ПолучитьШапку();
	
	об = ТД.НайтиТекст("@3");
	пока об<>Неопределено цикл
		об.ЦветТекста = WebЦвета.Зеленый;
		об.Текст = Сред(об.Текст,3);
		об = ТД.НайтиТекст("@3", об);
	КонецЦикла;	
	
	об = ТД.НайтиТекст("@1");
	пока об<>Неопределено цикл
		об.ЦветТекста = Новый Цвет(51,0,204);
		об.Текст = Сред(об.Текст,3);
		об = ТД.НайтиТекст("@1", об);
	КонецЦикла;	
	
	об = ТД.НайтиТекст("@2");
	пока об<>Неопределено цикл
		об.ЦветТекста = WebЦвета.Красный;
		об.Текст = Сред(об.Текст,3);
		об = ТД.НайтиТекст("@2", об);
	КонецЦикла;	
	
	для нн = 1 по 4 Цикл
		об = ТД.НайтиТекст("Ш"+нн+"_1");
		Если об = Неопределено Тогда
			Продолжить;
		КонецЕсли; 
		лево = об.Лево;
		верх = об.Верх;
		
		комм = ТД.НайтиТекст("комм"+нн);
		Если комм <> Неопределено Тогда
			комм.Шрифт = Новый Шрифт("Arial",РазмерШрифта_Коммент,Ложь);
		КонецЕсли; 
		
		Для каждого стр Из ШапкаТаблицыВыборка Цикл
			об = ТД.Область("R"+верх+"C"+лево);  
			об.Текст = стр.Заголовок;
			об.ШиринаКолонки = ШиринаКолонки;
			об.Шрифт = Новый Шрифт("Arial",РазмерШрифта_Шапка,Ложь);
			Если ПустаяСтрока(стр.ЦветФона) Тогда
				//Цв = Новый Цвет;
			Иначе
				об.ЦветФона = СериализаторXDTO.XMLЗначение(Тип("Цвет"), стр.ЦветФона);
			КонецЕсли;
			
			об = ТД.Область("R"+Строка(верх+1)+"C"+лево);
			об.Шрифт = Новый Шрифт("Arial",РазмерШрифта_Количество,Ложь);
			об = ТД.Область("R"+Строка(верх+2)+"C"+лево);
			об.Шрифт = Новый Шрифт("Arial",РазмерШрифта_Квант,Ложь);
			об = ТД.Область("R"+Строка(верх-1)+"C"+лево);
			об.Шрифт = Новый Шрифт("Arial",РазмерШрифта_Заказ,Ложь);
			
			лево = лево + 1;
			КолСтолбцов = стр.ПорядковыйНомер;
		КонецЦикла; 
		
		для уд = КолСтолбцов+1 по 25 Цикл
			//об = ТД.НайтиТекст("К"+нн+"_"+уд);
			об = ТД.Область("R"+Строка(верх)+"C"+лево);
			СтеретьГраницы(об);
			об = ТД.Область("R"+Строка(верх+1)+"C"+лево);  
			СтеретьГраницы(об);
			об = ТД.Область("R"+Строка(верх+2)+"C"+лево);  
			СтеретьГраницы(об);
			
			лево = лево + 1;
		КонецЦикла;
		
		ОбластьИсточник = ТД.НайтиТекст("[Дт"+нн+"]");
		Если ОбластьИсточник<>Неопределено Тогда
			лево = ОбластьИсточник.Лево;
			верх = ОбластьИсточник.Верх;
			ОбластьПриемник = ТД.Область("R"+Строка(верх)+"C"+Строка(лево+КолСтолбцов-10));
			ТД.ВставитьОбласть(ОбластьИсточник, ОбластьПриемник);
			ТД.УдалитьОбласть(ОбластьИсточник);
			ОбластьПриемник.Текст = Сред(ОбластьПриемник.Текст,6);
		КонецЕсли; 
		
	КонецЦикла;	
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	Если ПолучитьПользователя() = "Экран текущих заказов" Тогда
		Элементы.ГруппаНастройки.Видимость = Ложь;
	КонецЕсли; 
	Сформировать();
	Элементы.ИнтервалОбновления.Заголовок = "Интервал обновления "+ИнтервалОбновления+" сек";
КонецПроцедуры

&НаСервере
Функция ПолучитьПользователя()
	возврат Строка(ПараметрыСеанса.ТекущийПользователь);
КонецФункции

&НаКлиенте
Процедура Сформировать()
	СформироватьНаСервере(Объект.ТД);
	ПостОбработка();
	ПодключитьОбработчикОжидания("Сформировать", ИнтервалОбновления, Истина);
КонецПроцедуры

&НаКлиенте
Процедура ПостОбработка()
	ЗаписатьЛогВФайл();
	ОбновитьПолзунок();
КонецПроцедуры

&НаКлиенте
Процедура ЗаписатьЛогВФайл()
	ЛогФайл = "C:\temp\OrdersDashBoard.txt";
	ф = Новый Файл(ЛогФайл);
	Если ф.Существует() Тогда
		лф = Новый ТекстовыйДокумент;
		лф.Прочитать(ЛогФайл);
		Если лф.КоличествоСтрок() = 0 Тогда
			лф.ДобавитьСтроку(ТекущаяДата());
		иначе	
			лф.ЗаменитьСтроку(1, ТекущаяДата());
		КонецЕсли;	
		лф.Записать(ЛогФайл);
		лф = Неопределено;
	КонецЕсли; 
КонецПроцедуры
	
&НаСервере
Функция ПолучитьШапку()
	
	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ
	|	Т.Наименование КАК Наименование,
	|	Т.Заголовок КАК Заголовок,
	|	Т.ЦветФонаШапки КАК ЦветФона,
	|	Т.ПорядковыйНомер КАК ПорядковыйНомер
	|ИЗ
	|	Справочник.актСвойстваГруппНоменклатурыДляДоскиСортировки КАК Т
	|ГДЕ
	|	НЕ Т.ПометкаУдаления
	|
	|УПОРЯДОЧИТЬ ПО
	|	Т.ПорядковыйНомер";
	
	Результат = Запрос.Выполнить().Выгрузить();
	возврат Результат;
КонецФункции

&НаСервере
Функция ПолучитьСписокТоваров(заказ)
	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ
	|	актЗарегистрированныеПаллеты.Партия КАК Паллета,
	|	ВЫБОР
	|		КОГДА актЗарегистрированныеПаллеты.КоличествоУпаковок = 0
	|			ТОГДА ВЫБОР
	|					КОГДА актЗарегистрированныеПаллеты.Коробка.Знаменатель = 0
	|						ТОГДА 0
	|					ИНАЧЕ актЗарегистрированныеПаллеты.Коробка.Числитель / актЗарегистрированныеПаллеты.Коробка.Знаменатель
	|				КОНЕЦ * актЗарегистрированныеПаллеты.КоличествоКоробок
	|		ИНАЧЕ актЗарегистрированныеПаллеты.КоличествоУпаковок
	|	КОНЕЦ КАК ВЗаказеКоличествоШтук,
	|	актЗарегистрированныеПаллеты.Номенклатура КАК Номенклатура,
	|	ВЫБОР
	|		КОГДА актЗарегистрированныеПаллеты.Упаковка = ЗНАЧЕНИЕ(Справочник.УпаковкиЕдиницыИзмерения.ПустаяСсылка)
	|			ТОГДА актЗарегистрированныеПаллеты.Коробка.ВесЕдиницаИзмерения
	|		ИНАЧЕ актЗарегистрированныеПаллеты.Упаковка
	|	КОНЕЦ КАК Упаковка,
	|	актЗарегистрированныеПаллеты.ДатаОтгрузки КАК ДатаОтгрузки,
	|	ВЫБОР
	|		КОГДА актЗарегистрированныеПаллеты.Упаковка = ЗНАЧЕНИЕ(Справочник.УпаковкиЕдиницыИзмерения.ПустаяСсылка)
	|			ТОГДА ВЫБОР
	|					КОГДА актЗарегистрированныеПаллеты.Коробка.Знаменатель = 0
	|						ТОГДА 0
	|					ИНАЧЕ актЗарегистрированныеПаллеты.Коробка.Числитель / актЗарегистрированныеПаллеты.Коробка.Знаменатель
	|				КОНЕЦ
	|		ИНАЧЕ актЗарегистрированныеПаллеты.Коробка.КоличествоУпаковок
	|	КОНЕЦ КАК КоличествоВложений,
	|	ВЫБОР
	|		КОГДА актЗарегистрированныеПаллеты.Регистратор.Грузополучатель = ЗНАЧЕНИЕ(Справочник.Контрагенты.ПустаяСсылка)
	|			ТОГДА ""<РЦ не указан>""
	|		ИНАЧЕ актЗарегистрированныеПаллеты.Регистратор.Грузополучатель
	|	КОНЕЦ КАК РЦ,
	|	актЗарегистрированныеПаллеты.Партия.ПланируемоеВремяОтгрузки КАК ПланируемоеВремяОтгрузки,
	|	актЗарегистрированныеПаллеты.Партия.СостояниеПаллеты КАК Состояние
	|   ,актЗарегистрированныеПаллеты.Партия.НоменклатураПоставщика.Упаковка как УпаковкаПоставщика
	|ПОМЕСТИТЬ втПаллетыКОтгрузке
	|ИЗ
	|	РегистрСведений.актЗарегистрированныеПаллеты КАК актЗарегистрированныеПаллеты
	|ГДЕ
	|	актЗарегистрированныеПаллеты.Регистратор = &заказ
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	СУММА(втПаллетыКОтгрузке.Паллета.КоличествоУпаковок) КАК Количество,
	|	ВЫБОР
	|		КОГДА втПаллетыКОтгрузке.Паллета.КоличествоКоробок > 0
	|			ТОГДА втПаллетыКОтгрузке.Паллета.КоличествоУпаковок / втПаллетыКОтгрузке.Паллета.КоличествоКоробок
	|		ИНАЧЕ втПаллетыКОтгрузке.Паллета.УпаковкаКоробка.Числитель
	|	КОНЕЦ КАК Квант,
	|	втПаллетыКОтгрузке.Номенклатура КАК Номенклатура,
	|	МИНИМУМ(ВЫБОР
	|			КОГДА втПаллетыКОтгрузке.Состояние В (ЗНАЧЕНИЕ(Перечисление.актСостоянияПаллет.Маркируется), ЗНАЧЕНИЕ(Перечисление.актСостоянияПаллет.Промаркировано), ЗНАЧЕНИЕ(Перечисление.актСостоянияПаллет.ЗапланированаСборкаНаВесах), ЗНАЧЕНИЕ(Перечисление.актСостоянияПаллет.СобраноНаВесах), 
	|				ЗНАЧЕНИЕ(Перечисление.актСостоянияПаллет.Перемещается), ЗНАЧЕНИЕ(Перечисление.актСостоянияПаллет.СобираетсяНаПоддон), ЗНАЧЕНИЕ(Перечисление.актСостоянияПаллет.ЗапланированаМаркировка), ЗНАЧЕНИЕ(Перечисление.актСостоянияПаллет.ЗапланированаСборкаНаПоддон), ЗНАЧЕНИЕ(Перечисление.актСостоянияПаллет.ОжидаетВзвешиванияПередПеремещением), ЗНАЧЕНИЕ(Перечисление.актСостоянияПаллет.ОжидаетВзвешиванияПередОтгрузкой), ЗНАЧЕНИЕ(Перечисление.актСостоянияПаллет.ВзвешеноПередОтгрузкой))
	|				ТОГДА 2
	|			КОГДА втПаллетыКОтгрузке.Состояние В (ЗНАЧЕНИЕ(Перечисление.актСостоянияПаллет.ЗапланированаОтгрузка), ЗНАЧЕНИЕ(Перечисление.актСостоянияПаллет.Отгружено))
	|				ТОГДА 3
	|			КОГДА втПаллетыКОтгрузке.Состояние В (ЗНАЧЕНИЕ(Перечисление.актСостоянияПаллет.ВЗаказе))
	|				ТОГДА 1
	|			ИНАЧЕ 0
	|		КОНЕЦ) КАК Статус
	|   ,втПаллетыКОтгрузке.УпаковкаПоставщика.Числитель как Квант2
	|ПОМЕСТИТЬ втПаллеты
	|ИЗ
	|	втПаллетыКОтгрузке КАК втПаллетыКОтгрузке
	|ГДЕ
	|	НЕ втПаллетыКОтгрузке.Состояние В (ЗНАЧЕНИЕ(Перечисление.актСостоянияПаллет.Предварительный))
	|	И НЕ втПаллетыКОтгрузке.РЦ = ""<РЦ не указан>""
	|	И втПаллетыКОтгрузке.Паллета.КоличествоУпаковок > 0
	|
	|СГРУППИРОВАТЬ ПО
	|	ВЫБОР
	|		КОГДА втПаллетыКОтгрузке.Паллета.КоличествоКоробок > 0
	|			ТОГДА втПаллетыКОтгрузке.Паллета.КоличествоУпаковок / втПаллетыКОтгрузке.Паллета.КоличествоКоробок
	|		ИНАЧЕ втПаллетыКОтгрузке.Паллета.УпаковкаКоробка.Числитель
	|	КОНЕЦ,
	|	втПаллетыКОтгрузке.Номенклатура  , втПаллетыКОтгрузке.УпаковкаПоставщика.Числитель
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	актНастройка.Ссылка.ПорядковыйНомер КАК ПорядковыйНомер,
	|	МИНИМУМ(втПаллеты.Статус) КАК Статус,
	|	втПаллеты.Квант КАК Квант,       втПаллеты.Квант2 КАК Квант2,
	|	СУММА(втПаллеты.Количество) КАК Количество
	|ИЗ
	|	втПаллеты КАК втПаллеты
	|		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.актСвойстваГруппНоменклатурыДляДоскиСортировки.Товары КАК актНастройка
	|		ПО втПаллеты.Номенклатура = актНастройка.Номенклатура
	|
	|СГРУППИРОВАТЬ ПО
	|	актНастройка.Ссылка.ПорядковыйНомер,
	|	втПаллеты.Квант,   втПаллеты.Квант2
	|
	|УПОРЯДОЧИТЬ ПО
	|	ПорядковыйНомер";
	
	Запрос.УстановитьПараметр("заказ", заказ);
	
	Результат = Запрос.Выполнить().Выгрузить();
	
	возврат Результат;
КонецФункции

&НаСервере
Функция ПолучитьСписокЗаказов()
	Запрос = Новый Запрос;
	
	Запрос.Текст = "ВЫБРАТЬ
	               |	Паллеты.Регистратор КАК Заказ,
	               |	Паллеты.Регистратор.Грузополучатель КАК РЦ,
	               |	Паллеты.ДатаОтгрузки КАК ДатаОтгрузки,
	               |	Паллеты.Партия.ПланируемоеВремяОтгрузки КАК ВремяОтгрузки
				   |    ,выбор когда ЗаказКлиентаДополнительныеРеквизиты.Значение В (&СтатусыЭДО) Тогда Истина иначе Ложь Конец КАК Подтвержден 
	               |ИЗ
	               |	РегистрСведений.актЗарегистрированныеПаллеты КАК Паллеты
	               |		ЛЕВОЕ СОЕДИНЕНИЕ Документ.РеализацияТоваровУслуг КАК РеализацияТоваровУслуг
	               |		ПО Паллеты.Регистратор = РеализацияТоваровУслуг.ЗаказКлиента
	               |			И (РеализацияТоваровУслуг.Проведен)
	               |		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.ПартнерыСегмента КАК ПартнерыСегмента
	               |		ПО Паллеты.Регистратор.Партнер = ПартнерыСегмента.Партнер
	               |		ЛЕВОЕ СОЕДИНЕНИЕ Документ.ЗаказКлиента.ДополнительныеРеквизиты КАК ЗаказКлиентаДополнительныеРеквизиты
	               |		ПО Паллеты.Регистратор = ЗаказКлиентаДополнительныеРеквизиты.Ссылка И ЗаказКлиентаДополнительныеРеквизиты.Значение ССЫЛКА Перечисление.актСтатусыЗаказаЭДО
	               |ГДЕ
	               |	Паллеты.ДатаОтгрузки МЕЖДУ &НачалоПериода И &КонецПериода
	               |	И Паллеты.Партия.Организация = &Организация
	               |	И НЕ Паллеты.Регистратор.Грузополучатель = ЗНАЧЕНИЕ(Справочник.Контрагенты.ПустаяСсылка)
	               |	И НЕ Паллеты.Партия.СостояниеПаллеты В (ЗНАЧЕНИЕ(Перечисление.актСостоянияПаллет.Предварительный))
	               |	И РеализацияТоваровУслуг.Ссылка ЕСТЬ NULL
	               |	И ПартнерыСегмента.Сегмент В(&СегментыСети)
	               |
	               |СГРУППИРОВАТЬ ПО
	               |	Паллеты.Регистратор,
	               |	Паллеты.Регистратор.Грузополучатель,
	               |	Паллеты.ДатаОтгрузки,
	               |	Паллеты.Партия.ПланируемоеВремяОтгрузки
				   |    ,выбор когда ЗаказКлиентаДополнительныеРеквизиты.Значение В (&СтатусыЭДО) Тогда Истина иначе Ложь Конец 
	               |
	               |УПОРЯДОЧИТЬ ПО
	               |	РЦ,
	               |	ДатаОтгрузки,
	               |	ВремяОтгрузки";
	
	Запрос.УстановитьПараметр("Организация", РегистрыСведений.актКонстанты.мПолучить("Справочники.Организации.АгроГриб"));
	Запрос.УстановитьПараметр("НачалоПериода", НачалоПериода);
	Запрос.УстановитьПараметр("КонецПериода", КонецПериода); 
	массСегменты = новый Массив;
	Если флСети Тогда
		массСегменты.Добавить(РегистрыСведений.актКонстанты.мПолучить("Справочники.СегментыПартнеров.СЕТИ"));
	КонецЕсли; 
	Если флХорека Тогда
		массСегменты.Добавить(РегистрыСведений.актКонстанты.мПолучить("Справочники.СегментыПартнеров.HORECA"));
	КонецЕсли; 
	Если флОпт Тогда
		массСегменты.Добавить(РегистрыСведений.актКонстанты.мПолучить("Справочники.СегментыПартнеров.ОПТ"));
	КонецЕсли; 
	Запрос.УстановитьПараметр("СегментыСети", массСегменты); 
	
	массСтатусы = новый Массив;
	Если статус_НеЭДО Тогда
		массСтатусы.Добавить(Перечисления.актСтатусыЗаказаЭДО.НеЭДО);
	КонецЕсли; 
	Если статус_НеОбработанный Тогда
		массСтатусы.Добавить(Перечисления.актСтатусыЗаказаЭДО.НеОбработанныйЭДО);
	КонецЕсли; 
	Если статус_Обработанный Тогда
		массСтатусы.Добавить(Перечисления.актСтатусыЗаказаЭДО.ОбработанныйЭДО);
	КонецЕсли; 
	Если статус_ЭТП Тогда
		массСтатусы.Добавить(Перечисления.актСтатусыЗаказаЭДО.ЭТП);
	КонецЕсли; 
	Запрос.УстановитьПараметр("СтатусыЭДО", массСтатусы); 
	
	//Запрос.УстановитьПараметр("НачалоПериода", Дата('20200326'));
	//Запрос.УстановитьПараметр("КонецПериода",  Дата('20200330'));  
	
	рез = Запрос.Выполнить();
	Объект.ТЧ.Загрузить(рез.Выгрузить());
	возврат 0;
КонецФункции

&НаКлиенте
Процедура ТДВыбор(Элемент, Область, СтандартнаяОбработка)
	СтандартнаяОбработка = Ложь;
	Возврат;
	
	ОткрытьФорму("Обработка.актЭкранЗаказов.Форма.Настройки",,ЭтаФорма,,,,,РежимОткрытияОкнаФормы.БлокироватьОкноВладельца);
КонецПроцедуры

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	ИнициализироватьПериод();	
КонецПроцедуры

&НаСервере
Процедура ЗагрузитьНастройкиОтчета()
	Пользователь = ПараметрыСеанса.ТекущийПользователь;
	юзер = ПользователиИнформационнойБазы.НайтиПоУникальномуИдентификатору(Пользователь.ИдентификаторПользователяИБ);
	
	стр = ХранилищеОбщихНастроек.Загрузить("ЭкранЗаказов", "НастройкиОтчета5",, юзер.Имя);
	Если стр = Неопределено Тогда
		ИнтервалОбновления = 15;
		НачалоПериода = НачалоДня(ТекущаяДата());
		КонецПериода = КонецДня(ТекущаяДата());
		флПересчитыватьВУпаковки = истина;
		флСети = Истина;
		флОпт = Ложь;
		флХорека = Ложь;
		ШиринаКолонки = 15.4;
		РазмерШрифта_Шапка = 16;
		РазмерШрифта_Количество = 24;
		РазмерШрифта_Квант = 18;
		РазмерШрифта_Коммент = 16;
		РазмерШрифта_Заказ = 16;
		статус_НеЭДО = Истина;
		статус_НеОбработанный = Ложь;
		статус_Обработанный = Истина;
		статус_ЭТП = Истина;
		разделитель1 = 0;
		разделитель2 = 0;
		разделитель3 = 0;
		стр = Новый Структура("ИнтервалОбновления, НачалоПериода, КонецПериода, флПересчитыватьВУпаковки, ШиринаКолонки, 
							   |разделитель1, флСети, флОпт, флХорека, 
							   |разделитель2, статус_НеЭДО, статус_НеОбработанный, статус_Обработанный, статус_ЭТП,
							   |разделитель3, РазмерШрифта_Шапка, РазмерШрифта_Количество, РазмерШрифта_Квант, РазмерШрифта_Коммент, РазмерШрифта_Заказ", 
							   ИнтервалОбновления, НачалоПериода, КонецПериода, флПересчитыватьВУпаковки, ШиринаКолонки, 
							   разделитель1, флСети, флОпт, флХорека, 
							   разделитель2, статус_НеЭДО, статус_НеОбработанный, статус_Обработанный, статус_ЭТП,
							   разделитель3, РазмерШрифта_Шапка, РазмерШрифта_Количество, РазмерШрифта_Квант, РазмерШрифта_Коммент, РазмерШрифта_Заказ);
	КонецЕсли;	
	для Каждого сс из стр Цикл
		//ЗначениеВРеквизитФормы(сс.Значение, сс.Ключ);	
		ЭтаФорма[сс.Ключ] = сс.Значение;
	КонецЦикла;	
	
	ОбновитьСостояниеПереключателяСегментов();
КонецПроцедуры

&НаСервере
Процедура ОбновитьСостояниеПереключателяСегментов()
	Если Число(флСети) + Число(флОпт) + Число(флХорека) > 1 Тогда
		ПереключательСегмент = "Все";
	Иначе
		Если флОпт Тогда
			ПереключательСегмент = "Опт";
		ИначеЕсли флСети Тогда
			ПереключательСегмент = "Сети";
		ИначеЕсли флХорека Тогда
			ПереключательСегмент = "Horeca";
		КонецЕсли; 	
	КонецЕсли; 
	
КонецПроцедуры

&НаКлиенте
Процедура ПереключательСегментПриИзменении(Элемент)
	Если ПереключательСегмент="Все" Тогда
		флОпт=Истина;
		флСети=Истина;
		флХорека=Истина;
	ИначеЕсли ПереключательСегмент="Сети" Тогда
		флОпт=Ложь;
		флСети=Истина;
		флХорека=Ложь;
	ИначеЕсли ПереключательСегмент="Опт" Тогда
		флОпт=Истина;
		флСети=Ложь;
		флХорека=Ложь;
	ИначеЕсли ПереключательСегмент="Horeca" Тогда
		флОпт=Ложь;
		флСети=Ложь;
		флХорека=Истина;
	КонецЕсли; 	
	
	Объект.ТЧ.Очистить();
	СохранитьНастройки();
	Сформировать();
КонецПроцедуры

&НаСервере
Процедура СохранитьНастройки()
	стр = Новый Структура("ИнтервалОбновления, НачалоПериода, КонецПериода, флПересчитыватьВУпаковки, ШиринаКолонки, 
							   |разделитель1, флСети, флОпт, флХорека, 
							   |разделитель2, статус_НеЭДО, статус_НеОбработанный, статус_Обработанный, статус_ЭТП,
							   |разделитель3, РазмерШрифта_Шапка, РазмерШрифта_Количество, РазмерШрифта_Квант, РазмерШрифта_Коммент, РазмерШрифта_Заказ", 
							   ИнтервалОбновления, НачалоПериода, КонецПериода, флПересчитыватьВУпаковки, ШиринаКолонки, 
							   разделитель1, флСети, флОпт, флХорека, 
							   разделитель2, статус_НеЭДО, статус_НеОбработанный, статус_Обработанный, статус_ЭТП,
							   разделитель3, РазмерШрифта_Шапка, РазмерШрифта_Количество, РазмерШрифта_Квант, РазмерШрифта_Коммент, РазмерШрифта_Заказ);
							   
	Пользователь = ПараметрыСеанса.ТекущийПользователь;
	юзер = ПользователиИнформационнойБазы.НайтиПоУникальномуИдентификатору(Пользователь.ИдентификаторПользователяИБ);
							   
	ХранилищеОбщихНастроек.Сохранить("ЭкранЗаказов", "НастройкиОтчета5", стр, , юзер.Имя);	
КонецПроцедуры	

&НаКлиенте
Процедура ИнтервалОбновленияПриИзменении(Элемент)
	СохранитьНастройки();	
	ОбновитьПолзунок();
КонецПроцедуры

&НаКлиенте
Процедура ОбновитьПолзунок()
	Элементы.ИнтервалОбновления.Заголовок = "Интервал обновления "+ИнтервалОбновления+" сек";
КонецПроцедуры

&НаКлиенте
Процедура НадписьСправочникНажатие(Элемент)
	ОткрытьФорму("Справочник.актСвойстваГруппНоменклатурыДляДоскиСортировки.ФормаСписка");	
КонецПроцедуры

&НаКлиенте
Процедура НадписьНастройкиНажатие(Элемент)
	ОткрытьФорму("Обработка.актЭкранЗаказов.Форма.Настройки",,ЭтаФорма,,,,Новый ОписаниеОповещения("ОбработкаЗакрытияНастроек", ЭтотОбъект));	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаЗакрытияНастроек(Результат, ДопПараметры) Экспорт
	Если Результат = Истина Тогда
		Сформировать();	
	КонецЕсли;	
КонецПроцедуры


