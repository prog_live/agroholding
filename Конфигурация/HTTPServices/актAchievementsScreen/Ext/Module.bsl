﻿
Функция ОсновнойШаблонURLGetScreen(Запрос)
	
	HTTPОтвет = Новый HTTPСервисОтвет(200);
	HTTPОтвет.Заголовки["Content-Type"] = "text/html; charset=utf-8"; 
	
	КодЭкранаДостижений = Запрос.ПараметрыЗапроса.Получить("code");
	
	Если КодЭкранаДостижений = Неопределено Тогда
		
		тОтвет = актРастениеводствоЭкраныДостиженийВызовСервера.СформироватьТекстHTMLВсеДоступныеЭкраныДостижений();
		
		HTTPОтвет.УстановитьТелоИзСтроки(тОтвет);
		
		Возврат HTTPОтвет;
		
	Иначе
		
		КодЭкранаДостижений = Число(КодЭкранаДостижений);
		КодЭкранаДостижений = Формат(КодЭкранаДостижений, "ЧЦ=9; ЧДЦ=; ЧВН=; ЧГ=0");
		
		ЭкранДостижений = Справочники.актЭкраныДостижений.НайтиПоКоду(КодЭкранаДостижений);
		
		Если ЭкранДостижений.Пустая() Тогда
			тОтвет = "По указанному коду " + КодЭкранаДостижений + " не найден экран достижений";
		Иначе
			тОтвет = актРастениеводствоЭкраныДостиженийВызовСервера.СформироватьАктуальнуюHTMLСтраницуДляЭкранаДостижений(ЭкранДостижений);
		КонецЕсли;
		
		HTTPОтвет.УстановитьТелоИзСтроки(тОтвет);
		
		Возврат HTTPОтвет;
		
	КонецЕсли;
	
КонецФункции
