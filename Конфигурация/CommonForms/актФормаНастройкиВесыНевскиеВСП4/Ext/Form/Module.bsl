﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Параметры.Свойство("Идентификатор", Идентификатор);
	Параметры.Свойство("ДрайверОборудования", ДрайверОборудования);
	
	врПрогИд         = Неопределено;
	
	Параметры.ПараметрыОборудования.Свойство("ПрогИд",         врПрогИд);
	
	ПрогИд = ?(врПрогИд   = Неопределено, "NW_Cntrl.DVal", врПрогИд);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиРеквизитовКомандФормы

&НаКлиенте
Процедура СохранитьНастройки1(Команда)
	
	НовыеЗначениеПараметров = Новый Структура;
	НовыеЗначениеПараметров.Вставить("ПрогИд"         , ПрогИд);
		
	Результат = Новый Структура;
	Результат.Вставить("Идентификатор", Идентификатор);
	Результат.Вставить("ПараметрыОборудования", НовыеЗначениеПараметров);
	
	Закрыть(Результат);

КонецПроцедуры

&НаКлиенте
Процедура ТестСоздатьКОМОбъект(Команда)
	
	Попытка
		КОМ = Новый COMОбъект(ПрогИд);
		КОМ = Неопределено;
		ПоказатьПредупреждение(, "Успешно!");
	Исключение
		ПоказатьПредупреждение(, ПодробноеПредставлениеОшибки(ИнформацияОбОшибке()));
	КонецПопытки;
	
КонецПроцедуры

&НаКлиенте
Процедура ТестПолучитьВес(Команда)
	
	Попытка
		КОМ = Новый COMОбъект(ПрогИд);
		ПоказатьПредупреждение(, "Вес - " + Строка(КОМ.GetWeight()));
	Исключение
		ПоказатьПредупреждение(, ПодробноеПредставлениеОшибки(ИнформацияОбОшибке()));
	КонецПопытки;
	
КонецПроцедуры

&НаКлиенте
Процедура ТестПолучитьСтатус(Команда)
	Попытка
		КОМ = Новый COMОбъект(ПрогИд);
		ПоказатьПредупреждение(, "Статус - " + Строка(КОМ.GetStatus()));
	Исключение
		ПоказатьПредупреждение(, ПодробноеПредставлениеОшибки(ИнформацияОбОшибке()));
	КонецПопытки;
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#КонецОбласти