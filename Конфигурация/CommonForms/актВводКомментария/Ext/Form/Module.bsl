﻿
&НаКлиенте
Процедура Записать(Команда)
	ЗаписатьНаСервере();
	Закрыть();
КонецПроцедуры

&НаСервере
Процедура ЗаписатьНаСервере()
	О = Партнер.ПолучитьОбъект();	
	Строка = О.ДополнительныеРеквизиты.Найти(Свойство);
	Если ЗначениеЗаполнено(Строка) Тогда
		Строка.Значение = Комментарий;
		Строка.ТекстоваяСтрока = Комментарий;
	Иначе
		НС = О.ДополнительныеРеквизиты.Добавить();
		НС.Свойство = Свойство;
		НС.Значение = Комментарий;
		НС.ТекстоваяСтрока = Комментарий;
	КонецЕсли;
	О.Записать();
КонецПроцедуры

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	Партнер = Параметры.Партнер;
	Свойство = РегистрыСведений.актКонстанты.мПолучить(Параметры.Свойство);
	
	// Получать текущее значение? 
КонецПроцедуры
