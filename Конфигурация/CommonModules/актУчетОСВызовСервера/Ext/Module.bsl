﻿
#Область СписаниеОСОбработкаПРоведенеия
//aydafz
Функция ПодготовитьТаблицыСведенийПоВыбытиюОСДобавленный(ТаблицаОС, ТаблицаРеквизиты, Отказ, ВыдаватьСообщения = Ложь) Экспорт

	Параметры = ПодготовитьПараметрыПодготовитьТаблицыСведенийПоВыбытиюОС(ТаблицаОС, ТаблицаРеквизиты);

	Реквизиты = Параметры.Реквизиты[0];

	// Проинициализируем структуру, чтобы в случае "досрочного" выхода в структуре были все таблицы, хоть и пустые
	ПараметрыВыбытия = ПолучитьИнициализированнуюСтруктуруПараметровВыбытия();

	Параметры.Реквизиты.Колонки.Добавить("ДатаРасчета", Новый ОписаниеТипов("Дата"));
	Параметры.Реквизиты.ЗаполнитьЗначения(Реквизиты.Период, "ДатаРасчета");
	Параметры.Реквизиты.Колонки.Добавить("ВыдаватьСообщения", Новый ОписаниеТипов("Булево"));
	Параметры.Реквизиты.ЗаполнитьЗначения(ВыдаватьСообщения = Истина, "ВыдаватьСообщения");
	Если Параметры.ТаблицаОС.Количество() = 0 Тогда
		ТаблицаАмортизации = ПолучитьПустуюТаблицуАмортизацииОС();
	Иначе
		ТаблицаАмортизации = УчетОСВызовСервера.ПодготовитьТаблицуАмортизацияОС(Параметры.ТаблицаОС, Параметры.Реквизиты, 1,Отказ);
		Если Отказ Тогда
			Возврат ПараметрыВыбытия;
		КонецЕсли;
	КонецЕсли;

	Запрос = Новый Запрос();
	Запрос.МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;

	Запрос.УстановитьПараметр("ТабАмортизации", ТаблицаАмортизации);
	Запрос.Текст =
	"ВЫБРАТЬ
	|	ТабАмортизации.ОбъектУчета КАК ОбъектУчета,
	|	ТабАмортизации.СуммаБУ КАК СуммаАмортизацииБУ,
	|	ТабАмортизации.СуммаНУ КАК СуммаАмортизацииНУ,
	|	ТабАмортизации.СуммаПР КАК СуммаАмортизацииПР,
	|	ТабАмортизации.СуммаВР КАК СуммаАмортизацииВР,
	|	ТабАмортизации.СуммаКапитальныхВложенийВключаемыхВРасходы КАК СуммаКапитальныхВложенийВключаемыхВРасходы
	|ПОМЕСТИТЬ ТабАмортизации
	|ИЗ
	|	&ТабАмортизации КАК ТабАмортизации";
	Запрос.Выполнить();

	ТаблицаЗатрат = УправлениеВнеоборотнымиАктивамиПереопределяемый.ПодготовитьТаблицуРаспределениеАмортизацииПоНаправлениямРегл(ТаблицаАмортизации, Параметры.Реквизиты, Отказ);
	Если Отказ Тогда
		Возврат ПараметрыВыбытия;
	КонецЕсли;

	ПараметрыРасходыПоАреднымПлатежамНУ = УчетОСВызовСервера.ПодготовитьТаблицыРасходовПоАренднымПлатежамНУ(Параметры.ТаблицаОС, Параметры.Реквизиты, ТаблицаЗатрат,1,Отказ);
	Если Отказ Тогда
		Возврат ПараметрыВыбытия;
	КонецЕсли;
	
	ТабРасходыПоАренднымПлатежамНУ = ПараметрыРасходыПоАреднымПлатежамНУ.ТаблицаРасходыПоАренднымПлатежамНУ.Скопировать();
	ТабРасходыПоАренднымПлатежамНУ.Свернуть("ОсновноеСредство", "СуммаПлатежаНУ");
	
	Запрос.УстановитьПараметр("ТабРасходыПоАренднымПлатежамНУ", ТабРасходыПоАренднымПлатежамНУ);
	Запрос.Текст =
	"ВЫБРАТЬ
	|	РасходыПоАренднымПлатежамНУ.ОсновноеСредство КАК ОсновноеСредство,
	|	РасходыПоАренднымПлатежамНУ.СуммаПлатежаНУ КАК СуммаПлатежаНУ
	|ПОМЕСТИТЬ ТабРасходыПоАренднымПлатежамНУ
	|ИЗ
	|	&ТабРасходыПоАренднымПлатежамНУ КАК РасходыПоАренднымПлатежамНУ
	|
	|ИНДЕКСИРОВАТЬ ПО
	|	ОсновноеСредство";
	Запрос.Выполнить();
	
	ТабКорректировкаАмортизации = ПараметрыРасходыПоАреднымПлатежамНУ.ТаблицаКорректировкаАмортизации.Скопировать();
	ТабКорректировкаАмортизации.Свернуть("ОсновноеСредство", "КорректировкаАмортизацииНУ");
	
	Запрос.УстановитьПараметр("ТабКорректировкаАмортизации", ТабКорректировкаАмортизации);
	Запрос.Текст =
	"ВЫБРАТЬ
	|	КорректировкаАмортизации.ОсновноеСредство КАК ОсновноеСредство,
	|	КорректировкаАмортизации.КорректировкаАмортизацииНУ КАК СуммаНУ
	|ПОМЕСТИТЬ ТабКорректировкаАмортизации
	|ИЗ
	|	&ТабКорректировкаАмортизации КАК КорректировкаАмортизации
	|
	|ИНДЕКСИРОВАТЬ ПО
	|	ОсновноеСредство";
	Запрос.Выполнить();
	
	ПараметрыВыбытия.Вставить("РасходыПоАреднымПлатежамНУ", ПараметрыРасходыПоАреднымПлатежамНУ);
	
	ПараметрыНачисленияАмортизации = Новый Структура;
	ПараметрыНачисленияАмортизации.Вставить("ТаблицаЗатрат", ТаблицаЗатрат);
	ПараметрыНачисленияАмортизации.Вставить("ТаблицаРеквизиты", Параметры.Реквизиты);

	// Параметры для процедуры СформироватьДвиженияНачислениеАмортизации
	ПараметрыВыбытия.Вставить("НачислениеАмортизации", ПараметрыНачисленияАмортизации);

	ТаблицаНачисленияАмортизационнойПремии = УчетОСВызовСервера.ПодготовитьТаблицуСуммАмортизационнойПремии(ТаблицаАмортизации, Параметры.Реквизиты, Отказ);
	Если Отказ Тогда
		Возврат ПараметрыВыбытия;
	КонецЕсли;

	АмортизационнаяПремия = Новый Структура;
	АмортизационнаяПремия.Вставить("ТаблицаНачисленияАмортизационнойПремии", ТаблицаНачисленияАмортизационнойПремии);
	АмортизационнаяПремия.Вставить("ТаблицаРеквизиты", Параметры.Реквизиты);

	// Параметры для процедуры СформироватьДвиженияНачисленияАмортизационнойПремии
	ПараметрыВыбытия.Вставить("АмортизационнаяПремия", АмортизационнаяПремия);

	// Параметры для восстановления амортизационной премии
	ПараметрыВыбытия.Вставить("СуммаКапитальныхВложенийВключаемыхВРасходы", ТаблицаАмортизации.Итог("СуммаКапитальныхВложенийВключаемыхВРасходы"));

	ТаблицаИзноса = УчетОСВызовСервера.ПодготовитьТаблицуИзносаБухРегл(Параметры.ТаблицаОС, Параметры.Реквизиты, Отказ);
	Если Отказ Тогда
		Возврат ПараметрыВыбытия;
	КонецЕсли;

	НачислениеИзноса = Новый Структура;
	НачислениеИзноса.Вставить("ТаблицаИзноса", ТаблицаИзноса);
	НачислениеИзноса.Вставить("ТаблицаРеквизиты", Параметры.Реквизиты);

	// Параметры для процедуры СформироватьДвиженияНачислениеИзноса
	ПараметрыВыбытия.Вставить("НачислениеИзноса", НачислениеИзноса);

	Запрос.УстановитьПараметр("ТабИзноса", ТаблицаИзноса);
	Запрос.Текст =
	"ВЫБРАТЬ
	|	ТабИзноса.ОсновноеСредство КАК ОсновноеСредство,
	|	ТабИзноса.Сумма КАК СуммаИзноса
	|ПОМЕСТИТЬ ТабИзноса
	|ИЗ
	|	&ТабИзноса КАК ТабИзноса";
	Запрос.Выполнить();

	ТаблицаТолькоЛинейный = УчетОСВызовСервера.ПодготовитьТаблицуТолькоЛинейныйМетодНУ(Параметры.ТаблицаОС,Параметры.Реквизиты, Отказ);

	Запрос.УстановитьПараметр("Дата", Новый Граница(Реквизиты.Период, ВидГраницы.Исключая));
	Запрос.УстановитьПараметр("Организация", Реквизиты.Организация);
	Запрос.УстановитьПараметр("Ссылка", Реквизиты.Регистратор);
	Запрос.УстановитьПараметр("ТаблицаОС", Параметры.ТаблицаОС);
	Запрос.УстановитьПараметр("ТаблицаТолькоЛинейный", ТаблицаТолькоЛинейный);
	Запрос.Текст =
	"ВЫБРАТЬ
	|	ТаблицаОС.НомерСтроки КАК НомерСтроки,
	|	ТаблицаОС.ОсновноеСредство КАК ОсновноеСредство
	|ПОМЕСТИТЬ СписокОС
	|ИЗ
	|	&ТаблицаОС КАК ТаблицаОС
	|
	|ИНДЕКСИРОВАТЬ ПО
	|	ОсновноеСредство
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ТаблицаТолькоЛинейный.ОсновноеСредство КАК ОсновноеСредство,
	|	ТаблицаТолькоЛинейный.ТолькоЛинейный КАК ТолькоЛинейный
	|ПОМЕСТИТЬ ПризнакТолькоЛинейный
	|ИЗ
	|	&ТаблицаТолькоЛинейный КАК ТаблицаТолькоЛинейный
	|
	|ИНДЕКСИРОВАТЬ ПО
	|	ОсновноеСредство
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	СчетаУчетаОС.ОсновноеСредство КАК ОсновноеСредство,
	|	СчетаУчетаОС.СчетУчета КАК СчетУчета,
	|	СчетаУчетаОС.СчетНачисленияАмортизации КАК СчетНачисленияАмортизации
	|ПОМЕСТИТЬ СчетаУчетаОС
	|ИЗ
	|	РегистрСведений.СчетаБухгалтерскогоУчетаОС.СрезПоследних(
	|			&Дата,
	|			Организация = &Организация
	|				И ОсновноеСредство В
	|					(ВЫБРАТЬ
	|						СписокОС.ОсновноеСредство
	|					ИЗ
	|						СписокОС КАК СписокОС)) КАК СчетаУчетаОС
	|
	|ИНДЕКСИРОВАТЬ ПО
	|	ОсновноеСредство
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	СчетаУчетаОС.СчетУчета КАК Счет
	|ПОМЕСТИТЬ РазличныеСчетаУчетаОС
	|ИЗ
	|	СчетаУчетаОС КАК СчетаУчетаОС
	|
	|ОБЪЕДИНИТЬ
	|
	|ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	СчетаУчетаОС.СчетНачисленияАмортизации
	|ИЗ
	|	СчетаУчетаОС КАК СчетаУчетаОС
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ПервоначальныеСведенияОСБУ.ОсновноеСредство КАК ОсновноеСредство,
	|	ПервоначальныеСведенияОСБУ.ПорядокПогашенияСтоимости
	|ПОМЕСТИТЬ ПервоначальныеСведенияОСБУ
	|ИЗ
	|	РегистрСведений.ПервоначальныеСведенияОСБухгалтерскийУчет.СрезПоследних(
	|			&Дата,
	|			Организация = &Организация
	|				И ОсновноеСредство В
	|					(ВЫБРАТЬ
	|						СписокОС.ОсновноеСредство
	|					ИЗ
	|						СписокОС)) КАК ПервоначальныеСведенияОСБУ
	|
	|ИНДЕКСИРОВАТЬ ПО
	|	ОсновноеСредство
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ПервоначальныеСведенияОСНУ.ОсновноеСредство КАК ОсновноеСредство,
	|	ПервоначальныеСведенияОСНУ.ПорядокВключенияСтоимостиВСоставРасходов
	|ПОМЕСТИТЬ ПервоначальныеСведенияОСНУ
	|ИЗ
	|	РегистрСведений.ПервоначальныеСведенияОСНалоговыйУчет.СрезПоследних(
	|			&Дата,
	|			Организация = &Организация
	|				И ОсновноеСредство В
	|					(ВЫБРАТЬ
	|						СписокОС.ОсновноеСредство
	|					ИЗ
	|						СписокОС)) КАК ПервоначальныеСведенияОСНУ
	|
	|ИНДЕКСИРОВАТЬ ПО
	|	ОсновноеСредство
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ПодразделенияОС.ОсновноеСредство КАК ОсновноеСредство,
	|	ПодразделенияОС.Местонахождение КАК Подразделение
	|ПОМЕСТИТЬ ПодразделенияОС
	|ИЗ
	|	РегистрСведений.МестонахождениеОСБухгалтерскийУчет.СрезПоследних(
	|			&Дата,
	|			Организация = &Организация
	|				И ОсновноеСредство В
	|					(ВЫБРАТЬ
	|						СписокОС.ОсновноеСредство
	|					ИЗ
	|						СписокОС)) КАК ПодразделенияОС
	|
	|ИНДЕКСИРОВАТЬ ПО
	|	ОсновноеСредство";
	Запрос.Выполнить();

	Если ТранзакцияАктивна() И ОбщегоНазначенияБПВызовСервераПовтИсп.ИспользоватьУправляемыеБлокировки() Тогда
		// Блокировка регистра бухгалтерии.
		СтруктураПараметровБлокировки = Новый Структура("ТипТаблицы, ИмяТаблицы, ИсточникДанных, ИмяВременнойТаблицы",
			"РегистрБухгалтерии", "Хозрасчетный", Запрос.МенеджерВременныхТаблиц, "РазличныеСчетаУчетаОС");
		СтруктураЗначенийБлокировки = Новый Структура("Период, Организация",
			Новый Диапазон(, КонецМесяца(Реквизиты.Период)), Реквизиты.Организация);
		СтруктураИсточникаДанных = Новый Структура("Счет", "Счет");
		ОбщегоНазначенияБПВызовСервера.УстановитьУправляемуюБлокировку(СтруктураПараметровБлокировки, СтруктураЗначенийБлокировки, СтруктураИсточникаДанных);
	КонецЕсли;

	ВидыСубконто = Новый Массив;
	ВидыСубконто.Добавить(ПланыВидовХарактеристик.ВидыСубконтоХозрасчетные.ОсновныеСредства);
	Запрос.УстановитьПараметр("ВидыСубконто", ВидыСубконто);

	Запрос.Текст =
	"ВЫБРАТЬ
	|	СтоимостьОС.Субконто1 КАК ОсновноеСредство,
	|	СтоимостьОС.Счет КАК Счет,
	|	СтоимостьОС.СуммаОстатокДт КАК СуммаОстатокДт,
	|	СтоимостьОС.СуммаНУОстатокДт КАК СуммаНУОстатокДт,
	|	СтоимостьОС.СуммаПРОстатокДт КАК СуммаПРОстатокДт,
	|	СтоимостьОС.СуммаВРОстатокДт КАК СуммаВРОстатокДт,
	|	СтоимостьОС.СуммаОстатокКт КАК СуммаОстатокКт,
	|	СтоимостьОС.СуммаНУОстатокКт КАК СуммаНУОстатокКт,
	|	СтоимостьОС.СуммаПРОстатокКт КАК СуммаПРОстатокКт,
	|	СтоимостьОС.СуммаВРОстатокКт КАК СуммаВРОстатокКт
	|ПОМЕСТИТЬ СтоимостьОС
	|ИЗ
	|	РегистрБухгалтерии.Хозрасчетный.Остатки(
	|			&Дата,
	|			Счет В
	|				(ВЫБРАТЬ
	|					РазличныеСчетаУчетаОС.Счет
	|				ИЗ
	|					РазличныеСчетаУчетаОС КАК РазличныеСчетаУчетаОС),
	|			&ВидыСубконто,
	|			Организация = &Организация
	|				И Субконто1 В
	|					(ВЫБРАТЬ
	|						СписокОС.ОсновноеСредство
	|					ИЗ
	|						СписокОС КАК СписокОС)) КАК СтоимостьОС
	|
	|ИНДЕКСИРОВАТЬ ПО
	|	ОсновноеСредство,
	|	Счет
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	КорректировкаСтоимости.Субконто1 КАК ОсновноеСредство,
	|	КорректировкаСтоимости.СуммаОстатокДт КАК СуммаОстатокДт,
	|	КорректировкаСтоимости.СуммаНУОстатокДт КАК СуммаНУОстатокДт,
	|	КорректировкаСтоимости.СуммаПРОстатокДт КАК СуммаПРОстатокДт,
	|	КорректировкаСтоимости.СуммаВРОстатокДт КАК СуммаВРОстатокДт,
	|	КорректировкаСтоимости.СуммаОстатокКт КАК СуммаОстатокКт,
	|	КорректировкаСтоимости.СуммаНУОстатокКт КАК СуммаНУОстатокКт,
	|	КорректировкаСтоимости.СуммаПРОстатокКт КАК СуммаПРОстатокКт,
	|	КорректировкаСтоимости.СуммаВРОстатокКт КАК СуммаВРОстатокКт
	|ПОМЕСТИТЬ КорректировкаСтоимости
	|ИЗ
	|	РегистрБухгалтерии.Хозрасчетный.Остатки(
	|			&Дата,
	|			Счет = ЗНАЧЕНИЕ(ПланСчетов.Хозрасчетный.КорректировкаСтоимостиАрендованногоИмущества),
	|			&ВидыСубконто,
	|			Организация = &Организация
	|				И Субконто1 В
	|					(ВЫБРАТЬ
	|						СписокОС.ОсновноеСредство
	|					ИЗ
	|						СписокОС КАК СписокОС)) КАК КорректировкаСтоимости
	|
	|ИНДЕКСИРОВАТЬ ПО
	|	ОсновноеСредство
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	СписокОС.НомерСтроки КАК НомерСтроки,
	|	СписокОС.ОсновноеСредство КАК ОсновноеСредство,
	|	ПодразделенияОС.Подразделение КАК Подразделение,
	|	ПервоначальныеСведенияОСБУ.ПорядокПогашенияСтоимости КАК ПорядокПогашенияСтоимостиБУ,
	|	ПервоначальныеСведенияОСНУ.ПорядокВключенияСтоимостиВСоставРасходов КАК ПорядокВключенияСтоимостиВСоставРасходовНУ,
	|	СчетаУчетаОС.СчетУчета КАК СчетУчета,
	|	СчетаУчетаОС.СчетНачисленияАмортизации КАК СчетНачисленияАмортизации,
	|	ЕСТЬNULL(СтоимостьОС.СуммаОстатокДт, 0) КАК СтоимостьОС,
	|	ЕСТЬNULL(СтоимостьОС.СуммаНУОстатокДт, 0) - ЕСТЬNULL(ТабАмортизации.СуммаКапитальныхВложенийВключаемыхВРасходы, 0) КАК СтоимостьОСНУ,
	|	ЕСТЬNULL(СтоимостьОС.СуммаПРОстатокДт, 0) КАК СтоимостьОСПР,
	|	ЕСТЬNULL(СтоимостьОС.СуммаВРОстатокДт, 0) + ЕСТЬNULL(ТабАмортизации.СуммаКапитальныхВложенийВключаемыхВРасходы, 0) КАК СтоимостьОСВР,
	|	ЕСТЬNULL(АмортизацияОС.СуммаОстатокКт, 0) + ЕСТЬNULL(ТабАмортизации.СуммаАмортизацииБУ, 0) КАК АмортизацияОС,
	|	ЕСТЬNULL(АмортизацияОС.СуммаНУОстатокКт, 0) + ЕСТЬNULL(ТабАмортизации.СуммаАмортизацииНУ, 0) + ЕСТЬNULL(ТабКорректировкаАмортизации.СуммаНУ, 0) КАК АмортизацияОСНУ,
	|	ЕСТЬNULL(АмортизацияОС.СуммаПРОстатокКт, 0) + ЕСТЬNULL(ТабАмортизации.СуммаАмортизацииПР, 0) КАК АмортизацияОСПР,
	|	ЕСТЬNULL(АмортизацияОС.СуммаВРОстатокКт, 0) + ЕСТЬNULL(ТабАмортизации.СуммаАмортизацииВР, 0) - ЕСТЬNULL(ТабКорректировкаАмортизации.СуммаНУ, 0) КАК АмортизацияОСВР,
	|	ЕСТЬNULL(АмортизацияОС.СуммаОстатокДт, 0) + ЕСТЬNULL(ТабИзноса.СуммаИзноса, 0) КАК ИзносОС,
	|	ВЫБОР
	|		КОГДА ПервоначальныеСведенияОСБУ.ОсновноеСредство ЕСТЬ NULL 
	|			ТОГДА ЛОЖЬ
	|		ИНАЧЕ ИСТИНА
	|	КОНЕЦ КАК ОтражалосьВБухгалтерскомУчете,
	|	ВЫБОР
	|		КОГДА ПервоначальныеСведенияОСНУ.ОсновноеСредство ЕСТЬ NULL 
	|			ТОГДА ЛОЖЬ
	|		ИНАЧЕ ИСТИНА
	|	КОНЕЦ КАК ОтражалосьВНалоговомУчете,
	|	ПризнакТолькоЛинейный.ТолькоЛинейный,
	|	ВЫБОР
	|		КОГДА ПервоначальныеСведенияОСБУ.ПорядокПогашенияСтоимости = ЗНАЧЕНИЕ(Перечисление.ПорядокПогашенияСтоимостиОС.НачислениеАмортизации)
	|				ИЛИ ПервоначальныеСведенияОСБУ.ПорядокПогашенияСтоимости = ЗНАЧЕНИЕ(Перечисление.ПорядокПогашенияСтоимостиОС.НачислениеИзносаПоЕНАОФ)
	|			ТОГДА ИСТИНА
	|		ИНАЧЕ ЛОЖЬ
	|	КОНЕЦ КАК ПрекратитьНачислениеАмортизацииБУ,
	|	ВЫБОР
	|		КОГДА ПервоначальныеСведенияОСНУ.ПорядокВключенияСтоимостиВСоставРасходов = ЗНАЧЕНИЕ(Перечисление.ПорядокВключенияСтоимостиОСВСоставРасходовНУ.НачислениеАмортизации)
	|			ТОГДА ИСТИНА
	|		ИНАЧЕ ЛОЖЬ
	|	КОНЕЦ КАК ПрекратитьНачислениеАмортизацииНУ,
	//|	ЕСТЬNULL(КорректировкаСтоимости.СуммаНУОстатокДт, 0) - ЕСТЬNULL(ТабРасходыПоАренднымПлатежамНУ.СуммаПлатежаНУ, 0) КАК КорректировкаСтоимостиНУ,
	//|	ЕСТЬNULL(КорректировкаСтоимости.СуммаПРОстатокДт, 0) КАК КорректировкаСтоимостиПР,
	//|	ЕСТЬNULL(КорректировкаСтоимости.СуммаВРОстатокДт, 0) - ЕСТЬNULL(-ТабРасходыПоАренднымПлатежамНУ.СуммаПлатежаНУ, 0) КАК КорректировкаСтоимостиВР,
	|	ЕСТЬNULL(КорректировкаСтоимости.СуммаНУОстатокДт, 0)  КАК КорректировкаСтоимостиНУ,
	|	ЕСТЬNULL(КорректировкаСтоимости.СуммаПРОстатокДт, 0) КАК КорректировкаСтоимостиПР,
	|	ЕСТЬNULL(КорректировкаСтоимости.СуммаВРОстатокДт, 0) КАК КорректировкаСтоимостиВР,
	|	КорректировкаСтоимости.СуммаНУОстатокДт как СуммаНУОстатокДт,
	|   ТабРасходыПоАренднымПлатежамНУ.СуммаПлатежаНУ как СуммаПлатежаНУ 
	|ИЗ
	|	СписокОС КАК СписокОС
	|		ЛЕВОЕ СОЕДИНЕНИЕ ПервоначальныеСведенияОСБУ КАК ПервоначальныеСведенияОСБУ
	|		ПО СписокОС.ОсновноеСредство = ПервоначальныеСведенияОСБУ.ОсновноеСредство
	|		ЛЕВОЕ СОЕДИНЕНИЕ ПервоначальныеСведенияОСНУ КАК ПервоначальныеСведенияОСНУ
	|		ПО СписокОС.ОсновноеСредство = ПервоначальныеСведенияОСНУ.ОсновноеСредство
	|		ЛЕВОЕ СОЕДИНЕНИЕ СчетаУчетаОС КАК СчетаУчетаОС
	|		ПО СписокОС.ОсновноеСредство = СчетаУчетаОС.ОсновноеСредство
	|		ЛЕВОЕ СОЕДИНЕНИЕ СтоимостьОС КАК СтоимостьОС
	|		ПО (СчетаУчетаОС.ОсновноеСредство = СтоимостьОС.ОсновноеСредство)
	|			И (СчетаУчетаОС.СчетУчета = СтоимостьОС.Счет)
	|		ЛЕВОЕ СОЕДИНЕНИЕ СтоимостьОС КАК АмортизацияОС
	|		ПО (СчетаУчетаОС.ОсновноеСредство = АмортизацияОС.ОсновноеСредство)
	|			И (СчетаУчетаОС.СчетНачисленияАмортизации = АмортизацияОС.Счет)
	|		ЛЕВОЕ СОЕДИНЕНИЕ ПодразделенияОС КАК ПодразделенияОС
	|		ПО СписокОС.ОсновноеСредство = ПодразделенияОС.ОсновноеСредство
	|		ЛЕВОЕ СОЕДИНЕНИЕ ТабАмортизации КАК ТабАмортизации
	|		ПО СписокОС.ОсновноеСредство = ТабАмортизации.ОбъектУчета
	|		ЛЕВОЕ СОЕДИНЕНИЕ ТабИзноса КАК ТабИзноса
	|		ПО СписокОС.ОсновноеСредство = ТабИзноса.ОсновноеСредство
	|		ЛЕВОЕ СОЕДИНЕНИЕ ПризнакТолькоЛинейный КАК ПризнакТолькоЛинейный
	|		ПО СписокОС.ОсновноеСредство = ПризнакТолькоЛинейный.ОсновноеСредство
	|		ЛЕВОЕ СОЕДИНЕНИЕ ТабРасходыПоАренднымПлатежамНУ КАК ТабРасходыПоАренднымПлатежамНУ
	|		ПО СписокОС.ОсновноеСредство = ТабРасходыПоАренднымПлатежамНУ.ОсновноеСредство
	|		ЛЕВОЕ СОЕДИНЕНИЕ ТабКорректировкаАмортизации КАК ТабКорректировкаАмортизации
	|		ПО СписокОС.ОсновноеСредство = ТабКорректировкаАмортизации.ОсновноеСредство
	|		ЛЕВОЕ СОЕДИНЕНИЕ КорректировкаСтоимости КАК КорректировкаСтоимости
	|		ПО СписокОС.ОсновноеСредство = КорректировкаСтоимости.ОсновноеСредство
	|
	|УПОРЯДОЧИТЬ ПО
	|	НомерСтроки";
	ТаблицаПараметрыСписания = Запрос.Выполнить().Выгрузить();

	// Параметры для процедуры СформироватьДвиженияРегистрацияСобытияОС
	ПараметрыСобытияОС = Новый Структура;
	ПараметрыСобытияОС.Вставить("ТаблицаРеквизиты", Параметры.Реквизиты);

	Параметры.ТаблицаОС.Колонки.Добавить("СуммаЗатратБУ", ОбщегоНазначения.ОписаниеТипаЧисло(15, 2));
	Параметры.ТаблицаОС.Колонки.Добавить("СуммаЗатратНУ", ОбщегоНазначения.ОписаниеТипаЧисло(15, 2));
	Параметры.ТаблицаОС.Колонки.Добавить("СуммаЗатратУСН", ОбщегоНазначения.ОписаниеТипаЧисло(15, 2));

	ПараметрыСобытияОС.Вставить("ТаблицаОС", Параметры.ТаблицаОС);

	ПараметрыВыбытия.Вставить("СобытияОС", ПараметрыСобытияОС);

	// Параметры для процедуры СформироватьДвиженияИзменениеПризнакаНачисленияАмортизацииОСБУ
	ПараметрыНачислениеАмортизацииОСБУ = Новый Структура;
	Параметры.Реквизиты.Колонки.Добавить("НачислятьАмортизацию", Новый ОписаниеТипов("Булево"));
	Параметры.Реквизиты.ЗаполнитьЗначения(Ложь, "НачислятьАмортизацию");
	ПараметрыНачислениеАмортизацииОСБУ.Вставить("ТаблицаРеквизиты", Параметры.Реквизиты);
	ТаблицаНачислениеАмортизацииОСБУ = ТаблицаПараметрыСписания.Скопировать(
		Новый Структура("ПрекратитьНачислениеАмортизацииБУ", Истина), "ОсновноеСредство");
	ПараметрыНачислениеАмортизацииОСБУ.Вставить("ТаблицаОС", ТаблицаНачислениеАмортизацииОСБУ);

	ПараметрыВыбытия.Вставить("НачислениеАмортизацииОСБУ", ПараметрыНачислениеАмортизацииОСБУ);

	// Параметры для процедура СформироватьДвиженияИзменениеПризнакаНачисленияАмортизацииОСНУ
	ОтражатьВНалоговомУчете = УчетнаяПолитика.ПлательщикНалогаНаПрибыль(Реквизиты.Организация, Реквизиты.Период);
	МетодНачисленияАмортизацииНУ = УчетнаяПолитика.МетодНачисленияАмортизацииНУ(Реквизиты.Организация, Реквизиты.Период);
	
	ПараметрыНачислениеАмортизацииОСНУ = Новый Структура;
	ПараметрыНачислениеАмортизацииОСНУ.Вставить("ТаблицаРеквизиты", Параметры.Реквизиты);
	
	Если ОтражатьВНалоговомУчете
		И МетодНачисленияАмортизацииНУ = Перечисления.МетодыНачисленияАмортизации.Нелинейный Тогда
		Отбор = Новый Структура("ПрекратитьНачислениеАмортизацииНУ, ТолькоЛинейный", Истина, Истина);
	Иначе
		Отбор = Новый Структура("ПрекратитьНачислениеАмортизацииНУ", Истина);
	КонецЕсли;
	ТаблицаНачислениеАмортизацииОСНУ = ТаблицаПараметрыСписания.Скопировать(Отбор, "ОсновноеСредство");
		
	ПараметрыНачислениеАмортизацииОСНУ.Вставить("ТаблицаОС", ТаблицаНачислениеАмортизацииОСНУ);
	ПараметрыВыбытия.Вставить("НачислениеАмортизацииОСНУ", ПараметрыНачислениеАмортизацииОСНУ);

	ПараметрыВыбытия.Вставить("ТаблицаПараметрыСписания", ТаблицаПараметрыСписания);

	Возврат ПараметрыВыбытия;

КонецФункции

// Взято из модуля УчетОСВызовСервера
Функция ПодготовитьПараметрыПодготовитьТаблицыСведенийПоВыбытиюОС(ТаблицаОС, ТаблицаРеквизиты)

	Параметры = Новый Структура;

	// Подготовка таблицы Параметры.ТаблицаОС

	СписокОбязательныхКолонок = ""
	+ "НомерСтроки,"      // <Число, 5, 0>
	+ "ОсновноеСредство," // <СправочникСсылка.ОбъектыЭксплуатации>
	+ "Регистратор";      // <ДокументСсылка.*>

	Параметры.Вставить("ТаблицаОС",
		УправлениеВнеоборотнымиАктивамиПереопределяемый.ПолучитьТаблицуПараметровПроведения(ТаблицаОС, СписокОбязательныхКолонок));

	// Подготовка таблицы Параметры.Реквизиты

	СписокОбязательныхКолонок = ""
	+ "Период,"        // <Дата>
	+ "ИмяСписка,"     // <Строка, 0>
	+ "Номер,"         // <Строка, 0> - номер документа-выбытия
	+ "Организация,"   // <СправочникСсылка.Организации>
	+ "Подразделение," // 
	+ "СобытиеОС,"     // <СправочникСсылка.СобытияОС> - событие с ОС, которым отражается выбытие
	+ "Содержание,"    // <Строка, 150>
	+ "Регистратор";   // <ДокументСсылка.*>

	Параметры.Вставить("Реквизиты",
		УправлениеВнеоборотнымиАктивамиПереопределяемый.ПолучитьТаблицуПараметровПроведения(ТаблицаРеквизиты, СписокОбязательныхКолонок));

	Возврат Параметры;

КонецФункции

// Взято из модуля УчетОСВызовСервера
Функция ПолучитьПустуюТаблицуАмортизацииОС()
	
	ТаблицаАмортизации = Новый ТаблицаЗначений();
	
	ТаблицаАмортизации.Колонки.Добавить("ОбъектУчета", Новый ОписаниеТипов("СправочникСсылка.ОбъектыЭксплуатации"));
	ТаблицаАмортизации.Колонки.Добавить("Подразделение", БухгалтерскийУчетКлиентСерверПереопределяемый.ОписаниеТиповПодразделения());
	
	ТаблицаАмортизации.Колонки.Добавить("СуммаБУ", ОбщегоНазначения.ОписаниеТипаЧисло(15,2));
	ТаблицаАмортизации.Колонки.Добавить("СуммаНУ", ОбщегоНазначения.ОписаниеТипаЧисло(15,2));
	ТаблицаАмортизации.Колонки.Добавить("СуммаВР", ОбщегоНазначения.ОписаниеТипаЧисло(15,2));
	ТаблицаАмортизации.Колонки.Добавить("СуммаПР", ОбщегоНазначения.ОписаниеТипаЧисло(15,2));
	
	ТаблицаАмортизации.Колонки.Добавить("Коэффициент", Новый ОписаниеТипов("Число"));
	
	ТаблицаАмортизации.Колонки.Добавить("СтатьяРасходов", Новый ОписаниеТипов("ПланВидовХарактеристикСсылка.СтатьиРасходов"));
	ТаблицаАмортизации.Колонки.Добавить("АналитикаРасходов", Метаданные.ПланыВидовХарактеристик.СтатьиРасходов.Тип);
	ТаблицаАмортизации.Колонки.Добавить("ПередаватьРасходыВДругуюОрганизацию", Новый ОписаниеТипов("Булево"));
	ТаблицаАмортизации.Колонки.Добавить("ОрганизацияПолучательРасходов", Новый ОписаниеТипов("СправочникСсылка.Организации"));
	ТаблицаАмортизации.Колонки.Добавить("СчетПередачиРасходов", Новый ОписаниеТипов("ПланСчетовСсылка.Хозрасчетный"));
	
	ТаблицаАмортизации.Колонки.Добавить("НачислятьИзнос", Новый ОписаниеТипов("Булево"));
	
	ТаблицаАмортизации.Колонки.Добавить("ЭтоЦелевыеСредства", Новый ОписаниеТипов("Булево"));
	
	ТаблицаАмортизации.Колонки.Добавить("ЭтоАмортизационнаяПремия", Новый ОписаниеТипов("Булево"));
	ТаблицаАмортизации.Колонки.Добавить("ДокументАмортизационнойПремии", ПланыВидовХарактеристик.ВидыСубконтоХозрасчетные.ДокументыАмортизационнойПремии.ТипЗначения);
	
	Возврат ТаблицаАмортизации;
	
КонецФункции

// Взято из модуля УчетОСВызовСервера
// Структура ПараметрыВыбытия может быть досрочно возвращена из ПодготовитьТаблицыСведенийПоВыбытиюОС,
// и чтобы "ПодготовитьПараметры*" не выдавали ошибки, надо заранее проинициализировать структуру
// всеми возможными значениями (пустыми таблицами значений с колонками и т.п.)
//
Функция ПолучитьИнициализированнуюСтруктуруПараметровВыбытия()

	ПараметрыВыбытия = Новый Структура;

	// СуммаКапитальныхВложенийВключаемыхВРасходы, число
	ПараметрыВыбытия.Вставить("СуммаКапитальныхВложенийВключаемыхВРасходы", 0);

	// НачислениеАмортизации, структура из двух таблиц: ТаблицаЗатрат и ТаблицаРеквизиты
	ПараметрыВыбытия.Вставить("НачислениеАмортизации",
		Новый Структура("ТаблицаЗатрат, ТаблицаРеквизиты",
			УправлениеВнеоборотнымиАктивамиПереопределяемый.ПолучитьПустуюТаблицуЗначенийСКолонками(Новый Структура("ОбъектУчета, ПодразделениеЗатрат, Подразделение,
				|Субконто1, Субконто2, Субконто3, СуммаБУ, СуммаНУ, СуммаПР, СуммаВР, СчетЗатрат, СчетАмортизации")), // ТаблицаЗатрат
			УправлениеВнеоборотнымиАктивамиПереопределяемый.ПолучитьПустуюТаблицуЗначенийСКолонками(Новый Структура("Период, Организация, Содержание")))); // ТаблицаРеквизиты

	// АмортизационнаяПремия, структура из двух таблиц: ТаблицаНачисленияАмортизационнойПремии и ТаблицаРеквизиты
	ПараметрыВыбытия.Вставить("АмортизационнаяПремия",
		Новый Структура("ТаблицаНачисленияАмортизационнойПремии, ТаблицаРеквизиты",
			УправлениеВнеоборотнымиАктивамиПереопределяемый.ПолучитьПустуюТаблицуЗначенийСКолонками(Новый Структура("ДокументАмортизационнойПремии, ОсновноеСредство,
				|Подразделение, ПодразделениеПоАмортизационнойПремии,
				|СубконтоПоАмортизационнойПремии1, СубконтоПоАмортизационнойПремии2, СубконтоПоАмортизационнойПремии3,
				|СуммаНУ, СуммаПР, СуммаВР, СчетУчета, СчетУчетаЗатратПоАмортизационнойПремии")), // ТаблицаНачисленияАмортизационнойПремии
			УправлениеВнеоборотнымиАктивамиПереопределяемый.ПолучитьПустуюТаблицуЗначенийСКолонками(Новый Структура("Период, Организация")))); // ТаблицаРеквизиты

	// НачислениеИзноса, структура из двух таблиц: ТаблицаИзноса и ТаблицаРеквизиты
	ПараметрыВыбытия.Вставить("НачислениеИзноса",
		Новый Структура("ТаблицаИзноса, ТаблицаРеквизиты",
			УправлениеВнеоборотнымиАктивамиПереопределяемый.ПолучитьПустуюТаблицуЗначенийСКолонками(Новый Структура("ОсновноеСредство, Подразделение, Сумма, СчетИзноса")), // ТаблицаИзноса
			УправлениеВнеоборотнымиАктивамиПереопределяемый.ПолучитьПустуюТаблицуЗначенийСКолонками(Новый Структура("Период, Организация, Регистратор")))); // ТаблицаРеквизиты

	// СобытияОС, структура из двух таблиц: ТаблицаРеквизиты и ТаблицаОС
	ПараметрыВыбытия.Вставить("СобытияОС",
		Новый Структура("ТаблицаОС, ТаблицаРеквизиты",
			УправлениеВнеоборотнымиАктивамиПереопределяемый.ПолучитьПустуюТаблицуЗначенийСКолонками(Новый Структура("ОсновноеСредство, СуммаЗатратБУ, СуммаЗатратНУ, СуммаЗатратУСН")), // ТаблицаОС
			УправлениеВнеоборотнымиАктивамиПереопределяемый.ПолучитьПустуюТаблицуЗначенийСКолонками(Новый Структура("Период, Номер, Организация, СобытиеОС, Регистратор")))); // ТаблицаРеквизиты

	// НачислениеАмортизацииОСБУ, структура из булево НачислятьАмортизацию и двух таблиц: ТаблицаРеквизиты и ТаблицаОС
	ПараметрыВыбытия.Вставить("НачислениеАмортизацииОСБУ",
		Новый Структура("НачислятьАмортизацию, ТаблицаОС, ТаблицаРеквизиты",
			Ложь,
			УправлениеВнеоборотнымиАктивамиПереопределяемый.ПолучитьПустуюТаблицуЗначенийСКолонками(Новый Структура("ОсновноеСредство")), // ТаблицаОС
			УправлениеВнеоборотнымиАктивамиПереопределяемый.ПолучитьПустуюТаблицуЗначенийСКолонками(Новый Структура("Период, НачислятьАмортизацию, Организация")))); // ТаблицаРеквизиты

	// НачислениеАмортизацииОСНУ, структура из булево НачислятьАмортизацию и двух таблиц: ТаблицаРеквизиты и ТаблицаОС
	ПараметрыВыбытия.Вставить("НачислениеАмортизацииОСНУ",
		Новый Структура("НачислятьАмортизацию, ТаблицаОС, ТаблицаРеквизиты",
			Ложь,
			УправлениеВнеоборотнымиАктивамиПереопределяемый.ПолучитьПустуюТаблицуЗначенийСКолонками(Новый Структура("ОсновноеСредство")), // ТаблицаОС
			УправлениеВнеоборотнымиАктивамиПереопределяемый.ПолучитьПустуюТаблицуЗначенийСКолонками(Новый Структура("Период, НачислятьАмортизацию, Организация")))); // ТаблицаРеквизиты

	// ТаблицаПараметрыСписания, таблица значений
	ПараметрыВыбытия.Вставить("ТаблицаПараметрыСписания", Новый ТаблицаЗначений); // Достаточно указать ТЗ без строк, колонки прописывать не нужно

	Возврат ПараметрыВыбытия;

КонецФункции

#КонецОбласти

// Гумановский С.В. 2018_08_09 начало

#Область ЗаведениеШтрихкодовДляОС

Процедура СоздатьШтрихкодыДля_ОС_ВСлучаеИхОтсутствия(ОсновныеСредства, Отказ = Ложь) Экспорт
	
	ОсновныеСредстваБезШтрихкодов = ОтобратьОсновныеСредстваБезШтрихкодов(ОсновныеСредства);
	
	Попытка
		
		Блокировка = Новый БлокировкаДанных();
		
		ЭлементБлокировки = Блокировка.Добавить("РегистрСведений.актШтрихкодыОС");
		ЭлементБлокировки.Режим = РежимБлокировкиДанных.Исключительный;
		
		Блокировка.Заблокировать();
		
		МаксимальныеЗначенияКодовШтучныхШтрихкодов = РегистрыСведений.актШтрихкодыОС.ПолучитьМаксимальныеЗначенияКодовШтучныхШтрихкодов();
		МаксимальныйКодШтучногоТовара              = РегистрыСведений.актШтрихкодыОС.МаксимальныйКодШтучногоТовара();
		
		Количество = 0;
		
		Для Каждого ОсновноеСредство Из ОсновныеСредстваБезШтрихкодов Цикл
			
			Количество = Количество + 1;
			
			Код = Неопределено;
			Диапазон = Неопределено;
							
			Для Каждого СтрокаТЧМаксимальныеЗначения Из МаксимальныеЗначенияКодовШтучныхШтрихкодов Цикл
				
				Если Не СтрокаТЧМаксимальныеЗначения.Код < МаксимальныйКодШтучногоТовара Тогда
					Продолжить;
				КонецЕсли;
				
				СтрокаТЧМаксимальныеЗначения.Код = СтрокаТЧМаксимальныеЗначения.Код + 1;
				
				Код      = СтрокаТЧМаксимальныеЗначения.Код; 
				Диапазон = СтрокаТЧМаксимальныеЗначения.Диапазон;
				
				Прервать;
			КонецЦикла;
			
			Если Код = Неопределено Тогда
				ВызватьИсключение РегистрыСведений.актШтрихкодыОС.ТекстСообщенияНетСвободныхКодовШтучныхШтрихкодов();
			КонецЕсли;
			
			Штрихкод = РегистрыСведений.актШтрихкодыОС.ПолучитьШтрихкодПоКоду(Код, Диапазон);
			
			НовыйШтрихкод = РегистрыСведений.актШтрихкодыОС.СоздатьМенеджерЗаписи();
			НовыйШтрихкод.ОсновноеСредство = ОсновноеСредство; 
			НовыйШтрихкод.Штрихкод = Штрихкод;
			НовыйШтрихкод.Записать();
			
		КонецЦикла;
		
	Исключение
		
		Отказ = Истина;
		
		ОписаниеОшибки = НСтр("ru = 'При записи штрихкодов произошла ошибка.
		                      |Запись штрихкодов не выполнена.
		                      |Дополнительное описание:
		                      |%ДополнительноеОписание%'");
		ОписаниеОшибки = СтрЗаменить(ОписаниеОшибки, "%ДополнительноеОписание%", ИнформацияОбОшибке().Описание);
		
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ОписаниеОшибки);
		
	КонецПопытки;
	
КонецПроцедуры

Функция ОтобратьОсновныеСредстваБезШтрихкодов(ОсновныеСредства)
	
	ОсновныеСредстваБезШтрихКодов = Новый Массив;
	
	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ
	               |	ОбъектыЭксплуатации.Ссылка КАК Ссылка
	               |ПОМЕСТИТЬ ТабОсновныхСредств
	               |ИЗ
	               |	Справочник.ОбъектыЭксплуатации КАК ОбъектыЭксплуатации
	               |ГДЕ
	               |	ОбъектыЭксплуатации.Ссылка В(&ОсновныеСредства)
	               |;
	               |
	               |////////////////////////////////////////////////////////////////////////////////
	               |ВЫБРАТЬ
	               |	ТабОсновныхСредств.Ссылка КАК Ссылка
	               |ИЗ
	               |	ТабОсновныхСредств КАК ТабОсновныхСредств
	               |		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.актШтрихкодыОС КАК актШтрихкодыОС
	               |		ПО ТабОсновныхСредств.Ссылка = актШтрихкодыОС.ОсновноеСредство
	               |			И (актШтрихкодыОС.Штрихкод <> """")
	               |ГДЕ
	               |	актШтрихкодыОС.Штрихкод ЕСТЬ NULL";
	Запрос.УстановитьПараметр("ОсновныеСредства", ОсновныеСредства);
	Выборка = Запрос.Выполнить().Выбрать();
	
	Пока Выборка.Следующий() Цикл
		ОсновныеСредстваБезШтрихКодов.Добавить(Выборка.Ссылка);
	КонецЦикла;
	
	Возврат ОсновныеСредстваБезШтрихКодов;
	
КонецФункции

#КонецОбласти
// Гумановский С.В. 2018_08_09 конец
