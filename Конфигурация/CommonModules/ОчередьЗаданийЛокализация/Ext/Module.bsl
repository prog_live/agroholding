﻿
#Область ПрограммныйИнтерфейс

// Формирует список шаблонов заданий очереди.
//
// см. ОчередьЗаданийПереопределяемый.ПриПолученииСпискаШаблонов()
//
Процедура ПриПолученииСпискаШаблонов(ШаблоныЗаданий) Экспорт
	
	//++ Локализация
	// ИнтеграцияС1СДокументооборотом
	ШаблоныЗаданий.Добавить(Метаданные.РегламентныеЗадания.ИнтеграцияС1СДокументооборотВыполнитьОбменДанными.Имя);
	// Конец ИнтеграцияС1СДокументооборотом
	
	// ЭлектронноеВзаимодействие
	ЭлектронноеВзаимодействие.ПриПолученииСпискаШаблонов(ШаблоныЗаданий);
	// Конец ЭлектронноеВзаимодействие
	
	//++ НЕ УТ
	ОбщегоНазначенияБРО.ПриПолученииСпискаШаблонов(ШаблоныЗаданий);
	ЗарплатаКадры.ПриПолученииСпискаШаблоновОчередиЗаданий(ШаблоныЗаданий);
	//-- НЕ УТ	
	ШаблоныЗаданий.Добавить(Метаданные.РегламентныеЗадания.АрхивированиеЧековККМ.Имя);	
	//++ НЕ УТКА
	ШаблоныЗаданий.Добавить(Метаданные.РегламентныеЗадания.ОтражениеДокументовВМеждународномУчете.Имя);
	//-- НЕ УТКА	
	//++ НЕ УТ
	ШаблоныЗаданий.Добавить(Метаданные.РегламентныеЗадания.ОтражениеДокументовВРеглУчете.Имя);
	ШаблоныЗаданий.Добавить(Метаданные.РегламентныеЗадания.СписаниеЗатратНаВыпуск.Имя);
	//-- НЕ УТ
	ШаблоныЗаданий.Добавить(Метаданные.РегламентныеЗадания.УдалениеОтложенныхЧековККМ.Имя);
	ШаблоныЗаданий.Добавить(Метаданные.РегламентныеЗадания.УдалениеЧековККМ.Имя);
	
	//++ НЕ ГОСИС
	ШаблоныЗаданий.Добавить(Метаданные.РегламентныеЗадания.ОтправкаПолучениеДанныхВЕТИС.Имя);
	ШаблоныЗаданий.Добавить(Метаданные.РегламентныеЗадания.СверткаРегистраСоответствиеНоменклатурыВЕТИС.Имя);
	ШаблоныЗаданий.Добавить(Метаданные.РегламентныеЗадания.СверткаРегистраСоответствиеНоменклатурыЕГАИС.Имя);
	//-- НЕ ГОСИС
	//-- Локализация
	
КонецПроцедуры

// Заполняет соответствие имен методов их псевдонимам для вызова из очереди заданий.
//
// см. ОчередьЗаданийПереопределяемый.ПриОпределенииПсевдонимовОбработчиков()
//
Процедура ПриОпределенииПсевдонимовОбработчиков(СоответствиеИменПсевдонимам) Экспорт
	
	//++ Локализация
	//++ НЕ УТ
	ОбщегоНазначенияБРО.ПриОпределенииПсевдонимовОбработчиков(СоответствиеИменПсевдонимам);
	ЗарплатаКадры.ПриОпределенииПсевдонимовОбработчиков(СоответствиеИменПсевдонимам);
	//-- НЕ УТ
	
	// ЭлектронноеВзаимодействие
	ЭлектронноеВзаимодействие.ПриОпределенииПсевдонимовОбработчиков(СоответствиеИменПсевдонимам);
	// Конец ЭлектронноеВзаимодействие
	
	СоответствиеИменПсевдонимам.Вставить(Метаданные.РегламентныеЗадания.ЗакрытиеМесяца.ИмяМетода);
	СоответствиеИменПсевдонимам.Вставить(Метаданные.РегламентныеЗадания.ОбменССайтом.ИмяМетода);
	
	//++ НЕ УТ
	СоответствиеИменПсевдонимам.Вставить(Метаданные.РегламентныеЗадания.СписаниеЗатратНаВыпуск.ИмяМетода);	
	//-- НЕ УТ
	
	//++ НЕ УТКА
	СоответствиеИменПсевдонимам.Вставить(Метаданные.РегламентныеЗадания.РасчетГрафикаПроизводства.ИмяМетода);
	//-- НЕ УТКА
	
	//++ НЕ ГОСИС
	СоответствиеИменПсевдонимам.Вставить(Метаданные.РегламентныеЗадания.ОтправкаПолучениеДанныхВЕТИС.ИмяМетода);
	СоответствиеИменПсевдонимам.Вставить(Метаданные.РегламентныеЗадания.СверткаРегистраСоответствиеНоменклатурыВЕТИС.ИмяМетода);
	СоответствиеИменПсевдонимам.Вставить(Метаданные.РегламентныеЗадания.СверткаРегистраСоответствиеНоменклатурыЕГАИС.ИмяМетода);
	//-- НЕ ГОСИС
	
	//-- Локализация
	
КонецПроцедуры

#КонецОбласти