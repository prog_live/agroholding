﻿
// Определяет список объектов конфигурации, в модулях менеджеров которых предусмотрена процедура 
// ДобавитьКомандыСозданияНаОсновании, формирующая команды создания на основании объектов.
// Синтаксис процедуры ДобавитьКомандыСозданияНаОсновании см. в документации.
//
// см. ОбщийМодуль.СозданиеНаОснованииПереопределяемый.ПриОпределенииОбъектовСКомандамиСозданияНаОсновании()
//   
Процедура ПриОпределенииОбъектовСКомандамиСозданияНаОсновании(Объекты) Экспорт
	
	//++ Локализация
	УчетНДСУП.ПриОпределенииОбъектовСКомандамиСозданияНаОсновании(Объекты);
	
	//++ НЕ УТ
	
	// РегламентированныйУчетПодсистемы.БухгалтерскийУчет
	Объекты.Добавить(Метаданные.Документы.ОперацияБух);
	// Конец РегламентированныйУчетПодсистемы.БухгалтерскийУчет
	
	// ЗарплатаКадрыПриложения.ОтражениеВФинансовомУчете
	Объекты.Добавить(Метаданные.Документы.ОтражениеЗарплатыВФинансовомУчете);
	// Конец ЗарплатаКадрыПриложения.ОтражениеВФинансовомУчете
	
	Объекты.Добавить(Метаданные.Документы.АмортизацияНМА);
	Объекты.Добавить(Метаданные.Документы.АмортизацияОС);
	Объекты.Добавить(Метаданные.Документы.ВводОстатковВнеоборотныхАктивов);
	Объекты.Добавить(Метаданные.Документы.ВозвратМатериаловИзПроизводства);
	Объекты.Добавить(Метаданные.Документы.ВозвратОСОтАрендатора);
	Объекты.Добавить(Метаданные.Документы.ВыбытиеАрендованныхОС);
	Объекты.Добавить(Метаданные.Документы.ВыбытиеДенежныхДокументов);
	Объекты.Добавить(Метаданные.Документы.ВыпускПродукции);
	Объекты.Добавить(Метаданные.Документы.ВыработкаНМА);
	Объекты.Добавить(Метаданные.Документы.ДепонированиеЗарплаты);
	Объекты.Добавить(Метаданные.Документы.ЗаписьКУДиР);
	Объекты.Добавить(Метаданные.Документы.ИзделияИЗатратыНЗП);
	Объекты.Добавить(Метаданные.Документы.ИзменениеПараметровНМА);
	Объекты.Добавить(Метаданные.Документы.ИзменениеПараметровОС);
	Объекты.Добавить(Метаданные.Документы.ИзменениеСостоянияОС);
	Объекты.Добавить(Метаданные.Документы.ИзменениеСпособаОтраженияИмущественныхНалогов);
	Объекты.Добавить(Метаданные.Документы.ИнвентаризацияОС);
	Объекты.Добавить(Метаданные.Документы.ИнвентаризацияНМА);
	Объекты.Добавить(Метаданные.Документы.КонтролируемаяСделка);
	Объекты.Добавить(Метаданные.Документы.МодернизацияОС);
	Объекты.Добавить(Метаданные.Документы.НачислениеДивидендов);
	Объекты.Добавить(Метаданные.Документы.ОтменаРегистрацииЗемельныхУчастков);
	Объекты.Добавить(Метаданные.Документы.ОтменаРегистрацииТранспортныхСредств);
	Объекты.Добавить(Метаданные.Документы.ПередачаМатериаловВПроизводство);
	Объекты.Добавить(Метаданные.Документы.ПередачаОСАрендатору);
	Объекты.Добавить(Метаданные.Документы.ПеремещениеВЭксплуатации);
	Объекты.Добавить(Метаданные.Документы.ПеремещениеМатериаловВПроизводстве);
	Объекты.Добавить(Метаданные.Документы.ПеремещениеОС);
	Объекты.Добавить(Метаданные.Документы.ПереоценкаНМА);
	Объекты.Добавить(Метаданные.Документы.ПереоценкаОС);
	Объекты.Добавить(Метаданные.Документы.ПогашениеСтоимостиТМЦВЭксплуатации);
	Объекты.Добавить(Метаданные.Документы.ПодготовкаКПередачеНМА);
	Объекты.Добавить(Метаданные.Документы.ПодготовкаКПередачеОС);
	Объекты.Добавить(Метаданные.Документы.ПоступлениеАрендованныхОС);
	Объекты.Добавить(Метаданные.Документы.ПоступлениеДенежныхДокументов);
	Объекты.Добавить(Метаданные.Документы.ПоступлениеПредметовЛизинга);
	Объекты.Добавить(Метаданные.Документы.ПриобретениеУслугПоЛизингу);
	Объекты.Добавить(Метаданные.Документы.ПринятиеКУчетуНМА);
	Объекты.Добавить(Метаданные.Документы.ПринятиеКУчетуОС);
	Объекты.Добавить(Метаданные.Документы.РегистрацияЗемельныхУчастков);
	Объекты.Добавить(Метаданные.Документы.НаработкаТМЦВЭксплуатации);
	Объекты.Добавить(Метаданные.Документы.РегистрацияПорядкаНалогообложенияПоНалогуНаИмущество);
	Объекты.Добавить(Метаданные.Документы.РегистрацияПрочихКонтролируемыхСделок);
	Объекты.Добавить(Метаданные.Документы.РегистрацияТранспортныхСредств);
	Объекты.Добавить(Метаданные.Документы.РегламентнаяОперация);
	Объекты.Добавить(Метаданные.Документы.СписаниеЗатратНаВыпуск);
	Объекты.Добавить(Метаданные.Документы.СписаниеИзЭксплуатации);
	Объекты.Добавить(Метаданные.Документы.СписаниеНМА);
	Объекты.Добавить(Метаданные.Документы.СписаниеОС);
	Объекты.Добавить(Метаданные.Документы.УведомлениеОКонтролируемыхСделках);
	Объекты.Добавить(Метаданные.Справочники.НематериальныеАктивы);
	Объекты.Добавить(Метаданные.Справочники.ОбъектыЭксплуатации);
	Объекты.Добавить(Метаданные.Справочники.ДоговорыЛизинга);
	
	Объекты.Добавить(Метаданные.Документы.АмортизацияНМА2_4);
	Объекты.Добавить(Метаданные.Документы.АмортизацияОС2_4);
	Объекты.Добавить(Метаданные.Документы.ВводОстатковВнеоборотныхАктивов2_4);
	Объекты.Добавить(Метаданные.Документы.ВозвратОСОтАрендатора2_4);
	Объекты.Добавить(Метаданные.Документы.ИзменениеПараметровНМА2_4);
	Объекты.Добавить(Метаданные.Документы.ИзменениеПараметровОС2_4);
	Объекты.Добавить(Метаданные.Документы.МодернизацияОС2_4);
	Объекты.Добавить(Метаданные.Документы.ПередачаОСАрендатору2_4);
	Объекты.Добавить(Метаданные.Документы.ПеремещениеНМА2_4);
	Объекты.Добавить(Метаданные.Документы.ПеремещениеОС2_4);
	Объекты.Добавить(Метаданные.Документы.ПереоценкаОС2_4);
	Объекты.Добавить(Метаданные.Документы.ПодготовкаКПередачеНМА2_4);
	Объекты.Добавить(Метаданные.Документы.ПодготовкаКПередачеОС2_4);
	Объекты.Добавить(Метаданные.Документы.ПринятиеКУчетуНМА2_4);
	Объекты.Добавить(Метаданные.Документы.ПринятиеКУчетуОС2_4);
	Объекты.Добавить(Метаданные.Документы.РазукомплектацияОС);
	Объекты.Добавить(Метаданные.Документы.СписаниеНМА2_4);
	Объекты.Добавить(Метаданные.Документы.СписаниеОС2_4);
	Объекты.Добавить(Метаданные.Документы.ОтчетОператораСистемыПлатон);
	//-- НЕ УТ
	
	//++ НЕ УТКА
	Объекты.Добавить(Метаданные.Документы.АмортизацияНМАМеждународныйУчет);
	Объекты.Добавить(Метаданные.Документы.АмортизацияОСМеждународныйУчет);
	Объекты.Добавить(Метаданные.Документы.ВводОстатковНМАМеждународныйУчет);
	Объекты.Добавить(Метаданные.Документы.ВводОстатковОСМеждународныйУчет);
	Объекты.Добавить(Метаданные.Документы.ДоступностьВидаРабочихЦентров);
	Объекты.Добавить(Метаданные.Документы.ЗаказНаПроизводство);
	Объекты.Добавить(Метаданные.Документы.ИзменениеПараметровНМАМеждународныйУчет);
	Объекты.Добавить(Метаданные.Документы.ИзменениеПараметровОСМеждународныйУчет);
	Объекты.Добавить(Метаданные.Документы.КорректировкаЗаказаМатериаловВПроизводство);
	Объекты.Добавить(Метаданные.Документы.МаршрутныйЛистПроизводства);
	Объекты.Добавить(Метаданные.Документы.ОперацияМеждународный);
	Объекты.Добавить(Метаданные.Документы.ПереводОсновныхСредствИнвестиционногоИмуществаМеждународныйУчет);
	Объекты.Добавить(Метаданные.Документы.ПеремещениеНМАМеждународныйУчет);
	Объекты.Добавить(Метаданные.Документы.ПеремещениеОСМеждународныйУчет);
	Объекты.Добавить(Метаданные.Документы.ПлановаяКалькуляция);
	Объекты.Добавить(Метаданные.Документы.ПринятиеКУчетуНМАМеждународныйУчет);
	Объекты.Добавить(Метаданные.Документы.ПринятиеКУчетуОСМеждународныйУчет);
	Объекты.Добавить(Метаданные.Документы.РегламентнаяОперацияМеждународныйУчет);
	Объекты.Добавить(Метаданные.Документы.СписаниеНМАМеждународныйУчет);
	Объекты.Добавить(Метаданные.Документы.СписаниеОСМеждународныйУчет);	
	Объекты.Добавить(Метаданные.Документы.ЭкземплярФинансовогоОтчета);
	//-- НЕ УТКА
	
	Объекты.Добавить(Метаданные.Документы.АннулированиеПодарочныхСертификатов);
	Объекты.Добавить(Метаданные.Документы.ВнесениеДенежныхСредствВКассуККМ);
	Объекты.Добавить(Метаданные.Документы.ВозвратПодарочныхСертификатов);
	Объекты.Добавить(Метаданные.Документы.ВыемкаДенежныхСредствИзКассыККМ);
	Объекты.Добавить(Метаданные.Документы.ЛистКассовойКниги);
	Объекты.Добавить(Метаданные.Документы.РеализацияПодарочныхСертификатов);
	Объекты.Добавить(Метаданные.Документы.ТранспортнаяНакладная);
	
	// ЭлектронноеВзаимодействие.ИнтеграцияСЯндексКассой
	Объекты.Добавить(Метаданные.Документы.ОперацияПоЯндексКассе);
	// Конец ЭлектронноеВзаимодействие.ИнтеграцияСЯндексКассой
	
	// ЭлектронноеВзаимодействие.КоммерческиеПредложения
	Объекты.Добавить(Метаданные.Документы.КоммерческоеПредложениеПоставщика);
	// Конец ЭлектронноеВзаимодействие.КоммерческиеПредложения
		
	//-- Локализация
	
КонецПроцедуры


// Вызывается для формирования списка команд создания на основании КомандыСозданияНаОсновании, однократно для при первой
// необходимости, а затем результат кэшируется с помощью модуля с повторным использованием возвращаемых значений.
// Здесь можно определить команды создания на основании, общие для большинства объектов конфигурации.
//
// см. ОбщийМодуль.СозданиеНаОснованииПереопределяемый.ПриОпределенииОбъектовСКомандамиСозданияНаОсновании()
//
Процедура ПередДобавлениемКомандСозданияНаОсновании(КомандыСозданияНаОсновании, Параметры, СтандартнаяОбработка) Экспорт
	
	//++ Локализация
	Если ПравоДоступа("Просмотр", Метаданные.Обработки.ИнтеграцияС1СДокументооборот) Тогда
		КомандаСозданияНаОсновании = КомандыСозданияНаОсновании.Добавить();
		КомандаСозданияНаОсновании.Обработчик = "СозданиеНаОснованииУТКлиент.ИнтеграцияС1СДокументооборотСоздатьПисьмо";
		КомандаСозданияНаОсновании.Идентификатор = "ИнтеграцияС1СДокументооборотСоздатьПисьмо";
		КомандаСозданияНаОсновании.Представление = НСтр("ru = 'Документооборот: Письмо';
														|en = 'Data interchange: Email'");
		КомандаСозданияНаОсновании.РежимЗаписи = ?(Параметры.ВидВРег = "ДОКУМЕНТ", "Проводить", "Записывать");
		КомандаСозданияНаОсновании.ФункциональныеОпции = "ИспользоватьЭлектроннуюПочту1СДокументооборота";
		КомандаСозданияНаОсновании.Порядок = 98;
		
		КомандаСозданияНаОсновании = КомандыСозданияНаОсновании.Добавить();
		КомандаСозданияНаОсновании.Обработчик = "СозданиеНаОснованииУТКлиент.ИнтеграцияС1СДокументооборотСоздатьБизнесПроцесс";
		КомандаСозданияНаОсновании.Идентификатор = "ИнтеграцияС1СДокументооборотСоздатьБизнесПроцесс";
		КомандаСозданияНаОсновании.Представление = НСтр("ru = 'Документооборот: Процесс...';
														|en = 'Data interchange: Process ...'");
		КомандаСозданияНаОсновании.РежимЗаписи = ?(Параметры.ВидВРег = "ДОКУМЕНТ", "Проводить", "Записывать");
		КомандаСозданияНаОсновании.ФункциональныеОпции = "ИспользоватьПроцессыИЗадачи1СДокументооборота";
		КомандаСозданияНаОсновании.Порядок = 99;
	КонецЕсли;
	//-- Локализация
	
КонецПроцедуры

Функция ДобавитьКомандуСоздатьНаОснованииПисьмоОбменСБанками(КомандыСозданияНаОсновании) Экспорт

	//++ Локализация
	Если ПравоДоступа("Добавление", Метаданные.Документы.ПисьмоОбменСБанками) Тогда
		КомандаСоздатьНаОсновании = КомандыСозданияНаОсновании.Добавить();
		КомандаСоздатьНаОсновании.Менеджер = Метаданные.Документы.ПисьмоОбменСБанками.ПолноеИмя();
		КомандаСоздатьНаОсновании.Представление = ОбщегоНазначенияУТ.ПредставлениеОбъекта(Метаданные.Документы.ПисьмоОбменСБанками);
		КомандаСоздатьНаОсновании.РежимЗаписи = "Проводить";
		
		Возврат КомандаСоздатьНаОсновании;
	КонецЕсли;
	//-- Локализация
	
	Возврат Неопределено;
КонецФункции