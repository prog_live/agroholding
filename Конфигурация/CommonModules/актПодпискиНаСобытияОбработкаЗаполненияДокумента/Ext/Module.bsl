﻿
////////////////////////////////////////////////////////////////////////////////
// Модуль содержит процедуры и функции подписок для всех справочников. Автоор Кудаков Р.И.
//  
////////////////////////////////////////////////////////////////////////////////

#Область СлужебныйПрограммныйИнтерфейс

// Вызывается из подписки актПередЗаписьюСправочникаУниверсальный
Процедура ОбработкаЗаполненияЛюбогоДокумента(Источник, ДанныеЗаполнения, ТекстЗаполнения, СтандартнаяОбработка) Экспорт
	
	Если ТипЗнч(ДанныеЗаполнения) = Тип("Структура") 
		И ДанныеЗаполнения.Свойство("ДокументОснование")
		И ДанныеЗаполнения.ДокументОснование.Количество() > 0
		И ТипЗнч(ДанныеЗаполнения.ДокументОснование[0]) = Тип("ДокументСсылка.ЗаказКлиента") Тогда
		
		актЛогистикаСервер.актРеализацияТоваровУслугОбработкаЗаполнения(Источник, ДанныеЗаполнения, ТекстЗаполнения, СтандартнаяОбработка);
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти