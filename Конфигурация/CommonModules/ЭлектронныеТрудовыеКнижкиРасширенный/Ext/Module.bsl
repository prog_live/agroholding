﻿#Область СлужебныйПрограммныйИнтерфейс

// ЗарплатаКадрыРасширеннаяПодсистемы.ИсправленияДокументов

Процедура ПриИсправленииТрудовойДеятельности(ИсправленныйДокумент) Экспорт
	
	МетаданныеДокумента = ИсправленныйДокумент.Метаданные();
	Если МетаданныеДокумента.Движения.Содержит(Метаданные.РегистрыСведений.МероприятияТрудовойДеятельности) Тогда
		
		НаборЗаписей = РегистрыСведений.МероприятияТрудовойДеятельности.СоздатьНаборЗаписей();
		НаборЗаписей.Отбор.Регистратор.Установить(ИсправленныйДокумент);
		НаборЗаписей.Записать();
		
	КонецЕсли;
	
КонецПроцедуры

Процедура ОтменитьИсправлениеТрудовойДеятельности(Источник, Отказ) Экспорт
	
	ИсправленныйДокумент = Источник.ИсправленныйДокумент;
	
	Если ЗначениеЗаполнено(ИсправленныйДокумент) Тогда
		
		МетаданныеДокумента = ИсправленныйДокумент.Метаданные();
		Если МетаданныеДокумента.Движения.Содержит(Метаданные.РегистрыСведений.МероприятияТрудовойДеятельности) Тогда
			
			МенеджерДокумента = ОбщегоНазначения.МенеджерОбъектаПоПолномуИмени(МетаданныеДокумента.ПолноеИмя());
			МероприятияТрудовойДеятельности = МенеджерДокумента.ДанныеДляПроведенияМероприятияТрудовойДеятельности(
				ИсправленныйДокумент).Получить(ИсправленныйДокумент);
			
			НаборЗаписей = РегистрыСведений.МероприятияТрудовойДеятельности.СоздатьНаборЗаписей();
			НаборЗаписей.Отбор.Регистратор.Установить(ИсправленныйДокумент);
			
			МенеджерДокумента.СформироватьДвиженияМероприятийТрудовойДеятельности(
				НаборЗаписей, МероприятияТрудовойДеятельности);
			
			НаборЗаписей.Записать();
			
		КонецЕсли;
		
	КонецЕсли;
	
КонецПроцедуры

Процедура ПриЗаполненииДокументаИсправления(ДокументИсправление, ИсправляемыйДокумент) Экспорт
	
	МетаданныеДокумента = ДокументИсправление.Метаданные();
	Если ОбщегоНазначения.ЕстьРеквизитОбъекта("НомерПриказа", МетаданныеДокумента) Тогда
		РеквизитыИсправления = ОбщегоНазначения.ЗначенияРеквизитовОбъекта(ИсправляемыйДокумент, "Номер,НомерПриказа");
		ДокументИсправление.НомерПриказа = ЗарплатаКадрыОтчеты.НомерНаПечать(РеквизитыИсправления.Номер, РеквизитыИсправления.НомерПриказа);
	КонецЕсли;
	
КонецПроцедуры

// Конец ЗарплатаКадрыРасширеннаяПодсистемы.ИсправленияДокументов

Функция РазрядКатегорияВидимость() Экспорт
	
	Возврат ПолучитьФункциональнуюОпцию("ИспользоватьРазрядыКатегорииКлассыДолжностейИПрофессийВШтатномРасписании")
		Или ПолучитьФункциональнуюОпцию("ИспользоватьКвалификационнуюНадбавку")
		Или ПолучитьФункциональнуюОпцию("ИспользоватьТарифныеСеткиПриРасчетеЗарплатыХозрасчет");
	
КонецФункции

Функция КодПоРееструДолжностейВидимость() Экспорт
	
	Возврат ПерсонифицированныйУчетРасширенный.ИспользоватьЗамещениеГосударственныхМуниципальныхДолжностей();
	
КонецФункции

Процедура УстановитьОтборПараметровПолученияСотрудниковОрганизации(ПараметрыПолучения) Экспорт
	
	Если ОбщегоНазначения.ПодсистемаСуществует("ЗарплатаКадрыПриложения.ГосударственнаяСлужба") Тогда
		
		МодульГосударственнаяСлужба = ОбщегоНазначения.ОбщийМодуль("ГосударственнаяСлужба");
		МодульГосударственнаяСлужба.УстановитьОтборПараметровПолученияСотрудниковОрганизации
			(ПараметрыПолучения);
		
	КонецЕсли;
	
КонецПроцедуры

Функция ИменаКадровыхДанныхСотрудниковДляНачалаУчета() Экспорт
	
	Возврат ЭлектронныеТрудовыеКнижкиБазовый.ИменаКадровыхДанныхСотрудниковДляНачалаУчета() + ",РазрядКатегория";
	
КонецФункции

Процедура ДополнитьМероприятияЭТКДаннымиРеестраКадровыхПриказов(ДанныеСотрудниковБезМероприятий) Экспорт
	
	Сотрудники = Новый Массив;
	Для Каждого ДанныеСотрудника Из ДанныеСотрудниковБезМероприятий Цикл
		
		Если Не ЗначениеЗаполнено(ДанныеСотрудника.ВидМероприятия) Тогда
			Сотрудники.Добавить(ДанныеСотрудника.СотрудникЗаписи);
		КонецЕсли;
		
	КонецЦикла;
	
	Запрос = Новый Запрос;
	Запрос.МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;
	
	Запрос.УстановитьПараметр("НаименованиеДокументаОснования", ЭлектронныеТрудовыеКнижки.НаименованиеДокументаОснования());
	Запрос.УстановитьПараметр("Сотрудники", Сотрудники);
	Запрос.УстановитьПараметр("ВидыДоговоров", Перечисления.ВидыДоговоровССотрудниками.ВидыДоговоровВоеннойСлужбы());
	
	Запрос.Текст =
		"ВЫБРАТЬ
		|	РеестрКадровыхПриказов.Сотрудник КАК СотрудникЗаписи,
		|	РеестрКадровыхПриказов.ФизическоеЛицо КАК Сотрудник,
		|	РеестрКадровыхПриказов.Организация КАК Организация,
		|	РеестрКадровыхПриказов.ДокументОснование КАК ДокументОснование,
		|	РеестрКадровыхПриказов.Дата КАК ДатаМероприятия,
		|	РеестрКадровыхПриказов.ВидДоговора КАК ВидДоговора,
		|	РеестрКадровыхПриказов.ВидСобытия КАК ВидСобытия,
		|	РеестрКадровыхПриказов.ДатаПриказа КАК ДатаДокументаОснования,
		|	РеестрКадровыхПриказов.НомерПриказа КАК НомерДокументаОснования
		|ПОМЕСТИТЬ ВТРеестрКадровыхПриказов
		|ИЗ
		|	РегистрСведений.РеестрКадровыхПриказов КАК РеестрКадровыхПриказов
		|ГДЕ
		|	РеестрКадровыхПриказов.Сотрудник В(&Сотрудники)
		|	И РеестрКадровыхПриказов.Дата < ДАТАВРЕМЯ(2020, 1, 1)
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	РеестрКадровыхПриказов.СотрудникЗаписи КАК СотрудникЗаписи,
		|	МАКСИМУМ(РеестрКадровыхПриказов.ДатаМероприятия) КАК ДатаМероприятия
		|ПОМЕСТИТЬ ВТПоследниеДатыМероприятий
		|ИЗ
		|	ВТРеестрКадровыхПриказов КАК РеестрКадровыхПриказов
		|
		|СГРУППИРОВАТЬ ПО
		|	РеестрКадровыхПриказов.СотрудникЗаписи
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	РеестрКадровыхПриказов.СотрудникЗаписи КАК СотрудникЗаписи,
		|	РеестрКадровыхПриказов.ДатаМероприятия КАК ДатаМероприятия,
		|	ВЫБОР
		|		КОГДА РеестрКадровыхПриказов.ВидСобытия = ЗНАЧЕНИЕ(Перечисление.ВидыКадровыхСобытий.Прием)
		|			ТОГДА ЗНАЧЕНИЕ(Перечисление.ВидыМероприятийТрудовойДеятельности.Прием)
		|		КОГДА РеестрКадровыхПриказов.ВидСобытия = ЗНАЧЕНИЕ(Перечисление.ВидыКадровыхСобытий.Перемещение)
		|			ТОГДА ЗНАЧЕНИЕ(Перечисление.ВидыМероприятийТрудовойДеятельности.Перевод)
		|		ИНАЧЕ ЗНАЧЕНИЕ(Перечисление.ВидыМероприятийТрудовойДеятельности.ПустаяСсылка)
		|	КОНЕЦ КАК ВидМероприятия,
		|	&НаименованиеДокументаОснования КАК НаименованиеДокументаОснования,
		|	РеестрКадровыхПриказов.ДатаДокументаОснования КАК ДатаДокументаОснования,
		|	РеестрКадровыхПриказов.НомерДокументаОснования КАК НомерДокументаОснования
		|ИЗ
		|	ВТПоследниеДатыМероприятий КАК ПоследниеДатыМероприятий
		|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ ВТРеестрКадровыхПриказов КАК РеестрКадровыхПриказов
		|		ПО ПоследниеДатыМероприятий.СотрудникЗаписи = РеестрКадровыхПриказов.СотрудникЗаписи
		|			И ПоследниеДатыМероприятий.ДатаМероприятия = РеестрКадровыхПриказов.ДатаМероприятия
		|ГДЕ
		|	НЕ РеестрКадровыхПриказов.ВидДоговора В (&ВидыДоговоров)";
	
	ДанныеРеестра = Запрос.Выполнить().Выгрузить();
	Для Каждого ДанныеСотрудника Из ДанныеСотрудниковБезМероприятий Цикл
		
		СтрокаРеестра = ДанныеРеестра.Найти(ДанныеСотрудника.СотрудникЗаписи, "СотрудникЗаписи");
		Если СтрокаРеестра <> Неопределено Тогда
			ЗаполнитьЗначенияСвойств(ДанныеСотрудника, СтрокаРеестра);
		КонецЕсли;
		
	КонецЦикла;
	
КонецПроцедуры

Процедура УточнитьЗапросПолученияДанныхНаНачалоУчета(Запрос) Экспорт
	
	Запрос.Текст = СтрЗаменить(Запрос.Текст, "&РазрядКатегория", "СотрудникиОрганизации.РазрядКатегория");
	
КонецПроцедуры

Процедура УстановитьВидимостьПолейОтраженияТрудовойДеятельностиВоеннослужащих(УправляемаяФорма) Экспорт
	
	ОтражениеТрудовойДеятельностиГруппаВидимость = Ложь;
	Если ОбщегоНазначения.ПодсистемаСуществует("ЗарплатаКадрыПриложения.ГосударственнаяСлужба") Тогда
		МодульГосударственнаяСлужба = ОбщегоНазначения.ОбщийМодуль("ГосударственнаяСлужба");
		ОтражениеТрудовойДеятельностиГруппаВидимость = МодульГосударственнаяСлужба.ОтражатьТрудовуюДеятельностьВоеннослужащих();
	КонецЕсли;
	
	ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(
		УправляемаяФорма.Элементы,
		"ОтражениеТрудовойДеятельностиГруппа",
		"Видимость",
		ОтражениеТрудовойДеятельностиГруппаВидимость);
	
КонецПроцедуры

Функция КодДолжностиПоРееструГосударственнойСлужбы(Должность) Экспорт
	
	Если ОбщегоНазначения.ПодсистемаСуществует("ЗарплатаКадрыПриложения.ГосударственнаяСлужба") Тогда
		
		МодульГосударственнаяСлужба = ОбщегоНазначения.ОбщийМодуль("ГосударственнаяСлужба");
		Возврат МодульГосударственнаяСлужба.КодДолжностиПоРееструГосударственнойСлужбы(Должность);
		
	КонецЕсли;
	
	Возврат ЭлектронныеТрудовыеКнижкиБазовый.КодДолжностиПоРееструГосударственнойСлужбы(Должность);
	
КонецФункции

Процедура ДополнитьЗапросПолучениемРазрядовКатегорийПозицийШтатногоРасписания(Запрос, ПКУВДанныхДокументов) Экспорт
	
	Если ПКУВДанныхДокументов
		И ПолучитьФункциональнуюОпцию("РаботаВБюджетномУчреждении")
		И ПолучитьФункциональнуюОпцию("РазрешеноИзменениеПКУВКадровыхДокументах") Тогда
		Если ОбщегоНазначения.ПодсистемаСуществует("ЗарплатаКадрыПриложения.Медицина") 
			И ПолучитьФункциональнуюОпцию("РаботаВМедицинскомУчреждении") Тогда
			Возврат;
		КонецЕсли;
		Запрос.Текст = СтрЗаменить(Запрос.Текст, "ДанныеДокументов.РазрядКатегория", "ДанныеДокументов.ПКУ");
		
	Иначе
		
		Если Не ПолучитьФункциональнуюОпцию("ИспользоватьТарифныеСеткиПриРасчетеЗарплаты")
			И ПолучитьФункциональнуюОпцию("ИспользоватьРазрядыКатегорииКлассыДолжностейИПрофессийВШтатномРасписании") Тогда
			
			ПараметрыПостроения = УправлениеШтатнымРасписанием.ПараметрыПостроенияВТШтатноеРасписаниеПоТаблицеФильтра(
				"ВТДанныеДокументов", "ДатаМероприятия", "ДолжностьПоШтатномуРасписанию");
			
			УправлениеШтатнымРасписанием.СоздатьВТШтатноеРасписание(
				Запрос.МенеджерВременныхТаблиц, Истина, ПараметрыПостроения, "РазрядКатегория");
			
			Запрос.Текст = СтрЗаменить(Запрос.Текст, "ДанныеДокументов.РазрядКатегория", "ШтатноеРасписание.РазрядКатегория");
			
			Запрос.Текст = СтрЗаменить(Запрос.Текст, "ВТДанныеДокументов КАК ДанныеДокументов",
			"ВТДанныеДокументов КАК ДанныеДокументов
				|ЛЕВОЕ СОЕДИНЕНИЕ ВТШтатноеРасписание КАК ШтатноеРасписание
				|ПО ДанныеДокументов.ДатаМероприятия = ШтатноеРасписание.Период
				|	И ДанныеДокументов.ДолжностьПоШтатномуРасписанию = ШтатноеРасписание.ПозицияШтатногоРасписания");
			
		КонецЕсли;
		
	КонецЕсли;
	
КонецПроцедуры

Функция МероприятияСотрудникаДо2020Года(Сотрудник, Организация) Экспорт
	
	ДанныеСотрудника = Новый Массив;
	
	Запрос = Новый Запрос;
	Запрос.МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;
	
	ПараметрыПолучения = КадровыйУчет.ПараметрыПолученияСотрудниковОрганизацийПоСпискуФизическихЛиц();
	ПараметрыПолучения.Организация = Организация;
	ПараметрыПолучения.НачалоПериода = ЭлектронныеТрудовыеКнижки.ДатаНачалаУчета() - 1;
	ПараметрыПолучения.ОкончаниеПериода = ПараметрыПолучения.НачалоПериода;
	ПараметрыПолучения.КадровыеДанные = ЭлектронныеТрудовыеКнижки.ИменаКадровыхДанныхСотрудниковДляНачалаУчета();
	ПараметрыПолучения.СписокФизическихЛиц = ОбщегоНазначенияКлиентСервер.ЗначениеВМассиве(Сотрудник);
	
	ЭлектронныеТрудовыеКнижки.УстановитьОтборПараметровПолученияСотрудниковОрганизации(ПараметрыПолучения);
	
	КадровыйУчет.СоздатьВТСотрудникиОрганизации(Запрос.МенеджерВременныхТаблиц, Истина, ПараметрыПолучения);
	
	ПараметрыПостроения = КадровыйУчетРасширенный.ПараметрыПостроенияВТРеестрКадровыхПриказовПоВременнойТаблице();
	ПараметрыПостроения.ИмяВТОтборовСотрудников = "ВТСотрудникиОрганизации";
	ПараметрыПостроения.ИмяПоляДатаНачала = "ДатаПриема";
	ПараметрыПостроения.ИмяПоляДатаОкончания = "Период";
	
	КадровыйУчетРасширенный.СоздатьВТРеестрКадровыхПриказов(Запрос.МенеджерВременныхТаблиц, Истина, ПараметрыПостроения);
	
	Запрос.УстановитьПараметр("ДатаНачалаУчета", ЭлектронныеТрудовыеКнижки.ДатаНачалаУчета());
	Запрос.УстановитьПараметр("НаименованиеДокументаОснования", ЭлектронныеТрудовыеКнижки.НаименованиеДокументаОснования());
	
	Запрос.Текст =
		"ВЫБРАТЬ
		|	СотрудникиОрганизации.Сотрудник КАК СотрудникЗаписи,
		|	СотрудникиОрганизации.ФизическоеЛицо КАК Сотрудник,
		|	РеестрКадровыхПриказов.Подразделение КАК Подразделение,
		|	РеестрКадровыхПриказов.Должность КАК Должность,
		|	ВЫБОР
		|		КОГДА СотрудникиОрганизации.ВидЗанятости = ЗНАЧЕНИЕ(Перечисление.ВидыЗанятости.ОсновноеМестоРаботы)
		|			ТОГДА ЛОЖЬ
		|		ИНАЧЕ ИСТИНА
		|	КОНЕЦ КАК ЯвляетсяСовместителем,
		|	ВЫБОР
		|		КОГДА РеестрКадровыхПриказов.ВидСобытия = ЗНАЧЕНИЕ(Перечисление.ВидыКадровыхСобытий.Прием)
		|			ТОГДА ЗНАЧЕНИЕ(Перечисление.ВидыМероприятийТрудовойДеятельности.Прием)
		|		КОГДА РеестрКадровыхПриказов.ВидСобытия = ЗНАЧЕНИЕ(Перечисление.ВидыКадровыхСобытий.Перемещение)
		|			ТОГДА ЗНАЧЕНИЕ(Перечисление.ВидыМероприятийТрудовойДеятельности.Перевод)
		|		ИНАЧЕ ЗНАЧЕНИЕ(Перечисление.ВидыМероприятийТрудовойДеятельности.ПустаяСсылка)
		|	КОНЕЦ КАК ВидМероприятия,
		|	РеестрКадровыхПриказов.Период КАК ДатаМероприятия,
		|	&НаименованиеДокументаОснования КАК НаименованиеДокументаОснования,
		|	РеестрКадровыхПриказов.ДатаПриказа КАК ДатаДокументаОснования,
		|	РеестрКадровыхПриказов.НомерПриказа КАК НомерДокументаОснования,
		|	РеестрКадровыхПриказов.Разряд КАК РазрядКатегория,
		|	РеестрКадровыхПриказов.Регистратор КАК Регистратор
		|ИЗ
		|	ВТСотрудникиОрганизации КАК СотрудникиОрганизации
		|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ ВТРеестрКадровыхПриказов КАК РеестрКадровыхПриказов
		|		ПО СотрудникиОрганизации.ФизическоеЛицо = РеестрКадровыхПриказов.ФизическоеЛицо
		|			И СотрудникиОрганизации.Сотрудник = РеестрКадровыхПриказов.Сотрудник
		|			И (РеестрКадровыхПриказов.ВидСобытия В (ЗНАЧЕНИЕ(Перечисление.ВидыКадровыхСобытий.Прием), ЗНАЧЕНИЕ(Перечисление.ВидыКадровыхСобытий.Перемещение)))
		|ГДЕ
		|	СотрудникиОрганизации.ВидСобытия <> ЗНАЧЕНИЕ(Перечисление.ВидыКадровыхСобытий.Увольнение)
		|	И СотрудникиОрганизации.ДатаПриема < &ДатаНачалаУчета
		|	И (СотрудникиОрганизации.ДатаУвольнения = ДАТАВРЕМЯ(1, 1, 1)
		|			ИЛИ СотрудникиОрганизации.ДатаУвольнения >= &ДатаНачалаУчета)
		|
		|УПОРЯДОЧИТЬ ПО
		|	СотрудникиОрганизации.ФИОПолные,
		|	ЯвляетсяСовместителем";
	
	УстановитьПривилегированныйРежим(Истина);
	
	ДанныеРеестра = Запрос.Выполнить().Выгрузить();
	РеквизитыПриказов = КадровыйУчетРасширенный.РеквизитыКадровыхПриказов(ДанныеРеестра.ВыгрузитьКолонку("Регистратор"));
	
	УстановитьПривилегированныйРежим(Ложь);
	
	Для Каждого ДанныеЗаписи Из ДанныеРеестра Цикл
		
		ДанныеМероприятияСотрудника = ЭлектронныеТрудовыеКнижки.ПустаяСтруктураЗаписиОТрудовойДеятельности();
		ЗаполнитьЗначенияСвойств(ДанныеМероприятияСотрудника, ДанныеЗаписи);
		
		Если ДанныеМероприятияСотрудника.ВидМероприятия = Перечисления.ВидыМероприятийТрудовойДеятельности.Прием Тогда
			ДанныеМероприятияСотрудника.НаименованиеДокументаОснования = ЭлектронныеТрудовыеКнижки.НаименованиеДокументаПоВидуДокументаСобытия(Организация, "ПриемНаРаботу");
		ИначеЕсли ДанныеМероприятияСотрудника.ВидМероприятия = Перечисления.ВидыМероприятийТрудовойДеятельности.Перевод Тогда
			ДанныеМероприятияСотрудника.НаименованиеДокументаОснования = ЭлектронныеТрудовыеКнижки.НаименованиеДокументаПоВидуДокументаСобытия(Организация, "КадровыйПеревод");
		КонецЕсли;
		
		Если ДанныеМероприятияСотрудника.НомерДокументаОснования = Неопределено Тогда
			
			РеквизитыПриказа = РеквизитыПриказов.Получить(ДанныеЗаписи.Регистратор);
			Если РеквизитыПриказа <> Неопределено Тогда
				ДанныеМероприятияСотрудника.НомерДокументаОснования = ЗарплатаКадрыОтчеты.НомерНаПечать(РеквизитыПриказа.Номер, РеквизитыПриказа.НомерПриказа);
				ДанныеМероприятияСотрудника.ДатаДокументаОснования = РеквизитыПриказа.Дата;
			КонецЕсли;
			
		КонецЕсли;
		
		ДанныеСотрудника.Добавить(ДанныеМероприятияСотрудника);
		
	КонецЦикла;
	
	Возврат ДанныеСотрудника;
	
КонецФункции

#КонецОбласти
