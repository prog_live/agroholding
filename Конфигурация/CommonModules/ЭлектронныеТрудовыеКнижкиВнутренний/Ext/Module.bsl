﻿#Область СлужебныеПроцедурыИФункции

Функция РазрядКатегорияВидимость() Экспорт
	
	Возврат ЭлектронныеТрудовыеКнижкиРасширенный.РазрядКатегорияВидимость();
	
КонецФункции

Функция КодПоРееструДолжностейВидимость() Экспорт
	
	Возврат ЭлектронныеТрудовыеКнижкиРасширенный.КодПоРееструДолжностейВидимость();
	
КонецФункции

Процедура УстановитьОтборПараметровПолученияСотрудниковОрганизации(ПараметрыПолучения) Экспорт
	
	ЭлектронныеТрудовыеКнижкиРасширенный.УстановитьОтборПараметровПолученияСотрудниковОрганизации(ПараметрыПолучения);
	
КонецПроцедуры

Функция ИменаКадровыхДанныхСотрудниковДляНачалаУчета() Экспорт
	
	Возврат ЭлектронныеТрудовыеКнижкиРасширенный.ИменаКадровыхДанныхСотрудниковДляНачалаУчета();
	
КонецФункции

Процедура ДополнитьМероприятияЭТКДаннымиРеестраКадровыхПриказов(ДанныеСотрудниковБезМероприятий) Экспорт
	
	ЭлектронныеТрудовыеКнижкиРасширенный.ДополнитьМероприятияЭТКДаннымиРеестраКадровыхПриказов(ДанныеСотрудниковБезМероприятий);
	
КонецПроцедуры

Процедура УточнитьЗапросПолученияДанныхНаНачалоУчета(Запрос) Экспорт
	
	ЭлектронныеТрудовыеКнижкиРасширенный.УточнитьЗапросПолученияДанныхНаНачалоУчета(Запрос);
	
КонецПроцедуры

Функция КодДолжностиПоРееструГосударственнойСлужбы(Должность) Экспорт
	
	Возврат ЭлектронныеТрудовыеКнижкиРасширенный.КодДолжностиПоРееструГосударственнойСлужбы(Должность);
	
КонецФункции

Функция МероприятияСотрудникаДо2020Года(Сотрудник, Организация) Экспорт
	
	Возврат ЭлектронныеТрудовыеКнижкиРасширенный.МероприятияСотрудникаДо2020Года(Сотрудник, Организация);
	
КонецФункции

#КонецОбласти