﻿////////////////////////////////////////////////////////////////////////////////
// КадровыйУчетКлиентСервер: методы кадрового учета, работающие на стороне 
//							клиента и сервера.
//  
////////////////////////////////////////////////////////////////////////////////
#Область СлужебныйПрограммныйИнтерфейс

// Возвращает полное наименование сотрудника из ФИО с учетом уточнений наименований физлица и сотрудника.
//
// Параметры:
//   Фамилия  - Строка
//   Имя      - Строка
//   Отчество - Строка
//   УточнениеНаименованияФизЛица    - Строка
//   УточнениеНаименованияСотрудника - Строка
//
Функция ПолноеНаименованиеСотрудника(Фамилия, Имя, Отчество, УточнениеНаименованияФизЛица, УточнениеНаименованияСотрудника = "") Экспорт
	
	ЧастиНаименования = Новый Массив;
	
	ЧастиНаименования.Добавить(Фамилия);
	
	Если ЗначениеЗаполнено(Имя) Тогда
		ЧастиНаименования.Добавить(Имя);
	КонецЕсли;
	Если ЗначениеЗаполнено(Отчество) Тогда
		ЧастиНаименования.Добавить(Отчество);
	КонецЕсли;
	Если ЗначениеЗаполнено(УточнениеНаименованияФизЛица) Тогда
		ЧастиНаименования.Добавить(УточнениеНаименованияФизЛица);
	КонецЕсли;
	Если ЗначениеЗаполнено(УточнениеНаименованияСотрудника) Тогда
		ЧастиНаименования.Добавить(УточнениеНаименованияСотрудника);
	КонецЕсли;
	
	Возврат СтрСоединить(ЧастиНаименования, " ")
	
КонецФункции

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

// Обработчик события ОбработкаПолученияДанныхВыбора перечисления ВидыЗанятости.
//
// Параметры:
//   ДанныеВыбора         - СписокЗначений
//   Параметры            - Структура
//   СтандартнаяОбработка - Булево
//
Процедура ОбработкаПолученияДанныхВыбораВидовЗанятости(ДанныеВыбора, Параметры, СтандартнаяОбработка) Экспорт
	
	КадровыйУчетКлиентСерверВнутренний.ОбработкаПолученияДанныхВыбораВидовЗанятости(ДанныеВыбора, Параметры, СтандартнаяОбработка);
	
КонецПроцедуры

// Проверяет СНИЛС на заполненность, исключая из СНИЛСа служебные символы-разделители.
//
// Параметры:
//		СНИЛС	- Строка
//
// Возвращаемое значение:
//		Булево
//
Функция СНИЛСЗаполнен(Знач СНИЛС) Экспорт
	
	Возврат НЕ ПустаяСтрока(СтрЗаменить(СНИЛС, "-", ""));
	
КонецФункции

#КонецОбласти
