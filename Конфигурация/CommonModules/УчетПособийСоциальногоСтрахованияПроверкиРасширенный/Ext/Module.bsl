﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область СлужебныйПрограммныйИнтерфейс

Процедура ПроверкаУволенныхСотрудниковСОплатойБольничного(Проверка, ПараметрыПроверки) Экспорт
	
	Если Не ОбщегоНазначения.ПодсистемаСуществует("СтандартныеПодсистемы.КонтрольВеденияУчета") Тогда
		Возврат;
	КонецЕсли;
	
	УстановитьПривилегированныйРежим(Истина);
	Результат = РезультатПроверкиУволенныхСотрудниковСОплатойБольничного();
	УстановитьПривилегированныйРежим(Ложь);
	
	Если Результат.Пустой() Тогда
		Возврат;
	КонецЕсли;
	
	Выборка = Результат.Выбрать();
	Пока Выборка.СледующийПоЗначениюПоля("Сотрудник") Цикл
		ПредставлениеДокументов = "";
		Пока Выборка.Следующий() Цикл
			ДобавитьПредставлениеДокумента(ПредставлениеДокументов, Выборка.БольничныйЛист);
		КонецЦикла;
		МодульКонтрольВеденияУчета = ОбщегоНазначения.ОбщийМодуль("КонтрольВеденияУчета");
		Проблема = МодульКонтрольВеденияУчета.ОписаниеПроблемы(Выборка.Сотрудник, ПараметрыПроверки);
		Проблема.ВажностьПроблемы = Перечисления["ВажностьПроблемыУчета"].Предупреждение;
		Проблема.УточнениеПроблемы = СтрШаблон(НСтр("ru = 'Начислена сумма по оплате больничного после увольнения
                                                     |%1';
                                                     |en = 'Начислена сумма по оплате больничного после увольнения
                                                     |%1'"), ПредставлениеДокументов);
		УстановитьПривилегированныйРежим(Истина);
		КонтрольВеденияУчетаБЗК.ЗаписатьПроблему(Проблема, ПараметрыПроверки);
		УстановитьПривилегированныйРежим(Ложь);
	КонецЦикла;
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

Функция РезультатПроверкиУволенныхСотрудниковСОплатойБольничного()
	
	МенеджерВТ = Новый МенеджерВременныхТаблиц;
	
	ТекущийРасчетныйМесяц = ЗарплатаКадрыБазовый.РасчетныйМесяц(ТекущаяДатаСеанса());
	СостоянияСотрудников.СоздатьВТСостоянияСотрудников(
		МенеджерВТ, , Перечисления.СостоянияСотрудника.Увольнение, ТекущийРасчетныйМесяц);
	
	Запрос = Новый Запрос;
	Запрос.МенеджерВременныхТаблиц = МенеджерВТ;
	Запрос.Текст =
		"ВЫБРАТЬ
		|	БольничныйЛист.Ссылка КАК Ссылка
		|ПОМЕСТИТЬ ВТДопустимыеБольничные
		|ИЗ
		|	ВТСостоянияСотрудников КАК УволенныеСотрудники
		|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Документ.БольничныйЛист КАК БольничныйЛист
		|		ПО УволенныеСотрудники.Сотрудник = БольничныйЛист.Сотрудник
		|			И (БольничныйЛист.Проведен)
		|			И (БольничныйЛист.ПричинаНетрудоспособности = ЗНАЧЕНИЕ(Перечисление.ПричиныНетрудоспособности.ОбщееЗаболевание))
		|			И (БольничныйЛист.ПроцентОплаты = 60)
		|			И (БольничныйЛист.ДатаНачалаСобытия МЕЖДУ УволенныеСотрудники.Начало И ДОБАВИТЬКДАТЕ(УволенныеСотрудники.Начало, ДЕНЬ, 30))
		|
		|ИНДЕКСИРОВАТЬ ПО
		|	БольничныйЛист.Ссылка
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	УволенныеСотрудники.Сотрудник КАК Сотрудник,
		|	БольничныйЛист.Ссылка КАК БольничныйЛист
		|ИЗ
		|	ВТСостоянияСотрудников КАК УволенныеСотрудники
		|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Документ.БольничныйЛист КАК БольничныйЛист
		|		ПО УволенныеСотрудники.Сотрудник = БольничныйЛист.Сотрудник
		|			И (БольничныйЛист.ДатаНачалаСобытия >= УволенныеСотрудники.Начало)
		|			И (НЕ БольничныйЛист.Ссылка В
		|					(ВЫБРАТЬ
		|						ДопустимыеБольничные.Ссылка
		|					ИЗ
		|						ВТДопустимыеБольничные КАК ДопустимыеБольничные))
		|
		|УПОРЯДОЧИТЬ ПО
		|	Сотрудник";
	
	РезультатЗапроса = Запрос.Выполнить();
	
	Возврат РезультатЗапроса;
	
КонецФункции

Процедура ДобавитьПредставлениеДокумента(Представление, ДокументСсылка)
	
	Если Не ПустаяСтрока(Представление) Тогда
		Представление = Представление + Символы.ПС;
	КонецЕсли;
	
	Представление = Представление + ДокументСсылка;
	
КонецПроцедуры

#КонецОбласти

#КонецЕсли