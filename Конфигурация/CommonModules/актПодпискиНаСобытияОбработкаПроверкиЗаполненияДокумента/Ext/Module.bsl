﻿
////////////////////////////////////////////////////////////////////////////////
// Модуль содержит процедуры и функции подписок Обработки проверки заполнения для всех Доументов.
// Автор Кудаков Р.И.
//  
////////////////////////////////////////////////////////////////////////////////

#Область СлужебныйПрограммныйИнтерфейс

// Вызывается из подписки актОбработкаЗаполненияДокументаУниверсальный.
Процедура ОбработкаПроверкиЗаполненияУниверсальный(Источник, Отказ, ПроверяемыеРеквизиты) Экспорт
	
	Если Источник.ОбменДанными.Загрузка Тогда
		Возврат;
	КонецЕсли;
	
	ТекстИсключения = НСтр("ru = 'Недостаточно прав на редактирование.'");
	Если ТипЗнч(Источник) = Тип("ДокументОбъект.ОперацияБух") 
		И (Не РольДоступна(Метаданные.Роли.ПолныеПрава) Или Не РольДоступна(Метаданные.Роли.актДобавлениеИзменениеОперацииБух)) Тогда
		
		ВызватьИсключение ТекстИсключения + НСтр("ru = 'Требуется право (акт) Добавление изменение операции бух'"); 
	ИначеЕсли ТипЗнч(Источник) = Тип("ДокументОбъект.ВыпускПродукции") Тогда
		
	ИначеЕсли ТипЗнч(Источник) = Тип("ДокументОбъект.ПересортицаТоваров") Тогда
		
		ПроверитьЗаполнениеПересортицыТоваров(Источник, Отказ, ПроверяемыеРеквизиты);
	ИначеЕсли ТипЗнч(Источник) = Тип("ДокументОбъект.ВнутреннееПотреблениеТоваров") 
		И Источник. ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.СписаниеТоваровПоТребованию Тогда
		
		ПроверитьСоответствиеПодразделенияСтатьеРасходов(Источник, "Товары", Источник.Подразделение, Отказ);
	ИначеЕсли ТипЗнч(Источник) = Тип("ДокументОбъект.ПоступлениеБезналичныхДенежныхСредств") Тогда
		
		ПроверитьЗаполнениеПоступлениеБезналичныхДенежныхСредств(Источник, Отказ, ПроверяемыеРеквизиты);
	ИначеЕсли ТипЗнч(Источник) = Тип("ДокументОбъект.ПеремещениеТоваров") Тогда
		
		ПроверитьЗаполнениеПеремещениеТоваров(Источник, Отказ, ПроверяемыеРеквизиты);
	ИначеЕсли ТипЗнч(Источник) = Тип("ДокументОбъект.ПриобретениеТоваровУслуг") Тогда
		
		//ПроверитьНаличиеНоменклатурыПоПрайсЛисту(Источник, Отказ, ПроверяемыеРеквизиты);
	ИначеЕсли ТипЗнч(Источник) = Тип("ДокументОбъект.ПриобретениеУслугПрочихАктивов") Тогда
		
		//ПроверитьНаличиеНоменклатурыПоПрайсЛисту(Источник, Отказ, ПроверяемыеРеквизиты);
		
	// АйТиКит, Чернецкий, 06.06.19, начало
	ИначеЕсли ТипЗнч(Источник) = Тип("ДокументОбъект.ЗаказКлиента") Тогда
		
		ПроверитьЗаполнениеЗаказКлиента(Источник, Отказ, ПроверяемыеРеквизиты);
	// АйТиКит, Чернецкий, 06.06.19, конец
	КонецЕсли;
	
КонецПроцедуры

#КонецОБласти

#Область СлужебныеПроцедурыИФункции

Процедура ПроверитьЗаполнениеПересортицыТоваров(Источник, Отказ, ПроверяемыеРеквизиты)
	// Проверка на различную номенклатуру в ТЧ
	Для Счетчик = 0 По Источник.Товары.Количество() - 1 Цикл
		СтрокаДок = Источник.Товары[Счетчик];
		Если СтрокаДок.Номенклатура <> СтрокаДок.НоменклатураОприходование Тогда
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(
				НСтр("ru = 'Документ не записан! Не допускается указывать различную номенклатуру в одной строке.'"),
				Источник, 
				ОбщегоНазначенияКлиентСервер.ПутьКТабличнойЧасти("Товары", СтрокаДок.НомерСтроки, "Номенклатура"),,
				Отказ
			);
		КонецЕсли;
	КонецЦикла;
	// Проверка активности галочки приходования по себестоимости
	Если Не Источник.ПриходоватьТоварыПоСебестоимостиСписания Тогда 
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(
			НСтр("ru = 'Документ не записан! Запрещается приходование по себестоимости списания. Включите галочку'"),
			Источник,
			"ПриходоватьТоварыПоСебестоимостиСписания",,
			Отказ
		);
	КонецЕсли;
	
КонецПроцедуры

Функция ПолучитьЗначениеПоНастройкеИУсловию(ВидНастройки,Условие)
	//
	//мзНастройки = РегистрыСведений.актНастройкиОтраженияЗарплаты.СоздатьМенеджерЗаписи();
	//мзНастройки.ВидНастройки		= ВидНастройки;
	//мзНастройки.Условие				= Условие;
	//мзНастройки.Прочитать();
	//
	//Если мзНастройки.Выбран() Тогда
	//	Возврат мзНастройки.Значение;
	//Иначе
	//	Возврат Неопределено;
	//КонецЕсли;
	
КонецФункции

Функция ПроверитьСоответствиеПодразделенияСтатьеРасходов(Источник, ИмяТЧ, Подразделение, Отказ);
	
	//
	
КонецФункции

Процедура ПроверитьЗаполнениеПоступлениеБезналичныхДенежныхСредств(Источник, Отказ, ПроверяемыеРеквизиты)

	// Проверим заполненность реквизита "Договор" 
	Если Не ЗначениеЗаполнено(Источник.Договор) И Источник.ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ПоступлениеОплатыОтКлиента Тогда

		Сообщение = Новый СообщениеПользователю();
		Сообщение.Текст = "Не указан договор контрагента!";
		Сообщение.Поле 	= "Договор";
		Сообщение.ПутьКДанным = "Объект";
		Сообщение.УстановитьДанные(Источник);
		Сообщение.Сообщить();
		//Отказ = Истина;

	КонецЕсли;

КонецПроцедуры

Процедура ПроверитьЗаполнениеПеремещениеТоваров(Источник, Отказ, ПроверяемыеРеквизиты) Экспорт
	
	Если Источник.ОбменДанными.Загрузка Тогда
		Возврат;
	КонецЕсли;
	
	//УстановитьПривилегированныйРежим(Истина);
	
	// МКЮ ОТКЛЮЧЕНО по указанию руководства изза неиспользования
	//Если Не Константы.актИспользоватьОграничениеПеремещенияТоваровПоГруппамПользователей.Получить() Тогда
	//	Возврат;
	//КонецЕсли;
	//
	//Запрос = Новый Запрос();
	//Запрос.Текст = 
	//"ВЫБРАТЬ
	//|	ВТ_ТОВАРЫ.Номенклатура КАК ГруппаНоменклатуры,
	//|	ВТ_ТОВАРЫ.НомерСтроки КАК НомерСтроки
	//|ПОМЕСТИТЬ ВТ_Товары
	//|ИЗ
	//|	&ВТ_ТОВАРЫ КАК ВТ_ТОВАРЫ
	//|;
	//|
	//|////////////////////////////////////////////////////////////////////////////////
	//|ВЫБРАТЬ
	//|	актНастройкиПеремещенияТоваровПоГруппамПользователей.ГруппаПользователей КАК ГруппаПользователей,
	//|	актНастройкиПеремещенияТоваровПоГруппамПользователей.ГруппаНоменклатуры КАК ГруппаНоменклатуры,
	//|	актНастройкиПеремещенияТоваровПоГруппамПользователей.СкладОтправитель КАК СкладОтправитель,
	//|	актНастройкиПеремещенияТоваровПоГруппамПользователей.СкладПолучатель КАК СкладПолучатель
	//|ПОМЕСТИТЬ НастройкиПоПользователю
	//|ИЗ
	//|	Справочник.ГруппыПользователей.Состав КАК ГруппыПользователейСостав
	//|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ РегистрСведений.актНастройкиПеремещенияТоваровПоГруппамПользователей КАК актНастройкиПеремещенияТоваровПоГруппамПользователей
	//|		ПО ГруппыПользователейСостав.Ссылка = актНастройкиПеремещенияТоваровПоГруппамПользователей.ГруппаПользователей
	//|ГДЕ
	//|	ГруппыПользователейСостав.Пользователь = &ТекущийПользователь
	//|	И (актНастройкиПеремещенияТоваровПоГруппамПользователей.СкладОтправитель <> ЗНАЧЕНИЕ(Справочник.Склады.ПустаяСсылка)
	//|			ИЛИ актНастройкиПеремещенияТоваровПоГруппамПользователей.СкладПолучатель <> ЗНАЧЕНИЕ(Справочник.Склады.ПустаяСсылка))
	//|;
	//|
	//|////////////////////////////////////////////////////////////////////////////////
	//|ВЫБРАТЬ
	//|	КОЛИЧЕСТВО(*) > 0 КАК ЕстьОграниченияПоПользователю,
	//|	МАКСИМУМ(НастройкиПоПользователю.ГруппаНоменклатуры) КАК ГруппаНоменклатуры
	//|ИЗ
	//|	НастройкиПоПользователю КАК НастройкиПоПользователю
	//|
	//|СГРУППИРОВАТЬ ПО
	//|	НастройкиПоПользователю.ГруппаНоменклатуры
	//|;
	//|
	//|////////////////////////////////////////////////////////////////////////////////
	//|ВЫБРАТЬ
	//|	НастройкиПоПользователю.ГруппаПользователей КАК ГруппаПользователей,
	//|	НастройкиПоПользователю.ГруппаНоменклатуры КАК ГруппаНоменклатуры,
	//|	НастройкиПоПользователю.СкладОтправитель КАК СкладОтправитель,
	//|	НастройкиПоПользователю.СкладПолучатель КАК СкладПолучатель,
	//|	ВТ_Товары.НомерСтроки КАК НомерСтроки
	//|ИЗ
	//|	НастройкиПоПользователю КАК НастройкиПоПользователю
	//|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ ВТ_Товары КАК ВТ_Товары
	//|		ПО НастройкиПоПользователю.ГруппаНоменклатуры = ВТ_Товары.ГруппаНоменклатуры
	//|ГДЕ
	//|	НастройкиПоПользователю.СкладОтправитель = &СкладОтправитель
	//|	ИЛИ НастройкиПоПользователю.СкладПолучатель = &СкладПолучатель";
	//Запрос.УстановитьПараметр("ТекущийПользователь", ПараметрыСеанса.ТекущийПользователь);
	//Запрос.УстановитьПараметр("ВТ_ТОВАРЫ", ТЧТоварыСИерархиейНоменклатуры(Источник.Товары));
	//Запрос.УстановитьПараметр("СкладОтправитель", Источник.СкладОтправитель);
	//Запрос.УстановитьПараметр("СкладПолучатель", Источник.СкладПолучатель);
	//
	//РезультатПакета = Запрос.ВыполнитьПакет();
	//
	//ВЫборкаПоПользователю = РезультатПакета[2].Выбрать();
	//ВЫборкаПоПользователю.Следующий();
	//Если ВЫборкаПоПользователю.ЕстьОграниченияПоПользователю <> Истина Тогда
	//	// Ограничений по данному пользователю и номенклатурам в документе нет, а значит нет и по складам 
	//	Возврат; 
	//КонецЕсли;
	//
	//Выборка = РезультатПакета[3].Выбрать();
	//
	//ПеремещениеРазрешено = Ложь;
	//Пока Выборка.Следующий() Цикл
	//	
	//	ОтправительРазрешен = (Выборка.СкладОтправитель = Источник.СкладОтправитель Или Выборка.СкладОтправитель = Справочники.Склады.ПустаяСсылка());
	//	ПолучательРазрешен = (Выборка.СкладПолучатель = Источник.СкладПолучатель Или Выборка.СкладПолучатель = Справочники.Склады.ПустаяСсылка());
	//	
	//	Если ОтправительРазрешен И ПолучательРазрешен Тогда
	//		ПеремещениеРазрешено = Истина;
	//		Прервать;
	//	КонецЕсли;
	//	
	//КонецЦикла;
	//
	//Если Не ПеремещениеРазрешено Тогда
	//	// указаны склады, которых нет в ограничениях по пользователю, по таким тоже выдаем ошибки
	//	ТекстСообщенияПользователю = СтрШаблон("Строка: №%1, группа: ""%2"". Запрещено перемещение со склада: ""%3"" на склад: ""%4""", 
	//			1,
	//			СокрЛП(ВЫборкаПоПользователю.ГруппаНоменклатуры),
	//			Источник.СкладОтправитель,
	//			Источник.СкладПолучатель
	//	);
	//	
	//	ОбщегоНазначенияКлиентСервер.СообщитьПользователю(
	//		ТекстСообщенияПользователю,
	//		Источник, 
	//		ОбщегоНазначенияКлиентСервер.ПутьКТабличнойЧасти("Товары", 1, "Номенклатура"),,
	//		Отказ
	//	);
	//КонецЕсли;
	//
	//УстановитьПривилегированныйРежим(Ложь);
	//
КонецПроцедуры

//==Акита Кудаков AI-33 начало
Процедура ПроверитьНаличиеНоменклатурыПоПрайсЛисту(Источник, Отказ, ПроверяемыеРеквизиты)
	
	ИспользоватьСоглашенияСПоставщиками = ПолучитьФункциональнуюОпцию("ИспользоватьСоглашенияСПоставщиками");
	Если Не ИспользоватьСоглашенияСПоставщиками Тогда
		Возврат;
	КонецЕсли;
	
	КонтролироватьЦеныЗакупки = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Источник.Соглашение,"КонтролироватьЦеныЗакупки");
	Если Не КонтролироватьЦеныЗакупки Тогда
		Возврат;
	КонецЕсли;
	
	// Вся номенклатура в документе должна быть заведена в прайс листе, иначе выбрасываем ошибку
	
	ДокументЗакупки = Источник;
	
	Если ПраваПользователяПовтИсп.ОтклонениеОтУсловийЗакупок() Тогда
		Возврат;
	КонецЕсли;
	
	Запрос = Новый Запрос(
	"ВЫБРАТЬ
	|	ТЧТовары.Номенклатура КАК Номенклатура,
	|	ТЧТовары.НомерСтроки КАК НомерСтроки,
	|	ТЧТовары.ВидЦеныПоставщика КАК ВидЦеныПоставщика
	|ПОМЕСТИТЬ ТЧТОвары
	|ИЗ
	|	&ТЧТовары КАК ТЧТовары
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ТЧТОвары.Номенклатура КАК Номенклатура,
	|	ЦеныНоменклатурыПоставщиков.ВидЦеныПоставщика КАК ВидЦеныПоставщика,
	|	ТЧТОвары.НомерСтроки КАК НомерСтроки
	|ИЗ
	|	ТЧТОвары КАК ТЧТОвары
	|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.ЦеныНоменклатурыПоставщиков.СрезПоследних(&Дата, ) КАК ЦеныНоменклатурыПоставщиков
	|		ПО ТЧТОвары.Номенклатура = ЦеныНоменклатурыПоставщиков.Номенклатура
	|			И ТЧТОвары.ВидЦеныПоставщика = ЦеныНоменклатурыПоставщиков.ВидЦеныПоставщика
	|ГДЕ
	|	ЦеныНоменклатурыПоставщиков.Номенклатура ЕСТЬ NULL");
	Запрос.УстановитьПараметр("ТЧТовары", Источник.Товары);
	Запрос.УстановитьПараметр("Дата", Источник.Дата);
	
	Выборка = Запрос.Выполнить().Выбрать();
	
	Пока Выборка.Следующий() Цикл
		
		ТекстОшибки = НСтр("ru = 'Цена на номенклатуру %1 (%2) не установлена в прайс-листе! Заведите цену в прайс-листе.'");
		ТекстОшибки = СтрШаблон(ТекстОшибки, Выборка.Номенклатура, Источник.Валюта);
		
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(
			ТекстОшибки,
			ДокументЗакупки,
			ОбщегоНазначенияКлиентСервер.ПутьКТабличнойЧасти("Товары", Выборка.НомерСтроки, "Цена"),
			,
			Отказ
		);
	
	КонецЦикла;
	
КонецПроцедуры
//==Акита Кудаков AI-33 конец

// АйТиКит, Чернецкий, 06.06.19, начало
Процедура ПроверитьЗаполнениеЗаказКлиента(Источник, Отказ, ПроверяемыеРеквизиты)

	Если НЕ ЗначениеЗаполнено(Источник.ДатаОтгрузки) Тогда
		
		Сообщение = Новый СообщениеПользователю();
		Сообщение.Текст = "Не заполнена дата отгрузки заказа на вкладке ""Товары""";
		Сообщение.Поле 	= "ДатаОтгрузки";
		Сообщение.ПутьКДанным = "Объект";
		Сообщение.УстановитьДанные(Источник);
		Сообщение.Сообщить();
		Отказ = Истина;
		
	КонецЕсли;
		
КонецПроцедуры
// АйТиКит, Чернецкий, 06.06.19, конец

Функция ТЧТоварыСИерархиейНоменклатуры(ТЧ_Товары)
	
	Запрос = Новый Запрос(
	"ВЫБРАТЬ
	|	ТЧ_Товары.Номенклатура КАК Номенклатура,
	|	ТЧ_Товары.НомерСтроки КАК НомерСтроки
	|ПОМЕСТИТЬ ТЧ_ТОВАРЫ
	|ИЗ
	|	&ТЧ_Товары КАК ТЧ_Товары
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ТЧ_ТОВАРЫ.Номенклатура КАК Номенклатура,
	|	ТЧ_ТОВАРЫ.НомерСтроки КАК НомерСтроки
	|ИЗ
	|	ТЧ_ТОВАРЫ КАК ТЧ_ТОВАРЫ
	|ИТОГИ
	|	МАКСИМУМ(НомерСтроки)
	|ПО
	|	Номенклатура ИЕРАРХИЯ");
	Запрос.УстановитьПараметр("ТЧ_Товары", ТЧ_Товары);
	Возврат Запрос.Выполнить().Выгрузить();
	
КонецФункции

#КонецОбласти