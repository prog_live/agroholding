﻿////////////////////////////////////////////////////////////////////////////////
// УправлениеШтатнымРасписаниемВызовСервера:
//  
////////////////////////////////////////////////////////////////////////////////

#Область СлужебныеПроцедурыИФункции

Процедура ОбработкаПолученияДанныхВыбораСправочникаШтатноеРасписание(ДанныеВыбора, Параметры, СтандартнаяОбработка) Экспорт
	
	СтандартнаяОбработка = Ложь;
	Запрос = Неопределено;
	
	ВидДоговора = Неопределено;
	Параметры.Свойство("ВидДоговора", ВидДоговора);
	Если ТипЗнч(ВидДоговора) = Тип("ПеречислениеСсылка.ВидыДоговоровССотрудниками") Тогда
		
		Если ОбщегоНазначения.ПодсистемаСуществует("ЗарплатаКадрыПриложения.ГосударственнаяСлужба") Тогда
			МодульГосударственнаяСлужба = ОбщегоНазначения.ОбщийМодуль("ГосударственнаяСлужба");
			Запрос = МодульГосударственнаяСлужба.ЗапросПолученияДанныхВыбораСправочникаШтатноеРасписание(Параметры);
		КонецЕсли;
		
	КонецЕсли;
	
	Если Запрос = Неопределено Тогда
		
		Запрос = Новый Запрос;
		Запрос.Текст =
			"ВЫБРАТЬ
			|	*
			|ИЗ
			|	Справочник.ШтатноеРасписание КАК ШтатноеРасписание
			|ГДЕ
			|	НЕ ШтатноеРасписание.ГруппаПозицийПодразделения
			|	И ШтатноеРасписание.Владелец = &Владелец
			|	И ШтатноеРасписание.Подразделение = &Подразделение
			|	И ШтатноеРасписание.ДатаУтверждения <= &ДатаНачалаПримененияОтбора
			|	И (ШтатноеРасписание.ДатаЗакрытия = ДатаВремя(1, 1, 1)
			|		ИЛИ ШтатноеРасписание.ДатаЗакрытия > &ДатаОкончанияПримененияОтбора)
			|	И &ДополнительноеУсловие";
		
		Если Параметры.Отбор.Свойство("Владелец")
			И ЗначениеЗаполнено(Параметры.Отбор.Владелец) Тогда
			
			Запрос.УстановитьПараметр("Владелец", Параметры.Отбор.Владелец);
			
		Иначе
			Запрос.Текст = СтрЗаменить(Запрос.Текст, "ШтатноеРасписание.Владелец = &Владелец", "(ИСТИНА)");
		КонецЕсли;
		Параметры.Отбор.Удалить("Владелец");
		
		Если Параметры.Отбор.Свойство("Подразделение")
			И ЗначениеЗаполнено(Параметры.Отбор.Подразделение) Тогда
			
			Запрос.УстановитьПараметр("Подразделение", Параметры.Отбор.Подразделение);
			
		Иначе
			Запрос.Текст = СтрЗаменить(Запрос.Текст, "ШтатноеРасписание.Подразделение = &Подразделение", "(ИСТИНА)");
		КонецЕсли;
		Параметры.Отбор.Удалить("Подразделение");
		
		Если Параметры.Отбор.Свойство("ДатаПримененияОтбора")
			И ЗначениеЗаполнено(Параметры.Отбор.ДатаПримененияОтбора) Тогда
			
			Запрос.УстановитьПараметр("ДатаНачалаПримененияОтбора", Параметры.Отбор.ДатаПримененияОтбора);
			Запрос.УстановитьПараметр("ДатаОкончанияПримененияОтбора", Параметры.Отбор.ДатаПримененияОтбора);
			
		Иначе
			Запрос.УстановитьПараметр("ДатаНачалаПримененияОтбора", ЗарплатаКадрыПериодическиеРегистры.МаксимальнаяДата());
			Запрос.УстановитьПараметр("ДатаОкончанияПримененияОтбора", '00010101');
		КонецЕсли;
		Параметры.Отбор.Удалить("ДатаПримененияОтбора");
		
	КонецЕсли;
	
	ЗарплатаКадры.ЗаполнитьДанныеВыбораСправочника(ДанныеВыбора, Метаданные.Справочники.Должности, Параметры, Запрос, "ШтатноеРасписание");
	
КонецПроцедуры

Функция ПолноеИмяМетаданныхДокумента(СсылкаНаДокумент) Экспорт
	
	МетаданныеРегистратора = СсылкаНаДокумент.Метаданные();
	Возврат МетаданныеРегистратора.ПолноеИмя();
	
КонецФункции

#КонецОбласти
