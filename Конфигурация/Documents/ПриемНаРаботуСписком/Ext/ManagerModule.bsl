﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ПрограммныйИнтерфейс

// Сторнирует документ по учетам. Используется подсистемой исправления документов.
//
// Параметры:
//  Движения				 - КоллекцияДвижений, Структура	 - Коллекция движений исправляющего документа в которую будут добавлены сторно стоки.
//  Регистратор				 - ДокументСсылка				 - Документ регистратор исправления (документ исправление).
//  ИсправленныйДокумент	 - ДокументСсылка				 - Исправленный документ движения которого будут сторнированы.
//  СтруктураВидовУчета		 - Структура					 - Виды учета, по которым будет выполнено сторнирование исправленного документа.
//  					Состав полей см. в ПроведениеРасширенныйСервер.СтруктураВидовУчета().
//  ДополнительныеПараметры	 - Структура					 - Структура со свойствами:
//  					* ИсправлениеВТекущемПериоде - Булево - Истина когда исправление выполняется в периоде регистрации исправленного документа.
//						* ОтменаДокумента - Булево - Истина когда исправление вызвано документом СторнированиеНачислений.
//  					* ПериодРегистрации	- Дата - Период регистрации документа регистратора исправления.
// 
// Возвращаемое значение:
//  Булево - "Истина" если сторнирование выполнено этой функцией, "Ложь" если специальной процедуры не предусмотрено.
//
Функция СторнироватьПоУчетам(Движения, Регистратор, ИсправленныйДокумент, СтруктураВидовУчета, ДополнительныеПараметры) Экспорт
	
	Возврат Документы.ПриемНаРаботу.СторнироватьПоУчетам(Движения, Регистратор, ИсправленныйДокумент, СтруктураВидовУчета, ДополнительныеПараметры);
	
КонецФункции

#Область ДляВызоваИзДругихПодсистем

// СтандартныеПодсистемы.УправлениеДоступом

// См. УправлениеДоступомПереопределяемый.ПриЗаполненииСписковСОграничениемДоступа.
Процедура ПриЗаполненииОграниченияДоступа(Ограничение) Экспорт
	Ограничение.Текст =
	"РазрешитьЧтениеИзменение
	|ГДЕ
	|	ДляВсехСтрок( ЗначениеРазрешено(Сотрудники.ФизическоеЛицо, NULL КАК ИСТИНА)
	|	) И ЗначениеРазрешено(Организация)";
КонецПроцедуры

// Конец СтандартныеПодсистемы.УправлениеДоступом

#КонецОбласти

#КонецОбласти

#Область СлужебныйПрограммныйИнтерфейс

// Возвращает описание состава документа
//
// Возвращаемое значение:
//  Структура - см. ЗарплатаКадрыСоставДокументов.НовоеОписаниеСоставаОбъекта.
Функция ОписаниеСоставаОбъекта() Экспорт
	
	МетаданныеДокумента = Метаданные.Документы.ПриемНаРаботуСписком;
	Возврат ЗарплатаКадрыСоставДокументов.ОписаниеСоставаОбъектаПоМетаданнымФизическиеЛицаВТабличныхЧастях(МетаданныеДокумента);
	
КонецФункции

Функция ОписаниеРеквизитаКадровогоРешения() Экспорт
	Возврат Метаданные.Документы.ПриемНаРаботуСписком.ТабличныеЧасти.Сотрудники.Реквизиты.Решение;
КонецФункции

#Область ОбработчикиРегистрацииФизическихЛиц

Функция ПринадлежностиОбъекта() Экспорт
	Возврат ОбщегоНазначенияКлиентСервер.ЗначениеВМассиве("Организация");
КонецФункции

#КонецОбласти

#Область ОбработчикиПравилРегистрации

Процедура ЗарегистрироватьИзмененияПослеОбработки(ИмяПланаОбмена, ПРО, Объект, Отказ, Получатели, Выгрузка) Экспорт
	
	Если Выгрузка Или Объект.ОбменДанными.Загрузка Или (Объект.ДополнительныеСвойства.Свойство("Выгрузка") И Объект.ДополнительныеСвойства.Выгрузка) Тогда
		Возврат;
	КонецЕсли;
	
	Для Каждого СтрокаДокумента Из Объект.Сотрудники Цикл
		Если ЗначениеЗаполнено(СтрокаДокумента.Сотрудник) И ОбщегоНазначения.СсылкаСуществует(СтрокаДокумента.Сотрудник) Тогда
			ПланыОбмена.ЗарегистрироватьИзменения(Получатели, СтрокаДокумента.Сотрудник);
		КонецЕсли;
	КонецЦикла;
	
КонецПроцедуры

Процедура ВыполнитьПравилаРегистрацииРегистров(ИмяПланаОбмена, Отказ, Получатели, СтандартнаяОбработка) Экспорт
	
КонецПроцедуры

#КонецОбласти

#Область ОбменДанными

// Регистрирует изменение организации или структурного подразделения для сотрудников и физических лиц
//
// Параметры:
//		МассивДокументов - Массив - Массив объектов заполненный при загрузке сообщения обмена
//
Процедура ЗарегистрироватьЗависимыеОбъектыПослеЗагрузкиОбменаДанными(МассивДокументов) Экспорт
	
	// Зарегистрируем сотрудников по виду документа, изменяющего принадлежность к организации
	Для Каждого ДокументОбъект Из МассивДокументов Цикл
		Для Каждого СтрокаДокумента Из ДокументОбъект.Сотрудники Цикл
			Если ЗначениеЗаполнено(СтрокаДокумента.Сотрудник) И ОбщегоНазначения.СсылкаСуществует(СтрокаДокумента.Сотрудник) Тогда
				ПланыОбмена.ЗарегистрироватьИзменения(ДокументОбъект.ОбменДанными.Получатели, СтрокаДокумента.Сотрудник);
			КонецЕсли;
		КонецЦикла;
		
		СинхронизацияДанныхЗарплатаКадры.ПринадлежностьФизлицаОрганизацииПриЗаписи(ДокументОбъект);
		СинхронизацияДанныхЗарплатаКадры.ОрганизацииСотрудниковПриЗаписи(ДокументОбъект);
		
	КонецЦикла;
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

// СтандартныеПодсистемы.ВерсионированиеОбъектов

// Определяет настройки объекта для подсистемы ВерсионированиеОбъектов.
//
// Параметры:
//  Настройки - Структура - настройки подсистемы.
Процедура ПриОпределенииНастроекВерсионированияОбъектов(Настройки) Экспорт

КонецПроцедуры

// Конец СтандартныеПодсистемы.ВерсионированиеОбъектов

Функция ДобавитьКомандыСозданияДокументов(КомандыСозданияДокументов, ДополнительныеПараметры) Экспорт
	
	ЗарплатаКадрыРасширенный.ДобавитьВКоллекциюКомандуСозданияДокументаПоМетаданнымДокумента(
		КомандыСозданияДокументов, Метаданные.Документы.ПриемНаРаботуСписком);
	
КонецФункции

// Заполняет список команд печати.
// 
// Параметры:
//   КомандыПечати - ТаблицаЗначений - состав полей см. в функции УправлениеПечатью.СоздатьКоллекциюКомандПечати.
//
Процедура ДобавитьКомандыПечати(КомандыПечати) Экспорт
	
	Если Пользователи.РолиДоступны("ДобавлениеИзменениеДанныхДляНачисленияЗарплаты,ЧтениеДанныхДляНачисленияЗарплаты", , Ложь) Тогда
		
		// Бронирование позиции
		КомандаПечати = КомандыПечати.Добавить();
		КомандаПечати.Обработчик = "УправлениеПечатьюБЗККлиент.ВыполнитьКомандуПечати";
		КомандаПечати.МенеджерПечати = "Обработка.ПечатьКадровыхПриказовРасширенная";
		КомандаПечати.Идентификатор = "ПФ_MXL_ПодтверждениеБронированияПозиции";
		КомандаПечати.Представление = НСтр("ru = 'Подтверждение брони';
											|en = 'Reservation confirmation'");
		КомандаПечати.ПроверкаПроведенияПередПечатью = Истина;
		КомандаПечати.ФункциональныеОпции = "ИспользоватьБронированиеПозиций";
		
	КонецЕсли;
	
	// Приказ о приеме
	Если ПравоДоступа("Просмотр", Метаданные.Отчеты.ПечатнаяФормаТ1а) Тогда
		
		КомандаПечати = КомандыПечати.Добавить();
		КомандаПечати.Обработчик = "УправлениеПечатьюБЗККлиент.ВыполнитьКомандуПечати";
		КомандаПечати.МенеджерПечати = "Отчет.ПечатнаяФормаТ1а";
		КомандаПечати.Идентификатор = "ПФ_MXL_Т1а";
		КомандаПечати.Порядок = 10;
		КомандаПечати.Представление = НСтр("ru = 'Приказ о приеме (Т-1а)';
											|en = 'Hiring order (T-1a)'");
		КомандаПечати.ДополнительныеПараметры.Вставить("ТребуетсяЧтениеБезОграничений", Истина);
		КомандаПечати.ПроверкаПроведенияПередПечатью = Истина;
		
	КонецЕсли;
	
	Если Пользователи.РолиДоступны("ДобавлениеИзменениеКадровогоСостоянияРасширенная,ЧтениеКадровогоСостоянияРасширенная", , Ложь) Тогда
		
		// Трудовой договор
		КомандаПечати = КомандыПечати.Добавить();
		КомандаПечати.Обработчик = "УправлениеПечатьюБЗККлиент.ВыполнитьКомандуПечати";
		КомандаПечати.МенеджерПечати = "Обработка.ПечатьКадровыхПриказовРасширенная";
		КомандаПечати.Идентификатор = "ПФ_MXL_ТрудовойДоговор";
		КомандаПечати.Представление = НСтр("ru = 'Трудовые договоры';
											|en = 'Employment contracts'");
		КомандаПечати.Порядок = 20;
		КомандаПечати.ПроверкаПроведенияПередПечатью = Истина;
		
	КонецЕсли;
	
	// Трудовой договор микропредприятий
	Отчеты.ПечатнаяФормаТрудовойДоговорМикропредприятий.ДобавитьКомандуПечати(КомандыПечати);
	
	Если Пользователи.РолиДоступны("ДобавлениеИзменениеКадровогоСостоянияРасширенная,ЧтениеКадровогоСостоянияРасширенная", , Ложь) Тогда
		
		// Трудовой договор при дистанционной работе.
		КомандаПечати = КомандыПечати.Добавить();
		КомандаПечати.Обработчик = "УправлениеПечатьюБЗККлиент.ВыполнитьКомандуПечати";
		КомандаПечати.МенеджерПечати = "Обработка.ПечатьКадровыхПриказовРасширенная";
		КомандаПечати.Идентификатор = "ПФ_MXL_ТрудовойДоговорПриДистанционнойРаботе";
		КомандаПечати.Представление = НСтр("ru = 'Трудовые договоры при дистанционной работе';
											|en = 'Employment contracts for remote work'");
		КомандаПечати.Порядок = 30;
		КомандаПечати.ПроверкаПроведенияПередПечатью = Истина;
		
	КонецЕсли;
	
	Если ОбщегоНазначения.ПодсистемаСуществует("ЗарплатаКадрыПриложения.ГосударственнаяСлужба") Тогда
		
		Модуль = ОбщегоНазначения.ОбщийМодуль("ГосударственнаяСлужба");
		Модуль.ДобавитьКомандыПечатиДокументаПриемНаРаботу(КомандыПечати);
		Модуль.ДобавитьКомандуПечатиПриказаОПрисвоенииКлассногоЧинаСписком(КомандыПечати);
		Модуль.ДобавитьКомандуПечатиПриказаОПрисвоенииКлассногоЧина(КомандыПечати);
		
	КонецЕсли;
		
КонецПроцедуры

Функция ПолныеПраваНаДокумент() Экспорт 
	
	Возврат Пользователи.РолиДоступны("ДобавлениеИзменениеДанныхДляНачисленияЗарплатыРасширенная, ЧтениеДанныхДляНачисленияЗарплатыРасширенная", , Ложь);
	
КонецФункции	

Функция ДанныеДляПроверкиОграниченийНаУровнеЗаписей(Объект) Экспорт 
	
	ДанныеДляПроверкиОграничений = Новый Массив;

	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("Сотрудники", Объект.Сотрудники.Выгрузить(, "Подразделение,ФизическоеЛицо"));
	
	Запрос.Текст =
		"ВЫБРАТЬ
		|	ПриемНаРаботуСпискомСотрудники.Подразделение КАК Подразделение,
		|	ПриемНаРаботуСпискомСотрудники.ФизическоеЛицо КАК ФизическоеЛицо
		|ПОМЕСТИТЬ ВТСотрудники
		|ИЗ
		|	&Сотрудники КАК ПриемНаРаботуСпискомСотрудники
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ РАЗЛИЧНЫЕ
		|	Сотрудники.Подразделение КАК Подразделение,
		|	Сотрудники.ФизическоеЛицо КАК ФизическоеЛицо
		|ИЗ
		|	ВТСотрудники КАК Сотрудники
		|
		|УПОРЯДОЧИТЬ ПО
		|	Подразделение,
		|	ФизическоеЛицо";
		
	Выборка = Запрос.Выполнить().Выбрать();
	Пока Выборка.СледующийПоЗначениюПоля("Подразделение") Цикл
		
		ОписаниеСтруктурыДанных = ЗарплатаКадрыРасширенный.ОписаниеСтруктурыДанныхДляПроверкиОграниченийНаУровнеЗаписей();
		ОписаниеСтруктурыДанных.Организация = Объект.Организация;
		ОписаниеСтруктурыДанных.Подразделение = Выборка.Подразделение;
		
		Пока Выборка.Следующий() Цикл
			Если ОписаниеСтруктурыДанных.МассивФизическихЛиц = Неопределено Тогда
				ОписаниеСтруктурыДанных.МассивФизическихЛиц = Новый Массив;
			КонецЕсли; 
			ОписаниеСтруктурыДанных.МассивФизическихЛиц.Добавить(Выборка.ФизическоеЛицо);
		КонецЦикла; 
		
		ДанныеДляПроверкиОграничений.Добавить(ОписаниеСтруктурыДанных);

	КонецЦикла;
	
	Возврат ДанныеДляПроверкиОграничений;
	
КонецФункции

Функция ДанныеДляРегистрацииВУчетаСтажаПФР(МассивСсылок) Экспорт
	
	Возврат Документы.ПриемНаРаботу.ДанныеДляРегистрацииВУчетаСтажаПФР(МассивСсылок, Истина);
														
КонецФункции	

Процедура ЗаполнитьДатуЗапретаРедактирования(ОбъектДокумента) Экспорт
	
	ЗарплатаКадры.ЗаполнитьДатуЗапретаРедактированияСписочногоДокумента(ОбъектДокумента, "Сотрудники", "ДатаПриема");
	
КонецПроцедуры

Процедура ЗаполнитьДатыЗапрета(ПараметрыОбновления) Экспорт
	
	ОбновлениеВыполнено = Истина;
	
	Запрос = Новый Запрос;
	Запрос.Текст =
		"ВЫБРАТЬ ПЕРВЫЕ 100
		|	ПриемНаРаботуСписком.Ссылка КАК Ссылка,
		|	ПриемНаРаботуСписком.Дата КАК Дата
		|ИЗ
		|	Документ.ПриемНаРаботуСписком КАК ПриемНаРаботуСписком
		|ГДЕ
		|	ПриемНаРаботуСписком.ДатаЗапрета = ДАТАВРЕМЯ(1, 1, 1)
		|
		|УПОРЯДОЧИТЬ ПО
		|	ПриемНаРаботуСписком.Дата УБЫВ";
	
	РезультатЗапроса = Запрос.Выполнить();
	Если Не РезультатЗапроса.Пустой() Тогда
		
		ОбновлениеВыполнено = Ложь;
		Выборка = РезультатЗапроса.Выбрать();
		Пока Выборка.Следующий() Цикл
			
			Если Не ОбновлениеИнформационнойБазыЗарплатаКадрыБазовый.ПодготовитьОбновлениеДанных(
				ПараметрыОбновления, Выборка.Ссылка.Метаданные().ПолноеИмя(), "Ссылка", Выборка.Ссылка) Тогда
				
				Продолжить;
				
			КонецЕсли;
			
			ОбъектДокумента = Выборка.Ссылка.ПолучитьОбъект();
			
			МенеджерДокумента = ОбщегоНазначения.МенеджерОбъектаПоСсылке(Выборка.Ссылка);
			МенеджерДокумента.ЗаполнитьДатуЗапретаРедактирования(ОбъектДокумента);
			
			ОбъектДокумента.ДополнительныеСвойства.Вставить("ОтключитьПроверкуДатыЗапретаИзменения", Истина);
			
			ОбновлениеИнформационнойБазы.ЗаписатьОбъект(ОбъектДокумента);
			ОбновлениеИнформационнойБазыЗарплатаКадрыБазовый.ЗавершитьОбновлениеДанных(ПараметрыОбновления);
			
		КонецЦикла;
		
	КонецЕсли;
	
	ОбновлениеИнформационнойБазыЗарплатаКадрыБазовый.УстановитьПараметрОбновления(ПараметрыОбновления, "ОбработкаЗавершена", ОбновлениеВыполнено);
	
КонецПроцедуры

Функция ОписаниеПодписейДокумента() Экспорт 

	ОписаниеПодписей = ПодписиДокументов.ОписаниеТаблицыПодписей();

	ОписаниеПодписиРуководитель = ПодписиДокументов.ОписаниеРеквизитовПодписанта();
	ОписаниеПодписиРуководитель.ФизическоеЛицо = "Руководитель";
	ОписаниеПодписиРуководитель.Должность = "ДолжностьРуководителя";
	ОписаниеПодписиРуководитель.ОснованиеПодписи = "ОснованиеПредставителяНанимателя";

	ПереопределяемыеИмена = Новый Соответствие;
	ПереопределяемыеИмена.Вставить("Руководитель", ОписаниеПодписиРуководитель);

	ПодписиДокументов.ДобавитьОписаниеПодписейОрганизации(
		ОписаниеПодписей,
		"Руководитель",
		ПереопределяемыеИмена);

	Возврат ОписаниеПодписей;

КонецФункции

Процедура СформироватьДвиженияМероприятийТрудовойДеятельности(НаборЗаписей, ДанныеДляПроведения) Экспорт
	
	Документы.ПриемНаРаботу.СформироватьДвиженияМероприятийТрудовойДеятельности(НаборЗаписей, ДанныеДляПроведения);
	
КонецПроцедуры

Функция ДанныеДляПроведенияМероприятияТрудовойДеятельности(СсылкаНаДокумент, ТолькоПроведенные = Ложь) Экспорт
	
	Возврат Документы.ПриемНаРаботу.ДанныеДляПроведенияМероприятияТрудовойДеятельности(СсылкаНаДокумент, ТолькоПроведенные);
	
КонецФункции

#КонецОбласти

#КонецЕсли
