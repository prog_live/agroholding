﻿
Процедура ОбработкаЗаполнения(ДанныеЗаполнения, СтандартнаяОбработка)
	Если ТипЗнч(ДанныеЗаполнения) = Тип("ДокументСсылка.ИнвентаризационнаяОпись") Тогда
		// Заполнение шапки
		ДатаНачала = ДанныеЗаполнения.ДатаНачала;
		ДатаОкончания = ДанныеЗаполнения.ДатаОкончания;
		Организация = ДанныеЗаполнения.Организация;
		Ответственный = ДанныеЗаполнения.Ответственный;
		ДокИнвентаризация = ДанныеЗаполнения.Ссылка;
		Вид = "Приказ";
	КонецЕсли;
КонецПроцедуры
