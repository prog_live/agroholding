﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Свойство("АвтоТест") Тогда
		Возврат;
	КонецЕсли;
	
	Если Параметры.Отбор.Свойство("Организация") Тогда
		ОтборОрганизация = Параметры.Отбор.Организация;
	КонецЕсли;
	
	Если Параметры.Отбор.Свойство("РазделУчета") Тогда
		ОтборРазделУчета = Параметры.Отбор.РазделУчета;
	КонецЕсли;
	// СтандартныеПодсистемы.ПодключаемыеКоманды
	ПодключаемыеКоманды.ПриСозданииНаСервере(ЭтотОбъект);
	// Конец СтандартныеПодсистемы.ПодключаемыеКоманды
	СобытияФорм.ПриСозданииНаСервере(ЭтаФорма, Отказ, СтандартнаяОбработка);
	
	СтандартныеПодсистемыСервер.УстановитьУсловноеОформлениеПоляДата(ЭтотОбъект, "Список.Дата", Элементы.Дата.Имя);
	
	УдаляемыеРазделыУчета = Новый Массив;
	Если Не ПолучитьФункциональнуюОпцию("ИспользоватьЛизинг") Тогда
		УдаляемыеРазделыУчета.Добавить(Перечисления.ТипыОперацийВводаОстатков.ОстаткиВзаиморасчетовПоДоговорамЛизинга);
		УдаляемыеРазделыУчета.Добавить(Перечисления.ТипыОперацийВводаОстатков.ОстаткиПереданныхВАрендуПредметовЛизингаНаБалансе);
		УдаляемыеРазделыУчета.Добавить(Перечисления.ТипыОперацийВводаОстатков.ОстаткиПредметовЛизингаЗаБалансом);
		УдаляемыеРазделыУчета.Добавить(Перечисления.ТипыОперацийВводаОстатков.ОстаткиПредметовЛизингаНаБалансе);
	КонецЕсли;
	
	СписокВыбора = Элементы.ОтборРазделУчета.СписокВыбора;
	Для Каждого УдаляемыйРаздел Из УдаляемыеРазделыУчета Цикл
		ЭлементСписка = СписокВыбора.НайтиПоЗначению(УдаляемыйРаздел);
		Если ЭлементСписка <> Неопределено Тогда
			СписокВыбора.Удалить(ЭлементСписка);
		КонецЕсли;
	КонецЦикла;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыСписок

&НаКлиенте
Процедура СписокПриАктивизацииСтроки(Элемент)
	
	// СтандартныеПодсистемы.ПодключаемыеКоманды
	ПодключаемыеКомандыКлиент.НачатьОбновлениеКоманд(ЭтотОбъект);
	// Конец СтандартныеПодсистемы.ПодключаемыеКоманды
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытий

&НаКлиенте
Процедура ОтборОрганизацияПриИзменении(Элемент)
	
	ОтборыСписковКлиентСервер.ИзменитьЭлементОтбораСписка(Список, "Организация", ОтборОрганизация, ЗначениеЗаполнено(ОтборОрганизация));
	
КонецПроцедуры

&НаКлиенте
Процедура ОтборРазделУчетаПриИзменении(Элемент)
	
	ОтборыСписковКлиентСервер.ИзменитьЭлементОтбораСписка(Список, "ТипОперации", ОтборРазделУчета, ЗначениеЗаполнено(ОтборРазделУчета));
	
КонецПроцедуры

&НаКлиенте
Процедура СписокПередНачаломДобавления(Элемент, Отказ, Копирование, Родитель, Группа)
	
	Если Не Копирование Тогда
		Отказ = Истина;
		
		ЗначенияЗаполнения = Новый Структура("ТипОперации", ПредопределенноеЗначение("Перечисление.ТипыОперацийВводаОстатков.ОстаткиОС"));
		ОткрытьФорму("Документ.ВводОстатковВнеоборотныхАктивов.ФормаОбъекта", Новый Структура("ЗначенияЗаполнения", ЗначенияЗаполнения));
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура СоздатьВводОстатковНМА(Команда)
	
	ЗначенияЗаполнения = Новый Структура("ТипОперации", ПредопределенноеЗначение("Перечисление.ТипыОперацийВводаОстатков.ОстаткиНМА"));
	ОткрытьФорму("Документ.ВводОстатковВнеоборотныхАктивов.ФормаОбъекта", Новый Структура("ЗначенияЗаполнения", ЗначенияЗаполнения));
	
КонецПроцедуры

&НаКлиенте
Процедура СоздатьВводОстатковПереданныхВАрендуОС(Команда)
	
	ЗначенияЗаполнения = Новый Структура("ТипОперации", ПредопределенноеЗначение("Перечисление.ТипыОперацийВводаОстатков.ОстаткиПереданныхВАрендуОС"));
	ОткрытьФорму("Документ.ВводОстатковВнеоборотныхАктивов.ФормаОбъекта", Новый Структура("ЗначенияЗаполнения", ЗначенияЗаполнения));
	
КонецПроцедуры

&НаКлиенте
Процедура СоздатьВводОстатковАрендованныхОС(Команда)
	
	ЗначенияЗаполнения = Новый Структура("ТипОперации", ПредопределенноеЗначение("Перечисление.ТипыОперацийВводаОстатков.ОстаткиАрендованныхОС"));
	ОткрытьФорму("Документ.ВводОстатковВнеоборотныхАктивов.ФормаОбъекта", Новый Структура("ЗначенияЗаполнения", ЗначенияЗаполнения));
	
КонецПроцедуры

&НаКлиенте
Процедура СоздатьВводОстатковВзаиморасчетовПоДоговорамЛизинга(Команда)
	
	ЗначенияЗаполнения = Новый Структура("ТипОперации", ПредопределенноеЗначение("Перечисление.ТипыОперацийВводаОстатков.ОстаткиВзаиморасчетовПоДоговорамЛизинга"));
	ОткрытьФорму("Документ.ВводОстатковВнеоборотныхАктивов.ФормаОбъекта", Новый Структура("ЗначенияЗаполнения", ЗначенияЗаполнения));
	
КонецПроцедуры

&НаКлиенте
Процедура СоздатьВводОстатковПредметовЛизингаЗаБалансом(Команда)
	
	ЗначенияЗаполнения = Новый Структура("ТипОперации", ПредопределенноеЗначение("Перечисление.ТипыОперацийВводаОстатков.ОстаткиПредметовЛизингаЗаБалансом"));
	ОткрытьФорму("Документ.ВводОстатковВнеоборотныхАктивов.ФормаОбъекта", Новый Структура("ЗначенияЗаполнения", ЗначенияЗаполнения));
	
КонецПроцедуры

&НаКлиенте
Процедура СоздатьВводОстатковПредметовЛизингаНаБалансе(Команда)
	
	ЗначенияЗаполнения = Новый Структура("ТипОперации", ПредопределенноеЗначение("Перечисление.ТипыОперацийВводаОстатков.ОстаткиПредметовЛизингаНаБалансе"));
	ОткрытьФорму("Документ.ВводОстатковВнеоборотныхАктивов.ФормаОбъекта", Новый Структура("ЗначенияЗаполнения", ЗначенияЗаполнения));
	
КонецПроцедуры

&НаКлиенте
Процедура СоздатьВводОстатковПереданныхВАрендуПредметовЛизингаНаБалансе(Команда)
	
	ЗначенияЗаполнения = Новый Структура("ТипОперации", ПредопределенноеЗначение("Перечисление.ТипыОперацийВводаОстатков.ОстаткиПереданныхВАрендуПредметовЛизингаНаБалансе"));
	ОткрытьФорму("Документ.ВводОстатковВнеоборотныхАктивов.ФормаОбъекта", Новый Структура("ЗначенияЗаполнения", ЗначенияЗаполнения));
	
КонецПроцедуры

#Область СтандатрныеПодсистемы

// СтандартныеПодсистемы.ПодключаемыеКоманды
&НаКлиенте
Процедура Подключаемый_ВыполнитьКоманду(Команда)
	ПодключаемыеКомандыКлиент.ВыполнитьКоманду(ЭтотОбъект, Команда, Элементы.Список);
КонецПроцедуры

&НаСервере
Процедура Подключаемый_ВыполнитьКомандуНаСервере(Контекст, Результат) Экспорт
	ПодключаемыеКоманды.ВыполнитьКоманду(ЭтотОбъект, Контекст, Элементы.Список, Результат);
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ОбновитьКоманды()
	ПодключаемыеКомандыКлиентСервер.ОбновитьКоманды(ЭтотОбъект, Элементы.Список);
КонецПроцедуры
// Конец СтандартныеПодсистемы.ПодключаемыеКоманды

&НаКлиенте
Процедура Подключаемый_ВыполнитьПереопределяемуюКоманду(Команда)
	
	СобытияФормКлиент.ВыполнитьПереопределяемуюКоманду(ЭтаФорма, Команда);
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти

