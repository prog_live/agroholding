﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ПрограммныйИнтерфейс

#Область УсловияПродаж

// Заполняет условия продаж в поступлении товаров от хранителя.
//
// Параметры:
//	УсловияПродаж - Структура - Данные для заполнения.
//
Процедура ЗаполнитьУсловияПродаж(Знач УсловияПродаж) Экспорт
	
	Если УсловияПродаж = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	Валюта = УсловияПродаж.Валюта;
	ЦенаВключаетНДС         = УсловияПродаж.ЦенаВключаетНДС;
	НаправлениеДеятельности = УсловияПродаж.НаправлениеДеятельности;
	
	Если ЗначениеЗаполнено(УсловияПродаж.Организация) Тогда
		Организация = УсловияПродаж.Организация;
	КонецЕсли;
	
	Если Не УсловияПродаж.Типовое Тогда
		Если ЗначениеЗаполнено(УсловияПродаж.Контрагент) Тогда
			Контрагент = УсловияПродаж.Контрагент;
		КонецЕсли;
	КонецЕсли;
	
	ПартнерыИКонтрагенты.ЗаполнитьКонтрагентаПартнераПоУмолчанию(Партнер, Контрагент);
	
	ХозяйственнаяОперацияДоговора = Перечисления.ХозяйственныеОперации.ПередачаНаХранениеСПравомПродажи;
	
	Договор = ПродажиСервер.ПолучитьДоговорПоУмолчанию(ЭтотОбъект, ХозяйственнаяОперацияДоговора);
	
	Если ПолучитьФункциональнуюОпцию("ИспользоватьУчетДоходовПоНаправлениямДеятельности") Тогда
		НаправленияДеятельностиСервер.ЗаполнитьНаправлениеПоУмолчанию(НаправлениеДеятельности, Соглашение, Договор);
	КонецЕсли;
	
	Если ЗначениеЗаполнено(УсловияПродаж.Склад) Тогда
		Склад = УсловияПродаж.Склад;
	КонецЕсли;
	
	Если ЗначениеЗаполнено(УсловияПродаж.ВидЦен) Тогда
		ВидЦены = УсловияПродаж.ВидЦен;
	КонецЕсли;
	
КонецПроцедуры

// Заполняет условия продаж по умолчанию в поступлении товаров от хранителя.
//
Процедура ЗаполнитьУсловияПродажПоУмолчанию() Экспорт
	
	ИспользоватьСоглашенияСКлиентами = ПолучитьФункциональнуюОпцию("ИспользоватьСоглашенияСКлиентами");
	
	Если ЗначениеЗаполнено(Партнер)
		Или Не ИспользоватьСоглашенияСКлиентами Тогда
		
		ПараметрыОтбора = Новый Структура;
		ПараметрыОтбора.Вставить("ВыбранноеСоглашение",   Соглашение);
		ПараметрыОтбора.Вставить("ПустаяСсылкаДокумента", Документы.ПоступлениеТоваровОтХранителя.ПустаяСсылка());
		ПараметрыОтбора.Вставить("ХозяйственныеОперации",
								Перечисления.ХозяйственныеОперации.ПередачаНаХранениеСПравомПродажи);
		
		УсловияПродажПоУмолчанию = ПродажиСервер.ПолучитьУсловияПродажПоУмолчанию(Партнер, ПараметрыОтбора);
		
		Если УсловияПродажПоУмолчанию <> Неопределено Тогда
			
			Если Не ИспользоватьСоглашенияСКлиентами
				Или (Соглашение <> УсловияПродажПоУмолчанию.Соглашение
					И ЗначениеЗаполнено(УсловияПродажПоУмолчанию.Соглашение)) Тогда
				
				Соглашение = УсловияПродажПоУмолчанию.Соглашение;
				ЗаполнитьУсловияПродаж(УсловияПродажПоУмолчанию);
				
				Если ИспользоватьСоглашенияСКлиентами Тогда
					ЗаполнитьЦеныПоУсловиямПродаж();
				КонецЕсли;
				
			Иначе
				Соглашение = УсловияПродажПоУмолчанию.Соглашение;
				
				ПартнерыИКонтрагенты.ЗаполнитьКонтрагентаПартнераПоУмолчанию(Партнер, Контрагент);
			КонецЕсли;
			
		Иначе
			Соглашение = Неопределено;
			
			ПартнерыИКонтрагенты.ЗаполнитьКонтрагентаПартнераПоУмолчанию(Партнер, Контрагент);
		КонецЕсли;
		
	КонецЕсли;
	
КонецПроцедуры

// Заполняет условия продаж по соглашению в поступлении товаров от хранителя.
//
Процедура ЗаполнитьУсловияПродажПоСоглашению() Экспорт
	
	УсловияПродаж = ПродажиСервер.ПолучитьУсловияПродаж(Соглашение);
	
	ЗаполнитьУсловияПродаж(УсловияПродаж);
	ЗаполнитьЦеныПоУсловиямПродаж();
	
КонецПроцедуры

#КонецОбласти

// Функция формирует временные данные документа.
//
// Возвращаемое значение:
//	МенеджерВременныхТаблиц - менеджер временных таблиц.
//
Функция ВременныеТаблицыДанныхДокумента() Экспорт
	
	ТекстЗапроса = 
	"ВЫБРАТЬ
	|	&Дата                  КАК Дата,
	|	&Партнер               КАК Партнер,
	|	&Контрагент            КАК Контрагент,
	|	ЗНАЧЕНИЕ(Справочник.СоглашенияСПоставщиками.ПустаяСсылка) КАК Соглашение,
	|	&ХозяйственнаяОперация КАК ХозяйственнаяОперация,
	|	&Организация           КАК Организация,
	|	&Договор               КАК Договор,
	|	ЗНАЧЕНИЕ(Справочник.Валюты.ПустаяСсылка) КАК Валюта,
	|	ЗНАЧЕНИЕ(Перечисление.ТипыНалогообложенияНДС.ПустаяСсылка) КАК НалогообложениеНДС,
	|	ЛОЖЬ                   КАК ЕстьСделкиВТабличнойЧасти,
	|	&ТипЗапасов            КАК ТипЗапасов
	|ПОМЕСТИТЬ ТаблицаДанныхДокумента
	|;
	|
	|//////////////////////////////////////////////////////////////////////////////// 1
	|ВЫБРАТЬ
	|	ТаблицаТоваров.НомерСтроки         КАК НомерСтроки,
	|	ТаблицаТоваров.Номенклатура        КАК Номенклатура,
	|	ТаблицаТоваров.АналитикаУчетаНоменклатурыТоварыУПартнеров КАК АналитикаУчетаНоменклатуры,
	|	ТаблицаТоваров.Характеристика      КАК Характеристика,
	|	ТаблицаТоваров.Назначение          КАК Назначение,
	|	ВЫБОР
	|		КОГДА ТаблицаТоваров.СтатусУказанияСерийПереданныхТоваров = 18
	|				ИЛИ (ТаблицаТоваров.СтатусУказанияСерийПереданныхТоваров = 0
	|					И ТаблицаТоваров.СтатусУказанияСерийНаСкладах = 14)
	|			ТОГДА ТаблицаТоваров.Серия
	|		ИНАЧЕ ЗНАЧЕНИЕ(Справочник.СерииНоменклатуры.ПустаяСсылка)
	|	КОНЕЦ                              КАК Серия,
	|	ВЫБОР
	|		КОГДА ТаблицаТоваров.СтатусУказанияСерийПереданныхТоваров = 18
	|			ТОГДА ТаблицаТоваров.СтатусУказанияСерийПереданныхТоваров
	|		КОГДА ТаблицаТоваров.СтатусУказанияСерийПереданныхТоваров = 0
	|				И ТаблицаТоваров.СтатусУказанияСерийНаСкладах = 14
	|			ТОГДА ТаблицаТоваров.СтатусУказанияСерийНаСкладах
	|		ИНАЧЕ 0
	|	КОНЕЦ                              КАК СтатусУказанияСерий,
	|	ТаблицаТоваров.Количество          КАК Количество,
	|	&Договор                           КАК Склад,
	|	ТаблицаТоваров.НомерГТД            КАК НомерГТД,
	|	ЗНАЧЕНИЕ(Справочник.СделкиСКлиентами.ПустаяСсылка) КАК Сделка,
	|	ТаблицаТоваров.СтавкаНДС           КАК СтавкаНДС,
	|	ТаблицаТоваров.СуммаСНДС           КАК СуммаСНДС,
	|	ТаблицаТоваров.СуммаНДС            КАК СуммаНДС,
	|	0                                  КАК СуммаВознаграждения,
	|	0                                  КАК СуммаНДСВознаграждения
	|ПОМЕСТИТЬ ВтТаблицаТоваров
	|ИЗ
	|	&ТаблицаТоваров КАК ТаблицаТоваров
	|;
	|
	|//////////////////////////////////////////////////////////////////////////////// 2
	|ВЫБРАТЬ
	|	ТаблицаТоваров.НомерСтроки            КАК НомерСтроки,
	|	ТаблицаТоваров.Номенклатура           КАК Номенклатура,
	|	ТаблицаТоваров.АналитикаУчетаНоменклатуры КАК АналитикаУчетаНоменклатуры,
	|	ТаблицаТоваров.Характеристика         КАК Характеристика,
	|	ТаблицаТоваров.Назначение             КАК Назначение,
	|	ТаблицаТоваров.Серия                  КАК Серия,
	|	ТаблицаТоваров.СтатусУказанияСерий	  КАК СтатусУказанияСерий,
	|	ТаблицаТоваров.Количество             КАК Количество,
	|	ТаблицаТоваров.Склад                  КАК Склад,
	|	ТаблицаТоваров.НомерГТД               КАК НомерГТД,
	|	ТаблицаТоваров.Сделка                 КАК Сделка,
	|	ТаблицаТоваров.СтавкаНДС              КАК СтавкаНДС,
	|	ТаблицаТоваров.СуммаСНДС              КАК СуммаСНДС,
	|	ТаблицаТоваров.СуммаНДС               КАК СуммаНДС,
	|	ТаблицаТоваров.СуммаВознаграждения    КАК СуммаВознаграждения,
	|	ТаблицаТоваров.СуммаНДСВознаграждения КАК СуммаНДСВознаграждения,
	|	ВЫБОР
	|		КОГДА СпрНоменклатура.ТипНоменклатуры = ЗНАЧЕНИЕ(Перечисление.ТипыНоменклатуры.МногооборотнаяТара)
	|				И &ВозвратПереданнойМногооборотнойТары
	|			ТОГДА ЛОЖЬ
	|		ИНАЧЕ ИСТИНА
	|	КОНЕЦ                              КАК ПодбиратьВидыЗапасов
	|ПОМЕСТИТЬ ТаблицаТоваров
	|ИЗ
	|	ВтТаблицаТоваров КАК ТаблицаТоваров
	|		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.Номенклатура КАК СпрНоменклатура
	|		ПО ТаблицаТоваров.Номенклатура = СпрНоменклатура.Ссылка
	|;
	|
	|//////////////////////////////////////////////////////////////////////////////// 3
	|ВЫБРАТЬ
	|	ТаблицаВидыЗапасов.НомерСтроки      КАК НомерСтроки,
	|	ТаблицаВидыЗапасов.АналитикаУчетаНоменклатурыТоварыУПартнеров КАК АналитикаУчетаНоменклатуры,
	|	ТаблицаВидыЗапасов.ВидЗапасов       КАК ВидЗапасов,
	|	ТаблицаВидыЗапасов.Количество       КАК Количество,
	|	ТаблицаВидыЗапасов.НомерГТД         КАК НомерГТД,
	|	ТаблицаВидыЗапасов.Серия            КАК Серия,
	|	ТаблицаВидыЗапасов.СтавкаНДС        КАК СтавкаНДС,
	|	ТаблицаВидыЗапасов.СуммаНДС         КАК СуммаНДС,
	|	ТаблицаВидыЗапасов.СуммаСНДС        КАК СуммаСНДС
	|ПОМЕСТИТЬ ВтВидыЗапасов
	|ИЗ
	|	&ТаблицаВидыЗапасов КАК ТаблицаВидыЗапасов
	|;
	|
	|///////////////////////////////////////////////////////////////////////////////// 4
	|ВЫБРАТЬ
	|	ТаблицаВидыЗапасов.НомерСтроки КАК НомерСтроки,
	|	ТаблицаВидыЗапасов.ВидЗапасов  КАК ВидЗапасов,
	|	ТаблицаВидыЗапасов.АналитикаУчетаНоменклатуры КАК АналитикаУчетаНоменклатуры,
	|	Аналитика.Номенклатура         КАК Номенклатура,
	|	Аналитика.Характеристика       КАК Характеристика,
	|	Аналитика.Серия                КАК Серия,
	|	ТаблицаВидыЗапасов.Количество  КАК Количество,
	|	НЕОПРЕДЕЛЕНО                   КАК ВидЗапасовПолучателя,
	|	ТаблицаВидыЗапасов.НомерГТД    КАК НомерГТД,
	|	Аналитика.МестоХранения        КАК Склад,
	|	ТаблицаВидыЗапасов.СтавкаНДС   КАК СтавкаНДС,
	|	ТаблицаВидыЗапасов.СуммаСНДС   КАК СуммаСНДС,
	|	ТаблицаВидыЗапасов.СуммаНДС    КАК СуммаНДС,
	|	0                              КАК СуммаВознаграждения,
	|	0                              КАК СуммаНДСВознаграждения,
	|	ЗНАЧЕНИЕ(Справочник.СделкиСКлиентами.ПустаяСсылка) КАК Сделка,
	|	&ВидыЗапасовУказаныВручную     КАК ВидыЗапасовУказаныВручную
	|ПОМЕСТИТЬ ТаблицаВидыЗапасов
	|ИЗ
	|	ВтВидыЗапасов КАК ТаблицаВидыЗапасов
	|		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.КлючиАналитикиУчетаНоменклатуры КАК Аналитика
	|		ПО ТаблицаВидыЗапасов.АналитикаУчетаНоменклатуры = Аналитика.Ссылка
	|
	|ИНДЕКСИРОВАТЬ ПО
	|	АналитикаУчетаНоменклатуры";
	
	Запрос = Новый Запрос;
	Запрос.Текст = ТекстЗапроса;
	Запрос.МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;
	
	Запрос.УстановитьПараметр("ТаблицаТоваров",        Товары);
	Запрос.УстановитьПараметр("ТаблицаВидыЗапасов",    ВидыЗапасов);
	Запрос.УстановитьПараметр("Ссылка",                Ссылка);
	Запрос.УстановитьПараметр("Дата",                  Дата);
	Запрос.УстановитьПараметр("Партнер",               Партнер);
	Запрос.УстановитьПараметр("Контрагент",            Контрагент);
	Запрос.УстановитьПараметр("Договор",               Договор);
	Запрос.УстановитьПараметр("Организация",           Организация);
	Запрос.УстановитьПараметр("ХозяйственнаяОперация", ХозяйственнаяОперация);
	Запрос.УстановитьПараметр("Склад",                 Склад);
	Запрос.УстановитьПараметр("ТипЗапасов",            Перечисления.ТипыЗапасов.Товар);
	Запрос.УстановитьПараметр("ВидыЗапасовУказаныВручную", ВидыЗапасовУказаныВручную);
	Запрос.УстановитьПараметр("ВозвратПереданнойМногооборотнойТары", ВозвратПереданнойМногооборотнойТары);
	
	ЗапасыСервер.ДополнитьВременныеТаблицыОбязательнымиКолонками(Запрос);
	
	Запрос.Выполнить();
	
	Возврат Запрос.МенеджерВременныхТаблиц;
	
КонецФункции

#КонецОбласти

#Область ОбработчикиСобытий

Процедура ОбработкаПроверкиЗаполнения(Отказ, ПроверяемыеРеквизиты)
	
	МассивНепроверяемыхРеквизитов = Новый Массив;
	
	ОбщегоНазначенияУТ.ПроверитьЗаполнениеКоличества(ЭтотОбъект, ПроверяемыеРеквизиты, Отказ);
	
	ПараметрыУказанияСерий = НоменклатураСервер.ПараметрыУказанияСерий(ЭтотОбъект, Документы.ПоступлениеТоваровОтХранителя);
	
	НоменклатураСервер.ПроверитьЗаполнениеХарактеристик(ЭтотОбъект, МассивНепроверяемыхРеквизитов, Отказ);
	НоменклатураСервер.ПроверитьЗаполнениеСерий(ЭтотОбъект, ПараметрыУказанияСерий, Отказ, МассивНепроверяемыхРеквизитов);
	
	Если ЗначениеЗаполнено(НаправлениеДеятельности)
		Или Не НаправленияДеятельностиСервер.УказаниеНаправленияДеятельностиОбязательно(ХозяйственнаяОперация) Тогда
		
		МассивНепроверяемыхРеквизитов.Добавить("НаправлениеДеятельности");
		
	КонецЕсли;
	
	МассивНепроверяемыхРеквизитов.Добавить("Товары.НомерГТД");
	Если ПолучитьФункциональнуюОпцию("ЗапретитьПоступлениеТоваровБезНомеровГТД") Тогда
		ЗапасыСервер.ПроверитьЗаполнениеНомеровГТД(ЭтотОбъект, Отказ);
	КонецЕсли;
	
	ОбщегоНазначения.УдалитьНепроверяемыеРеквизитыИзМассива(ПроверяемыеРеквизиты, МассивНепроверяемыхРеквизитов);
	
	Если Не Отказ
		И ОбщегоНазначенияУТ.ПроверитьЗаполнениеРеквизитовОбъекта(ЭтотОбъект, ПроверяемыеРеквизиты) Тогда
		
		Отказ = Истина;
		
	КонецЕсли;
	
	ПродажиСервер.ПроверитьКорректностьЗаполненияДокументаПродажи(ЭтотОбъект, Отказ);
	
	ПоступлениеТоваровОтХранителяЛокализация.ОбработкаПроверкиЗаполнения(ЭтотОбъект, Отказ, ПроверяемыеРеквизиты);
	
КонецПроцедуры

Процедура ОбработкаЗаполнения(ДанныеЗаполнения, СтандартнаяОбработка)
	
	ТипДанныхЗаполнения = ТипЗнч(ДанныеЗаполнения);
	
	Если ТипЗнч(ДанныеЗаполнения) = Тип("Структура")
		И ДанныеЗаполнения.Свойство("МассивЗаказов")
		И ДанныеЗаполнения.МассивЗаказов.Количество() > 0 Тогда
		
		РаспоряжениеЗаполнения = ДанныеЗаполнения.МассивЗаказов[0];
		ОперацияРаспоряжения   = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(РаспоряжениеЗаполнения, "ХозяйственнаяОперация");
		
		Если ТипЗнч(РаспоряжениеЗаполнения) = Тип("ДокументСсылка.ПередачаТоваровХранителю")
			И ОперацияРаспоряжения = Перечисления.ХозяйственныеОперации.ПередачаНаХранениеСПравомПродажи Тогда
			
			ОперацияРаспоряжения = Перечисления.ХозяйственныеОперации.ВозвратОтХранителя;
			
		КонецЕсли;
		
		ОбщегоНазначенияУТ.ПроверитьВозможностьВводаНаОснованииПоОперации(ДанныеЗаполнения.МассивЗаказов[0],
																			"ПоступлениеТоваровОтХранителя",
																			ОперацияРаспоряжения);
	КонецЕсли;
	
	Если ТипДанныхЗаполнения = Тип("Структура")
		И ДанныеЗаполнения.Свойство("МассивЗаказов") Тогда
		
		ЗаполнитьПоЗаказу(ДанныеЗаполнения);
		
	ИначеЕсли ТипДанныхЗаполнения = Тип("Структура")
		И ДанныеЗаполнения.Свойство("ОснованиеАкта") Тогда
		
		ЗаполнитьПоАктуОРасхождениях(ДанныеЗаполнения);
		
	КонецЕсли;
	
	Если Не ПолучитьФункциональнуюОпцию("ИспользоватьСоглашенияСКлиентами") Тогда
		
		ЗаполнитьУсловияПродажПоУмолчанию();
		
	КонецЕсли;
	
	ИнициализироватьДокумент(ДанныеЗаполнения);
	
	ЗаполнениеСвойствПоСтатистикеСервер.ЗаполнитьСвойстваОбъекта(ЭтотОбъект, ДанныеЗаполнения);
	
	ПоступлениеТоваровОтХранителяЛокализация.ОбработкаЗаполнения(ЭтотОбъект, ДанныеЗаполнения, СтандартнаяОбработка);
	
КонецПроцедуры

Процедура ПередЗаписью(Отказ, РежимЗаписи, РежимПроведения)
	
	Если ОбменДанными.Загрузка Тогда
		Возврат;
	КонецЕсли;
	
	ОбновлениеИнформационнойБазы.ПроверитьОбъектОбработан(ЭтотОбъект);
	
	ПроведениеСерверУТ.УстановитьРежимПроведения(ЭтотОбъект, РежимЗаписи, РежимПроведения);

	ДополнительныеСвойства.Вставить("ЭтоНовый",    ЭтоНовый());
	ДополнительныеСвойства.Вставить("РежимЗаписи", РежимЗаписи);
	
	ОбщегоНазначенияУТ.ОкруглитьКоличествоШтучныхТоваров(ЭтотОбъект, РежимЗаписи);
	
	СуммаДокумента         = ПолучитьСуммуДокумента();
	ПараметрыУказанияСерий = НоменклатураСервер.ПараметрыУказанияСерий(ЭтотОбъект, Документы.ПоступлениеТоваровОтХранителя);
	
	НоменклатураСервер.ОчиститьНеиспользуемыеСерии(ЭтотОбъект, ПараметрыУказанияСерий);
	
	Если РежимЗаписи = РежимЗаписиДокумента.Проведение Тогда
		
		МестаУчета = РегистрыСведений.АналитикаУчетаНоменклатуры.МестаУчета(Неопределено,
																			Склад,
																			Подразделение,
																			Партнер,
																			Договор);
		
		ИменаПолей = РегистрыСведений.АналитикаУчетаНоменклатуры.ИменаПолейКоллекцииПоУмолчанию();
		ИменаПолей.СтатусУказанияСерий = "СтатусУказанияСерийНаСкладах";
		
		РегистрыСведений.АналитикаУчетаНоменклатуры.ЗаполнитьВКоллекции(Товары,
																		МестаУчета,
																		ИменаПолей);
		
		МестаУчетаУПартнеров = РегистрыСведений.АналитикаУчетаНоменклатуры.МестаУчета(ХозяйственнаяОперация,
																					Договор,
																					Подразделение,
																					Партнер,
																					Договор);
		
		ИменаПолей.СтатусУказанияСерий        = "СтатусУказанияСерийПереданныхТоваров";
		ИменаПолей.АналитикаУчетаНоменклатуры = "АналитикаУчетаНоменклатурыТоварыУПартнеров";
		
		РегистрыСведений.АналитикаУчетаНоменклатуры.ЗаполнитьВКоллекции(Товары,
																		МестаУчетаУПартнеров,
																		ИменаПолей);
		
		ЗаполнитьВидыЗапасов(Отказ);
		ВзаиморасчетыСервер.ЗаполнитьИдентификаторыСтрокВТабличнойЧасти(ВидыЗапасов);
		
	ИначеЕсли РежимЗаписи = РежимЗаписиДокумента.ОтменаПроведения Тогда
		
		Если Не ВидыЗапасовУказаныВручную Тогда
			ВидыЗапасов.Очистить();
		КонецЕсли;
		
	КонецЕсли;
	
	ПоступлениеТоваровОтХранителяЛокализация.ПередЗаписью(ЭтотОбъект, Отказ, РежимЗаписи, РежимПроведения);
	
КонецПроцедуры

Процедура ПриЗаписи(Отказ)
	
	Если ОбменДанными.Загрузка Тогда
		
		Возврат;
		
	КонецЕсли;
	
	Если Не Отказ
		И Не ДополнительныеСвойства.РежимЗаписи = РежимЗаписиДокумента.Проведение Тогда
		
		РегистрыСведений.РеестрДокументов.ИнициализироватьИЗаписатьДанныеДокумента(Ссылка, ДополнительныеСвойства, Отказ);
		
	КонецЕсли;
	
	Если ДополнительныеСвойства.РежимЗаписи = РежимЗаписиДокумента.Запись Тогда
		Возврат;
	КонецЕсли;
	
	ПоступлениеТоваровОтХранителяЛокализация.ПриЗаписи(ЭтотОбъект, Отказ);
	
КонецПроцедуры

Процедура ОбработкаПроведения(Отказ, РежимПроведения)
	
	ПроведениеСерверУТ.ИнициализироватьДополнительныеСвойстваДляПроведения(Ссылка, ДополнительныеСвойства, РежимПроведения);
	
	Документы.ПоступлениеТоваровОтХранителя.ИнициализироватьДанныеДокумента(Ссылка, ДополнительныеСвойства);
	
	ПроведениеСерверУТ.ПодготовитьНаборыЗаписейКРегистрацииДвижений(ЭтотОбъект);
	
	ДоходыИРасходыСервер.ОтразитьСебестоимостьТоваров(ДополнительныеСвойства, Движения, Отказ);
	ЗаказыСервер.ОтразитьДвижениеТоваров(ДополнительныеСвойства, Движения, Отказ);
	ЗапасыСервер.ОтразитьОбеспечениеЗаказов(ДополнительныеСвойства, Движения, Отказ);
	ЗаказыСервер.ОтразитьТоварыКПоступлению(ДополнительныеСвойства, Движения, Отказ);
	ЗапасыСервер.ОтразитьСвободныеОстатки(ДополнительныеСвойства, Движения, Отказ);
	ЗапасыСервер.ОтразитьТоварыНаСкладах(ДополнительныеСвойства, Движения, Отказ);
	ЗапасыСервер.ОтразитьТоварыОрганизаций(ДополнительныеСвойства, Движения, Отказ);
	СкладыСервер.ОтразитьДвиженияСерийТоваров(ДополнительныеСвойства, Движения, Отказ);
	ЗаказыСервер.ОтразитьЗаявкиНаВозвратТоваровОтКлиентов(ДополнительныеСвойства, Движения, Отказ);
	РегистрыСведений.РеестрДокументов.ЗаписатьДанныеДокумента(Ссылка, ДополнительныеСвойства, Отказ);
	
	// Движения по оборотным регистрам управленческого учета
	УправленческийУчетПроведениеСервер.ОтразитьДвиженияНоменклатураНоменклатура(ДополнительныеСвойства, Движения, Отказ);
	
	СформироватьСписокРегистровДляКонтроля();
	
	ЗапасыСервер.ПодготовитьЗаписьТоваровОрганизаций(ЭтотОбъект, РежимЗаписиДокумента.Проведение);
	
	ПоступлениеТоваровОтХранителяЛокализация.ОбработкаПроведения(ЭтотОбъект, Отказ, РежимПроведения);
	
	ПроведениеСерверУТ.ЗаписатьНаборыЗаписей(ЭтотОбъект);
	
	//++ НЕ УТКА
	МеждународныйУчетПроведениеСервер.ЗарегистрироватьКОтражению(ЭтотОбъект, ДополнительныеСвойства, Движения, Отказ);
	//-- НЕ УТКА
	
	ПараметрыЗаполнения = ЗапасыСервер.ПараметрыЗаполненияВидовЗапасов();
	ЗапасыСервер.СформироватьРезервыПоТоварамОрганизаций(ЭтотОбъект, Отказ, ПараметрыЗаполнения);
	
	ПроведениеСерверУТ.ВыполнитьКонтрольРезультатовПроведения(ЭтотОбъект, Отказ);
	
	РегистрыСведений.СостоянияЗаказовКлиентов.ОтразитьСостояниеЗаказа(ЭтотОбъект, Отказ);
	
	ПроведениеСерверУТ.СформироватьЗаписиРегистровЗаданий(ЭтотОбъект);
	ПроведениеСерверУТ.ОчиститьДополнительныеСвойстваДляПроведения(ДополнительныеСвойства);
	
КонецПроцедуры

Процедура ОбработкаУдаленияПроведения(Отказ)
	
	ПроведениеСерверУТ.ИнициализироватьДополнительныеСвойстваДляПроведения(Ссылка, ДополнительныеСвойства);
	
	ПроведениеСерверУТ.ПодготовитьНаборыЗаписейКРегистрацииДвижений(ЭтотОбъект);
	
	СформироватьСписокРегистровДляКонтроля();
	
	ЗапасыСервер.ПодготовитьЗаписьТоваровОрганизаций(ЭтотОбъект, РежимЗаписиДокумента.ОтменаПроведения);
	
	ПоступлениеТоваровОтХранителяЛокализация.ОбработкаУдаленияПроведения(ЭтотОбъект, Отказ);
	
	ПроведениеСерверУТ.ЗаписатьНаборыЗаписей(ЭтотОбъект);
	
	ПараметрыЗаполнения = ЗапасыСервер.ПараметрыЗаполненияВидовЗапасов();
	ЗапасыСервер.СформироватьРезервыПоТоварамОрганизаций(ЭтотОбъект, Отказ, ПараметрыЗаполнения);
	
	ПроведениеСерверУТ.ВыполнитьКонтрольРезультатовПроведения(ЭтотОбъект, Отказ);
	
	РегистрыСведений.СостоянияЗаказовКлиентов.ОтразитьСостояниеЗаказа(ЭтотОбъект, Отказ);
	
	ПроведениеСерверУТ.СформироватьЗаписиРегистровЗаданий(ЭтотОбъект);
	ПроведениеСерверУТ.ОчиститьДополнительныеСвойстваДляПроведения(ДополнительныеСвойства);
	
КонецПроцедуры

Процедура ПриКопировании(ОбъектКопирования)
	
	Распоряжение = Документы.ЗаявкаНаВозвратТоваровОтКлиента.ПустаяСсылка();
	СостояниеЗаполненияМногооборотнойТары = Перечисления.СостоянияЗаполненияМногооборотнойТары.ПустаяСсылка();
	
	Для Каждого ТекСтрока Из Товары Цикл
		ТекСтрока.КодСтроки = 0;
	КонецЦикла;
	
	Серии.Очистить();
	ВидыЗапасов.Очистить();
	
	ИнициализироватьДокумент();
	
	ПоступлениеТоваровОтХранителяЛокализация.ПриКопировании(ЭтотОбъект, ОбъектКопирования);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область ИнициализацияИЗаполнение

Процедура ЗаполнитьПоЗаказу(СтруктураЗаполнения)
	
	МассивЗаказов = СтруктураЗаполнения.МассивЗаказов;
	
	Если СтруктураЗаполнения.Свойство("Склад")
		И ЗначениеЗаполнено(СтруктураЗаполнения.Склад) Тогда
		
		СтруктураЗаполнения.РеквизитыШапки.Склад = СтруктураЗаполнения.Склад;
		
	КонецЕсли;
	
	ПараметрыЗаполнения = Документы.ПоступлениеТоваровОтХранителя.ПараметрыЗаполненияДокумента();
	ПараметрыЗаполнения.ДополнятьСериямиПоЗаявке = Истина;
	ЗаполнитьЗначенияСвойств(ПараметрыЗаполнения, СтруктураЗаполнения);
	
	Документы.ПоступлениеТоваровОтХранителя.ИнициализироватьПараметрыЗаполнения(ПараметрыЗаполнения,
		СтруктураЗаполнения.РеквизитыШапки, МассивЗаказов);
	
	Если СтруктураЗаполнения.Свойство("ЗаполнятьПоОрдеру") Тогда
		ПараметрыЗаполнения.ЗаполнятьПоОрдеру = СтруктураЗаполнения.ЗаполнятьПоОрдеру;
	Иначе
		СкладПоступления = СтруктураЗаполнения.РеквизитыШапки.Склад;
		ИспользоватьОрдернуюСхему = СкладыСервер.ИспользоватьОрдернуюСхемуПриПоступлении(СкладПоступления,
																						ТекущаяДатаСеанса());
		
		ПараметрыЗаполнения.ЗаполнятьПоОрдеру = ИспользоватьОрдернуюСхему;
	КонецЕсли;
	
	ТаблицаНакладная = Документы.ПоступлениеТоваровОтХранителя.ДанныеТаблицыТоварыДокумента(Ссылка);
	
	Документы.ПоступлениеТоваровОтХранителя.ЗаполнитьПоЗаказамОрдерам(ТаблицаНакладная, Ссылка, ПараметрыЗаполнения);
	
	Если ПараметрыЗаполнения.ЗаполнятьПоОрдеру Тогда
		ТаблицаНакладная.Колонки.Количество.Имя        = "КоличествоДоИзменения";
		ТаблицаНакладная.Колонки.КоличествоВОрдере.Имя = "Количество";
	Иначе
		ТаблицаНакладная.Колонки.Количество.Имя        = "КоличествоДоИзменения";
		ТаблицаНакладная.Колонки.КоличествоВЗаказе.Имя = "Количество";
	КонецЕсли;
	
	НакладныеСервер.УдалитьПустыеСтроки(ТаблицаНакладная, "Количество");
	
	Товары.Загрузить(ТаблицаНакладная);
	
	Документы.ПоступлениеТоваровОтХранителя.ЗаполнитьШапкуДокументаПоЗаказу(ЭтотОбъект, ПараметрыЗаполнения, СтруктураЗаполнения.МассивЗаказов);
	
	Документы.ПоступлениеТоваровОтХранителя.ОбновитьЗависимыеРеквизитыТабличнойЧасти(Товары, ПараметрыЗаполнения);
	
	ПараметрыУказанияСерий = НоменклатураСервер.ПараметрыУказанияСерий(ЭтотОбъект, Документы.ПоступлениеТоваровОтХранителя);
	НоменклатураСервер.ЗаполнитьСтатусыУказанияСерий(ЭтотОбъект, ПараметрыУказанияСерий);
	НоменклатураСервер.ОчиститьНеиспользуемыеСерии(ЭтотОбъект, ПараметрыУказанияСерий);
	
	ЗаполнитьУсловияПродажПоСоглашению();
	
КонецПроцедуры

Процедура ЗаполнитьПоАктуОРасхождениях(СтруктураЗаполнения)
	
	ПараметрыЗаполнения = Документы.ПоступлениеТоваровОтХранителя.ПараметрыЗаполненияДокумента();
	ЗаполнитьЗначенияСвойств(ПараметрыЗаполнения, СтруктураЗаполнения);
	
	РеквизитыШапки = "Распоряжение, ХозяйственнаяОперация, Организация, Подразделение, Склад, Сделка, Партнер, Контрагент,
						|Соглашение, Договор, НаправлениеДеятельности, ЦенаВключаетНДС";
	ПоляШапки      = ОбщегоНазначения.ЗначенияРеквизитовОбъекта(СтруктураЗаполнения.ОснованиеАкта, РеквизитыШапки);
	
	МассивЗаказов = Новый Массив();
	МассивЗаказов.Добавить(ПоляШапки.Распоряжение);
	
	ПараметрыЗаполнения.РеквизитыШапки = ПоляШапки;
	ЗаполнитьЗначенияСвойств(ПараметрыЗаполнения, ПоляШапки);
	
	ПустоеПоступлениОтХранителя = Документы.ПоступлениеТоваровОтХранителя.ПустаяСсылка();
	
	ТаблицаНакладная = Документы.ПоступлениеТоваровОтХранителя.ДанныеТаблицыТоварыДокумента(ПустоеПоступлениОтХранителя);
	
	Документы.ПоступлениеТоваровОтХранителя.ЗаполнитьПоЗаказамОрдерам(ТаблицаНакладная, Ссылка, ПараметрыЗаполнения);
	
	ТаблицаНакладная.Колонки.Количество.Имя        = "КоличествоДоИзменения";
	ТаблицаНакладная.Колонки.КоличествоВЗаказе.Имя = "Количество";
	
	НакладныеСервер.УдалитьПустыеСтроки(ТаблицаНакладная, "Количество");
	
	Товары.Загрузить(ТаблицаНакладная);
	
	Документы.ПоступлениеТоваровОтХранителя.ЗаполнитьШапкуДокументаПоЗаказу(ЭтотОбъект, ПараметрыЗаполнения, МассивЗаказов);
	
	ДовозвратПоПоступлению = СтруктураЗаполнения.ОснованиеАкта;
	
	Документы.ПоступлениеТоваровОтХранителя.ОбновитьЗависимыеРеквизитыТабличнойЧасти(Товары, ПараметрыЗаполнения);
	
	ЗаполнитьУсловияПродажПоСоглашению();
	
КонецПроцедуры

Процедура ИнициализироватьДокумент(ДанныеЗаполнения = Неопределено)
	
	Распоряжение  = ДокументОснованиеПриЗаполнении(ДанныеЗаполнения);
	Организация   = ЗначениеНастроекПовтИсп.ПолучитьОрганизациюПоУмолчанию(Организация);
	Склад         = ЗначениеНастроекПовтИсп.ПолучитьСкладПоУмолчанию(Склад);
	Валюта        = ЗначениеНастроекПовтИсп.ПолучитьВалютуРегламентированногоУчета(Валюта);
	Менеджер      = Пользователи.ТекущийПользователь();
	Подразделение = ЗначениеНастроекПовтИсп.ПодразделениеПользователя(Менеджер, Подразделение);
	
	ВариантПриемкиТоваров = ЗакупкиСервер.ПолучитьВариантПриемкиТоваров(Распоряжение);
	
КонецПроцедуры

Функция ДокументОснованиеПриЗаполнении(ДанныеЗаполнения)
	
	Если ТипЗнч(ДанныеЗаполнения) = Тип("Структура")
		И ДанныеЗаполнения.Свойство("МассивЗаказов")
		И ДанныеЗаполнения.МассивЗаказов.Количество() > 0
		И ТипЗнч(ДанныеЗаполнения.МассивЗаказов[0]) = Тип("ДокументСсылка.ЗаявкаНаВозвратТоваровОтКлиента") Тогда
		
		Возврат ДанныеЗаполнения.МассивЗаказов[0];
		
	ИначеЕсли ТипЗнч(ДанныеЗаполнения) = Тип("ДокументСсылка.ЗаявкаНаВозвратТоваровОтКлиента") Тогда
		
		Возврат ДанныеЗаполнения;
		
	КонецЕсли;
	
	Возврат Неопределено;
	
КонецФункции

#КонецОбласти

#Область ВидыЗапасов

Процедура ЗаполнитьВидыЗапасов(Отказ)
	
	Если Отказ Тогда
		Возврат;
	КонецЕсли;
	
	УстановитьПривилегированныйРежим(Истина);
	
	МенеджерВременныхТаблиц = ВременныеТаблицыДанныхДокумента();
	
	Если Не Проведен
		Или ЗапасыСервер.ПроверитьНеобходимостьПерезаполненияВидовЗапасовДокумента(ЭтотОбъект)
		Или ПроверитьИзменениеРеквизитовДокумента(МенеджерВременныхТаблиц)
		Или ПроверитьИзменениеТоваров(МенеджерВременныхТаблиц) Тогда
		
		ПараметрыЗаполнения = ПараметрыЗаполненияВидовЗапасов();
		ЗапасыСервер.ЗаполнитьВидыЗапасовПоТоварамОрганизаций(ЭтотОбъект, МенеджерВременныхТаблиц, Отказ, ПараметрыЗаполнения);
		
		КолонкиГруппировки  = "АналитикаУчетаНоменклатуры, АналитикаУчетаНоменклатурыТоварыУПартнеров, ВидЗапасов,
								|НомерГТД, СтавкаНДС";
		КолонкиСуммирования = "Количество, СуммаСНДС, СуммаНДС";
		
		ВидыЗапасов.Свернуть(КолонкиГруппировки, КолонкиСуммирования);
		
		Если Не Отказ Тогда
			СинхронизироватьАналитикуУчетаНоменклатурыМеждуТабличнымиЧастями();
		КонецЕсли;
		
	КонецЕсли;
	
	Если ПривилегированныйРежим() Тогда
		УстановитьПривилегированныйРежим(Ложь);
	КонецЕсли;
	
КонецПроцедуры

Функция ПроверитьИзменениеРеквизитовДокумента(МенеджерВременныхТаблиц)
	
	ИменаРеквизитов = "Организация";
	
	Возврат ЗапасыСервер.ПроверитьИзменениеРеквизитовДокумента(МенеджерВременныхТаблиц, Ссылка, ИменаРеквизитов);
	
КонецФункции

Функция ПроверитьИзменениеТоваров(МенеджерВременныхТаблиц)
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
	"////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ТаблицаТоваров.АналитикаУчетаНоменклатуры КАК АналитикаУчетаНоменклатуры
	|ИЗ
	|	(ВЫБРАТЬ
	|		ТаблицаТоваров.АналитикаУчетаНоменклатуры КАК АналитикаУчетаНоменклатуры,
	|		ТаблицаТоваров.Номенклатура   КАК Номенклатура,
	|		ТаблицаТоваров.Характеристика КАК Характеристика,
	|		ТаблицаТоваров.СтавкаНДС      КАК СтавкаНДС,
	|		ТаблицаТоваров.Количество     КАК Количество,
	|		ТаблицаТоваров.СуммаНДС       КАК СуммаНДС,
	|		ТаблицаТоваров.СуммаСНДС      КАК СуммаСНДС,
	|		ТаблицаТоваров.НомерГТД       КАК НомерГТД
	|	ИЗ
	|		ТаблицаТоваров КАК ТаблицаТоваров
	|	
	|	ОБЪЕДИНИТЬ ВСЕ
	|	
	|	ВЫБРАТЬ
	|		ТаблицаВидыЗапасов.АналитикаУчетаНоменклатуры КАК АналитикаУчетаНоменклатуры,
	|		ТаблицаВидыЗапасов.Номенклатура   КАК Номенклатура,
	|		ТаблицаВидыЗапасов.Характеристика КАК Характеристика,
	|		ТаблицаВидыЗапасов.СтавкаНДС      КАК СтавкаНДС,
	|		-ТаблицаВидыЗапасов.Количество    КАК Количество,
	|		-ТаблицаВидыЗапасов.СуммаНДС      КАК СуммаНДС,
	|		-ТаблицаВидыЗапасов.СуммаСНДС     КАК СуммаСНДС,
	|		ТаблицаВидыЗапасов.НомерГТД       КАК НомерГТД
	|	ИЗ
	|		ТаблицаВидыЗапасов КАК ТаблицаВидыЗапасов
	|	) КАК ТаблицаТоваров
	|
	|СГРУППИРОВАТЬ ПО
	|	ТаблицаТоваров.АналитикаУчетаНоменклатуры,
	|	ТаблицаТоваров.Номенклатура,
	|	ТаблицаТоваров.Характеристика,
	|	ТаблицаТоваров.СтавкаНДС,
	|	ТаблицаТоваров.НомерГТД
	|
	|ИМЕЮЩИЕ
	|	СУММА(ТаблицаТоваров.Количество) <> 0
	|	ИЛИ СУММА(ТаблицаТоваров.СуммаНДС) <> 0
	|	ИЛИ СУММА(ТаблицаТоваров.СуммаСНДС) <> 0
	|;
	|////////////////////////////////////////////////////////////////////////////////
	|";
	
	Запрос.МенеджерВременныхТаблиц = МенеджерВременныхТаблиц;
	
	РезультатЗапрос = Запрос.Выполнить();
	
	Возврат Не РезультатЗапрос.Пустой();
	
КонецФункции

Процедура СинхронизироватьАналитикуУчетаНоменклатурыМеждуТабличнымиЧастями()
	
	СтруктураПоиска = Новый Структура("АналитикаУчетаНоменклатуры");
	
	Для Каждого СтрокаТоваров Из Товары Цикл
		
		КоличествоТоваров = СтрокаТоваров.Количество;
		
		СуммаНДС  = СтрокаТоваров.СуммаНДС;
		СуммаСНДС = СтрокаТоваров.СуммаСНДС;
		
		СтруктураПоиска.АналитикаУчетаНоменклатуры = СтрокаТоваров.АналитикаУчетаНоменклатурыТоварыУПартнеров;
		
		НайденныеСтроки = ВидыЗапасов.НайтиСтроки(СтруктураПоиска);
		
		Для Каждого СтрокаЗапасов Из НайденныеСтроки Цикл
			
			Если СтрокаЗапасов.Количество = 0 Тогда
				Продолжить;
			КонецЕсли;
			
			Количество = Мин(КоличествоТоваров, СтрокаЗапасов.Количество);
			
			ЗаполняемыеСвойства = "АналитикаУчетаНоменклатуры, АналитикаУчетаНоменклатурыТоварыУПартнеров";
			
			НоваяСтрока = ВидыЗапасов.Добавить();
			ЗаполнитьЗначенияСвойств(НоваяСтрока, СтрокаЗапасов);
			ЗаполнитьЗначенияСвойств(НоваяСтрока, СтрокаТоваров, ЗаполняемыеСвойства);
			
			НоваяСтрока.Количество = Количество;
			
			Если КоличествоТоваров <> 0 Тогда
				НоваяСтрока.СуммаНДС  = Количество * СуммаНДС / КоличествоТоваров;
				НоваяСтрока.СуммаСНДС = Количество * СуммаСНДС / КоличествоТоваров;
			КонецЕсли;
			
			СтрокаЗапасов.Количество = СтрокаЗапасов.Количество - НоваяСтрока.Количество;
			
			СтрокаЗапасов.СуммаНДС  = СтрокаЗапасов.СуммаНДС - НоваяСтрока.СуммаНДС;
			СтрокаЗапасов.СуммаСНДС = СтрокаЗапасов.СуммаСНДС - НоваяСтрока.СуммаСНДС;
			
			КоличествоТоваров = КоличествоТоваров - НоваяСтрока.Количество;
			
			СуммаНДС  = СуммаНДС - НоваяСтрока.СуммаНДС;
			СуммаСНДС = СуммаСНДС - НоваяСтрока.СуммаСНДС;
			
			Если КоличествоТоваров = 0 Тогда
				Прервать;
			КонецЕсли;
			
		КонецЦикла;
		
	КонецЦикла;
	
	ОтборПустыхСтрок = Новый Структура("Количество", 0);
	МассивУдаляемыхСтрок = ВидыЗапасов.НайтиСтроки(ОтборПустыхСтрок);
	
	Для Каждого СтрокаТаблицы Из МассивУдаляемыхСтрок Цикл
		ВидыЗапасов.Удалить(СтрокаТаблицы);
	КонецЦикла;
	
КонецПроцедуры

Функция ПараметрыЗаполненияВидовЗапасов()
	
	Перем ПараметрыЗаполнения;
	
	ПараметрыЗаполнения = ЗапасыСервер.ПараметрыЗаполненияВидовЗапасов();
	ПараметрыЗаполнения.ДокументДелаетИПриходИРасход = Истина;
	ПараметрыЗаполнения.ОтборыВидовЗапасов.Организация = Организация;
	ПараметрыЗаполнения.ОтборыВидовЗапасов.ТипЗапасов = Новый Массив;
	ПараметрыЗаполнения.ОтборыВидовЗапасов.ТипЗапасов.Добавить(Перечисления.ТипыЗапасов.Товар);
	ПараметрыЗаполнения.ОтборыВидовЗапасов.ТипЗапасов.Добавить(Перечисления.ТипыЗапасов.ТоварНаХраненииСПравомПродажи);
	ПараметрыЗаполнения.ПодбиратьВТЧТоварыПринятыеНаОтветственноеХранение = "Всегда";
	Возврат ПараметрыЗаполнения;

КонецФункции

#КонецОбласти

#Область Прочее

Процедура ЗаполнитьЦеныПоУсловиямПродаж()
	
	ПараметрыЗаполнения = Новый Структура();
	ПараметрыЗаполнения.Вставить("ПоляЗаполнения", "Цена");
	ПараметрыЗаполнения.Вставить("Дата",           Дата);
	ПараметрыЗаполнения.Вставить("Валюта",         Валюта);
	ПараметрыЗаполнения.Вставить("Соглашение",     Соглашение);
	
	СтруктураПересчетаСуммы = ОбработкаТабличнойЧастиКлиентСервер.ПараметрыПересчетаСуммыНДСВСтрокеТЧ(ЭтотОбъект);
	
	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить("ПересчитатьСумму",     "КоличествоУпаковок");
	СтруктураДействий.Вставить("ПересчитатьСуммуНДС",  СтруктураПересчетаСуммы);
	СтруктураДействий.Вставить("ПересчитатьСуммуСНДС", СтруктураПересчетаСуммы);
	
	ПродажиСервер.ЗаполнитьЦены(Товары, Неопределено, ПараметрыЗаполнения, СтруктураДействий);
	
КонецПроцедуры

Функция ПолучитьСуммуДокумента() Экспорт
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	Товары.Номенклатура КАК Номенклатура,
	|	Товары.СуммаСНДС    КАК СуммаСНДС
	|ПОМЕСТИТЬ Товары
	|ИЗ
	|	&Товары КАК Товары
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ЕСТЬNULL(СУММА(Товары.СуммаСНДС), 0) КАК СуммаСНДС
	|ИЗ
	|	Товары КАК Товары
	|ГДЕ
	|	Товары.Номенклатура.ТипНоменклатуры <> ЗНАЧЕНИЕ(Перечисление.ТипыНоменклатуры.МногооборотнаяТара)
	|	ИЛИ НЕ &ВернутьМногооборотнуюТару";
	
	ТоварыДокумента = Товары.Выгрузить(,"Номенклатура, СуммаСНДС");
	
	Запрос.УстановитьПараметр("Товары",                    ТоварыДокумента);
	Запрос.УстановитьПараметр("ВернутьМногооборотнуюТару", ВозвратПереданнойМногооборотнойТары);
	
	РезультатЗапроса = Запрос.Выполнить().Выгрузить();
	СуммаИтого       = РезультатЗапроса[0].СуммаСНДС;
	
	Возврат СуммаИтого;
	
КонецФункции

Процедура СформироватьСписокРегистровДляКонтроля()
	
	Массив = Новый Массив;
	
	Если Не ЗапасыСервер.ПроверитьНеобходимостьПерезаполненияВидовЗапасовДокумента(ЭтотОбъект) Тогда
		
		// Приходы в регистр (сторно расхода из регистра) контролируем при перепроведении и отмене проведения.
		Если Не ДополнительныеСвойства.ЭтоНовый Тогда
			Массив.Добавить(Движения.ТоварыОрганизаций);
		КонецЕсли;
		
		Если ДополнительныеСвойства.РежимЗаписи = РежимЗаписиДокумента.Проведение Тогда
			
			Массив.Добавить(Движения.СвободныеОстатки);
			Массив.Добавить(Движения.ОбеспечениеЗаказов);
			Массив.Добавить(Движения.ТоварыКПоступлению);
			
		КонецЕсли;
		
	КонецЕсли;
	
	Если Распоряжение = Документы.ЗаявкаНаВозвратТоваровОтКлиента.ПустаяСсылка() Тогда
		Движения.ТоварыОрганизаций.ДополнительныеСвойства.Вставить("ФормироватьСводнуюТаблицуТоварыОрганизаций");
	Иначе
		Движения.ЗаявкиНаВозвратТоваровОтКлиентов.ДополнительныеСвойства.Вставить("ФормироватьСводнуюТаблицуЗаявкиНаВозврат");
		Массив.Добавить(Движения.ЗаявкиНаВозвратТоваровОтКлиентов);
	КонецЕсли;
	
	ДополнительныеСвойства.ДляПроведения.Вставить("РегистрыДляКонтроля", Массив);
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти

#КонецЕсли
