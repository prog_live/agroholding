﻿
#Область ПрограммныйИнтерфейс

// Если в параметр Партия передано значение Неопределено, то будет создана новая паартия, иначе обновлена переданная
//
// Параметры:
//  Партия			 - 	 - 
//  ВидНоменклатуры	 - 	 - 
//  ВидПартии		 - 	 - 
//  ДатаПартии		 - 	 - 
// 
// Возвращаемое значение:
//  СправочникСсылка.актАналитикаПартий -
//
Функция СоздатьОбновитьПартиюПаллеты(Партия, ВидПартии, КодПартии, ДатаПартии = Неопределено, НомерДокумента) Экспорт
	
	Перем ПартияОбъект;
	
	Если Не ЗначениеЗаполнено(Партия) Тогда
		ПартияОбъект = Справочники.актАналитикаПартий.СоздатьЭлемент();
	Иначе
		ПартияОбъект = Партия.ПолучитьОбъект();
	КонецЕсли;
	
	ПартияОбъект.ДатаПартии = НачалоДня(ДатаПартии);
	ПартияОбъект.ВидПартии = ВидПартии;
	ПартияОбъект.Код = КодПартии;
	ПартияОбъект.ПорядковыйНомер = актАвтоматизацияОтгрузокВызовСервера.ПолучитьНовыйНомерПаллетыПоНомеруЗаказаКлиента(НомерДокумента, ДатаПартии);

	
	ПартияОбъект.Записать();
	
	Возврат ПартияОбъект.Ссылка;
	
КонецФункции

// Ищет по коду партию, если не находит, то создает новую
//
// Параметры:
//  КодПартии		 - 	 - 
//  ВидНоменклатуры	 - 	 - 
//  ВидПартии		 - 	 - 
//  ДатаПартии		 - 	 - 
// 
// Возвращаемое значение:
//  СправочникСсылка.актАналитикаПартий - 
//
Функция НайтиОбновитьПартиюПаллеты(КодПартии, ВидПартии, ДатаПартии = Неопределено, НомерДокумента) Экспорт
	
	Если ДатаПартии = Неопределено Тогда
		ДатаПартии = ТекущаяДата();
	КонецЕсли;
	
	НачалоГода = НачалоГода(ДатаПартии);
	
	Запрос = Новый Запрос(
	"ВЫБРАТЬ ПЕРВЫЕ 1
	|	актАналитикаПартий.Ссылка КАК Ссылка
	|ИЗ
	|	Справочник.актАналитикаПартий КАК актАналитикаПартий
	|ГДЕ
	|	актАналитикаПартий.Код = &Код
	|	И НАЧАЛОПЕРИОДА(актАналитикаПартий.ДатаПартии, ГОД) = &НачалоГода"	
	);
	
	Запрос.УстановитьПараметр("Код", КодПартии);
	Запрос.УстановитьПараметр("НачалоГода", НачалоГода);
	
	Выборка = Запрос.Выполнить().Выбрать();
	
	Если Выборка.Следующий() Тогда
		ПартияСсылка = Выборка.Ссылка;
	Иначе
		ПартияСсылка = Справочники.актАналитикаПартий.ПустаяСсылка();
	КонецЕсли;
	
	Возврат СоздатьОбновитьПартиюПаллеты(ПартияСсылка, ВидПартии, КодПартии, ДатаПартии, НомерДокумента);
	
КонецФункции

Функция СегенерироватьКодПаллеты(НомерДокумента, ДатаПартии = Неопределено) Экспорт
	
	НомерПаллеты = актАвтоматизацияОтгрузокВызовСервера.ПолучитьНовыйНомерПаллетыПоНомеруЗаказаКлиента(НомерДокумента, ДатаПартии);
	
	Возврат СтрШаблон("%1-%2", НомерДокумента, НомерПаллеты);
	
КонецФункции 

#КонецОбласти

#Область Отчеты

// Определяет список команд отчетов.
//
// Параметры:
//   КомандыОтчетов - ТаблицаЗначений - Таблица с командами отчетов. Для изменения.
//       См. описание 1 параметра процедуры ВариантыОтчетовПереопределяемый.ПередДобавлениемКомандОтчетов().
//   Параметры - Структура - Вспомогательные параметры. Для чтения.
//       См. описание 2 параметра процедуры ВариантыОтчетовПереопределяемый.ПередДобавлениемКомандОтчетов().
//
Процедура ДобавитьКомандыОтчетов(КомандыОтчетов, Параметры) Экспорт
	
	ВариантыОтчетовУТПереопределяемый.ДобавитьКомандуСтруктураПодчиненности(КомандыОтчетов);

КонецПроцедуры

Процедура Печать(ТабДок, Ссылка) Экспорт
	//{{_КОНСТРУКТОР_ПЕЧАТИ(Печать)
	Запрос = Новый Запрос;
	Запрос.Текст =
	"ВЫБРАТЬ
	|	актАналитикаПартий.ДатаПартии КАК Период,
	|	актАналитикаПартий.Ссылка КАК Паллета,
	|	актАналитикаПартий.СостояниеПаллеты КАК Состояние,
	|	актАналитикаПартий.Номенклатура КАК Номенклатура,
	|	актАналитикаПартий.Характеристика КАК Характеристика,
	|	актАналитикаПартий.УпаковкаПаллета КАК УпаковкаПаллета,
	|	актАналитикаПартий.Упаковка КАК Упаковка,
	|	актАналитикаПартий.КоличествоКоробок КАК КоличествоКоробок,
	|	актАналитикаПартий.КоличествоУпаковок КАК КоличествоУпаковок,
	|	актАналитикаПартий.ДатаСбора КАК ДатаСбора,
	|	актАналитикаПартий.ДатаСортировки КАК ДатаСортировки,
	|	актАналитикаПартий.ШтрихкодПоддона КАК ШтрихкодПоддона,
	|	актАналитикаПартий.ЗаказКлиента.Контрагент КАК Контрагент,
	|	актАналитикаПартий.ЗаказКлиента.Договор КАК Договор,
	|	актАналитикаПартий.ЗаказКлиента КАК ЗаказКлиента,
	|	актАналитикаПартий.Вес КАК ВесБрутто,
	|	актАналитикаПартий.ДатаСбораДляЭтикетки КАК ДатаСбораДляЭтикетки,
	|	актАналитикаПартий.ДатаСортировкиДляЭтикетки КАК ДатаСортировкиДляЭтикетки,
	|	актАналитикаПартий.ЗаказКлиента.Склад КАК Склад
	|ИЗ
	|	Справочник.актАналитикаПартий КАК актАналитикаПартий
	|ГДЕ
	|	актАналитикаПартий.Ссылка В(&Ссылка)";
	Запрос.Параметры.Вставить("Ссылка", Ссылка);
	Выборка = Запрос.Выполнить().Выбрать();
	
	Макет = ПолучитьМакет("МакетВнутреннегоПаспортаПаллеты");
	ОбластьМакета = Макет.ПолучитьОбласть("Шапка");

	ТабДок.Очистить();

	Пока Выборка.Следующий() Цикл
		Паллета = Выборка.Паллета; 
		ХарактеристикиПаллеты = агАвтоматизацияОтгрузокВызовСервера.ПолучитьИнформациюПоПаллете(Паллета, ТекущаяДата());
		ЗаполнитьЗначенияСвойств(ОбластьМакета.Параметры,Выборка);
		
		Для Каждого Рисунок ИЗ ОбластьМакета.Рисунки Цикл
			
			ТипКода = Неопределено;
			
			Индекс = ОбластьМакета.Рисунки.Индекс(Рисунок);
			
			ТипКода = 16;
			
			Если ТипКода = 16 Тогда
				агАвтоматизацияОтгрузокВызовСервера.УстановитьКартинкуQRКода(ОбластьМакета.Рисунки[Индекс],ТипКода, ХарактеристикиПаллеты, Строка(Паллета.УникальныйИдентификатор()));
			КонецЕсли;
			
		КонецЦикла;

		ТабДок.Вывести(областьМакета);
	КонецЦикла;
	
	ТабДок.ПолеСверху = 0;
	ТабДок.ПолеСнизу = 0;
	ТабДок.ПолеСлева = 0;
	ТабДок.ПолеСправа = 0;
	ТабДок.ОриентацияСтраницы = ОриентацияСтраницы.Портрет;
	//}}
КонецПроцедуры

#КонецОбласти
