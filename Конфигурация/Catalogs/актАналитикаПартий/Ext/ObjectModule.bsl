﻿
Процедура ПриЗаписи(Отказ)	
	
КонецПроцедуры

Процедура ПередЗаписью(Отказ)
	
	//==Акита Морозов ??-??? начало 2020.06.22
	Если ЗначениеЗаполнено(Распоряжение) 
		И ЗначениеЗаполнено(Ссылка.Распоряжение)
		И ТипЗнч(Распоряжение) = Тип("ДокументСсылка.ЗаказКлиента") 
		И ТипЗнч(Ссылка.Распоряжение) = Тип("ДокументСсылка.ЗаказКлиента") 
		И Распоряжение <> Ссылка.Распоряжение Тогда
		
		// Подмняем в распоряжении заказ на новый
		Запрос = Новый Запрос();
		Запрос.Текст = 
		"ВЫБРАТЬ
		|	актРегистрацияЭтаповПроизводстваПаллеты.Ссылка КАК Ссылка,
		|	актРегистрацияЭтаповПроизводстваПаллеты.Распоряжение КАК Распоряжение
		|ИЗ
		|	Документ.актРегистрацияЭтаповПроизводстваПаллеты КАК актРегистрацияЭтаповПроизводстваПаллеты
		|ГДЕ
		|	актРегистрацияЭтаповПроизводстваПаллеты.Паллета = &Паллета";
		Запрос.УстановитьПараметр("Паллета", Ссылка);
		Результат = Запрос.Выполнить().Выбрать();
		Пока Результат.Следующий() Цикл
			Если Результат.Распоряжение = Ссылка.Распоряжение Тогда  
				О = Результат.Ссылка.ПолучитьОбъект();
				О.Распоряжение = Распоряжение;
				
				НайденныеСтроки = О.ЭтапыПроизводства.НайтиСтроки(Новый Структура("ДокументАналитики", Ссылка.Распоряжение));  
				Для Каждого НайденнаяСтрока из НайденныеСтроки Цикл
					НайденнаяСтрока.ДокументАналитики = Распоряжение; 	
				КонецЦикла;
				
				О.Записать();
			КонецЕсли; 
		КонецЦикла;
		
	КонецЕсли;	
	//==Акита Морозов конец
	
	// Обработать пометку удавления
	Если ПометкаУдаления И НЕ Ссылка.ПометкаУдаления Тогда
		// Ищем ссылки в заказах и вычищаем оттуда
		
		Запрос = Новый Запрос();
		Запрос.Текст = 
		"ВЫБРАТЬ РАЗЛИЧНЫЕ
		|	актЗарегистрированныеПаллеты.Регистратор КАК Регистратор
		|ИЗ
		|	РегистрСведений.актЗарегистрированныеПаллеты КАК актЗарегистрированныеПаллеты
		|ГДЕ
		|	актЗарегистрированныеПаллеты.Партия = &Паллета";
		Запрос.УстановитьПараметр("Паллета", Ссылка);
		Результат = Запрос.Выполнить().Выбрать();
		Пока Результат.Следующий() Цикл
			О = Результат.Регистратор.ПолучитьОбъект();
			О.Движения.актЗарегистрированныеПаллеты.Прочитать();
			Для Каждого Движение ИЗ О.Движения.актЗарегистрированныеПаллеты Цикл
				Если Движение.Партия = Ссылка Тогда
					О.Движения.актЗарегистрированныеПаллеты.Удалить(Движение);	
				КонецЕсли;			
			КонецЦикла;
			О.Движения.актЗарегистрированныеПаллеты.Записать();	
		КонецЦикла;
	КонецЕсли;
	
КонецПроцедуры