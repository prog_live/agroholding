﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Свойство("АвтоТест") Тогда // Возврат при получении формы для анализа.
		Возврат;
	КонецЕсли;

	ОбновлениеИнформационнойБазы.ПроверитьОбъектОбработан(Объект, ЭтотОбъект);
	
	СобытияФорм.ПриСозданииНаСервере(ЭтаФорма, Отказ, СтандартнаяОбработка);
	
	// СтандартныеПодсистемы.Свойства
	ДополнительныеПараметры = Новый Структура;
	ДополнительныеПараметры.Вставить("Объект", Объект);
	ДополнительныеПараметры.Вставить("ИмяЭлементаДляРазмещения", "ГруппаДополнительныеРеквизиты");
	УправлениеСвойствами.ПриСозданииНаСервере(ЭтотОбъект, ДополнительныеПараметры);
	// Конец СтандартныеПодсистемы.Свойства
	
	Если Не ПравоДоступа("Просмотр", Метаданные.РегистрыСведений.ПорядокОтраженияРезервов) Тогда
		Элементы.НастроитьСчетаРеглУчетаПоОрганизациям.Видимость = Ложь;
	КонецЕсли;
	
	Если Не ЗначениеЗаполнено(Объект.Ссылка) Тогда
		ПриСозданииЧтенииНаСервере();
	КонецЕсли; 
	
КонецПроцедуры

&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)
	
	ПриСозданииЧтенииНаСервере();
	
	// СтандартныеПодсистемы.Свойства
	УправлениеСвойствами.ПриЧтенииНаСервере(ЭтотОбъект, ТекущийОбъект);
	// Конец СтандартныеПодсистемы.Свойства
	
КонецПроцедуры

&НаКлиенте
Процедура ПослеЗаписи(ПараметрыЗаписи)
	
	Оповестить("Запись_Резервы", ПараметрыЗаписи, Объект.Ссылка);
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	// СтандартныеПодсистемы.Свойства
	УправлениеСвойствамиКлиент.ПослеЗагрузкиДополнительныхРеквизитов(ЭтотОбъект);
	// Конец СтандартныеПодсистемы.Свойства
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	
	// СтандартныеПодсистемы.Свойства 
	Если УправлениеСвойствамиКлиент.ОбрабатыватьОповещения(ЭтотОбъект, ИмяСобытия, Параметр) Тогда
		ОбновитьЭлементыДополнительныхРеквизитов();
		УправлениеСвойствамиКлиент.ПослеЗагрузкиДополнительныхРеквизитов(ЭтотОбъект);
	КонецЕсли;
	// Конец СтандартныеПодсистемы.Свойства

КонецПроцедуры

&НаСервере
Процедура ОбработкаПроверкиЗаполненияНаСервере(Отказ, ПроверяемыеРеквизиты)
	
	// СтандартныеПодсистемы.Свойства
	УправлениеСвойствами.ОбработкаПроверкиЗаполнения(ЭтотОбъект, Отказ, ПроверяемыеРеквизиты);
	// Конец СтандартныеПодсистемы.Свойства

КонецПроцедуры

&НаСервере
Процедура ПередЗаписьюНаСервере(Отказ, ТекущийОбъект, ПараметрыЗаписи)
	
	// СтандартныеПодсистемы.Свойства
	УправлениеСвойствами.ПередЗаписьюНаСервере(ЭтотОбъект, ТекущийОбъект);
	// Конец СтандартныеПодсистемы.Свойства
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийФормы

&НаКлиенте
Процедура НастроитьСчетаРеглУчетаПоОрганизациям(Команда)
	
	Если Не ЗначениеЗаполнено(Объект.Ссылка) Тогда
		ОписаниеОповещения = Новый ОписаниеОповещения("ОбработкаВопросЗаписиОбъекта", ЭтотОбъект);
		ТекстВопроса = НСтр("ru = 'Для продолжения необходимо записать объект. Записать?';
							|en = 'To continue, save the object. Do you want to save?'");
		Кнопки = Новый СписокЗначений;
		Кнопки.Добавить(КодВозвратаДиалога.Да, НСтр("ru = 'Записать';
													|en = 'Write'"));
		Кнопки.Добавить(КодВозвратаДиалога.Отмена);
		ПоказатьВопрос(ОписаниеОповещения, ТекстВопроса, Кнопки);
		Возврат;
	КонецЕсли;
	ОткрытьФормуНастройкиСчетовРеглУчетаПоОрганизациям();
	
	Возврат; // Чтобы в УТ был не пустой обработчик
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

#Область СтандартныеПодсистемы

// СтандартныеПодсистемы.Свойства

&НаКлиенте
Процедура Подключаемый_СвойстваВыполнитьКоманду(ЭлементИлиКоманда, НавигационнаяСсылка = Неопределено, СтандартнаяОбработка = Неопределено)
	
	УправлениеСвойствамиКлиент.ВыполнитьКоманду(ЭтотОбъект, ЭлементИлиКоманда, СтандартнаяОбработка);
	
КонецПроцедуры

// Конец СтандартныеПодсистемы.Свойства

#КонецОбласти

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура ПриСозданииЧтенииНаСервере()
	
	Если Объект.Ссылка = Справочники.Резервы.РезервЕжегодныхОтпусков ИЛИ Не ПолучитьФункциональнуюОпцию("ФормироватьРезервыПредстоящихРасходов") Тогда
		Элементы.ГруппаНастроекСчетовУчета.Видимость = Ложь;
	Иначе
		Элементы.ГруппаНастроекСчетовУчета.Видимость = Истина;
		ПолучитьСостояниеНастройкиСчетовРеглУчетаПоОрганизациям();
		Если ПравоДоступа("Просмотр", Метаданные.ПланыСчетов.Хозрасчетный) Тогда
			СчетаУчета = Обработки.НастройкаОтраженияДокументовВРеглУчете.ДоступныеСчетаУчетаРезервов();		
			ПараметрыВыбораСчета = Новый ПараметрВыбора("Отбор.Ссылка", Новый ФиксированныйМассив(СчетаУчета));
			Элементы.СчетУчета.ПараметрыВыбора = Новый ФиксированныйМассив(ОбщегоНазначенияКлиентСервер.ЗначениеВМассиве(ПараметрыВыбораСчета));
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаВопросЗаписиОбъекта(Результат, ДополнительныеПараметры) Экспорт
	
	Если Результат = КодВозвратаДиалога.Да Тогда
		Если Не Записать() Тогда
			Возврат;
		КонецЕсли;
		ОткрытьФормуНастройкиСчетовРеглУчетаПоОрганизациям();
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ОткрытьФормуНастройкиСчетовРеглУчетаПоОрганизациям()
	
	ПараметрыФормы = Новый Структура;
	ПараметрыФормы.Вставить("ВидРезервов", Объект.Ссылка);
	
	ОписаниеОповещения = Новый ОписаниеОповещения("ПослеНастройкиСчетовРеглУчетаПоОрганизациям", ЭтотОбъект);
	
	ОткрытьФорму("РегистрСведений.ПорядокОтраженияРезервов.ФормаСписка", 
		ПараметрыФормы, ЭтаФорма, , , , ОписаниеОповещения);
	
КонецПроцедуры

&НаКлиенте
Процедура ПослеНастройкиСчетовРеглУчетаПоОрганизациям(Результат, ДополнительныеПараметры) Экспорт
	
	ПолучитьСостояниеНастройкиСчетовРеглУчетаПоОрганизациям();
	
КонецПроцедуры

&НаСервере
Процедура ПолучитьСостояниеНастройкиСчетовРеглУчетаПоОрганизациям()
	
	ЗаголовокКоманды = НСтр("ru = 'Посмотреть настройки счетов учета по организациям';
							|en = 'View GL account setting by companies'");
	
	Если ПравоДоступа("Редактирование", Метаданные.РегистрыСведений.ПорядокОтраженияРезервов) Тогда
		
		Запрос = Новый Запрос;
		Запрос.Текст =
		"ВЫБРАТЬ ПЕРВЫЕ 1
		|	1 КАК Количество
		|ИЗ
		|	РегистрСведений.ПорядокОтраженияРезервов КАК ПорядокОтражения
		|ГДЕ
		|	ПорядокОтражения.ВидРезервов = &ВидРезервов";
		Запрос.УстановитьПараметр("ВидРезервов", Объект.Ссылка);
		
		РезультатЗапроса = Запрос.Выполнить();
		Если РезультатЗапроса.Пустой() Тогда
			ЗаголовокКоманды = НСтр("ru = 'Настроить счета учета по организациям';
									|en = 'Customize GL accounts by companies'");
		Иначе
			ЗаголовокКоманды  = НСтр("ru = 'Изменить настройки счетов учета по организациям';
									|en = 'Change GL account settings by companies'");
		КонецЕсли;
		
	КонецЕсли;
	
	Элементы.НастроитьСчетаРеглУчетаПоОрганизациям.Заголовок = ЗаголовокКоманды; 
	
КонецПроцедуры

#Область СтандартныеПодсистемыСвойства

// СтандартныеПодсистемы.Свойства 

&НаСервере
Процедура ОбновитьЭлементыДополнительныхРеквизитов()

	УправлениеСвойствами.ОбновитьЭлементыДополнительныхРеквизитов(ЭтотОбъект);

КонецПроцедуры

&НаКлиенте
Процедура ОбновитьЗависимостиДополнительныхРеквизитов()
	УправлениеСвойствамиКлиент.ОбновитьЗависимостиДополнительныхРеквизитов(ЭтотОбъект);
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ПриИзмененииДополнительногоРеквизита(Элемент)
	УправлениеСвойствамиКлиент.ОбновитьЗависимостиДополнительныхРеквизитов(ЭтотОбъект);
КонецПроцедуры

// Конец СтандартныеПодсистемы.Свойства

#КонецОбласти

#КонецОбласти
