﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Свойство("АвтоТест") Тогда
		Возврат;
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура РазрешитьРедактирование(Команда)
	
	Результат = Новый Массив;
	
	Если РазрешитьРедактированиеДанныхРегламентированногоУчета Тогда
		Результат.Добавить("КодПоОКОФ");
		Результат.Добавить("ШифрПоЕНАОФ");
		Результат.Добавить("ГруппаОС");
		Результат.Добавить("АмортизационнаяГруппа");
		Результат.Добавить("НедвижимоеИмущество");
	КонецЕсли;
	
	Если РазрешитьРедактированиеДанныхМеждународногоУчета Тогда
		Результат.Добавить("ГруппаОСМеждународныйУчет");
	КонецЕсли;
	
	Если РазрешитьРедактированиеНаправленияДеятельности Тогда
		Результат.Добавить("НаправлениеДеятельности");
	КонецЕсли;
	
	ОповеститьОВыборе(Результат);
	
КонецПроцедуры

#КонецОбласти