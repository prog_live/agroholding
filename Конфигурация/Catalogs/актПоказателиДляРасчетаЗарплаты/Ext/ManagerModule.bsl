﻿
#Область ПрограммныйИнтерфейс
// Возвращает значения показателя в раздличных разрезах за указанный периода
//
// Параметры:
//  Показатель - СправочникСсылка.актКлючевыеПоказателиДляРасчетаЗарплаты - показатель, значения которого будут расчитаны.
//  НачалоПериода - Дата и время - начало периода, начало месяца.
//
Функция ПолучитьЗначенияПоказателяЗаМесяц(Показатель, НачалоПериода, КонецПериода) Экспорт
	
	Если Показатель = Справочники.актПоказателиДляРасчетаЗарплаты.АИВыручка Тогда
		
		Возврат ПолучитьЗначенияПоказателяЗаПериодАИВыручка(НачалоПериода);
		
	КонецЕсли;
	
КонецФункции

#КонецОбласти

#Область РасчетПоказателей

Функция ПолучитьЗначенияПоказателяЗаПериодАИВыручка(НачалоПериода) 
	
	Запрос = Новый Запрос("ВЫБРАТЬ
	|	СУММА(ВыручкаИСебестоимостьПродажОбороты.КоличествоОборот * ВыручкаИСебестоимостьПродажОбороты.АналитикаУчетаНоменклатуры.Номенклатура.ВесЧислитель) КАК Вес
	|ПОМЕСТИТЬ втОбщийВесРеализованнойПродукции
	|ИЗ
	|	РегистрНакопления.ВыручкаИСебестоимостьПродаж.Обороты(&НачалоПериода, КОНЕЦПЕРИОДА(&НачалоПериода, МЕСЯЦ), , ) КАК ВыручкаИСебестоимостьПродажОбороты
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	СУММА(ТоварыНаСкладахОстатки.ВНаличииОстаток * ТоварыНаСкладахОстатки.Номенклатура.ВесЧислитель) КАК Остаток
	|ПОМЕСТИТЬ втОстатки
	|ИЗ
	|	РегистрНакопления.ТоварыНаСкладах.Остатки(ДОБАВИТЬКДАТЕ(&НачалоПериода, ЧАС, 8), Номенклатура В ИЕРАРХИИ (&ГотоваяПродукция)) КАК ТоварыНаСкладахОстатки
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	СУММА(ВыпускПродукцииОбороты.КоличествоОборот * ВыпускПродукцииОбороты.АналитикаУчетаНоменклатуры.Номенклатура.ВесЧислитель) КАК ОбъемПроизводства
	|ПОМЕСТИТЬ втОбъемПроизводства
	|ИЗ
	|	РегистрНакопления.ВыпускПродукции.Обороты(ДОБАВИТЬКДАТЕ(&НачалоПериода, ЧАС, 8), ДОБАВИТЬКДАТЕ(КОНЕЦПЕРИОДА(&НачалоПериода, МЕСЯЦ), ЧАС, -1 * &ВычетПроизводства), , ) КАК ВыпускПродукцииОбороты
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ЕСТЬNULL(втОстатки.Остаток, 0) КАК РасчетныйОбъемПроизводства
	|ПОМЕСТИТЬ втРасчетныйОбъемПроизводстваДетализация
	|ИЗ
	|	втОстатки КАК втОстатки
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	ЕСТЬNULL(втОбъемПроизводства.ОбъемПроизводства, 0)
	|ИЗ
	|	втОбъемПроизводства КАК втОбъемПроизводства
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	СУММА(втРасчетныйОбъемПроизводстваДетализация.РасчетныйОбъемПроизводства) КАК РасчетныйОбъемПроизводства
	|ПОМЕСТИТЬ втРасчетныйОбъемПроизводства
	|ИЗ
	|	втРасчетныйОбъемПроизводстваДетализация КАК втРасчетныйОбъемПроизводстваДетализация
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ВЫБОР
	|		КОГДА ЕСТЬNULL(втРасчетныйОбъемПроизводства.РасчетныйОбъемПроизводства, 0) = 0
	|			ТОГДА 0
	|		ИНАЧЕ ЕСТЬNULL(втОбщийВесРеализованнойПродукции.Вес, 0) / ЕСТЬNULL(втРасчетныйОбъемПроизводства.РасчетныйОбъемПроизводства, 0)
	|	КОНЕЦ КАК КоэффициентПроизводства
	|ПОМЕСТИТЬ втКоэффициентПроизводства
	|ИЗ
	|	втОбщийВесРеализованнойПродукции КАК втОбщийВесРеализованнойПродукции
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ втРасчетныйОбъемПроизводства КАК втРасчетныйОбъемПроизводства
	|		ПО (ИСТИНА)
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ВыручкаИСебестоимостьПродажОбороты.АналитикаУчетаПоПартнерам.Партнер.ОсновнойМенеджер КАК Менеджер,
	|	СУММА(ВыручкаИСебестоимостьПродажОбороты.СуммаВыручкиОборот) КАК ФактическаяВыручка
	|ПОМЕСТИТЬ втФактическаяВыручка
	|ИЗ
	|	РегистрНакопления.ВыручкаИСебестоимостьПродаж.Обороты(&НачалоПериода, КОНЕЦПЕРИОДА(&НачалоПериода, МЕСЯЦ), , ) КАК ВыручкаИСебестоимостьПродажОбороты
	|
	|СГРУППИРОВАТЬ ПО
	|	ВыручкаИСебестоимостьПродажОбороты.АналитикаУчетаПоПартнерам.Партнер.ОсновнойМенеджер
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ВыручкаИСебестоимостьПродажОбороты.АналитикаУчетаПоПартнерам.Партнер.ОсновнойМенеджер КАК Менеджер,
	|	ВыручкаИСебестоимостьПродажОбороты.АналитикаУчетаНоменклатуры.Номенклатура КАК Номенклатура,
	|	СУММА(ВыручкаИСебестоимостьПродажОбороты.КоличествоОборот * ВыручкаИСебестоимостьПродажОбороты.АналитикаУчетаНоменклатуры.Номенклатура.ВесЧислитель) КАК КоличествоКГ,
	|	СУММА(ВыручкаИСебестоимостьПродажОбороты.КоличествоОборот) КАК Количество
	|ПОМЕСТИТЬ втОбъемРеализацииНоменклатуры
	|ИЗ
	|	РегистрНакопления.ВыручкаИСебестоимостьПродаж.Обороты(&НачалоПериода, КОНЕЦПЕРИОДА(&НачалоПериода, МЕСЯЦ), , ) КАК ВыручкаИСебестоимостьПродажОбороты
	|
	|СГРУППИРОВАТЬ ПО
	|	ВыручкаИСебестоимостьПродажОбороты.АналитикаУчетаПоПартнерам.Партнер.ОсновнойМенеджер,
	|	ВыручкаИСебестоимостьПродажОбороты.АналитикаУчетаНоменклатуры.Номенклатура
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	актЗначенияБазовыхПоказателейДляРасчетаЗарплатыСрезПоследних.Аналитика КАК Номенклатура,
	|	актЗначенияБазовыхПоказателейДляРасчетаЗарплатыСрезПоследних.Значение КАК ПлановаяЦена
	|ПОМЕСТИТЬ втПлановыеЦеныНоменклатуры
	|ИЗ
	|	РегистрСведений.актЗначенияБазовыхПоказателейДляРасчетаЗарплаты.СрезПоследних(КОНЕЦПЕРИОДА(&НачалоПериода, МЕСЯЦ), Аналитика ССЫЛКА Справочник.Номенклатура) КАК актЗначенияБазовыхПоказателейДляРасчетаЗарплатыСрезПоследних
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	втОбъемРеализацииНоменклатуры.Менеджер КАК Менеджер,
	|	СУММА(втОбъемРеализацииНоменклатуры.Количество * ЕСТЬNULL(втПлановыеЦеныНоменклатуры.ПлановаяЦена, 0)) КАК ПлановаяВыручка
	|ПОМЕСТИТЬ втПлановаяВыручка
	|ИЗ
	|	втОбъемРеализацииНоменклатуры КАК втОбъемРеализацииНоменклатуры
	|		ЛЕВОЕ СОЕДИНЕНИЕ втПлановыеЦеныНоменклатуры КАК втПлановыеЦеныНоменклатуры
	|		ПО втОбъемРеализацииНоменклатуры.Номенклатура = втПлановыеЦеныНоменклатуры.Номенклатура
	|
	|СГРУППИРОВАТЬ ПО
	|	втОбъемРеализацииНоменклатуры.Менеджер
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	втПлановаяВыручка.Менеджер КАК Менеджер,
	|	втПлановаяВыручка.ПлановаяВыручка * втКоэффициентПроизводства.КоэффициентПроизводства КАК РасчетнаяВыручка
	|ПОМЕСТИТЬ втРасчетнаяВыручка
	|ИЗ
	|	втПлановаяВыручка КАК втПлановаяВыручка
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ втКоэффициентПроизводства КАК втКоэффициентПроизводства
	|		ПО (ИСТИНА)
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	втФактическаяВыручка.Менеджер КАК Менеджер,
	|	втФактическаяВыручка.ФактическаяВыручка КАК ФактическаяВыручка,
	|	втРасчетнаяВыручка.РасчетнаяВыручка КАК РасчетнаяВыручка,
	|	(ВЫБОР
	|		КОГДА втРасчетнаяВыручка.РасчетнаяВыручка = 0
	|			ТОГДА 0
	|		ИНАЧЕ втФактическаяВыручка.ФактическаяВыручка / втРасчетнаяВыручка.РасчетнаяВыручка
	|	КОНЕЦ * 100 - 70) * 100 * &МножительВыручки + &БазовыйБонус КАК ЗначениеПоказателя
	|ИЗ
	|	втФактическаяВыручка КАК втФактическаяВыручка
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ втРасчетнаяВыручка КАК втРасчетнаяВыручка
	|		ПО втФактическаяВыручка.Менеджер = втРасчетнаяВыручка.Менеджер");
	
	//КонецПериода = КонецМесяца(НачалоПериода);
	//
	//// Фактическая выручка
	//
	//Запрос = Новый Запрос("ВЫБРАТЬ
	//|	ВыручкаИСебестоимостьПродажОбороты.АналитикаУчетаПоПартнерам.Партнер.ОсновнойМенеджер КАК Менеджер,
	//|	ВыручкаИСебестоимостьПродажОбороты.АналитикаУчетаНоменклатуры.Номенклатура КАК Номенклатура,
	//|	ВыручкаИСебестоимостьПродажОбороты.СуммаВыручкиОборот КАК Сумма
	//|ПОМЕСТИТЬ втФактическаяВыручка
	//|ИЗ
	//|	РегистрНакопления.ВыручкаИСебестоимостьПродаж.Обороты(&НачалоПериода, &КонецПериода, , ) КАК ВыручкаИСебестоимостьПродажОбороты");	
	//
	//Запрос.УстановитьПараметр("НачалоПериода", НачалоПериода);
	//Запрос.УстановитьПараметр("КонецПериода", КонецПериода);
	//
	//// Пановые цены
	//
	//"ВЫБРАТЬ
	//|	актЗначенияБазовыхПоказателейДляРасчетаЗарплатыСрезПоследних.Аналитика КАК Номенклатура,
	//|	актЗначенияБазовыхПоказателейДляРасчетаЗарплатыСрезПоследних.Значение КАК ПлановаяЦена
	//|ПОМЕСТИТЬ втПлановыеЦеныНоменклатуры
	//|ИЗ
	//|	РегистрСведений.актЗначенияБазовыхПоказателейДляРасчетаЗарплаты.СрезПоследних(&КонецПериода, Показатель = ЗНАЧЕНИЕ(Справочник.актБазовыеПоказателиДляРасчетаЗарплаты.ПлановаяЦенаАИ)) КАК актЗначенияБазовыхПоказателейДляРасчетаЗарплатыСрезПоследних"
	//
	//// Объем реализованной продукции
	//
	//"ВЫБРАТЬ
	//|	ВыручкаИСебестоимостьПродажОбороты.АналитикаУчетаПоПартнерам.Партнер.ОсновнойМенеджер КАК Менеджер,
	//|	ВыручкаИСебестоимостьПродажОбороты.АналитикаУчетаНоменклатуры.Номенклатура КАК Номенклатура,
	//|	ВыручкаИСебестоимостьПродажОбороты.КоличествоОборот * ВыручкаИСебестоимостьПродажОбороты.АналитикаУчетаНоменклатуры.Номенклатура.ВесЧислитель КАК Объем
	//|ПОМЕСТИТЬ втОбъемРеализованнойПродукции
	//|ИЗ
	//|	РегистрНакопления.ВыручкаИСебестоимостьПродаж.Обороты(&НачалоПериода, &КонецПериода, , ) КАК ВыручкаИСебестоимостьПродажОбороты"
	//
	//// Расчетный объем продизводства
	//
	//""
	
КонецФункции

#КонецОбласти