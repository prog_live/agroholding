﻿
#Область ОбработчикиРеквизитовФормы

&НаКлиенте
Процедура ПартнерРЦПриИзменении(Элемент)
	
	Если ПустаяСтрока(Объект.Наименование) Тогда
		Объект.Наименование = Строка(Объект.Партнер);
	КонецЕсли;
	
	ЗаполнитьКонтрагентаПоПартнеруНаСервере();
	
КонецПроцедуры

#КонецОбласти

#Область ВспомогательныеПроцедурыИФункции

&НаСервере
Процедура ЗаполнитьКонтрагентаПоПартнеруНаСервере()
	
	Если Объект.Партнер.Пустая() Тогда
		
		Объект.Контрагент = Справочники.Контрагенты.ПустаяСсылка();
		
	Иначе
		
		Запрос = Новый Запрос("ВЫБРАТЬ
		|	Контрагенты.Ссылка КАК Контрагент
		|ИЗ
		|	Справочник.Контрагенты КАК Контрагенты
		|ГДЕ
		|	Контрагенты.Партнер = &Партнер");
		Запрос.УстановитьПараметр("Партнер", Объект.Партнер);
		
		Выборка = Запрос.Выполнить().Выбрать();
		
		Если Выборка.Следующий() Тогда
			Объект.Контрагент = Выборка.Контрагент;
		КонецЕсли;
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура АдресРЦПриИзменении(Элемент)
	ИмяРеквизитаАдресаДоставки = "АдресРЦ";
	ТекущиеДанные = Элементы.Маршрут.ТекущиеДанные;
	
	ДоставкаТоваровКлиент.ПриИзмененииПредставленияАдреса(
	    Элемент,
		объект.АдресРЦ,
		Объект.АдресРЦ);
КонецПроцедуры

&НаКлиенте
Процедура АдресРЦНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	ИмяРеквизитаАдресаДоставки = "АдресРЦ";
	ТекущиеДанные = Объект;
	
	ДоставкаТоваровКлиент.ОткрытьФормуВыбораАдресаИОбработатьРезультат(
	    Элемент,
		ТекущиеДанные,
		ИмяРеквизитаАдресаДоставки,
		СтандартнаяОбработка);
КонецПроцедуры

&НаКлиенте
Процедура АдресРЦОткрытие(Элемент, СтандартнаяОбработка)
	АдресРЦПриИзменении(Элемент);
КонецПроцедуры

&НаКлиенте
Процедура ВременноеОкноНачалоПриИзменении(Элемент)
	Объект.ВОПодачиДокументов = Объект.ВременноеОкноНачало - 30*60;
КонецПроцедуры

#КонецОбласти
