﻿#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Свойство("АвтоТест") Тогда // Возврат при получении формы для анализа.
		Возврат;
	КонецЕсли;
	
	ОбновлениеИнформационнойБазы.ПроверитьОбъектОбработан(Объект, ЭтотОбъект);
	
	// Обработчик подсистемы "Свойства"
	ДополнительныеПараметры = Новый Структура;
	ДополнительныеПараметры.Вставить("Объект", Объект);
	ДополнительныеПараметры.Вставить("ИмяЭлементаДляРазмещения", "ГруппаДополнительныеРеквизиты");
	УправлениеСвойствами.ПриСозданииНаСервере(ЭтаФорма, ДополнительныеПараметры);
	
	// Обработчик подсистемы "ВерсионированиеОбъектов"
	ВерсионированиеОбъектов.ПриСозданииНаСервере(ЭтаФорма);
	
	СобытияФорм.ПриСозданииНаСервере(ЭтаФорма, Отказ, СтандартнаяОбработка);
	
	СостояниеРядов.Параметры.УстановитьЗначениеПараметра("ТеплицаСсылка", Объект.Ссылка);
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
		
		// СтандартныеПодсистемы.Свойства
		УправлениеСвойствамиКлиент.ПослеЗагрузкиДополнительныхРеквизитов(ЭтотОбъект);
		// Конец СтандартныеПодсистемы.Свойства
		
	
КонецПроцедуры

&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)
	
	УправлениеСвойствами.ПриЧтенииНаСервере(ЭтаФорма, ТекущийОбъект);
	
	МодификацияКонфигурацииПереопределяемый.ПриЧтенииНаСервере(ЭтаФорма, ТекущийОбъект);
	
КонецПроцедуры

&НаСервере
Процедура ПередЗаписьюНаСервере(Отказ, ТекущийОбъект, ПараметрыЗаписи)
	
	// Обработчик подсистемы "Свойства"
	УправлениеСвойствами.ПередЗаписьюНаСервере(ЭтаФорма, ТекущийОбъект);
	
	МодификацияКонфигурацииПереопределяемый.ПередЗаписьюНаСервере(ЭтаФорма, Отказ, ТекущийОбъект, ПараметрыЗаписи);
	
КонецПроцедуры

&НаКлиенте
Процедура ПослеЗаписи(ПараметрыЗаписи)
	
	Оповестить("Запись_ЭтапыПроизводства",, Объект.Ссылка);
	
	МодификацияКонфигурацииКлиентПереопределяемый.ПослеЗаписи(ЭтаФорма, ПараметрыЗаписи);
	
КонецПроцедуры

&НаСервере
Процедура ПослеЗаписиНаСервере(ТекущийОбъект, ПараметрыЗаписи)

	МодификацияКонфигурацииПереопределяемый.ПослеЗаписиНаСервере(ЭтаФорма, ТекущийОбъект, ПараметрыЗаписи);

КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

// СтандартныеПодсистемы.Свойства

&НаКлиенте
Процедура ОбновитьЗависимостиДополнительныхРеквизитов()
	УправлениеСвойствамиКлиент.ОбновитьЗависимостиДополнительныхРеквизитов(ЭтотОбъект);
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ПриИзмененииДополнительногоРеквизита(Элемент)
	УправлениеСвойствамиКлиент.ОбновитьЗависимостиДополнительныхРеквизитов(ЭтотОбъект);
КонецПроцедуры

// Конец СтандартныеПодсистемы.Свойства

#Область ПроцедурыПодсистемыСвойств

&НаСервере
Процедура ОбновитьЭлементыДополнительныхРеквизитов()
	
	УправлениеСвойствами.ОбновитьЭлементыДополнительныхРеквизитов(ЭтаФорма);
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура Подключаемый_РедактироватьСоставСвойств(Команда)
	
	УправлениеСвойствамиКлиент.РедактироватьСоставСвойств(ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ВыполнитьПереопределяемуюКоманду(Команда)
	
	СобытияФормКлиент.ВыполнитьПереопределяемуюКоманду(ЭтаФорма, Команда);
	
КонецПроцедуры

// { Акита Ильенко [IC-75076] 03.02.2020
&НаСервере
Процедура УстановитьСостояниеНаСервере(НомерРяда, Состояние=Неопределено)
	МенеджерЗаписи = РегистрыСведений.актСтатусыРядовТеплицы.СоздатьМенеджерЗаписи();
	МенеджерЗаписи.Период = ТекущаяДата();
	МенеджерЗаписи.Теплица = Объект.Ссылка;
	МенеджерЗаписи.Ряд = НомерРяда;
	МенеджерЗаписи.СостояниеРяда = ?(Состояние=Неопределено, Справочники.актСостоянияРядов.ПустаяСсылка(), Состояние);
	МенеджерЗаписи.Записать();		
КонецПроцедуры

&НаКлиенте
Процедура УстановитьСостояние(Команда)
	НомерРяда = 0;
    Оповещение = Новый ОписаниеОповещения("ПослеВводаЧисла", ЭтотОбъект);
    ПоказатьВводЧисла(Оповещение, , "Укажите ряд", 4, 0);    
КонецПроцедуры

&НаКлиенте
Процедура ПослеВводаЧисла(НомерРяда, ДопПараметры) Экспорт
	Если Не НомерРяда = Неопределено Тогда
		Параметры.НомерРяда = НомерРяда;
		ОткрытьФорму("Справочник.актСостоянияРядов.ФормаВыбора", , ЭтаФорма, , ,,Новый ОписаниеОповещения("ОбработкаВыбораСостояния", ЭтотОбъект),РежимОткрытияОкнаФормы.БлокироватьОкноВладельца);
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаВыбораСостояния(Состояние, ДопПараметры) Экспорт
	Если Состояние<>Неопределено Тогда
		УстановитьСостояниеНаСервере(Параметры.НомерРяда, Состояние);
		Элементы.СостояниеРядов.Обновить();	
	КонецЕсли;	
КонецПроцедуры

&НаКлиенте
Процедура ИзменитьСостояние(Команда)
	ТекДанные = Элементы.СостояниеРядов.ТекущиеДанные;
	Если ТекДанные <> Неопределено И ТекДанные.Период <> НачалоДня(ТекущаяДата()) Тогда
		Параметры.НомерРяда = ТекДанные.Ряд;
		ОткрытьФорму("Справочник.актСостоянияРядов.ФормаВыбора", , ЭтаФорма, , ,,Новый ОписаниеОповещения("ОбработкаВыбораСостояния", ЭтотОбъект),РежимОткрытияОкнаФормы.БлокироватьОкноВладельца);
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура ВосстановитьРяд(Команда)
	ТекДанные = Элементы.СостояниеРядов.ТекущиеДанные;
	Если ТекДанные <> Неопределено И ТекДанные.Период <> НачалоДня(ТекущаяДата()) Тогда
		УстановитьСостояниеНаСервере(ТекДанные.Ряд);
	КонецЕсли;
	Элементы.СостояниеРядов.Обновить();
КонецПроцедуры

// } Акита Ильенко [IC-75076] 03.02.2020

#КонецОбласти


