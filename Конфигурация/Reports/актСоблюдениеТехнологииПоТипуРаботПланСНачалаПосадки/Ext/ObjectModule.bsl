﻿
#Область СведенияОВнешнейОбработке

// Интерфейс для регистрации обработки.
// Вызывается при добавлении обработки в справочник "ВнешниеОбработки"
//
// Возвращаемое значение:
// Структура:
// Вид - строка - возможные значения:	"ДополнительнаяОбработка"
//										"ДополнительныйОтчет"
//										"ЗаполнениеОбъекта"
//										"Отчет"
//										"ПечатнаяФорма"
//										"СозданиеСвязанныхОбъектов"
//
// Назначение - массив строк имен объектов метаданных в формате:
//			<ИмяКлассаОбъектаМетаданного>.[ * | <ИмяОбъектаМетаданных>]
//			Например, "Документ.СчетЗаказ" или "Справочник.*"
//			Прим. параметр имеет смысл только для назначаемых обработок
//
// Наименование - строка - наименование обработки, которым будет заполнено
//						наименование справочника по умолчанию - краткая строка для
//						идентификации обработки администратором
//
// Версия - строка - версия обработки в формате <старший номер>.<младший номер>
//					используется при загрузке обработок в информационную базу
// БезопасныйРежим – Булево – Если истина, обработка будет запущена в безопасном режиме.
//							Более подбробная информация в справке.
//
// Информация - Строка- краткая информация по обработке, описание обработки
//
// Команды - ТаблицаЗначений - команды, поставляемые обработкой, одная строка таблицы соотвествует
//							одной команде
//				колонки: 
//				 - Представление - строка - представление команды конечному пользователю
//				 - Идентификатор - строка - идентефикатор команды. В случае печатных форм
//											перечисление через запятую списка макетов
//				 - Использование - строка - варианты запуска обработки:
//						"ОткрытиеФормы" - открыть форму обработки
//						"ВызовКлиентскогоМетода" - вызов клиентского экспортного метода из формы обработки
//						"ВызовСерверногоМетода" - вызов серверного экспортного метода из модуля объекта обработки
//				 - ПоказыватьОповещение – Булево – если Истина, требуется оказывать оповещение при начале
//								и при окончании запуска обработки. Прим. Имеет смысл только
//								при запуске обработки без открытия формы.
//				 - Модификатор – строка - для печатных форм MXL, которые требуется
//										отображать в форме ПечатьДокументов подсистемы Печать
//										требуется установить как "ПечатьMXL"
//
Функция СведенияОВнешнейОбработке() Экспорт
	
	ПараметрыРегистрации = Новый Структура;
	
	ПараметрыРегистрации.Вставить("Вид", "ДополнительныйОтчет");
	ПараметрыРегистрации.Вставить("Назначение", Неопределено);
	ПараметрыРегистрации.Вставить("Наименование", НСтр("ru = 'Соблюдение технологии по типу работ (План с начала посадки)'"));
	ПараметрыРегистрации.Вставить("Версия", "1.0");
	ПараметрыРегистрации.Вставить("БезопасныйРежим", Истина);
	ПараметрыРегистрации.Вставить("Информация", НСтр("ru = 'Соблюдение технологии по типу работ (План с начала посадки)'"));
	
	ТаблицаКоманд = ПолучитьТаблицуКоманд();
	
	ДобавитьКоманду(ТаблицаКоманд,
					НСтр("ru = 'Соблюдение технологии по типу работ (План с начала посадки)'"),
					"СоблюдениеПланаПоТипуРабот",
					"ОткрытиеФормы");
	
	ПараметрыРегистрации.Вставить("Команды", ТаблицаКоманд);
	
	Возврат ПараметрыРегистрации;
	
КонецФункции

#КонецОбласти

#Область ОбработчикиСобытий

Процедура ПриКомпоновкеРезультата(ДокументРезультат, ДанныеРасшифровки, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	
	// Произвольный отчет
	
	Для Каждого ЭлементНастройки ИЗ ЭтотОбъект.КомпоновщикНастроек.ПользовательскиеНастройки.Элементы Цикл
		
		Если ТипЗнч(ЭлементНастройки) = Тип("ЗначениеПараметраНастроекКомпоновкиДанных") Тогда
			Если ЭлементНастройки.Параметр = Новый ПараметрКомпоновкиДанных("СтруктураТеплиц") Тогда
				СтруктураТеплиц = ЭлементНастройки.Значение;
			КонецЕсли;
			Если ЭлементНастройки.Параметр = Новый ПараметрКомпоновкиДанных("НачалоПериода") Тогда
				НачалоПериода = ЭлементНастройки.Значение;
			КонецЕсли;
			//Если ЭлементНастройки.Параметр = Новый ПараметрКомпоновкиДанных("КонецПериода") Тогда
			//	КонецПериода = ЭлементНастройки.Значение;
			//КонецЕсли;
		КонецЕсли;
		
	КонецЦикла;
	
	ВывестиТаблицу(ДокументРезультат,СтруктураТеплиц,НачалоПериода);//,НачалоПериода,КонецПериода);
	ДокументРезультат.ТолькоПросмотр = Истина;
	ДокументРезультат.ОтображатьЗаголовки = Ложь;
	
	// Стандартный вывод СКД
	КомпоновщикМакета = Новый КомпоновщикМакетаКомпоновкиДанных;
	МакетКомпоновки = КомпоновщикМакета.Выполнить(
	ЭтотОбъект.СхемаКомпоновкиДанных,
	ЭтотОбъект.КомпоновщикНастроек.Настройки,
	ДанныеРасшифровки);
	
	ПроцессорКомпоновки = Новый ПроцессорКомпоновкиДанных;
	ПроцессорКомпоновки.Инициализировать(МакетКомпоновки, , ДанныеРасшифровки, Истина);
	
	ПроцессорВывода = Новый ПроцессорВыводаРезультатаКомпоновкиДанныхВТабличныйДокумент;
	ПроцессорВывода.УстановитьДокумент(ДокументРезультат);
	ПроцессорВывода.Вывести(ПроцессорКомпоновки);
	

		
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

Функция ПолучитьТаблицуКоманд()
	
	Команды = Новый ТаблицаЗначений;
	Команды.Колонки.Добавить("Представление", Новый ОписаниеТипов("Строка"));
	Команды.Колонки.Добавить("Идентификатор", Новый ОписаниеТипов("Строка"));
	Команды.Колонки.Добавить("Использование", Новый ОписаниеТипов("Строка"));
	Команды.Колонки.Добавить("ПоказыватьОповещение", Новый ОписаниеТипов("Булево"));
	Команды.Колонки.Добавить("Модификатор", Новый ОписаниеТипов("Строка"));
	
	Возврат Команды;
	
КонецФункции

Процедура ДобавитьКоманду(ТаблицаКоманд, Представление, Идентификатор, Использование, ПоказыватьОповещение = Ложь, Модификатор = "")
	
	НоваяКоманда = ТаблицаКоманд.Добавить();
	НоваяКоманда.Представление = Представление;
	НоваяКоманда.Идентификатор = Идентификатор;
	НоваяКоманда.Использование = Использование;
	НоваяКоманда.ПоказыватьОповещение = ПоказыватьОповещение;
	НоваяКоманда.Модификатор = Модификатор;
	
КонецПроцедуры

Процедура ВывестиТаблицу(ТабДок,СтруктураТеплиц,НачалоПериода) Экспорт
	
	
	// ДатаОтчета
	
	//Дата = Дата('20160822');
	//ДатаОкончания = Дата('20170716');
	
	Запрос = Новый Запрос(
	"ВЫБРАТЬ
	|	МИНИМУМ(СтруктураПосевныхПлощадей.ДатаНачала) КАК ДатаНачала,
	|	МАКСИМУМ(СтруктураПосевныхПлощадей.ДатаОкончания) КАК ДатаОкончания
	|ИЗ
	|	Справочник.актСтруктураПосевныхПлощадей КАК СтруктураПосевныхПлощадей
	|ГДЕ
	|	СтруктураПосевныхПлощадей.ПометкаУдаления = ЛОЖЬ
	|	И СтруктураПосевныхПлощадей.Владелец В ИЕРАРХИИ(&актТеплица)"
	//|	И СтруктураПосевныхПлощадей.ДатаОкончания >= &ТекущаяДата"
	);
	Запрос.УстановитьПараметр("актТеплица",СтруктураТеплиц);
	Запрос.УстановитьПараметр("ТекущаяДата",НачалоГода(ТекущаяДата()));
	
	Выборка = Запрос.Выполнить().Выбрать();
	
	Если Выборка.Следующий() Тогда
		Дата = Выборка.ДатаНачала;
		ДатаОкончания = Выборка.ДатаОкончания;
	КонецЕсли;
	
	//Если Дата < Дата(2017,4,3) Тогда
	//	Дата = Дата(2017,4,3);
	//КонецЕсли;
	
	Если ЗначениеЗаполнено(НачалоПериода) И Дата < НачалоПериода.Дата Тогда
		Дата = НачалоПериода.Дата;
	КонецЕсли;
	
	
	
	// Определение периода по технологическим картам
	
	ТекущаяНеделяВПланах = Истина;
	ПосевнаяПлощадь = Справочники.актСтруктураПосевныхПлощадей.ПустаяСсылка();
	
	//////////////////////
	
	Если ТекущаяНеделяВПланах Тогда
		ТекущаяНеделя = ТекущаяДата()-7*86400;
	Иначе
		ТекущаяНеделя = ТекущаяДата();
	КонецЕсли;
	
	///
	
	СписокДатТЗ = Новый ТаблицаЗначений;
	СписокДатТЗ.Колонки.Добавить("ДатаНедели");
	
	СписокДатТЗ.Очистить();
	
	///
	
	Массив = Новый Массив;
	
	КЧ = Новый КвалификаторыЧисла(9,2);
	Массив.Добавить(Тип("Число"));
	ОписаниеТиповЧ = Новый ОписаниеТипов(Массив, , ,КЧ);
	Массив.Очистить();
	
	КС = Новый КвалификаторыСтроки(150);
	Массив.Добавить(Тип("Строка"));
	ОписаниеТиповС = Новый ОписаниеТипов(Массив, , , ,КС);
	Массив.Очистить();
	
	
	Планы = Новый ТаблицаЗначений;
	Планы.Колонки.Добавить("Работа",ОписаниеТиповС);
	Планы.Колонки.Добавить("ПосевнаяПлощадь",ОписаниеТиповС);
	Планы.Колонки.Добавить("Плановый");
	Планы.Колонки.Добавить("Проход",ОписаниеТиповС);
	Планы.Колонки.Добавить("Выполнено",ОписаниеТиповЧ);
	Планы.Колонки.Добавить("Перевыполнено",ОписаниеТиповЧ);
	Планы.Колонки.Добавить("Отделение",ОписаниеТиповС);
	Планы.Колонки.Добавить("Неделя");
	Планы.Колонки.Добавить("План");
	
	Планы.Очистить();
	
	ПланыРаботы = Новый ТаблицаЗначений;
	ПланыРаботы.Колонки.Добавить("Работа",ОписаниеТиповС);
	ПланыРаботы.Колонки.Добавить("ПосевнаяПлощадь",ОписаниеТиповС);
	ПланыРаботы.Колонки.Добавить("Отделение",ОписаниеТиповС);	
	
	ПланыРаботы.Очистить();
	
	ДатаНачалаФакта = НачалоНедели(Дата);
	ДатаНачалаПлана = КонецНедели(ДатаОкончания);
	
	Пока ДатаНачалаФакта <= ДатаНачалаПлана Цикл
		Нов = СписокДатТЗ.Добавить();
		Нов.ДатаНедели = ДатаНачалаФакта;
		
		ДатаНачалаФакта = ДатаНачалаФакта + 7*86400;
	КонецЦикла;
	
	Для каждого Дат из СписокДатТЗ Цикл
		ДатаНачала = Дат.ДатаНедели;
		
		Запрос = Новый Запрос;
		Запрос.Текст = 
		"ВЫБРАТЬ РАЗЛИЧНЫЕ
		|	ТехнологическаяКартаТовары.Номенклатура КАК ВидРабот,
		|	ТехнологическаяКартаТовары.Ссылка.ПосевнаяПлощадь КАК ПосевнаяПлощадь,
		|	ТехнологическаяКартаТовары.Ссылка.ПосевнаяПлощадь.Владелец КАК Отделение,
		|	ТехнологическаяКартаТовары.Ссылка КАК ТехнологическаяКарта,
		|	ТехнологическаяКартаТовары.ПроходовВНеделю КАК ПроходовВНеделю
		|ИЗ
		|	Документ.актТехнологическаяКарта.Товары КАК ТехнологическаяКартаТовары
		|ГДЕ
		|	ТехнологическаяКартаТовары.ДатаНачала <= &ДатаНачала
		|	И ТехнологическаяКартаТовары.ДатаОкончания >= &ДатаНачала
		|	И ТехнологическаяКартаТовары.Ссылка <> &ПустаяКарта
		|	И ТехнологическаяКартаТовары.ПроходовВНеделю > 0
		|	И ТехнологическаяКартаТовары.Ссылка.Неактуально = ЛОЖЬ
		|	И ТехнологическаяКартаТовары.Ссылка.ПометкаУдаления = ЛОЖЬ";
		
		Если НЕ ПустаяСтрока(Строка(ПосевнаяПлощадь)) Тогда
			Запрос.Текст = Запрос.Текст + "
			|	И ТехнологическаяКартаТовары.Ссылка.ПосевнаяПлощадь = &актПосевнаяПлощадь";
		Иначе
			Если НЕ ПустаяСтрока(Строка(СтруктураТеплиц)) Тогда
				Запрос.Текст = Запрос.Текст + "
				|	И ТехнологическаяКартаТовары.Ссылка.ПосевнаяПлощадь.Владелец В Иерархии (&СтруктураТеплиц)";
			КонецЕсли;
		КонецЕсли;
		
		Запрос.Текст = Запрос.Текст + "
		|
		|УПОРЯДОЧИТЬ ПО
		|	ТехнологическаяКартаТовары.Ссылка.ПосевнаяПлощадь.Владелец.Наименование,
		|	ТехнологическаяКартаТовары.Ссылка.ПосевнаяПлощадь,
		|	ТехнологическаяКартаТовары.Номенклатура.Наименование";
		
		Запрос.УстановитьПараметр("ДатаНачала", НачалоНедели(ДатаНачала));
		Запрос.УстановитьПараметр("актПосевнаяПлощадь", ПосевнаяПлощадь);
		Запрос.УстановитьПараметр("ПустаяКарта", Документы.актТехнологическаяКарта.ПустаяСсылка());
		Запрос.УстановитьПараметр("СтруктураТеплиц", СтруктураТеплиц);
		
		РезультатЗапроса = Запрос.Выполнить();
		
		ПланыТЗ = РезультатЗапроса.Выгрузить();
		
		Для каждого Стр из ПланыТЗ Цикл
			Отбор = Новый Структура();
			Отбор.Вставить("Работа",Строка(Стр.ВидРабот));
			Отбор.Вставить("ПосевнаяПлощадь",Строка(Стр.ПосевнаяПлощадь));
			Отбор.Вставить("Отделение",Строка(Стр.Отделение));
			
			Строки = ПланыРаботы.НайтиСтроки(Отбор);
			Если Строки.Количество() = 0 Тогда
				НовТЗ = ПланыРаботы.Добавить();
				НовТЗ.Работа = Строка(Стр.ВидРабот);
				НовТЗ.ПосевнаяПлощадь = Строка(Стр.ПосевнаяПлощадь);
				НовТЗ.Отделение = Строка(Стр.Отделение);
			КонецЕсли;
			
			ПроходовВНеделю = Стр.ПроходовВНеделю;
			Если ПроходовВНеделю = Неопределено или ПроходовВНеделю = Null Тогда
				ПроходовВНеделю = 0;
			КонецЕсли;
			
			Необработанных = 0;
			Всего = 0;
			Выполнено = 0;
			
			Для Проход = 1 по ПроходовВНеделю Цикл
				
				Запрос = Новый Запрос;
				Запрос.Текст = 
				"ВЫБРАТЬ
				|	ЗаказНарядНаРаботыПараметрыВыполненияРабот.РезультатВыполнения КАК Ряд,
				|	ЗаказНарядНаРаботыПараметрыВыполненияРабот.Ссылка.ДатаВыполнения КАК ДатаВыполнения,
				|	ЗаказНарядНаРаботыПараметрыВыполненияРабот.Ссылка.Исполнитель КАК Исполнитель
				|ИЗ
				|	Документ.актЗаказНарядНаРаботы.ПараметрыВыполненияРабот КАК ЗаказНарядНаРаботыПараметрыВыполненияРабот
				|ГДЕ
				|	ЗаказНарядНаРаботыПараметрыВыполненияРабот.Параметр.Наименование = ""Ряд""
				|	И ЗаказНарядНаРаботыПараметрыВыполненияРабот.Ссылка.ПометкаУдаления = ЛОЖЬ
				|	И НАЧАЛОПЕРИОДА(ЗаказНарядНаРаботыПараметрыВыполненияРабот.Ссылка.ДатаВыполнения, ДЕНЬ) >= &ДатаНачала
				|	И НАЧАЛОПЕРИОДА(ЗаказНарядНаРаботыПараметрыВыполненияРабот.Ссылка.ДатаВыполнения, ДЕНЬ) <= &ДатаОкончания
				//|	И ЗаказНарядНаРаботыПараметрыВыполненияРабот.Ссылка.Проведен = ИСТИНА
				|	И ЗаказНарядНаРаботыПараметрыВыполненияРабот.Ссылка.Факт = ИСТИНА
				//|	И ЗаказНарядНаРаботыПараметрыВыполненияРабот.Ссылка.ВидРабот.Родитель <> &СборТоматы
				//|	И ЗаказНарядНаРаботыПараметрыВыполненияРабот.Ссылка.ВидРабот.Родитель <> &СборОгурцы
				|	И ЗаказНарядНаРаботыПараметрыВыполненияРабот.Ссылка.ВидРабот.Родитель <> &Повреждения
				|	И ЗаказНарядНаРаботыПараметрыВыполненияРабот.Ссылка.ВидРабот.Родитель <> &Вредители
				|	И ЗаказНарядНаРаботыПараметрыВыполненияРабот.Ссылка.ПосевнаяПлощадь = &актПосевнаяПлощадь
				|	И ЗаказНарядНаРаботыПараметрыВыполненияРабот.Ссылка.ВидРабот = &ВидРабот
				|
				|УПОРЯДОЧИТЬ ПО
				|	ЗаказНарядНаРаботыПараметрыВыполненияРабот.РезультатВыполнения,
				|	ЗаказНарядНаРаботыПараметрыВыполненияРабот.Ссылка.Дата";
				
				Запрос.УстановитьПараметр("ДатаНачала", НачалоНедели(ДатаНачала));
				Запрос.УстановитьПараметр("ДатаОкончания", КонецНедели(ДатаНачала));
				Запрос.УстановитьПараметр("актПосевнаяПлощадь", Стр.ПосевнаяПлощадь);
				Запрос.УстановитьПараметр("ВидРабот", Стр.ВидРабот);
				//Запрос.УстановитьПараметр("СборТоматы", Справочники.ВидыРаботСотрудников.НайтиПоКоду("00-000011"));
				//Запрос.УстановитьПараметр("СборОгурцы", Справочники.ВидыРаботСотрудников.НайтиПоКоду("00-000074"));
				Запрос.УстановитьПараметр("Повреждения", РегистрыСведений.актКонстанты.мПолучить("Справочники.ВидыРаботСотрудников.ПоврежденияDamages"));
				Запрос.УстановитьПараметр("Вредители", РегистрыСведений.актКонстанты.мПолучить("Справочники.ВидыРаботСотрудников.ВредителиDeseases"));
				
				РезультатЗапроса = Запрос.Выполнить();
				
				ЗаказНарядыТЗ = РезультатЗапроса.Выгрузить();
				
				Ном = 1;
				//Необработанных = 0;
				//Всего = 0;
				//Выполнено = 0;
				//Перевыполнено = 0;
				
				Для каждого СтрРяд из Стр.ПосевнаяПлощадь.ТопологияТеплицы Цикл
					Для Ряд = СтрРяд.ОрдинатаНачальнаяПозиция по СтрРяд.ОрдинатаКонечнаяПозиция Цикл
						Отборы = Новый Структура;
						Отборы.Вставить("Ряд", Строка(Ряд));
						
						Строки = ЗаказНарядыТЗ.НайтиСтроки(Отборы);
						
						Если Проход = 1 Тогда
							Совпадений = 0;
						ИначеЕсли Проход = 2 Тогда
							Совпадений = 1;
						КонецЕсли;
						
						Всего = Всего + 1;
						Если Строки.Количество() <= Совпадений Тогда
							Необработанных = Необработанных + 1;
						Иначе
							Выполнено = Выполнено + 1;
							//Если Строки.Количество() = Проход Тогда
							//	//
							//ИначеЕсли Строки.Количество() > ПроходовВНеделю Тогда
							//	Если Проход = 1 Тогда
							//		Перевыполнено = Перевыполнено + 1;
							//	ИначеЕсли Строки.Количество() - ПроходовВНеделю > 1 Тогда
							//		Перевыполнено = Перевыполнено + 1;
							//		
							//	КонецЕсли;
							//	
							//КонецЕсли;
						КонецЕсли;
						
						Ном = Ном + 1;
					КонецЦикла;
				КонецЦикла;
				
				//Если Всего > 0 Тогда
				//	НеВыполнено = Формат(Необработанных/Всего * 100,"ЧДЦ=2; ЧН=0");
				//	
				//	//Если Перевыполнено > 0 Тогда
				//	//	Перевыполнено = Формат(Перевыполнено/Всего * 100,"ЧДЦ=2; ЧН=0");
				//	//Иначе
				//	//	Перевыполнено = 0;
				//	//КонецЕсли;
				//Иначе
				//	НеВыполнено = 100;
				//	//Перевыполнено = 0;
				//КонецЕсли;
				//
				//Если НачалоНедели(ДатаНачала) > ТекущаяДата() Тогда
				//	Нов.Выполнено = ПроходовВНеделю;
				//	Нов.План = Истина;
				//Иначе
				//	Нов.Выполнено = 100 - НеВыполнено;
				//	Нов.План = Ложь;
				//КонецЕсли;
				//Нов.Перевыполнено = Перевыполнено;
			КонецЦикла;
			
			Нов = Планы.Добавить();
			Нов.ПосевнаяПлощадь = Строка(Стр.ПосевнаяПлощадь);
			Нов.Отделение = Строка(Стр.Отделение);
			Нов.Неделя = ДатаНачала;
			
			Нов.Работа = Стр.ВидРабот;
			
			Если Всего > 0 Тогда
				НеВыполнено = Формат(Необработанных/Всего * 100,"ЧДЦ=0; ЧН=0");
				
				//Если Перевыполнено > 0 Тогда
				//	Перевыполнено = Формат(Перевыполнено/Всего * 100,"ЧДЦ=2; ЧН=0");
				//Иначе
				//	Перевыполнено = 0;
				//КонецЕсли;
			Иначе
				НеВыполнено = 100;
				//Перевыполнено = 0;
			КонецЕсли;
			
			Если НачалоНедели(ДатаНачала) > ТекущаяНеделя или ПроходовВНеделю = 0 Тогда
				Нов.Выполнено = ПроходовВНеделю;
				Нов.План = Истина;
			Иначе
				Нов.Выполнено = 100 - НеВыполнено;
				Нов.План = Ложь;
			КонецЕсли;
			
		КонецЦикла;
	КонецЦикла;
	
	//////////////////////////////
	
	Если ТекущаяНеделяВПланах Тогда
		ТекущаяНеделя = ТекущаяДата()-7*86400;
	Иначе
		ТекущаяНеделя = ТекущаяДата();
	КонецЕсли;
		
	//ТабДок = Новый ТабличныйДокумент;
	Макет = ЭтотОбъект.ПолучитьМакет("МакетСвод");
	
	ОбластьШапка = Макет.ПолучитьОбласть("Шапка|Основа");
	ОбластьШапкаСреднее = Макет.ПолучитьОбласть("Шапка|Среднее");
	ОбластьШапкаФакт = Макет.ПолучитьОбласть("Шапка|Факт");
	ОбластьШапкаПусто = Макет.ПолучитьОбласть("Шапка|Пусто");
	
	ОбластьОтделение = Макет.ПолучитьОбласть("Отделение|Основа");
	ОбластьОтделениеСК = Макет.ПолучитьОбласть("ОтделениеК|Среднее");
	ОбластьОтделениеСЗ = Макет.ПолучитьОбласть("ОтделениеЗ|Среднее");
	ОбластьОтделениеСЖ = Макет.ПолучитьОбласть("ОтделениеЖ|Среднее");
	ОбластьОтделениеСО = Макет.ПолучитьОбласть("ОтделениеО|Среднее");
	ОбластьОтделениеФакт = Макет.ПолучитьОбласть("Отделение|Факт");
	ОбластьОтделениеПусто = Макет.ПолучитьОбласть("Отделение|Пусто");
	
	ОбластьПосевнаяПлощадь = Макет.ПолучитьОбласть("ПосевнаяПлощадь|Основа");
	ОбластьПосевнаяПлощадьСК = Макет.ПолучитьОбласть("ПосевнаяПлощадьК|Среднее");
	ОбластьПосевнаяПлощадьСЗ = Макет.ПолучитьОбласть("ПосевнаяПлощадьЗ|Среднее");
	ОбластьПосевнаяПлощадьСЖ = Макет.ПолучитьОбласть("ПосевнаяПлощадьЖ|Среднее");
	ОбластьПосевнаяПлощадьСО = Макет.ПолучитьОбласть("ПосевнаяПлощадьО|Среднее");
	ОбластьПосевнаяПлощадьФакт = Макет.ПолучитьОбласть("ПосевнаяПлощадь|Факт");
	ОбластьПосевнаяПлощадьПусто = Макет.ПолучитьОбласть("ПосевнаяПлощадь|Пусто");
	
	ОбластьСтрока = Макет.ПолучитьОбласть("Работа|Основа");
	ОбластьСтрокаСК = Макет.ПолучитьОбласть("РаботаК|Среднее");
	ОбластьСтрокаСЗ = Макет.ПолучитьОбласть("Работа|Среднее");
	ОбластьСтрокаСЖ = Макет.ПолучитьОбласть("РаботаЖ|Среднее");
	ОбластьСтрокаСО = Макет.ПолучитьОбласть("РаботаО|Среднее");
	
	ОбластьСтрокаФ = Макет.ПолучитьОбласть("Работа|Факт");
	ОбластьСтрокаФК = Макет.ПолучитьОбласть("РаботаК|Факт");
	ОбластьСтрокаФЖ = Макет.ПолучитьОбласть("РаботаЖ|Факт");
	ОбластьСтрокаФО = Макет.ПолучитьОбласть("РаботаО|Факт");
	ОбластьСтрокаП = Макет.ПолучитьОбласть("Работа|План");
	ОбластьСтрокаПусто = Макет.ПолучитьОбласть("Работа|Пусто");
	
	ОбластьШапка.Параметры.Теплица = СтруктураТеплиц;
	
	ТабДок.НачатьАвтогруппировкуСтрок();
	
	ТабДок.Вывести(ОбластьШапка);
	ТабДок.Присоединить(ОбластьШапкаСреднее);
	ТабДок.Присоединить(ОбластьШапкаПусто);
	
	Надпись = "";
	СтолбцовДоПлана = 4;
	Для каждого Дат из СписокДатТЗ Цикл
		СтолбцовДоПлана = СтолбцовДоПлана + 1;
		Если НачалоНедели(Дат.ДатаНедели) > ТекущаяНеделя и НачалоНедели(Дат.ДатаНедели) < ТекущаяНеделя+(7*86400) Тогда
			ТабДок.Присоединить(ОбластьОтделениеПусто);
			Надпись = "Количество кругов";
			СтолбецПлана = СтолбцовДоПлана;
		КонецЕсли;
		
		НеделяФакт = "" + Формат(НачалоНедели(Дат.ДатаНедели),"ДФ=dd.MM") + "-" + Формат(КонецНедели(Дат.ДатаНедели),"ДФ=dd.MM");
		ОбластьШапкаФакт.Параметры.НеделяФакт = НеделяФакт;
		ОбластьШапкаФакт.Параметры.Надпись = Надпись;
		Надпись = "";
		
		ТабДок.Присоединить(ОбластьШапкаФакт);
	КонецЦикла;
	ТабДок.ФиксацияСверху = 6;
	
	СтароеОтделение = Неопределено;
	СтараяПлощадь = Неопределено;
	
	ПланыРаботы.Сортировать("Отделение, ПосевнаяПлощадь, Работа");
	Для Каждого Стр из ПланыРаботы Цикл
		Если СтароеОтделение <> Стр.Отделение или СтароеОтделение = Неопределено Тогда 
			ОбластьОтделение.Параметры.Отделение = Стр.Отделение;
			ТабДок.Вывести(ОбластьОтделение,1);
			
			//Среднее по отделению
			Отбор = Новый Структура();
			Отбор.Вставить("Отделение",Строка(Стр.Отделение));
			
			Строки = Планы.НайтиСтроки(Отбор);
			Сумма = 0;
			Количество = 0;
			Для каждого Работа из Строки Цикл 
				Если НЕ Работа.План Тогда
					Сумма = Сумма + Работа.Выполнено;
					Количество = Количество + 1;
				КонецЕсли;
			КонецЦикла;
			
			Если Количество > 0 Тогда
				Среднее = Сумма/Количество;
			Иначе
				Среднее = 0;
			КонецЕсли;
			
			Если Среднее = 0 Тогда
				Если Количество = 0 Тогда
					ТабДок.Присоединить(ОбластьСтрокаПусто);
				Иначе
					ОбластьОтделениеСК.Параметры.Среднее = 0;
					ТабДок.Присоединить(ОбластьОтделениеСК);
				КонецЕсли;
			ИначеЕсли Среднее <= 40 Тогда
				ОбластьОтделениеСК.Параметры.Среднее = Формат(Среднее,"ЧДЦ=0");
				ТабДок.Присоединить(ОбластьОтделениеСК);
			ИначеЕсли Среднее <= 70 Тогда
				ОбластьОтделениеСО.Параметры.Среднее = Формат(Среднее,"ЧДЦ=0");
				ТабДок.Присоединить(ОбластьОтделениеСО);
			ИначеЕсли Среднее < 99.5 Тогда
				ОбластьОтделениеСЖ.Параметры.Среднее = Формат(Среднее,"ЧДЦ=0");
				ТабДок.Присоединить(ОбластьОтделениеСЖ);
			Иначе
				ОбластьОтделениеСЗ.Параметры.Среднее = Формат(Среднее,"ЧДЦ=0");
				ТабДок.Присоединить(ОбластьОтделениеСЗ);
			КонецЕсли;
			
			ТабДок.Присоединить(ОбластьОтделениеПусто);
			
			Для каждого Дат из СписокДатТЗ Цикл
				Если НачалоНедели(Дат.ДатаНедели) > ТекущаяНеделя и НачалоНедели(Дат.ДатаНедели) < ТекущаяНеделя+(7*86400) Тогда
					ТабДок.Присоединить(ОбластьОтделениеПусто);
				КонецЕсли;
				
				Если НачалоНедели(Дат.ДатаНедели) < ТекущаяНеделя Тогда
					//За неделю по отделению
					Отбор = Новый Структура();
					Отбор.Вставить("Отделение",Строка(Стр.Отделение));
					Отбор.Вставить("Неделя",Дат.ДатаНедели);
					
					Строки = Планы.НайтиСтроки(Отбор);
					Сумма = 0;
					Количество = 0;
					Для каждого Работа из Строки Цикл 
						Если НЕ Работа.План Тогда
							Сумма = Сумма + Работа.Выполнено;
							Количество = Количество + 1;
						КонецЕсли;
					КонецЦикла;
					
					Если Количество > 0 Тогда
						Среднее = Сумма/Количество;
					Иначе
						Среднее = 0;
					КонецЕсли;
					
					Если Среднее = 0 Тогда
						Если Количество = 0 Тогда
							ТабДок.Присоединить(ОбластьСтрокаПусто);
						Иначе
							ОбластьОтделениеСК.Параметры.Среднее = 0;
							ТабДок.Присоединить(ОбластьОтделениеСК);
						КонецЕсли;
					ИначеЕсли Среднее <= 40 Тогда
						ОбластьОтделениеСК.Параметры.Среднее = Формат(Среднее,"ЧДЦ=0");
						ТабДок.Присоединить(ОбластьОтделениеСК);
					ИначеЕсли Среднее <= 70 Тогда
						ОбластьОтделениеСО.Параметры.Среднее = Формат(Среднее,"ЧДЦ=0");
						ТабДок.Присоединить(ОбластьОтделениеСО);
					ИначеЕсли Среднее < 99.5 Тогда
						ОбластьОтделениеСЖ.Параметры.Среднее = Формат(Среднее,"ЧДЦ=0");
						ТабДок.Присоединить(ОбластьОтделениеСЖ);
					Иначе
						ОбластьОтделениеСЗ.Параметры.Среднее = Формат(Среднее,"ЧДЦ=0");
						ТабДок.Присоединить(ОбластьОтделениеСЗ);
					КонецЕсли;
				Иначе
					ТабДок.Присоединить(ОбластьОтделениеФакт);
				КонецЕсли;
			КонецЦикла;
			СтароеОтделение = Стр.Отделение;
		КонецЕсли;
		
		Если СтараяПлощадь <> Стр.ПосевнаяПлощадь или СтараяПлощадь = Неопределено Тогда 
			ОбластьПосевнаяПлощадь.Параметры.ПосевнаяПлощадь = Стр.ПосевнаяПлощадь;
			ТабДок.Вывести(ОбластьПосевнаяПлощадь,2);
			
			//Среднее по площади
			Отбор = Новый Структура();
			Отбор.Вставить("ПосевнаяПлощадь",Строка(Стр.ПосевнаяПлощадь));
			Отбор.Вставить("Отделение",Строка(Стр.Отделение));
			
			Строки = Планы.НайтиСтроки(Отбор);
			Сумма = 0;
			Количество = 0;
			Для каждого Работа из Строки Цикл 
				Если НЕ Работа.План Тогда
					Сумма = Сумма + Работа.Выполнено;
					Количество = Количество + 1;
				КонецЕсли;
			КонецЦикла;
			
			Если Количество > 0 Тогда
				Среднее = Сумма/Количество;
			Иначе
				Среднее = 0;
			КонецЕсли;
			
			Если Среднее = 0 Тогда
				Если Количество = 0 Тогда
					ТабДок.Присоединить(ОбластьСтрокаПусто);
				Иначе
					ОбластьПосевнаяПлощадьСК.Параметры.Среднее = 0;
					ТабДок.Присоединить(ОбластьПосевнаяПлощадьСК);
				КонецЕсли;
			ИначеЕсли Среднее <= 40 Тогда
				ОбластьПосевнаяПлощадьСК.Параметры.Среднее = Формат(Среднее,"ЧДЦ=0");
				ТабДок.Присоединить(ОбластьПосевнаяПлощадьСК);
			ИначеЕсли Среднее <= 70 Тогда
				ОбластьПосевнаяПлощадьСО.Параметры.Среднее = Формат(Среднее,"ЧДЦ=0");
				ТабДок.Присоединить(ОбластьПосевнаяПлощадьСО);
			ИначеЕсли Среднее < 99.5 Тогда
				ОбластьПосевнаяПлощадьСЖ.Параметры.Среднее = Формат(Среднее,"ЧДЦ=0");
				ТабДок.Присоединить(ОбластьПосевнаяПлощадьСЖ);
			Иначе
				ОбластьПосевнаяПлощадьСЗ.Параметры.Среднее = Формат(Среднее,"ЧДЦ=0");
				ТабДок.Присоединить(ОбластьПосевнаяПлощадьСЗ);
			КонецЕсли;
			
			ТабДок.Присоединить(ОбластьПосевнаяПлощадьПусто);
			
			Для каждого Дат из СписокДатТЗ Цикл
				Если НачалоНедели(Дат.ДатаНедели) > ТекущаяНеделя и НачалоНедели(Дат.ДатаНедели) < ТекущаяНеделя+(7*86400) Тогда
					ТабДок.Присоединить(ОбластьОтделениеПусто);
				КонецЕсли;
				
				Если НачалоНедели(Дат.ДатаНедели) < ТекущаяНеделя Тогда
					//За неделю по площади
					Отбор = Новый Структура();
					Отбор.Вставить("ПосевнаяПлощадь",Строка(Стр.ПосевнаяПлощадь));
					Отбор.Вставить("Отделение",Строка(Стр.Отделение));
					Отбор.Вставить("Неделя",Дат.ДатаНедели);
					
					Строки = Планы.НайтиСтроки(Отбор);
					Сумма = 0;
					Количество = 0;
					Для каждого Работа из Строки Цикл 
						Если НЕ Работа.План Тогда
							Сумма = Сумма + Работа.Выполнено;
							Количество = Количество + 1;
						КонецЕсли;
					КонецЦикла;
					
					Если Количество > 0 Тогда
						Среднее = Сумма/Количество;
					Иначе
						Среднее = 0;
					КонецЕсли;
					
					Если Среднее = 0 Тогда
						Если Количество = 0 Тогда
							ТабДок.Присоединить(ОбластьСтрокаПусто);
						Иначе
							ОбластьПосевнаяПлощадьСК.Параметры.Среднее = 0;
							ТабДок.Присоединить(ОбластьПосевнаяПлощадьСК);
						КонецЕсли;
					ИначеЕсли Среднее <= 40 Тогда
						ОбластьПосевнаяПлощадьСК.Параметры.Среднее = Формат(Среднее,"ЧДЦ=0");
						ТабДок.Присоединить(ОбластьПосевнаяПлощадьСК);
					ИначеЕсли Среднее <= 70 Тогда
						ОбластьПосевнаяПлощадьСО.Параметры.Среднее = Формат(Среднее,"ЧДЦ=0");
						ТабДок.Присоединить(ОбластьПосевнаяПлощадьСО);
					ИначеЕсли Среднее < 99.5 Тогда
						ОбластьПосевнаяПлощадьСЖ.Параметры.Среднее = Формат(Среднее,"ЧДЦ=0");
						ТабДок.Присоединить(ОбластьПосевнаяПлощадьСЖ);
					Иначе
						ОбластьПосевнаяПлощадьСЗ.Параметры.Среднее = Формат(Среднее,"ЧДЦ=0");
						ТабДок.Присоединить(ОбластьПосевнаяПлощадьСЗ);
					КонецЕсли;
				Иначе
					ТабДок.Присоединить(ОбластьПосевнаяПлощадьФакт);
				КонецЕсли;
			КонецЦикла;
			
			СтараяПлощадь = Стр.ПосевнаяПлощадь;
		КонецЕсли;
		
		ОбластьСтрока.Параметры.Заполнить(Стр);	
		ТабДок.Вывести(ОбластьСтрока,3);
		
		//Среднее по работе
		Отбор = Новый Структура();
		Отбор.Вставить("Работа",Строка(Стр.Работа));
		Отбор.Вставить("ПосевнаяПлощадь",Строка(Стр.ПосевнаяПлощадь));
		Отбор.Вставить("Отделение",Строка(Стр.Отделение));
		
		Строки = Планы.НайтиСтроки(Отбор);
		Сумма = 0;
		Количество = 0;
		Для каждого Работа из Строки Цикл 
			Если НЕ Работа.План Тогда
				Сумма = Сумма + Работа.Выполнено;
				Количество = Количество + 1;
			КонецЕсли;
		КонецЦикла;
		
		Если Количество > 0 Тогда
			Среднее = Сумма/Количество;
		Иначе
			Среднее = 0;
		КонецЕсли;
		
		Если Среднее = 0 Тогда
			Если Количество = 0 Тогда
				ТабДок.Присоединить(ОбластьСтрокаПусто);
			Иначе
				ОбластьСтрокаСК.Параметры.Среднее = 0;
				ТабДок.Присоединить(ОбластьСтрокаСК);
			КонецЕсли;
		ИначеЕсли Среднее <= 40 Тогда
			ОбластьСтрокаСК.Параметры.Среднее = Формат(Среднее,"ЧДЦ=0");
			ТабДок.Присоединить(ОбластьСтрокаСК);
		ИначеЕсли Среднее <= 70 Тогда
			ОбластьСтрокаСО.Параметры.Среднее = Формат(Среднее,"ЧДЦ=0");
			ТабДок.Присоединить(ОбластьСтрокаСО);
		ИначеЕсли Среднее < 99.5 Тогда
			ОбластьСтрокаСЖ.Параметры.Среднее = Формат(Среднее,"ЧДЦ=0");
			ТабДок.Присоединить(ОбластьСтрокаСЖ);
		Иначе
			ОбластьСтрокаСЗ.Параметры.Среднее = Формат(Среднее,"ЧДЦ=0");
			ТабДок.Присоединить(ОбластьСтрокаСЗ);
		КонецЕсли;
		
		ТабДок.Присоединить(ОбластьОтделениеПусто);
		
		Для каждого Дат из СписокДатТЗ Цикл
			Если НачалоНедели(Дат.ДатаНедели) > ТекущаяНеделя и НачалоНедели(Дат.ДатаНедели) < ТекущаяНеделя+(7*86400) Тогда
				ТабДок.Присоединить(ОбластьОтделениеПусто);
			КонецЕсли;                      
			
			Отбор = Новый Структура();
			Отбор.Вставить("Работа",Строка(Стр.Работа));
			Отбор.Вставить("ПосевнаяПлощадь",Строка(Стр.ПосевнаяПлощадь));
			Отбор.Вставить("Отделение",Строка(Стр.Отделение));
			Отбор.Вставить("Неделя",Дат.ДатаНедели);
			
			Строки = Планы.НайтиСтроки(Отбор);
			Если Строки.Количество() > 0 Тогда
				Если Строки[0].План Тогда
					ОбластьСтрокаП.Параметры.Заполнить(Строки[0]);		
					ТабДок.Присоединить(ОбластьСтрокаП);
				ИначеЕсли Строки[0].Выполнено <= 40 Тогда
					ОбластьСтрокаФК.Параметры.Заполнить(Строки[0]);		
					ТабДок.Присоединить(ОбластьСтрокаФК);
				ИначеЕсли Строки[0].Выполнено <= 70 Тогда
					ОбластьСтрокаФО.Параметры.Заполнить(Строки[0]);		
					ТабДок.Присоединить(ОбластьСтрокаФО);
				ИначеЕсли Строки[0].Выполнено < 99.5 Тогда
					ОбластьСтрокаФЖ.Параметры.Заполнить(Строки[0]);		
					ТабДок.Присоединить(ОбластьСтрокаФЖ);
				Иначе
					ОбластьСтрокаФ.Параметры.Заполнить(Строки[0]);		
					ТабДок.Присоединить(ОбластьСтрокаФ);
				КонецЕсли;
			Иначе
				ОбластьСтрокаП.Параметры.Выполнено = 0;
				ТабДок.Присоединить(ОбластьСтрокаП);
			КонецЕсли;
		КонецЦикла;
	КонецЦикла;
	
	ТабДок.ЗакончитьАвтогруппировкуСтрок();
	
	//ТабДок.Записать("C:\test.xlsx",ТипФайлаТабличногоДокумента.XLSX);
	
	
	Попытка
		Область = ТабДок.Область(3,СтолбецПлана,3,СтолбецПлана+4);
		Область.Объединить();
	Исключение
	КонецПопытки;
	
	//ТабДок.Записать("C:\test1.xlsx",ТипФайлаТабличногоДокумента.XLSX);

	
	
КонецПроцедуры

#КонецОбласти

