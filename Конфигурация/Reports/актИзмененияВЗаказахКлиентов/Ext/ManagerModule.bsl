﻿#Область СлужебныйПрограммныйИнтерфейс

#Область ДляВызоваИзДругихПодсистем


// Для вызова из процедуры ВариантыОтчетовПереопределяемый.ПередДобавлениемКомандОтчетов.
// 
// Параметры:
//   КомандыОтчетов              - ТаблицаЗначений - таблица команд для вывода в подменю.
//                                 (См. ВариантыОтчетовПереопределяемый.ПередДобавлениемКомандОтчетов).
//   Параметры                   - Структура - структура, содержащая параметры подключения команды.
//   ДокументыСОтчетомОДвижениях - Массив, Неопределено - массив документов, в которых будет выводится
//                                 команда открытия отчета. Неопределено в том случае когда отчет выводится
//                                 для всех документов со свойством "Проведение" установленным в "Разрешить"
//                                 и непустой коллекцией движений.
//
// Возвращаемое значение:
//   СтрокаТаблицыЗначений, Неопределено - добавленная команда или Неопределено, если нет прав на просмотр отчета.
//
Функция ДобавитьКомандуОтчета(КомандыОтчетов, Параметры) Экспорт
	
	Если Не ПравоДоступа("Просмотр", Метаданные.Отчеты.ДвиженияДокумента) Тогда
		Возврат Неопределено;
	КонецЕсли;
		
	Команда                    = КомандыОтчетов.Добавить();
	Команда.Представление      = НСтр("ru = 'Хронология изменения клиентских заказов';
										|en = 'Changes of invoices'");
	Команда.МножественныйВыбор = Ложь;
	Команда.ИмяПараметраФормы  = "";
	Команда.Важность           = "СмТакже";
	МассивТипов = Новый Массив;
	МассивТипов.Добавить(Тип("ДокументСсылка.ЗаказКлиента"));
	Команда.ТипПараметра       = Новый ОписаниеТипов(МассивТипов);
	Команда.Менеджер           = "Отчет.актИзмененияВЗаказахКлиентов";
	
	Возврат Команда;
	
КонецФункции

// См. ВариантыОтчетовПереопределяемый.НастроитьВариантыОтчетов.
//
Процедура НастроитьВариантыОтчета(Настройки, НастройкиОтчета) Экспорт
	
	НастройкиОтчета.ОпределитьНастройкиФормы = Истина;

КонецПроцедуры


#КонецОбласти

#КонецОбласти
