﻿
// Возвращает массив используемых транспортов сообщений для этого плана обмена.
//
// Возвращаемое значение:
//	Массив - массив содержит значения перечисления ВидыТранспортаСообщенийОбмена.
//
// Пример:
// 1. Если план обмена поддерживает только два транспорта сообщений FILE и FTP,
// то тело функции следует определить следующим образом:
//
//	Результат = Новый Массив;
//	Результат.Добавить(Перечисления.ВидыТранспортаСообщенийОбмена.FILE);
//	Результат.Добавить(Перечисления.ВидыТранспортаСообщенийОбмена.FTP);
//	Возврат Результат;
//
// 2. Если план обмена поддерживает все транспорты сообщений, определенных в конфигурации,
// то тело функции следует определить следующим образом:
//
//	Возврат ОбменДаннымиСервер.ВсеТранспортыСообщенийОбменаКонфигурации();
//
Функция ИспользуемыеТранспортыСообщенийОбмена() Экспорт
	
	Результат = Новый Массив;
	Результат.Добавить(Перечисления.ВидыТранспортаСообщенийОбмена.WS);
	Результат.Добавить(Перечисления.ВидыТранспортаСообщенийОбмена.FILE);
	Результат.Добавить(Перечисления.ВидыТранспортаСообщенийОбмена.FTP);
	Результат.Добавить(Перечисления.ВидыТранспортаСообщенийОбмена.EMAIL);
	Результат.Добавить(Перечисления.ВидыТранспортаСообщенийОбмена.COM);
	
	Возврат Результат;
	
КонецФункции

// Заполняет настройки, влияющие на использование плана обмена.
// 
// Параметры:
//  Настройки - Структура - настройки плана обмена по умолчанию, см. ОбменДаннымиСервер.НастройкиПланаОбменаПоУмолчанию,
//                          описание возвращаемого значения функции.
//
Процедура ПриПолученииНастроек(Настройки) Экспорт
	
	Настройки.ПредупреждатьОНесоответствииВерсийПравилОбмена	= Истина;
	Настройки.ПланОбменаИспользуетсяВМоделиСервиса 				= Ложь;
	
	Настройки.Алгоритмы.ПриПолученииОписанияВариантаНастройки	= Истина;
	
КонецПроцедуры

// Заполняет набор параметров, определяющих вариант настройки обмена.
// 
// Параметры:
//  ОписаниеВарианта       - Структура - набор варианта настройки по умолчанию,
//                                       см. ОбменДаннымиСервер.ОписаниеВариантаНастройкиОбменаПоУмолчанию,
//                                       описание возвращаемого значения.
//  ИдентификаторНастройки - Строка    - идентификатор варианта настройки обмена.
//  ПараметрыКонтекста     - Структура - см. ОбменДаннымиСервер.ПараметрыКонтекстаПолученияОписанияВариантаНастройки,
//                                       описание возвращаемого значения функции.
//
Процедура ПриПолученииОписанияВариантаНастройки(ОписаниеВарианта, ИдентификаторНастройки, ПараметрыКонтекста) Экспорт
	
	ОписаниеВарианта.ЗаголовокПомощникаСозданияОбмена			= НСтр("ru='Настройка синхронизации с 1С:МУП'");
	ОписаниеВарианта.ЗаголовокУзлаПланаОбмена 					= НСтр("ru='Синхронизация с 1С:МУП'");
	ОписаниеВарианта.ИмяФайлаНастроекДляПриемника				= НСтр("ru = 'Настройки обмена МУП-ПП'");
	ОписаниеВарианта.ИспользоватьПомощникСозданияОбменаДанными	= Истина;
	ОписаниеВарианта.ИспользуемыеТранспортыСообщенийОбмена 		= ОбменДаннымиСервер.ВсеТранспортыСообщенийОбменаКонфигурации();
	ОписаниеВарианта.КраткаяИнформацияПоОбмену 					= НСтр("ru = 'Позволяет синхронизировать данные с конфигурацией ""1С:МУП""'");
	ОписаниеВарианта.ПодробнаяИнформацияПоОбмену 				= "ПланОбмена.актПланОбменаМУП.Форма.ПодробнаяИнформация";

КонецПроцедуры
