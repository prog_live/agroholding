﻿
Процедура ЗаписатьСообщениеСИзменениями(Каталог) Экспорт
			
	Сообщение = Новый СообщениеПользователю;
	Сообщение.Текст = "-------- Выгрузка в узел " + Строка(ЭтотОбъект) + " ------------";
	Сообщение.Сообщить();
	
	// Сформировать имя временного файла.
	
	ИмяФайла = Каталог + ?(Прав(Каталог, 1) = "\", "", "\") + "Message" + СокрЛП(ПланыОбмена.актМобильноеПриложениеИнвентаризацияОС.ЭтотУзел().Код) + "_" + СокрЛП(Ссылка.Код) + ".xml";
	
	// Создать объект записи XML
	// *** ЗаписьXML-документов.
	ЗаписьXML = Новый ЗаписьXML;
	ЗаписьXML.ОткрытьФайл(ИмяФайла);
	ЗаписьXML.ЗаписатьОбъявлениеXML();
	
	// *** Инфраструктура сообщений.
	ЗаписьСообщения = ПланыОбмена.СоздатьЗаписьСообщения();
	ЗаписьСообщения.НачатьЗапись(ЗаписьXML, Ссылка);
	Сообщение = Новый СообщениеПользователю;
	Сообщение.Текст = " Номер сообщения: " + ЗаписьСообщения.НомерСообщения;
	Сообщение.Сообщить();
	
	// Получить выборку измененных данных
	// *** Механизм регистрации изменений.
	МассивФильтрМетаданныхСправочники = ПланыОбмена.актМобильноеПриложениеИнвентаризацияОС.ПолучитьМассивМетаданныхИзПланаОбмена("Справочники");
	Если МассивФильтрМетаданныхСправочники.Количество() > 0 Тогда
		ВыборкаИзмененийСправочников = ПланыОбмена.ВыбратьИзменения(ЗаписьСообщения.Получатель,ЗаписьСообщения.НомерСообщения,
		МассивФильтрМетаданныхСправочники);
	
		МассивСсылокНаОС = Новый Массив;	
		Пока ВыборкаИзмененийСправочников.Следующий() Цикл
			ОбъектВыгрузки = ВыборкаИзмененийСправочников.Получить();
			Если ТипЗнч(ОбъектВыгрузки.Ссылка) = Тип("СправочникСсылка.ОсновныеСредства") Тогда
				МассивСсылокНаОС.Добавить(ОбъектВыгрузки.Ссылка);
			КонецЕсли;				
		КонецЦикла;
		
		ВыборкаИзмененийСправочников.Сбросить();
		
		ТаблицаСоШтрихкодамиОС = Неопределено;
		Если МассивСсылокНаОС.Количество() > 0 Тогда
			ТаблицаСоШтрихкодамиОС = ПланыОбмена.актМобильноеПриложениеИнвентаризацияОС.ПолучитьТаблицуСоШтрихкодамиОС(МассивСсылокНаОС);			
		КонецЕсли;
		
		Пока ВыборкаИзмененийСправочников.Следующий() Цикл
			
			Данные = ВыборкаИзмененийСправочников.Получить();
					
			// Записать данные в сообщение *** XML-сериализация.
			ПланыОбмена.актМобильноеПриложениеИнвентаризацияОС.ЗаписатьДанные(ЗаписьXML, Данные, ТаблицаСоШтрихкодамиОС);
					
		КонецЦикла;
	КонецЕсли;
	
	МассивФильтрМетаданныхДокументы = ПланыОбмена.актМобильноеПриложениеИнвентаризацияОС.ПолучитьМассивМетаданныхИзПланаОбмена("Документы");
	Если МассивФильтрМетаданныхДокументы.Количество() > 0 Тогда
		ВыборкаИзмененийДокументов = ПланыОбмена.ВыбратьИзменения(ЗаписьСообщения.Получатель,ЗаписьСообщения.НомерСообщения,
		ПланыОбмена.актМобильноеПриложениеИнвентаризацияОС.ПолучитьМассивМетаданныхИзПланаОбмена("Документы"));
		
		Пока ВыборкаИзмененийДокументов.Следующий() Цикл
			
			Данные = ВыборкаИзмененийДокументов.Получить();
					
			// Записать данные в сообщение *** XML-сериализация.
			ПланыОбмена.актМобильноеПриложениеИнвентаризацияОС.ЗаписатьДанные(ЗаписьXML, Данные, ТаблицаСоШтрихкодамиОС);
					
		КонецЦикла;
	КонецЕсли;
	
	ЗаписьСообщения.ЗакончитьЗапись();
	ЗаписьXML.Закрыть();
	
	
	Сообщение = Новый СообщениеПользователю;
	Сообщение.Текст = "-------- Конец выгрузки ------------";
	Сообщение.Сообщить();
	
КонецПроцедуры

Процедура ПрочитатьСообщениеСИзменениями(Каталог) Экспорт

	// Сформировать имя файла.
	ИмяФайла = Каталог + ?(Прав(Каталог, 1) = "\", "", "\") + "Message" + СокрЛП(Ссылка.Код) + "_" + СокрЛП(ПланыОбмена.актМобильноеПриложениеИнвентаризацияОС.ЭтотУзел().Код) + ".xml";
	
	Файл = Новый Файл(ИмяФайла);
	Если Не Файл.Существует() Тогда
		
		Возврат;
		
	КонецЕсли;
	
	// *** Чтение документов XML
	// Попытаться открыть файл.
	ЧтениеXML = Новый ЧтениеXML;
	Попытка
		
		ЧтениеXML.ОткрытьФайл(ИмяФайла);
		
	Исключение
		
		Сообщение = Новый СообщениеПользователю;
		Сообщение.Текст = "Невозможно открыть файл обмена данными.";
		Сообщение.Сообщить();
		
		Возврат;
		
	КонецПопытки;
	
	Сообщение = Новый СообщениеПользователю;
	Сообщение.Текст = "-------- Загрузка из " + Строка(ЭтотОбъект) + " ------------";
	Сообщение.Сообщить();
	Сообщение = Новый СообщениеПользователю;
	Сообщение.Текст = " – Считывается файл " + ИмяФайла;
	Сообщение.Сообщить();
	
	// Загрузить из найденного файла
	// *** Инфраструктура сообщений.
	ЧтениеСообщения = ПланыОбмена.СоздатьЧтениеСообщения();
	
	// Читать заголовок сообщения обмена данными – файла XML.
	ЧтениеСообщения.НачатьЧтение(ЧтениеXML);
	
	// Сообщение предназначено не для этого узла.
	Если ЧтениеСообщения.Отправитель <> Ссылка Тогда
		
		ВызватьИсключение "Неверный узел";
		
	КонецЕсли;
	
	// Удаляем регистрацию изменений для узла отправителя сообщения.
	// *** Служба регистрации изменений.
	ПланыОбмена.УдалитьРегистрациюИзменений(ЧтениеСообщения.Отправитель, ЧтениеСообщения.НомерПринятого);
	
	// Читаем данные из сообщения *** XML-сериализация.
	Пока ПланыОбмена.актМобильноеПриложениеИнвентаризацияОС.ВозможностьЧтенияДанных(ЧтениеXML) Цикл
		
		// Читаем очередное значение.
		Данные = ПланыОбмена.актМобильноеПриложениеИнвентаризацияОС.ПрочитатьДанные(ЧтениеXML);
				
		// Записать полученные данные.
		Данные.ОбменДанными.Отправитель = ЧтениеСообщения.Отправитель;
		Данные.ОбменДанными.Загрузка = Истина;
		Данные.Записать();
				
	КонецЦикла;
	
	ЧтениеСообщения.ЗакончитьЧтение();
	ЧтениеXML.Закрыть();
	УдалитьФайлы(ИмяФайла);
	
	Сообщение = Новый СообщениеПользователю;
	Сообщение.Текст = "-------- Конец загрузки ------------";
	Сообщение.Сообщить();

КонецПроцедуры
