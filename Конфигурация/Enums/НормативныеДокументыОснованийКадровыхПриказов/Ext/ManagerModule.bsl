﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область СлужебныйПрограммныйИнтерфейс

Функция НормативныйДокумент(Значение) Экспорт
	
	Если Значение = ФЗ79 Тогда
		
		Возврат НСтр("ru = 'Федеральный закон от 27.07.2004 № 79-ФЗ ""О государственной гражданской службе Российской Федерации""';
					|en = 'Federal Law dated 27.07.2004 No. 79-FZ ""On the State Civil Service of the Russian Federation""'");
		
	ИначеЕсли Значение = ЗРФ31321 Тогда
		
		Возврат НСтр("ru = 'Закон РФ от 26.06.1992 № 3132-1 ""О статусе судей в Российской Федерации""';
					|en = 'Law of the Russian Federation dated 26.06.1992 No. 3132-1 ""On the Status of Judges in the Russian Federation""'");
		
	ИначеЕсли Значение = ФЗ25 Тогда
		
		Возврат НСтр("ru = 'Федеральный закон от 02.03.2007 N 25-ФЗ ""О муниципальной службе в Российской Федерации""';
					|en = 'Federal Law dated 02.03.2007 No. 25-FZ ""On Municipal Service in the Russian Federation""'");
		
	ИначеЕсли Значение = ФЗ41 Тогда
		
		Возврат НСтр("ru = 'Федеральный закон от 08.05.1996 N 41-ФЗ ""О производственных кооперативах""';
					|en = 'Federal Law dated 08.05.1996 No. 41-FZ ""On Production Cooperatives""'");
		
	ИначеЕсли Значение = ФЗ328 Тогда
		
		Возврат НСтр("ru = 'Федеральный закон от 01.10.2019 N 328-ФЗ ""О службе в органах принудительного исполнения Российской Федерации и внесении изменений в отдельные законодательные акты Российской Федерации""';
					|en = 'Federal Law dated 01.10.2019 No. 328-FZ ""On Service in the Compulsory Enforcement Bodies of the Russian Federation and Amending Certain Legislative Acts of the Russian Federation""'");
		
	КонецЕсли;
	
	Возврат НСтр("ru = 'Трудовой кодекс Российской Федерации';
				|en = 'Labor Code of the Russian Federation'");
	
КонецФункции

Функция НормативныйДокументВРодительномПадеже(Значение) Экспорт
	
	Если Значение = ФЗ79 Тогда
		
		Возврат НСтр("ru = 'Федерального закона от 27.07.2004 № 79-ФЗ ""О государственной гражданской службе Российской Федерации""';
					|en = 'of the Federal Law dated 27.07.2004 No. 79-FZ ""On the State Civil Service of the Russian Federation""'");
		
	ИначеЕсли Значение = ЗРФ31321 Тогда
		
		Возврат НСтр("ru = 'Закона РФ от 26.06.1992 № 3132-1 ""О статусе судей в Российской Федерации""';
					|en = 'of the Law of the Russian Federation dated 26.06.1992 No. 3132-1 ""On the Status of Judges in the Russian Federation""'");
		
	ИначеЕсли Значение = ФЗ25 Тогда
		
		Возврат НСтр("ru = 'Федерального закона от 02.03.2007 N 25-ФЗ ""О муниципальной службе в Российской Федерации""';
					|en = 'of the Federal Law dated 02.03.2007 No. 25-FZ ""On Municipal Service in the Russian Federation""'");
		
	ИначеЕсли Значение = ФЗ41 Тогда
		
		Возврат НСтр("ru = 'Федерального закона от 08.05.1996 N 41-ФЗ ""О производственных кооперативах""';
					|en = 'of the Federal Law dated 08.05.1996 No. 41-FZ ""On Production Cooperatives""'");
		
	ИначеЕсли Значение = ФЗ328 Тогда
		
		Возврат НСтр("ru = 'Федерального закона от 01.10.2019 N 328-ФЗ ""О службе в органах принудительного исполнения Российской Федерации и внесении изменений в отдельные законодательные акты Российской Федерации""';
					|en = 'of the Federal Law dated 01.10.2019 No. 328-FZ ""On Service in the Compulsory Enforcement Bodies of the Russian Federation and Amending Certain Legislative Acts of the Russian Federation""'");
		
	КонецЕсли;
	
	Возврат НСтр("ru = 'Трудового кодекса Российской Федерации';
				|en = 'of the Labor Code of the Russian Federation'");
	
КонецФункции

#КонецОбласти

#КонецЕсли