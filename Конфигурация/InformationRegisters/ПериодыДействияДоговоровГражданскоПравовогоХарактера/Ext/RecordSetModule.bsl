﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область СлужебныеПроцедурыИФункции

Процедура ПередЗаписью(Отказ, Замещение)

	Если ЗарплатаКадры.ОтключитьБизнесЛогикуПриЗаписи(ЭтотОбъект) Тогда
		Возврат;
	КонецЕсли;
	
	Регистратор = Отбор.Регистратор.Значение;
	
	Для каждого Запись Из ЭтотОбъект Цикл
		Запись.ДокументОснование = Регистратор;
	КонецЦикла;
	
	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("ТекущийНабор", Выгрузить());
	Запрос.УстановитьПараметр("Регистратор", Регистратор);
	
	Запрос.Текст =
		"ВЫБРАТЬ
		|	ПериодыДействияДоговоровГражданскоПравовогоХарактера.Сотрудник
		|ПОМЕСТИТЬ ВТДанныеНабора
		|ИЗ
		|	&ТекущийНабор КАК ПериодыДействияДоговоровГражданскоПравовогоХарактера
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	ПериодыДействияДоговоровГражданскоПравовогоХарактера.Сотрудник
		|ПОМЕСТИТЬ ВТВсеСотрудникиРегистратора
		|ИЗ
		|	РегистрСведений.ПериодыДействияДоговоровГражданскоПравовогоХарактера КАК ПериодыДействияДоговоровГражданскоПравовогоХарактера
		|ГДЕ
		|	ПериодыДействияДоговоровГражданскоПравовогоХарактера.Регистратор = &Регистратор
		|
		|ОБЪЕДИНИТЬ ВСЕ
		|
		|ВЫБРАТЬ
		|	ДанныеНабора.Сотрудник
		|ИЗ
		|	ВТДанныеНабора КАК ДанныеНабора
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ РАЗЛИЧНЫЕ
		|	ВсеСотрудникиРегистратора.Сотрудник
		|ИЗ
		|	ВТВсеСотрудникиРегистратора КАК ВсеСотрудникиРегистратора";
	
	РезультатЗапроса = Запрос.Выполнить();
	Если НЕ РезультатЗапроса.Пустой() Тогда
		ДополнительныеСвойства.Вставить("ОбновляемыеСотрудники", РезультатЗапроса.Выгрузить().ВыгрузитьКолонку("Сотрудник"));
	КонецЕсли;
	
	// Обновление доступа к сотрудникам по организациям
	
	Запрос = Новый Запрос;
	Запрос.МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;
	
	Запрос.УстановитьПараметр("Сотрудники", ВыгрузитьКолонку("Сотрудник"));
	Запрос.УстановитьПараметр("Регистратор", Регистратор);
	
	Запрос.Текст =
		"ВЫБРАТЬ РАЗЛИЧНЫЕ
		|	ПериодыДействияДоговоровГражданскоПравовогоХарактера.Сотрудник КАК Сотрудник,
		|	ПериодыДействияДоговоровГражданскоПравовогоХарактера.Организация КАК Организация
		|ПОМЕСТИТЬ ВТОрганизацииСотрудников
		|ИЗ
		|	РегистрСведений.ПериодыДействияДоговоровГражданскоПравовогоХарактера КАК ПериодыДействияДоговоровГражданскоПравовогоХарактера
		|ГДЕ
		|	ПериодыДействияДоговоровГражданскоПравовогоХарактера.Регистратор = &Регистратор";
	
	Запрос.Выполнить();
	
	ДополнительныеСвойства.Вставить("ОрганизацииСотрудников", Запрос.МенеджерВременныхТаблиц);
	
КонецПроцедуры

Процедура ПриЗаписи(Отказ, Замещение)

	Если ЗарплатаКадры.ОтключитьБизнесЛогикуПриЗаписи(ЭтотОбъект) Тогда
		Возврат;
	КонецЕсли;
	
	Если ДополнительныеСвойства.Свойство("ОбновляемыеСотрудники") Тогда
		КадровыйУчет.ОбновитьДанныеДляПодбораДоговорниковГПХ(ДополнительныеСвойства.ОбновляемыеСотрудники);
		КадровыйУчет.ОбновитьОсновныхСотрудниковФизическихЛицСотрудниковПоДоговорамГПХ(ДополнительныеСвойства.ОбновляемыеСотрудники);
	КонецЕсли;
	
	// Обновление доступа к сотрудникам по организациям
	
	Если ДополнительныеСвойства.Свойство("ОрганизацииСотрудников") Тогда
		
		Запрос = Новый Запрос;
		Запрос.МенеджерВременныхТаблиц = ДополнительныеСвойства.ОрганизацииСотрудников;
		
		Запрос.УстановитьПараметр("Сотрудники", ВыгрузитьКолонку("Сотрудник"));
		Запрос.УстановитьПараметр("Регистратор", Отбор.Регистратор.Значение);
		
		Запрос.Текст =
			"ВЫБРАТЬ РАЗЛИЧНЫЕ
			|	ПериодыДействияДоговоровГражданскоПравовогоХарактера.Сотрудник КАК Сотрудник,
			|	ПериодыДействияДоговоровГражданскоПравовогоХарактера.Организация КАК Организация
			|ПОМЕСТИТЬ ВТТекущиеОрганизацииСотрудников
			|ИЗ
			|	РегистрСведений.ПериодыДействияДоговоровГражданскоПравовогоХарактера КАК ПериодыДействияДоговоровГражданскоПравовогоХарактера
			|ГДЕ
			|	ПериодыДействияДоговоровГражданскоПравовогоХарактера.Регистратор = &Регистратор
			|;
			|
			|////////////////////////////////////////////////////////////////////////////////
			|ВЫБРАТЬ
			|	ЕСТЬNULL(ТекущиеОрганизацииСотрудников.Сотрудник, ОрганизацииСотрудников.Сотрудник) КАК Сотрудник,
			|	ЕСТЬNULL(ТекущиеОрганизацииСотрудников.Организация, ОрганизацииСотрудников.Организация) КАК Организация,
			|	ВЫБОР
			|		КОГДА ТекущиеОрганизацииСотрудников.Сотрудник ЕСТЬ NULL
			|			ТОГДА -1
			|		ИНАЧЕ 1
			|	КОНЕЦ КАК ФлагИзменений
			|ИЗ
			|	ВТТекущиеОрганизацииСотрудников КАК ТекущиеОрганизацииСотрудников
			|		ПОЛНОЕ СОЕДИНЕНИЕ ВТОрганизацииСотрудников КАК ОрганизацииСотрудников
			|		ПО ТекущиеОрганизацииСотрудников.Сотрудник = ОрганизацииСотрудников.Сотрудник
			|			И ТекущиеОрганизацииСотрудников.Организация = ОрганизацииСотрудников.Организация
			|ГДЕ
			|	ЕСТЬNULL(ОрганизацииСотрудников.Организация, ЗНАЧЕНИЕ(Справочник.Организации.ПустаяСсылка)) <> ЕСТЬNULL(ТекущиеОрганизацииСотрудников.Организация, ЗНАЧЕНИЕ(Справочник.Организации.ПустаяСсылка))";
		
		РезультатЗапроса = Запрос.Выполнить();
		Если Не РезультатЗапроса.Пустой() Тогда
			
			ТаблицаАнализаИзменений = КадровыйУчет.ТаблицаАнализаИзменений();
			
			Выборка = РезультатЗапроса.Выбрать();
			Пока Выборка.Следующий() Цикл
				ЗаполнитьЗначенияСвойств(ТаблицаАнализаИзменений.Добавить(), Выборка);
			КонецЦикла;
			
			КадровыйУчет.ОбработатьИзменениеОрганизацийВНабореПоТаблицеИзменений(ТаблицаАнализаИзменений);
			
		КонецЕсли;
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Иначе
ВызватьИсключение НСтр("ru = 'Недопустимый вызов объекта на клиенте.';
						|en = 'Invalid object call on the client.'");
#КонецЕсли