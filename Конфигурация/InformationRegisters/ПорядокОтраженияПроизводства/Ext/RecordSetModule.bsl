﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ОбработчикиСобытий

Процедура ПередЗаписью(Отказ, Замещение)
	
	Если ОбменДанными.Загрузка Тогда
		Возврат;
	КонецЕсли;

	ОбновлениеИнформационнойБазы.ПроверитьОбъектОбработан(ЭтотОбъект);

КонецПроцедуры

Процедура ПриЗаписи(Отказ, Замещение)
	
	Если Отказ ИЛИ ОбменДанными.Загрузка Тогда
		Возврат;
	КонецЕсли;
	
	Запрос = Новый Запрос;
	Запрос.МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	ПорядокОтраженияПроизводства.Организация,
	|	ПорядокОтраженияПроизводства.МестоПроизводства,
	|	ПорядокОтраженияПроизводства.Организация = ЗНАЧЕНИЕ(Справочник.Организации.ПустаяСсылка)
	|		И ПорядокОтраженияПроизводства.МестоПроизводства = НЕОПРЕДЕЛЕНО КАК ЭтоОбщаяНастройка,
	|	ПорядокОтраженияПроизводства.СчетУчета
	|ПОМЕСТИТЬ ТаблицаНовыхДанных
	|ИЗ
	|	&ТаблицаНовыхДанных КАК ПорядокОтраженияПроизводства
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ТаблицаНовыхДанных.Организация,
	|	НЕОПРЕДЕЛЕНО КАК АналитикаУчета,
	|	ТаблицаНовыхДанных.МестоПроизводства КАК МестоУчета,
	|	ТаблицаНовыхДанных.ЭтоОбщаяНастройка КАК ЭтоОбщаяНастройка,
	|	ЗНАЧЕНИЕ(Перечисление.ВидыСчетовРеглУчета.Производство) КАК ВидСчета
	|ПОМЕСТИТЬ ДанныеЗаполненныхСчетовРегистра
	|ИЗ
	|	ТаблицаНовыхДанных КАК ТаблицаНовыхДанных
	|ГДЕ
	|	ТаблицаНовыхДанных.СчетУчета <> ЗНАЧЕНИЕ(ПланСчетов.Хозрасчетный.ПустаяСсылка)";
	Запрос.УстановитьПараметр("ТаблицаНовыхДанных", ЭтотОбъект.Выгрузить());
	Запрос.Выполнить();
	РегистрыСведений.СчетаРеглУчетаТребующиеНастройки.ОчиститьПриЗаписиРегистра(Запрос.МенеджерВременныхТаблиц);
	
КонецПроцедуры

#КонецОбласти

#КонецЕсли